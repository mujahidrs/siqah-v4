<?php

use App\Http\Controllers\Api\GetUserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'api'], function() {
    Route::get('user', 'Api\GetUserController')->name('user.getAll');
    Route::get('user/murabbi', 'Api\GetMurabbiController')->name('murabbi.getAll');
    Route::get('user/{id}', 'Api\GetUserByIdController')->name('user.getById');
    Route::get('user/group/{id}', 'Api\GetGroupMemberByIdController')->name('group.getMember');
    Route::delete('user/delete/{id}', 'Api\DeleteUserController')->name('user.delete');
    Route::put('user/nonaktif/{id}', 'Api\DisableUserController')->name('user.disable');
    Route::put('user/aktif/{id}', 'Api\EnableUserController')->name('user.enable');
    Route::post('user/store', 'Api\StoreUserController')->name('user.store');
    Route::put('user/createAccount/{id}', 'Api\CreateOrUpdateAccountController')->name('account.create');
    Route::put('user/editAccount/{id}', 'Api\CreateOrUpdateAccountController')->name('account.update');
    Route::put('user/edit/{id}', 'Api\UpdateUserController')->name('user.update');
    Route::post('user/transfer/{id}', 'Api\TransferUserController')->name('user.transfer');
    Route::get('updateAll', 'Api\AutoAssignUserRoleController')->name('user.autoAssignRole');

    Route::get('faculty', 'Api\GetFacultiesController')->name('faculty.getAll');
    Route::get('faculty/{id}', 'Api\ShowFacultyController')->name('faculty.show');
    Route::get('faculty/{id}/prodi', 'Api\ShowProdiByFacultyIdController')->name('prodi.getByFacultyId');
    Route::get('faculty/prodi/{id}', 'Api\GetProdiByIdController')->name('prodi.getById');
    Route::put('faculty/update/{id}', 'Api\UpdateFacultyController')->name('faculty.update');
    Route::post('faculty/store', 'Api\StoreFacultyController')->name('faculty.store');
    Route::delete('faculty/delete/{id}', 'Api\DeleteFacultyController')->name('faculty.delete');

    Route::get('prodi', 'Api\GetProdiesController')->name('prodi.getAll');
    Route::post('prodi/store', 'Api\StoreProdiController')->name('prodi.store');
    Route::put('prodi/update/{id}', 'Api\UpdateProdiController')->name('prodi.update');
    Route::delete('prodi/delete/{id}', 'Api\DeleteProdiController')->name('prodi.delete');

    Route::get('activity', 'Api\ActivitiesController@index');
    Route::post('activity/store', 'Api\ActivitiesController@store');
    Route::post('activity/edit/{id}', 'Api\ActivitiesController@edit');
    Route::get('activity/{id}', 'Api\ActivitiesController@show');
    Route::get('activity/employees/{id}', 'Api\ActivitiesController@users_id');
    Route::delete('activity/delete/{id}', 'Api\ActivitiesController@destroy');

    Route::post('edit', 'EditActivityController@edit');

    Route::get('assessment', 'Api\assessmentsController@index');
    Route::get('categories', 'Api\CategoriesController@index');
    Route::post('categories/edit/{id}', 'Api\CategoriesController@edit');

    Route::get('evaluation', 'Api\EvaluationsController@index');
    Route::post('evaluation/store', 'Api\EvaluationsController@store');
    Route::get('evaluation/{id}', 'Api\EvaluationsController@show');
    Route::get('evaluation/employees/{id}', 'Api\EvaluationsController@users_id');
    Route::delete('evaluation/delete/{id}', 'Api\EvaluationsController@destroy');

    Route::get('group', 'Api\GroupsController@index');
    Route::get('group/{id}', 'Api\GroupsController@show');
    Route::post('group/store', 'Api\GroupsController@store');
    Route::post('group/storeMentee', 'Api\GroupsController@storeMentee');
    Route::post('group/storeMenteePersonal', 'Api\GroupsController@storeMenteePersonal');
    Route::post('group/storePersonal', 'Api\GroupsController@storePersonal');
    Route::put('group/updateMentee/{id}', 'Api\GroupsController@updateMentee');
    Route::get('group/group_name/{id}', 'Api\GroupsController@group');
    Route::get('group/mutarabbi/{id}', 'Api\GroupsController@mentee');
    Route::get('group/murabbi/{id}', 'Api\GroupsController@murabbi');
    Route::delete('group/delete/{id}', 'Api\GroupsController@destroy');
    Route::delete('group/deletePermanent/{id}', 'Api\GroupsController@destroyPermanent');
    Route::delete('group/revive/{id}', 'Api\GroupsController@revive');
    Route::put('group/editGroup/{id}', 'Api\GroupsController@edit');


    Route::get('instrument', 'Api\InstrumentsController@index');

    Route::get('level', 'Api\GetLevelController');
    Route::put('level/update/{id}', 'Api\UpdateLevelController');
    Route::post('level/store', 'Api\StoreLevelController');
    Route::delete('level/delete/{id}', 'Api\DeleteLevelController');

    Route::get('angkatan', 'Api\AngkatanController@index');
    Route::post('angkatan/store', 'Api\AngkatanController@store');
    Route::put('angkatan/update/{id}', 'Api\AngkatanController@update');
    Route::delete('angkatan/delete/{id}', 'Api\AngkatanController@destroy');
    Route::get('personality', 'Api\PersonalitiesController@index');

    Route::get('presence', 'Api\PresencesController@index');
    Route::get('presence/{id}', 'Api\PresencesController@show');
    Route::get('presence/lihat/{id}', 'Api\PresencesController@lihat');
    Route::delete('presence/hapus/{id}', 'Api\PresencesController@destroy');
    Route::post('presence/store', 'Api\PresencesController@store');

    Route::get('subject', 'Api\SubjectsController@index');
    Route::post('subject/store', 'Api\SubjectsController@store');
    Route::post('subject/switch/{id}', 'Api\SubjectsController@switch');
    Route::post('subject/edit/{id}', 'Api\SubjectsController@update');
    Route::delete('subject/delete/{id}', 'Api\SubjectsController@destroy');

    Route::post('upload', 'Api\UploadController');
    Route::post('setMentor/{id}', 'Api\SetMentorController');
    Route::post('removeMentor/{id}', 'Api\RemoveMentorController');

    Route::get('storage/{filename}', function ($filename)
    {
        $path = public_path('storage/' . $filename);

        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);

        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });
});
