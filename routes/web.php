<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@index')->name('login');
Route::get('/tentang_kami', 'TentangKamiController@index')->name('tentang_kami');

Auth::routes();

Route::group(['prefix' => 'admin',  'middleware' => 'role:admin'], function() {
    Route::get('home', 'HomeAdminController@index')->name('admin.home');
    Route::get('data_murabbi', 'DataMurabbiController@index')->name('data_murabbi');
    Route::get('data_murabbi/add', 'DataMurabbiController@add')->name('data_murabbi.add');
    Route::get('data_murabbi/edit/{id}', 'DataMurabbiController@edit')->name('data_murabbi.edit');
    Route::get('data_murabbi/transfer/{id}', 'DataMurabbiController@transfer')->name('data_murabbi.transfer');
    Route::get('rekap_laporan', 'RekapLaporanController@index')->name('rekap_laporan');
    Route::get('pemantauan', 'PemantauanController@index')->name('pemantauan');
    Route::get('madah', 'PusatMadahAdminController@index')->name('madah');
    Route::get('cari', 'CariController@index')->name('cari');
    Route::get('data_r', 'DataRAdminController@index')->name('data_r');
    Route::get('pengaturan', 'PengaturanController@index')->name('pengaturan');
    Route::get('pengaturan/fakultas', 'PengaturanController@fakultas')->name('pengaturan.fakultas');
    Route::get('pengaturan/angkatan', 'PengaturanController@angkatan')->name('pengaturan.angkatan');
    Route::get('pengaturan/jenjang', 'PengaturanController@jenjang')->name('pengaturan.jenjang');
    Route::get('pengaturan/sarana', 'PengaturanController@sarana')->name('pengaturan.sarana');
    Route::get('pengaturan/fakultas/{id}/prodi', 'PengaturanController@prodi')->name('pengaturan.fakultas.prodi');
    Route::get('data_kelompok/{murabbi}', 'AdminDataKelompokController@kelompok')->name('data_kelompok.kelompok');
    Route::get('data_kelompok/{murabbi}/add/{id}', 'AdminDataKelompokController@add')->name('data_kelompok.add');
    Route::get('data_kelompok/{murabbi}/edit/{id}', 'AdminDataKelompokController@edit')->name('data_kelompok.edit');
    Route::get('data_kelompok/{murabbi}/data_mutarabbi/{id}', 'AdminDataKelompokController@data_mutarabbi')->name('admin.data_kelompok.data_mutarabbi');
    Route::get('data_kelompok/{murabbi}/data_mutarabbi/{kelompok}/capaian_madah/{id}', 'AdminDataKelompokController@capaian_madah')->name('data_kelompok.capaian_madah');
    Route::get('data_kelompok/{murabbi}/rekap/{id}', 'AdminRekapitulasiPresensiController@rekap')->name('data_kelompok.rekap');
    Route::get('data_kelompok/{murabbi}/madah/{id}', 'AdminRekapitulasiPresensiController@madah')->name('data_kelompok.madah');
    Route::get('data_kelompok/{murabbi}/madah/{id}/personal/{presences_id}', 'AdminRekapitulasiPresensiController@madah_personal')->name('data_kelompok.madah_personal');
    Route::get('export_user', 'ExportUserController@index');
    Route::get('export_excel/{category}/{gender}', 'ExportUserController@export_excel');
    Route::get('family_tree/{gender}', 'FamilyTreeController@index')->name('family_tree');
});

Route::group(['prefix' => 'user',  'middleware' => 'role:user'], function() {
    Route::get('home', 'HomeController@index')->name('user.home');

    Route::get('data_kelompok/{murabbi}', 'DataKelompokController@kelompok')->name('data_kelompok');
    Route::get('data_kelompok/{murabbi}/add/{id}', 'DataKelompokController@add')->name('data_kelompok.add');
    Route::get('data_kelompok/{murabbi}/edit/{id}', 'DataKelompokController@edit')->name('data_kelompok.edit');
    Route::get('data_kelompok/{murabbi}/data_mutarabbi/{id}', 'DataKelompokController@data_mutarabbi')->name('data_kelompok.data_mutarabbi');

    Route::get('laporan_halaqah', 'LaporanHalaqahController@index')->name('laporan_halaqah');
    Route::get('laporan_halaqah/lapor/{id}', 'LaporanHalaqahController@lapor')->name('laporan_halaqah.lapor');
    Route::get('laporan_halaqah/lihat/{id}', 'LaporanHalaqahController@lihat')->name('laporan_halaqah.lihat');
    Route::get('laporan_halaqah/riwayat/{id}', 'LaporanHalaqahController@riwayat')->name('laporan_halaqah.riwayat');

    Route::get('rekapitulasi_presensi', 'RekapitulasiPresensiController@index')->name('rekapitulasi_presensi');
    Route::get('rekapitulasi_presensi/rekap/{id}', 'RekapitulasiPresensiController@rekap')->name('rekapitulasi_presensi.rekap');

    Route::get('pusat_madah', 'PusatMadahController@index')->name('pusat_madah');
    Route::get('mutabaah', 'MutabaahController@index')->name('mutarabbi.mutabaah');
    Route::get('manage', 'ActivityController@manage')->name('mutarabbi.manage');
    Route::get('profile', 'ProfileController@index')->name('mutarabbi.profile');
    Route::get('create', 'ActivityController@create')->name('mutarabbi.manage.create');
    Route::get('edit/{id}', 'ActivityController@edit')->name('mutarabbi.manage.edit');
});
