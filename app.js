require('./components/Register');
require('./Admin/DataMurabbi/DataMurabbi');
require('./Admin/DataMurabbi/DataMutarabbi');
require('./Admin/DataMurabbi/components/Add');
require('./Admin/DataMurabbi/components/Edit');

require('./Admin/RekapLaporan/RekapLaporan');
require('./Admin/Pemantauan/Pemantauan');
require('./Admin/PusatMadah/PusatMadahAdmin');
require('./Admin/Pengaturan/ListPengaturan');
require('./Admin/Pengaturan/ListFakultasProdi');
require('./Admin/Pengaturan/ListProdi');

require('./Murabbi/DataKelompok/DataKelompok');
require('./Murabbi/DataKelompok/components/Add');
require('./Murabbi/DataKelompok/components/Edit');

require('./Murabbi/LaporanHalaqah/LaporanHalaqah');
require('./Murabbi/LaporanHalaqah/RiwayatLaporan');
require('./Murabbi/LaporanHalaqah/components/Lapor');

require('./Murabbi/RekapPresensi/RekapPresensi');
require('./Murabbi/RekapPresensi/components/Rekap');

require('./Murabbi/PusatMadah/PusatMadah');