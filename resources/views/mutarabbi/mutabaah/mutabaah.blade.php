@extends('layouts.app')

@section('title')
    Mutabaah
@endsection

@section('menu')
    <!-- Dropdown Trigger -->
    <li>
        <a class="dropdown-button" href="#" data-activates="dropdown2" data-beloworigin="true">
            Menu Binaan <span class="caret"></span>
        </a>
    </li>
@endsection

@section('isi-menu')
    <!-- Dropdown Structure -->
    <ul id="dropdown" class="dropdown-content">
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                {{ __('Keluar') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
@endsection

@section('side-nav')
    <li><a href="{{ route('mutarabbi.mutabaah') }}">Mutabaah Yaumiyah</a></li>
    <li><a href="">Pencapaian Pribadi</a></li>
    <li><a href="">Ranking Kelompok</a></li>
    <li><a href="">Pusat Materi</a></li>
@endsection

@section('breacrumb')
    <!-- Breacrumb Navigation -->
    <nav class="hide-on-med-and-down">
        <div class="nav-wrapper teal darken-3">
            <div class="container">
                <div class="col s12">
                    <a href="home" class="breadcrumb"><i class="material-icons">home</i></a>
                    <a href="" onclick="window.location.reload()" class="breadcrumb">Mutabaah</a>
                </div>
            </div>
        </div>
    </nav>
@endsection

@section('content')
    <div id="mutabaah" data-id="{{ Auth::user()->id }}"></div>
    <br>

    @include('layouts.footer')

@endsection
