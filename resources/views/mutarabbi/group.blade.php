@extends('layouts.app')

@section('title')
    Group
@endsection

@section('content')
    <div id="group" data-id={{ Auth::user()->id }}></div>
@endsection
