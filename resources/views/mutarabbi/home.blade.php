@extends('layouts.app')

@section('title')
    Home
@endsection

@section('menu')
    <li style="padding-left: 15px;">Dashboard Binaan</li>
@endsection

@section('isi-menu')
<!-- Dropdown Structure -->
<ul id="dropdown" class="dropdown-content">
    <li>
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            {{ __('Keluar') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
</ul>
@endsection

@section('side-nav')
    <li><a href="{{ route('mutarabbi.mutabaah') }}">Mutabaah Yaumiyah</a></li>
    <li><a href="{{ route('mutarabbi.profile') }}">Pencapaian Pribadi</a></li>
    <li><a href="{{ route('mutarabbi.mutabaah') }}">Ranking Kelompok</a></li>
    <li><a href="{{ route('mutarabbi.mutabaah') }}">Pusat Madah</a></li>
@endsection

@section('content')
    <div class="teal darken-3" style="height: 100%">

    <div class="row hide-on-med-and-down">
        <div class="col s12 m7" style="margin-left: 300px; margin-right: 300px;">
            <div class="card teal lighten-4">
                <div class="card-image">
                    <img src="/images/Screen.png" style="width: 100%;">
                </div>
                <div class="card-action center" style="padding-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px;">
                    <div class="row">
                        <div class="col s3 hoverable">
                            <a href="{{ route('mutarabbi.mutabaah') }}">
                                <img src="/images/Data_Kelompok.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Mutabaah Yaumiyah</strong></h6>
                            </a>
                        </div>
                        <div class="col s3 hoverable">
                            <a href="{{ route('mutarabbi.profile') }}">
                                <img src="/images/Laporan_Halaqah.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Pencapaian Pribadi</strong></h6>
                            </a>
                        </div>
                        <div class="col s3 hoverable">
                            <a href="{{ route('rekapitulasi_presensi') }}">
                                <img src="/images/Rekap_Presensi.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Ranking Kelompok</strong></h6>
                            </a>
                        </div>
                        <div class="col s3 hoverable">
                            <a href="{{ route('pusat_madah') }}">
                                <img src="/images/Pusat%20madah.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Pusat Materi</strong></h6>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row hide-on-large-only">
        <div class="col s12 m7">
            <div class="card teal lighten-4">
                <div class="card-image">
                    <img src="/images/Screen.png" style="width: 100%;">
                </div>
                <div class="card-action center" style="padding-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px;">
                    <div class="row hoverable">
                        <a href="{{ route('mutarabbi.mutabaah') }}">
                            <img src="/images/Data_Kelompok.png" style="width:10%;">
                            <strong style="color: black;">Mutabaah Yaumiyah</strong>
                        </a>
                    </div>
                    <div class="row hoverable">
                        <a href="{{ route('mutarabbi.profile') }}">
                            <img src="/images/Laporan_Halaqah.png" style="width:10%;">
                            <strong style="color: black;">Pencapaian Pribadi</strong>
                        </a>
                    </div>
                    <div class="row hoverable">
                        <a href="{{ route('rekapitulasi_presensi') }}">
                            <img src="/images/Rekap_Presensi.png" style="width:10%;">
                            <strong style="color: black;">Ranking Kelompok</strong>
                        </a>
                    </div>
                    <div class="row hoverable">
                        <a href="{{ route('pusat_madah') }}">
                            <img src="/images/Pusat%20madah.png" style="width:10%;">
                            <strong style="color: black;">Pusat Materi</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
