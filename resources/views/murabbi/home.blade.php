@extends('layouts.app')

@section('title')
    Home
@endsection

@include('murabbi.navigation_user')

@section('content')
    <div class="teal darken-3" style="height: 100%">

    <div class="row hide-on-med-and-down">
        <div class="col s12 offset-s3" style="max-width: 50%;">
            <div class="card teal lighten-4">
                <div class="card-image">
                    <img src="/images/Screen.png" style="width: 100%;">
                </div>
                <div class="card-action center" style="padding-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px;">
                    @if(Auth::user()->is_mentor === 1)
                    <div class="row">
                        <div class="col s3 hoverable">
                            <a href="{{ route('data_kelompok', ['murabbi' => Auth::user()->id]) }}">
                                <img src="/images/Data_Kelompok.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Data Kelompok</strong></h6>
                            </a>
                        </div>
                        <div class="col s3 hoverable">
                            <a href="{{ route('laporan_halaqah') }}">
                                <img src="/images/Laporan_Halaqah.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Laporan Pembinaan</strong></h6>
                            </a>
                        </div>
                        <div class="col s3 hoverable">
                            <a href="{{ route('rekapitulasi_presensi') }}">
                                <img src="/images/Rekap_Presensi.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Rekap Presensi</strong></h6>
                            </a>
                        </div>
                        <div class="col s3 hoverable">
                            <a href="{{ route('pusat_madah') }}">
                                <img src="/images/Pusat%20madah.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Pusat Madah</strong></h6>
                            </a>
                        </div>
                    </div>
                        {{--<div class="row">--}}
                            {{--<div class="col s4 hoverable">--}}
                                {{--<a href="{{ route('mutarabbi.mutabaah') }}">--}}
                                    {{--<img src="/images/Data_Kelompok.png" style="width:50%;"><br>--}}
                                    {{--<h6 style="color: black;"><strong>Mutabaah Yaumiyah</strong></h6>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="col s4 hoverable">--}}
                                {{--<a href="{{ route('mutarabbi.profile') }}">--}}
                                    {{--<img src="/images/Laporan_Halaqah.png" style="width:50%;"><br>--}}
                                    {{--<h6 style="color: black;"><strong>Pencapaian Pribadi</strong></h6>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            {{--<div class="col s4 hoverable">--}}
                                {{--<a href="{{ route('rekapitulasi_presensi') }}">--}}
                                    {{--<img src="/images/Rekap_Presensi.png" style="width:50%;"><br>--}}
                                    {{--<h6 style="color: black;"><strong>Ranking Kelompok</strong></h6>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        @endif
                    @if(Auth::user()->is_mentor === 0)
                            {{--<div class="row">--}}
                                {{--<div class="col s3 hoverable">--}}
                                    {{--<a href="{{ route('mutarabbi.mutabaah') }}">--}}
                                        {{--<img src="/images/Data_Kelompok.png" style="width:50%;"><br>--}}
                                        {{--<h6 style="color: black;"><strong>Mutabaah Yaumiyah</strong></h6>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                                {{--<div class="col s3 hoverable">--}}
                                    {{--<a href="{{ route('mutarabbi.profile') }}">--}}
                                        {{--<img src="/images/Laporan_Halaqah.png" style="width:50%;"><br>--}}
                                        {{--<h6 style="color: black;"><strong>Pencapaian Pribadi</strong></h6>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                                {{--<div class="col s3 hoverable">--}}
                                    {{--<a href="{{ route('rekapitulasi_presensi') }}">--}}
                                        {{--<img src="/images/Rekap_Presensi.png" style="width:50%;"><br>--}}
                                        {{--<h6 style="color: black;"><strong>Ranking Kelompok</strong></h6>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                                {{--<div class="col s3 hoverable">--}}
                                    {{--<a href="{{ route('pusat_madah') }}">--}}
                                        {{--<img src="/images/Pusat%20madah.png" style="width:50%;"><br>--}}
                                        {{--<h6 style="color: black;"><strong>Pusat Madah</strong></h6>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row hide-on-large-only">
        <div class="col s12">
            <div class="card teal lighten-4">
                <div class="card-image">
                    <img src="/images/Screen.png" style="width: 100%;">
                </div>
                <div class="card-action center" style="padding-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px;">
                    <div class="row hoverable">
                        <a href="{{ route('data_kelompok', ['murabbi' => Auth::user()->id]) }}">
                            <img src="/images/Data_Kelompok.png" style="width:10%;">
                            <strong style="color: black;">Data Kelompok</strong>
                        </a>
                    </div>
                    <div class="row hoverable">
                        <a href="{{ route('laporan_halaqah') }}">
                            <img src="/images/Laporan_Halaqah.png" style="width:10%;">
                            <strong style="color: black;">Laporan Pembinaan</strong>
                        </a>
                    </div>
                    <div class="row hoverable">
                        <a href="{{ route('rekapitulasi_presensi') }}">
                            <img src="/images/Rekap_Presensi.png" style="width:10%;">
                            <strong style="color: black;">Rekap Presensi</strong>
                        </a>
                    </div>
                    <div class="row hoverable">
                        <a href="{{ route('pusat_madah') }}">
                            <img src="/images/Pusat%20madah.png" style="width:10%;">
                            <strong style="color: black;">Pusat Madah</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @include('layouts.footer')
@endsection
