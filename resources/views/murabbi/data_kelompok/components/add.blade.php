@extends('layouts.app')

@section('title')
    Tambah Kelompok
@endsection

@include('murabbi.navigation_user')

@section('breacrumb')
<!-- Breacrumb Navigation -->
<nav class="hide-on-med-and-down">
    <div class="nav-wrapper teal darken-3">
        <div class="container">
            <div class="col s12">
                <a href="home" class="breadcrumb"><i class="material-icons">home</i></a>
                <a href="{{ route('data_kelompok', ['murabbi' => Auth::user()->id]) }}" class="breadcrumb">Data Kelompok</a>
                <a href="" onclick="window.location.reload()" class="breadcrumb">Tambah Kelompok</a>
            </div>
        </div>
    </div>
</nav>
@endsection

@section('content')
    <div id="add_kelompok" data-id="{{ Auth::user()->id }}" data-prefix="{{ isset($prefix) ? $prefix : null }}" data-role="{{ isset($role) ? $role : null }}"></div>
    <br>

    @include('layouts.footer')

@endsection
