@section('menu')
    <!-- Dropdown Trigger -->
    <li>
        <a class='dropdown-trigger' href='#' data-target='dropdown1'>
            Menu User <span class="caret"></span>
        </a>
    </li>
@endsection

@section('isi-menu')
    <!-- Dropdown Structure -->
    <ul id='dropdown1' class='dropdown-content'>
        <li>
            <a href="{{ route('data_kelompok', ['murabbi' => Auth::user()->id]) }}">
                Data Kelompok
            </a>
        </li>
        <li>
            <a href="{{ route('laporan_halaqah') }}">
                Laporan Pembinaan
            </a>
        </li>
        <li>
            <a href="{{ route('rekapitulasi_presensi') }}">
                Rekapitulasi Presensi
            </a>
        </li>
        <li>
            <a href="{{ route('pusat_madah') }}">
                Pusat Materi
            </a>
        </li>
    </ul>
@endsection

@section('side-nav')
    <li><a href="#" style="font-weight: bold">Menu User</a></li>
    <li><div class="divider"></div></li>
    <li><a href="{{ route('data_kelompok', ['murabbi' => Auth::user()->id]) }}">Data Kelompok</a></li>
    <li><a href="{{ route('laporan_halaqah') }}">Laporan Pembinaan</a></li>
    <li><a href="{{ route('rekapitulasi_presensi') }}">Rekapitulasi Presensi</a></li>
    <li><a href="{{ route('pusat_madah') }}">Pusat Madah</a></li>
@endsection