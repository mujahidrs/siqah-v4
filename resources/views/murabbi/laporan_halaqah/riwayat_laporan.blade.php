@extends('layouts.app')

@section('title')
    Riwayat Laporan
@endsection

@include('murabbi.navigation_user')

@section('breacrumb')
    <!-- Breacrumb Navigation -->
    <nav class="hide-on-med-and-down">
        <div class="nav-wrapper teal darken-3">
            <div class="container">
                <div class="col s12">
                    <a href="{{ route('user.home') }}" class="breadcrumb"><i class="material-icons">home</i></a>
                    <a href="{{ route('laporan_halaqah') }}" class="breadcrumb">Laporan Pembinaan</a>
                    <a href="" onclick="window.location.reload()" class="breadcrumb">Riwayat Laporan</a>
                </div>
            </div>
        </div>
    </nav>
@endsection

@section('content')
    <div id="riwayat_laporan" data-id={{ Auth::user()->id }}></div>
    <br/>

    @include('layouts.footer')

@endsection
