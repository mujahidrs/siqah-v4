@extends('layouts.app')

@section('title')
    Edit
@endsection

@include('murabbi.navigation_user')

@section('breacrumb')
<!-- Breacrumb Navigation -->
<nav class="hide-on-med-and-down">
    <div class="nav-wrapper teal darken-3">
        <div class="container">
            <div class="col s12">
                <a href="home" class="breadcrumb"><i class="material-icons">home</i></a>
                <a href="{{ route('laporan_halaqah') }}" class="breadcrumb">Laporan Pembinaan</a>
                <a href='/murabbi/laporan_halaqah/riwayat/{{$id}}' class="breadcrumb">Riwayat Laporan</a>
                <a href="" onclick="window.location.reload()" class="breadcrumb">Edit</a>
            </div>
        </div>
    </div>
</nav>
@endsection

@section('content')
    <div id="edit_laporan" data-id={{ Auth::user()->id }}></div>
    <br>

    @include('layouts.footer')

@endsection

@section('script')
    $(document).ready(function(){
        $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: false, // Use AM/PM or 24-hour format
        donetext: 'Oke', // text for done-button
        cleartext: 'Hapus', // text for clear-button
        canceltext: 'Batal', // Text for cancel-button,
        container: undefined, // ex. 'body' will append picker to body
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function(){} //Function for after opening timepicker
        });
    });

@endsection