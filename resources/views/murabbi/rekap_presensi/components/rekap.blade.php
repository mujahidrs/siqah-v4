@extends('layouts.app')

@section('title')
    Rekap
@endsection

@include('murabbi.navigation_user')

@section('breacrumb')
<!-- Breacrumb Navigation -->
<nav class="hide-on-med-and-down">
    <div class="nav-wrapper teal darken-3">
        <div class="container">
            <div class="col s12">
                <a href="{{ route('user.home') }}" class="breadcrumb"><i class="material-icons">home</i></a>
                <a href="{{ route('rekapitulasi_presensi') }}" class="breadcrumb">Rekap Presensi</a>
                <a href="" onclick="window.location.reload()" class="breadcrumb">Rekap</a>
            </div>
        </div>
    </div>
</nav>
@endsection

@section('content')
    <div id="rekap" data-id={{ Auth::user()->id }}></div>
    <br>

    @include('layouts.footer')

@endsection
