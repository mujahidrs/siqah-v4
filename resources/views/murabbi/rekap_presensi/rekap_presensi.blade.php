@extends('layouts.app')

@section('title')
    Rekapitulasi Presensi
@endsection

@include('murabbi.navigation_user')

@section('breacrumb')
    <!-- Breacrumb Navigation -->
    <nav class="hide-on-med-and-down">
        <div class="nav-wrapper teal darken-3">
            <div class="container">
                <div class="col s12">
                    <a href="home" class="breadcrumb"><i class="material-icons">home</i></a>
                    <a href="{{ route('rekapitulasi_presensi') }}" class="breadcrumb">Rekap Presensi</a>
                </div>
            </div>
        </div>
    </nav>
@endsection

@section('content')
    <div id="rekap_presensi" data-id={{ Auth::user()->id }}></div>
    <br/>

    @include('layouts.footer')

@endsection
