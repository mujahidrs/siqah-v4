@extends('layouts.app')

@section('title')
    Tentang Kami
@endsection

@include('admin.data_murabbi.navigation_admin')

@section('breacrumb')
<!-- Breacrumb Navigation -->
<nav class="hide-on-med-and-down">
    <div class="nav-wrapper teal darken-3">
        <div class="container">
            <div class="col s12">
                <a href="/" class="breadcrumb"><i class="material-icons">home</i></a>
                <a href="{{ route('data_murabbi') }}" class="breadcrumb">Data User</a>
                <a href="{{ route('family_tree', $gender) }}" class="breadcrumb">Pohon Keluarga</a>
            </div>
        </div>
    </div>
</nav>
@endsection

@section('content')
    <div id="family_tree" data-id="{{ Auth::user()->id }}" data-gender="{{$gender}}"></div>
    <br>
    <br>

    @include('layouts.footer')

@endsection
