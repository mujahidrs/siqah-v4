@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card-panel teal lighten-5 z-depth-3">
        <div style="text-align: center;">
        <div class="card-image">
            <img src="/favicon/icon-header3.png" class="responsive-img">
        </div>
        <p class="flow-text">Sistem Informasi Halaqah</p>
        </div>
        <div class="row">
            <form class="col s12" method="post" action="{{ route('login') }}" role="form">
                @csrf
                <div class="row">
                    <div class="input-field col s12">
                        <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
                        <label for="username">{{ __('Nama Pengguna') }}</label>
                        @if ($errors->has('username'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        <label for="password">{{ __('Password') }}</label>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <div style="text-align: center;">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>
                        </div>


                {{--@if (Route::has('password.request'))--}}
                    {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                        {{--{{ __('Forgot Your Password?') }}--}}
                    {{--</a>--}}
                {{--@endif--}}
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('layouts.footer')
@endsection
