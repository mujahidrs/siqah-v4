<button id="tombolScrollTop" class="btn-floating waves-effect waves-light" onclick="scrolltotop()" style="float: right; margin: 0 20px 20px 0;"><i class="material-icons">arrow_upward</i></button>

<footer class="page-footer teal darken-4" style="padding-top: 0px">
    <div class="footer-copyright">
        <div class="row container">
            <div class="col s6 grey-text text-lighten-4" style="text-align: center;">
            © <?php echo date("Y"); ?> Copyright LTT
            </div>
            <div class="col s6">
                <!-- Modal Trigger -->
                <div style="text-align: center;"><a class="grey-text text-lighten-4 modal-trigger" href="#modal5">Tentang Kami</a></div>

                <!-- Modal Structure -->
                <div id="modal5" class="modal black-text">
                    <div class="modal-content">
                        <div style="text-align: center;">
                            <h4>Tentang Kami</h4>
                            <div class="row">
                                <img src="/images/Head of Dev.png" style="width:10%;"><br>
                                <strong>Head of Development:</strong> Mujahid Robbani Sholahudin
                            </div>
                            <div class="row">
                                <div class="col s4">
                                    <img src="/images/Analyst.png" style="width:30%;"><br>
                                    <strong>System Analyst:</strong> Naufal Ibnu Salam
                                </div>
                                <div class="col s4">
                                    <img src="/images/Web Prog.png" style="width:30%;"><br>
                                    <strong>Web Programmer:</strong> Mujahid Robbani Sholahudin
                                </div>
                                <div class="col s4">
                                    <img src="/images/Database.png" style="width:30%;"><br>
                                    <strong>Database Engineer:</strong> Mujahid Robbani Sholahudin
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s6">
                                    <img src="/images/Web Des.png" style="width:20%;"><br>
                                    <strong>Web Designer:</strong> Rakha Ramadhana A. B.
                                </div>
                                <div class="col s6">
                                    <img src="/images/Graphic Des.png" style="width:20%;"><br>
                                    <strong>Graphic Designer:</strong> M. Ihya Ulumudin
                                </div>
                            </div>
                            <h6>© <?php echo date("Y"); ?> Copyright LTT</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
