<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="Content-Type" content=”text/html; charset=utf-8″ />
    <meta charset="utf-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Import Google Icon Font -->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
{{--<!-- Compiled and minified CSS -->--}}
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">--}}

    {{--<!-- Compiled and minified CSS -->--}}
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">--}}

    <!-- Favicon Properties -->
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    {{--<link rel="manifest" href="/favicon/manifest.json">--}}
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <style>
        .brand-logo{
            font-weight:900;
        }

        .dropdown-content {
            background-color: #FFFFFF;
            margin: 0;
            display: none;
            min-width: 200px; /* Changed this to accomodate content width */
            max-height: auto;
            margin-left: -1px; /* Add this to keep dropdown in line with edge of navbar */
            overflow: hidden; /* Changed this from overflow-y:auto; to overflow:hidden; */
            opacity: 0;
            position: relative;
            white-space: nowrap;
            z-index: 1;
            will-change: width, height;
        }

        .table-responsive {
            min-height: .01%;
            overflow-x: auto;
        }

        .dropdown-content {
            max-height: 250px;
        }

        .select-dropdown{
            overflow-y: auto !important;
        }

        @media screen and (max-width: 767px) {
            .table-responsive {
                width: 100%;
                margin-bottom: 15px;
                overflow-y: hidden;
                -ms-overflow-style: -ms-autohiding-scrollbar;
                border: 1px solid #ddd;
            }
            .table-responsive > .table {
                margin-bottom: 0;
            }
            .table-responsive > .table > thead > tr > th,
            .table-responsive > .table > tbody > tr > th,
            .table-responsive > .table > tfoot > tr > th,
            .table-responsive > .table > thead > tr > td,
            .table-responsive > .table > tbody > tr > td,
            .table-responsive > .table > tfoot > tr > td {
                white-space: nowrap;
            }
            .table-responsive > .table-bordered {
                border: 0;
            }
            .table-responsive > .table-bordered > thead > tr > th:first-child,
            .table-responsive > .table-bordered > tbody > tr > th:first-child,
            .table-responsive > .table-bordered > tfoot > tr > th:first-child,
            .table-responsive > .table-bordered > thead > tr > td:first-child,
            .table-responsive > .table-bordered > tbody > tr > td:first-child,
            .table-responsive > .table-bordered > tfoot > tr > td:first-child {
                border-left: 0;
            }
            .table-responsive > .table-bordered > thead > tr > th:last-child,
            .table-responsive > .table-bordered > tbody > tr > th:last-child,
            .table-responsive > .table-bordered > tfoot > tr > th:last-child,
            .table-responsive > .table-bordered > thead > tr > td:last-child,
            .table-responsive > .table-bordered > tbody > tr > td:last-child,
            .table-responsive > .table-bordered > tfoot > tr > td:last-child {
                border-right: 0;
            }
            .table-responsive > .table-bordered > tbody > tr:last-child > th,
            .table-responsive > .table-bordered > tfoot > tr:last-child > th,
            .table-responsive > .table-bordered > tbody > tr:last-child > td,
            .table-responsive > .table-bordered > tfoot > tr:last-child > td {
                border-bottom: 0;
            }
        }
    </style>
</head>
<body>

<header>
        <nav>
            <div class="nav-wrapper teal darken-4 z-depth-3">
                <a class="brand-logo center hide-on-med-and-down" href="/">
                    <img src="/favicon/icon-header2.png" class="responsive-img" style="max-height: 55px; margin-top: 5px;"></a>
                <a class="brand-logo center hide-on-large-only" href="/">
                    <img src="/favicon/icon-header2.png" class="responsive-img" style="max-height: 45px; margin-top: 5px;"></a>
                @auth
                <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                @endauth

                <ul class="left hide-on-med-and-down">
                    @yield('menu')
                </ul>
                @yield('isi-menu')

                @auth
                <ul class="right hide-on-med-and-down">
                <!-- Dropdown Trigger -->
                    <li>
                        <a class='dropdown-trigger' href='#' data-target='dropdown'>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                    </li>
                </ul>
                @endauth

            </div>
        </nav>

        <ul id="slide-out" class="sidenav">
            @yield('side-nav')
            <li><div class="divider"></div></li>
            @auth
            <li><a href="#" style="font-weight: bold">{{ Auth::user()->name }}</a></li>
            @endauth
            <li><div class="divider"></div></li>
            @auth
                @if(Auth::user()->role === 0)
                    <li><a href="{{ route('pengaturan') }}">Pengaturan</a></li>
                @endif
            @endauth
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form1').submit();">
                    {{ __('Keluar') }}
                </a>

                <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
</header>
        <!-- Dropdown Structure -->
        <ul id="dropdown" class="dropdown-content">
            @auth
                @if(Auth::user()->role === 0)
                    <li><a href="{{ route('pengaturan') }}">Pengaturan</a></li>
                    @endif
            @endauth
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    {{ __('Keluar') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
            @yield('breacrumb')
            @yield('content')

    <!-- Import jQuery before materialize.js -->
{{--    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>--}}

    <!-- Custom Javasciprt -->
    <script>
        // $(".button-collapse").sideNav();
        $(document).ready(function(){
            $('.sidenav').sidenav();
        });

        $(document).ready(function(){
            $('.modal').modal();
        });

        $('.dropdown-trigger').dropdown({
                            hover: true, //Activate on hover
                            coverTrigger: false, // Displays dropdown below the button
                            alignment: 'right' // Displays dropdown with edge aligned to the left of button
                        });

        <!--Back To Top-->
            $(document).ready(function(){
                $(window).scroll(function(){
                    if ($(window).scrollTop() > 100) {
                        $('#tombolScrollTop').fadeIn();
                    } else {
                        $('#tombolScrollTop').fadeOut();
                    }
                });
            });

        function scrolltotop()
        {
            $('html, body').animate({scrollTop : 0},500);
        }
    <!--END Back to Top-->

        @yield('script')
    </script>
</body>
</html>
