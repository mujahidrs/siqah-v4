<html>
<head>
    <title>Export Laporan Excel Pada Laravel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <center>
        <h4>Export Laporan Excel Pada Laravel</h4>
        <h5><a target="_blank" href="https://www.malasngoding.com/">www.malasngoding.com</a></h5>
    </center>

    <a href="export_user/export_excel" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a>

    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Role</th>
            <th>Username</th>
            <th>Jenis Kelamin</th>
            <th>Prodi</th>
            <th>Angkatan</th>
            <th>Nama Kelompok</th>
            <th>Status</th>
            <th>Pembina?</th>
        </tr>
        </thead>
        <tbody>
        @php $i=1 @endphp
        @foreach($user as $s)
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{$s->name}}</td>
            <td>{{$s->email}}</td>
            <td>{{$s->role}}</td>
            <td>{{$s->username}}</td>
            <td>{{$s->gender}}</td>
            <td>{{$s->prodi_id}}</td>
            <td>{{$s->angkatan}}</td>
            <td>{{$s->groups_id}}</td>
            <td>{{$s->status}}</td>
            <td>{{$s->is_mentor}}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>

</body>
</html>
