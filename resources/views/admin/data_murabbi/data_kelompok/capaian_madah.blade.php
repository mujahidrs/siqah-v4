@extends('layouts.app')

@section('title')
    Capaian Madah
@endsection

@include('admin.data_murabbi.navigation_admin')

@section('breacrumb')
<!-- Breacrumb Navigation -->
<nav class="hide-on-med-and-down">
    <div class="nav-wrapper teal darken-3">
        <div class="container">
            <div class="col s12">
                <a href="home" class="breadcrumb"><i class="material-icons">home</i></a>
                <a href="{{ route('data_murabbi') }}" class="breadcrumb">Data User</a>
                <a href="{{ route('data_kelompok.kelompok', ['murabbi' => $murabbi]) }}" class="breadcrumb">Data Kelompok</a>
                <a href="{{ route('admin.data_kelompok.data_mutarabbi', ['murabbi' => $murabbi, 'id' => $id]) }}" class="breadcrumb">Data Binaan</a>
                <a href="" onclick="window.location.reload()" class="breadcrumb">Capaian Madah</a>
            </div>
        </div>
    </div>
</nav>
@endsection

@section('content')
    <div id="capaian_madah" data-id="{{ Auth::user()->id }}" data-role="{{ isset($role) ? $role : null }}" data-prefix="{{ isset($prefix) ? $prefix : null }}"></div>
    <br>

    @include('layouts.footer')

@endsection
