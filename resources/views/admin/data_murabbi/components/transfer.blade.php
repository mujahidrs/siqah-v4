@extends('layouts.app')

@section('title')
    Data User
@endsection

@include('admin.data_murabbi.navigation_admin')

@section('breacrumb')
<!-- Breacrumb Navigation -->
<nav class="hide-on-med-and-down">
    <div class="nav-wrapper teal darken-3">
        <div class="container">
            <div class="col s12">
                <a href="{{ route('admin.home') }}" class="breadcrumb"><i class="material-icons">home</i></a>
                <a href="{{ route('data_murabbi') }}" class="breadcrumb">Data User</a>
                <a href="" onclick="window.location.reload()" class="breadcrumb">Transfer Binaan</a>
            </div>
        </div>
    </div>
</nav>
@endsection

@section('content')
    <div id="transfer_murabbi" data-id={{ Auth::user()->id }}></div>
    <br>

    @include('layouts.footer')

@endsection
