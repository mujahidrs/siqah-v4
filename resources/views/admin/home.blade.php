@extends('layouts.app')

@section('title')
    Home
@endsection

@include('admin.navigation_admin')

@section('content')
    <div class="teal darken-3" style="height: 100%">

    <div class="row hide-on-med-and-down">
        <div class="col s12 offset-s3" style="max-width: 50%;">
            <div class="card teal lighten-4">
                <div class="card-image">
                    <img src="/images/Screen.png" style="width: 100%;">
                </div>
                <div class="card-action center" style="padding-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px;">
                    <div class="row">
                        <div class="col s3 hoverable">
                            <a href="{{ route('data_murabbi') }}">
                                <img src="/images/Data_Murabbi.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Data User</strong></h6>
                            </a>
                        </div>
                        <div class="col s3 hoverable">
                            <a href="{{ route('rekap_laporan') }}">
                                <img src="/images/Rekap_Laporan.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Rekap Laporan</strong></h6>
                            </a>
                        </div>
                        <div class="col s3 hoverable">
                            <a href="{{ route('pemantauan') }}">
                                <img src="/images/Pemantauan.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Pemantauan</strong></h6>
                            </a>
                        </div>
                        <div class="col s3 hoverable">
                            <a href="{{ route('madah') }}">
                                <img src="/images/Pusat%20madah.png" style="width:50%;"><br>
                                <h6 style="color: black;"><strong>Madah</strong></h6>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row hide-on-large-only">
        <div class="col s12">
            <div class="card teal lighten-4">
                <div class="card-image">
                    <img src="/images/Screen.png" style="width: 100%;">
                </div>
                <div class="card-action center" style="padding-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px;">
                    <div class="row hoverable">
                        <a href="{{ route('data_murabbi') }}">
                            <img src="/images/Data_Murabbi.png" style="width:10%;">
                            <strong style="color: black;">Data User</strong>
                        </a>
                    </div>
                    <div class="row hoverable">
                        <a href="{{ route('rekap_laporan') }}">
                            <img src="/images/Rekap_Laporan.png" style="width:10%;">
                            <strong style="color: black;">Rekap Laporan</strong>
                        </a>
                    </div>
                    <div class="row hoverable">
                        <a href="{{ route('pemantauan') }}">
                            <img src="/images/Pemantauan.png" style="width:10%;">
                            <strong style="color: black;">Pemantauan</strong>
                        </a>
                    </div>
                    <div class="row hoverable">
                        <a href="{{ route('madah') }}">
                            <img src="/images/Pusat%20madah.png" style="width:10%;">
                            <strong style="color: black;">Madah</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @include('layouts.footer')
@endsection
