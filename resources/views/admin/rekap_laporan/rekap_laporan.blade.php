@extends('layouts.app')

@section('title')
    Rekap Laporan
@endsection

@include('admin.navigation_admin')

@section('breacrumb')
<!-- Breacrumb Navigation -->
<nav class="hide-on-med-and-down">
    <div class="nav-wrapper teal darken-3">
        <div class="container">
            <div class="col s12">
                <a href="home" class="breadcrumb"><i class="material-icons">home</i></a>
                <a href="{{ route('rekap_laporan') }}" class="breadcrumb">Rekap Laporan</a>
            </div>
        </div>
    </div>
</nav>
@endsection

@section('content')
    <div id="rekap_laporan" data-id={{ Auth::user()->id }}></div>
    <br>

    @include('layouts.footer')

@endsection