@extends('layouts.app')

@section('title')
    Pengaturan
@endsection

@include('admin.pengaturan.navigation_admin')

@section('breacrumb')
<!-- Breacrumb Navigation -->
<nav class="hide-on-med-and-down">
    <div class="nav-wrapper teal darken-3">
        <div class="container">
            <div class="col s12">
                <a href="home" class="breadcrumb"><i class="material-icons">home</i></a>
                <a href="{{ route('pengaturan') }}" class="breadcrumb">Pengaturan</a>
            </div>
        </div>
    </div>
</nav>
@endsection

@section('content')
    <div id="pengaturan" data-id={{ Auth::user()->id }}></div>
    <br>
    <br>

    @include('layouts.footer')

@endsection
