@section('menu')
    <!-- Dropdown Trigger -->
    <li>
        <a class='dropdown-trigger' href='#' data-target='dropdown1'>
            Menu Admin <span class="caret"></span>
        </a>
    </li>
@endsection

@section('isi-menu')
    <!-- Dropdown Structure -->
    <ul id='dropdown1' class='dropdown-content'>
        <li>
            <a href="{{ route('data_murabbi') }}">
                Data User
            </a>
        </li>
        <li>
            <a href="{{ route('rekap_laporan') }}">
                Rekap Laporan
            </a>
        </li>
        <li>
            <a href="{{ route('pemantauan') }}">
                Pemantauan
            </a>
        </li>
        <li>
            <a href="{{ route('madah') }}">
                Materi
            </a>
        </li>
    </ul>
@endsection

@section('side-nav')
    <li><a href="#" style="font-weight: bold">Menu Admin</a></li>
    <li><div class="divider"></div></li>
    <li><a href="{{ route('data_murabbi') }}">Data User</a></li>
    <li><a href="{{ route('rekap_laporan') }}">Rekap Laporan</a></li>
    <li><a href="{{ route('pemantauan') }}">Pemantauan</a></li>
    <li><a href="{{ route('madah') }}">Materi</a></li>
@endsection