import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Col} from 'react-materialize'
import axios from 'axios'

export default class Add extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            group_name: '',
            errors: [],
            users: [],
            angkatan: '',
            dataAngkatan: [],
            murabbi: '',
            level: '1',
            year: '',
            gender: '',
            binaan: [],
            levels: [],
            faculties: [],
            prodies: [],
            faculty: [],
            prodi: [],
            angkatan_mentee: [],
            user_data: [],
        };
        console.log(super());
    };

    componentDidMount() {
        let today = new Date();
        let tanggal = today.getDate();
        if(tanggal <= 10){
            tanggal = '0' + tanggal;
        }
        let sdate = tanggal + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
        let id = window.location.pathname.split('/')[5];
        if(window.location.pathname.split('/')[1] === 'admin'){
            id = window.location.pathname.split('/')[3];
        }

        axios.get('/api/user')
            .then(response => {
                this.setState({
                    users: response.data
                });

                id = parseInt(id);
                let gender = response.data.filter(user => user.id === id).map(user => user.gender);
                console.log(gender);
                if(gender[0] === 1){
                    this.setState({gender: 1})
                }
                else if(gender[0] === 2){
                    this.setState({gender: 2})
                }
            });

        axios.get('/api/user/'+id)
            .then(response => {
                this.setState({user_data: response.data});
            });

        axios.get('/api/level')
            .then(response => {
                this.setState({levels: response.data});
            });

        axios.get('/api/faculty')
            .then(response => {
                this.setState({faculties: response.data});
            });

        axios.get('/api/prodi')
            .then(response => {
                this.setState({prodies: response.data});
            });

        axios.get('/api/angkatan')
            .then(response => {
                this.setState({dataAngkatan: response.data});
            });

        let year = today.getFullYear();

        this.setState({
            year: year
        })
    }



    validate = (group_name, level, angkatan) =>{
        const errors = [];

        if(group_name.length === 0){
            errors.push('Nama Kelompok tidak boleh kosong');
        }

        if(level.length === 0){
            errors.push('Kelas tidak boleh kosong');
        }

        if(angkatan.length === 0){
            errors.push('Angkatan tidak boleh kosong');
        }

        return errors;
    };


    createOption () {
        let option = [];
        let year = this.state.year;

        for (let i = 0; i <= 10; i++) {
            option.push(<option key={i} value={year}>{year}</option>);
            year = year - 1;
        }
        return option
    }

    getName(id){
        let name = this.state.users.filter(user => user.id === id).map(user => user.name);
        return name[0];
    }

    getGender(id){
        id = parseInt(id);
        let gender = this.state.users.filter(user => user.id === id).map(user => user.gender);
        if(gender[0] === 1){
            return 1;
        }
        else if(gender[0] === 2){
            return 2;
        }
    }

    addBinaan(){
        this.setState({binaan: [...this.state.binaan, '']})
    }

    handleRemoveBinaan(index){
        this.state.binaan.splice(index,1);
        this.setState({binaan: this.state.binaan})
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    handleChangeBinaan(e, index){
        this.state.binaan[index] = e.target.value;
        this.setState({binaan: this.state.binaan})
    }

    handleChangeFakultas(e, index){
        this.state.faculty[index] = parseInt(e.target.value);
        this.setState({faculty: this.state.faculty})
    }

    handleChangeProdi(e, index){
        this.state.prodi[index] = parseInt(e.target.value);
        this.setState({prodi: this.state.prodi})
    }

    handleChangeAngkatan(e, index){
        this.state.angkatan_mentee[index] = e.target.value;
        this.setState({angkatan_mentee: this.state.angkatan_mentee})
    }

    onSubmit(e){
        e.preventDefault();

        const errors = this.validate(this.state.group_name, this.state.level, this.state.angkatan);
        if (errors.length > 0) {
            this.setState({ errors });
            return true;
        }
        else {
            const group =
                    {
                        mentors_id : this.state.user_data.id,
                        group_name : this.state.group_name,
                        levels_id : this.state.level,
                        angkatan: this.state.angkatan,
                        status: 'Aktif'
                    };

            // const group_2 = this.state.prodi.map((prodi, index)=>{
            //     return(
            //         {
            //             mentee_name: this.state.binaan[index],
            //             mentors_id : group.mentors_id,
            //             group_name : group.group_name,
            //             levels_id : group.levels_id,
            //             angkatan: this.state.angkatan_mentee[index],
            //             role : 1,
            //             password : this.state.binaan[index].replace(/[^A-Z0-9]/ig, "").toLowerCase(),
            //             username : null,
            //             email : null,
            //             gender : this.state.gender,
            //             prodi_id : prodi,
            //             status: 'Aktif',
            //             is_mentor: 0
            //         }
            //     )
            // });
            //
            // console.log(group);
            // console.log(group_2);

            axios.post('/api/group/store', group)
                .then(response =>{
                    console.log(response);
                    this.props.role === 'admin' ?
                        window.location = '/admin/data_kelompok/'+this.state.user_data.id
                        :
                        window.location = '/user/data_kelompok/'+this.props.id
                })
                .catch(error => {
                    console.log(error);
                });
            //
            // axios.post('/api/group/storeMentee', {group_2})
            //
            //     .catch();
        }
    }

    render() {
        // console.log(this.state);
        return (
            <div className="container">
                <h2>Tambah Kelompok</h2>
                    <div className="row">
                        <form className="col s12" method="post" action="#" role="form">
                            <Row>
                                <TextInput type="text" s={12} label="Nama Kelompok" value={this.state.group_name} name="group_name" onChange={this.handleChange} required/>
                                <Select s={12} type='select' label="Kelas" name="level" defaultValue='1' onChange={this.handleChange} required>
                                    {this.state.levels.filter(level=>level.id < this.state.user_data.levels_id || this.state.user_data.levels_id === 'notset').map((level,index)=>{
                                        return(
                                            <option key={index} value={level.id}>{level.level}</option>
                                        )
                                    })}
                                </Select>
                                <Select s={12} type='select' label="Angkatan Kelompok" name="angkatan" defaultValue='0' onChange={this.handleChange} required>
                                    <option value='0'>Pilih Angkatan</option>
                                    {
                                        this.state.dataAngkatan.map((data,key)=>{
                                            return(
                                                <option key={key} value={data.angkatan}>{data.angkatan}</option>
                                            )
                                        })
                                    }
                                </Select>
                            </Row>
                                {/*{*/}
                                    {/*this.state.binaan.map((binaan, index) => {*/}
                                        {/*let placeholderText = 'Nama Binaan ke-' + (index+1);*/}
                                        {/*return (*/}
                                            {/*<div key={index}>*/}
                                                {/*<Row>*/}
                                                    {/*<Col s={1}><a className="btn-floating btn-large waves-effect waves-light blue">{index+1}.</a></Col>*/}
                                                {/*<TextInput type="text" placeholder={placeholderText} s={5} onChange={(e)=>this.handleChangeBinaan(e, index)}/>*/}
                                                {/*<Select s={6} label="Fakultas" name="faculty" onChange={(e)=>this.handleChangeFakultas(e, index)} required>*/}
                                                    {/*<option value='0'>Pilih Fakultas</option>*/}
                                                    {/*{this.state.faculties.map((faculty, index) =>{*/}
                                                        {/*return(*/}
                                                            {/*<option key={index} value={index+1}>{faculty.faculty}</option>*/}
                                                        {/*)*/}
                                                    {/*})*/}
                                                    {/*}*/}
                                                {/*</Select>*/}
                                                {/*</Row>*/}
                                                {/*<Row>*/}
                                                {/*<Select s={6} label="Prodi" name="prodi" defaultValue='0' onChange={(e)=>this.handleChangeProdi(e, index)} required>*/}
                                                    {/*<option value='0'>Pilih Prodi</option>*/}
                                                    {/*{this.state.prodies.filter(prodi => prodi.faculty_id === this.state.faculty[index]).map((prodi, index) =>{*/}
                                                        {/*return(*/}
                                                            {/*<option key={index} value={prodi.id}>{prodi.prodi}</option>*/}
                                                        {/*)*/}
                                                    {/*})*/}
                                                    {/*}*/}
                                                {/*</Select>*/}
                                                {/*<Select key={index} s={5} type='select' label="Angkatan" name="angkatan_mentee" defaultValue='0' onChange={(e)=>this.handleChangeAngkatan(e, index)} required>*/}
                                                    {/*<option value='0'>Pilih Angkatan</option>*/}
                                                    {/*{this.createOption()}*/}
                                                {/*</Select>*/}
                                                {/*<Col s={1}><a className="btn-floating btn-large waves-effect waves-light red" onClick={()=>this.handleRemoveBinaan(index)}><i className="material-icons">close</i></a></Col>*/}
                                                {/*</Row>*/}
                                            {/*</div>*/}
                                        {/*)*/}
                                    {/*})*/}
                                {/*}*/}
                            {/*<Row>*/}
                                {/*/!*<Col s={11}><h5>Tambah Binaan</h5></Col><Col s={1}><a className="btn-floating btn-large waves-effect waves-light green" onClick={(e)=>this.addBinaan(e)}><i className="material-icons">add</i></a></Col>*!/*/}
                            {/*</Row>*/}
                            <Button waves='light' type='submit' onClick={this.onSubmit}>Submit</Button>
                            {this.state.errors.length > 0 ?
                                <div>
                                    <p>Error:</p>
                                    {this.state.errors.map(error => (
                                        <p key={error}>{error}</p>
                                    ))}
                                </div>
                                :
                                null
                            }
                        </form>
                    </div>
            </div>
        )
    }
}

if(document.getElementById('add_kelompok')) {
    const element = document.getElementById('add_kelompok');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Add {...props} />, document.getElementById('add_kelompok'));
}
