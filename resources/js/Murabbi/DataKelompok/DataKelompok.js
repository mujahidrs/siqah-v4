import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select, Button, Icon, Col} from 'react-materialize'
import axios from 'axios';
import scrollToComponent from "react-scroll-to-component";

function ButtonDelete(props) {
    return(
        <Button
            tooltip="Kosongkan"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="pink"
            waves="light"
            icon={<Icon>highlight_off</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonDeletePermanent(props) {
    return(
        <Button
            tooltip="Hapus Permanen"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="red"
            waves="light"
            icon={<Icon>delete</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonRevive(props) {
    return(
        <Button
            tooltip="Kembalikan"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="pink"
            waves="light"
            icon={<Icon>highlight</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonShowMentee(props) {
    return(
        <Button
            tooltip="Lihat Binaan"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            waves="light"
            className="brown"
            icon={<Icon>remove_red_eye</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonEditGroup(props) {
    return(
        <Button
            tooltip="Edit Kelompok"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            waves="light"
            className="indigo"
            icon={<Icon>edit</Icon>}
            onClick={props.onClick}
        />
    )
}

export default class DataKelompok extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            groups: [],
            users: [],
            editGroup: false,
            group_id: '',
            group_name_edit: '',
            levels_id_edit: '',
            angkatan_edit: '',
            levels: '',
            dataAngkatan: '',
            user_data: '',
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.murabbi;
        axios.get('/api/group/group_name/'+id)
            .then(response => {
                this.setState({groups: response.data});
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });

        axios.get('/api/user/'+id)
            .then(response => {
                this.setState({user_data: response.data});
            });

        axios.get('/api/level')
            .then(response => {
                this.setState({levels: response.data});
            });

        axios.get('/api/angkatan')
            .then(response => {
                this.setState({dataAngkatan: response.data});
            });
    }

    getJumlahMurabbi(gender){
        let jumlah = 0;
        jumlah = this.state.murabbi.filter(murabbi => murabbi.gender === gender).map(murabbi=>murabbi.id);
        return jumlah.length;
    }

    getJumlahAnggota(groups_id) {
        let anggota = this.state.users.filter(user => user.groups_id === groups_id && user.status === 'Aktif').map(user => user.name);
        return anggota.length;
    }


    getNamaProdi(prodi_id){
        let nama_prodi = '';
        nama_prodi = this.state.prodi.filter(prodi => prodi.id === prodi_id).map(prodi=>prodi.prodi);
        return nama_prodi[0];
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    onDelete(groups_id){
        let result = confirm('Kosongkan kelompok ini? Data binaan aman dan bisa dikembalikan');
        if(result === true){
            axios.delete('/api/group/delete/'+groups_id)
                .then(response=>{
                    console.log(response);
                    if(response.data.success === true){
                        let id = this.props.murabbi;
                        axios.get('/api/group/group_name/'+id)
                            .then(response => {
                                this.setState({groups: response.data});
                            });
                        axios.get('/api/user')
                            .then(response => {
                                this.setState({users: response.data});
                            });
                    }
                });
        }
    }

    onDeletePermanent(groups_id){
        let result = confirm('Hapus Permanen kelompok ini?');
        if(result === true){
            axios.delete('/api/group/deletePermanent/'+groups_id)
                .then(response=>{
                    let groups = this.state.groups;

                    for(let i=0; i< groups.length; i++)
                    {
                        if(groups[i].id === groups_id)
                        {
                            groups.splice(i,1);
                            this.setState({
                                groups:groups
                            })
                        }
                    }
                });
        }
    }

    onRevive(groups_id){
        let result = confirm('Kembalikan kelompok ini?');
        if(result === true){
            axios.delete('/api/group/revive/'+groups_id)
                .then(response=>{
                    if(response.data.success === true){
                        let id = this.props.murabbi;
                        axios.get('/api/group/group_name/'+id)
                            .then(response => {
                                this.setState({groups: response.data});
                            });
                        axios.get('/api/user')
                            .then(response => {
                                this.setState({users: response.data});
                            });
                    }
                });
        }
    }

    onEditGroup = (id) => {
        const groups = this.state.groups.filter(group=>group.id === id).map((group) => {
            return (
                {
                    group_id : group.id,
                    group_name_edit : group.group_name,
                    levels_id_edit : group.levels_id,
                    angkatan_edit: group.angkatan,
                }
            )
        });

        this.setState({
            editGroup: true,
            group_id: id,
            group_name_edit: groups[0].group_name_edit,
            levels_id_edit: groups[0].levels_id_edit,
            angkatan_edit: groups[0].angkatan_edit,
        });

        scrollToComponent(this.refs.scrollToThis);
    };

    onSubmitEditGroup = (e) => {
        e.preventDefault();

        let cekNamaGroup = this.state.groups.filter(data=>data.group_name === this.state.group_name_edit && data.id !== this.state.group_id).map(data=>data.group_name);

        if(cekNamaGroup.length === 0) {
            const group =
                {
                    group_id: this.state.group_id,
                    group_name: this.state.group_name_edit,
                    levels_id: this.state.levels_id_edit,
                    angkatan: this.state.angkatan_edit
                };

            console.log(group);

            axios.put('/api/group/editGroup/' + this.state.group_id, group)
                .then(response => {
                    if(response.data.success === true){
                        window.location.reload();
                    }
                })
                .catch();
        }
    };

    onClose = () => {
        this.setState({
            editGroup: false
        })
    };

    render() {
        return (
            <div className="container">
                <h3>Data Kelompok</h3>
                Jumlah: {this.state.groups.length} kelompok
                <table className="borded striped">
                    <thead>
                        <tr>
                            <th className='center aligned'>No.</th>
                            <th className='center aligned'>Nama Kelompok</th>
                            <th className='center aligned'>Angkatan Kelompok</th>
                            <th className='center aligned'>Anggota Aktif</th>
                            <th className='center aligned'>Tindakan</th>
                            {this.props.role === 'admin' && <th className='center aligned'>Menu</th>}
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.groups.filter(group=>group.status === 'Aktif').map((group,index) => {
                                return(
                                    <tr key={index}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{group.group_name}</td>
                                        <td className='center aligned'>{group.angkatan}</td>
                                        <td className='center aligned'>{this.getJumlahAnggota(group.id)}</td>
                                        <td className='center aligned'>
                                            <ButtonShowMentee onClick={()=>window.location=this.props.prefix + '/data_kelompok/'+this.props.murabbi+'/data_mutarabbi/'+group.id}/>
                                            <ButtonEditGroup onClick={()=>this.onEditGroup(group.id)}/>
                                            <ButtonDelete onClick={this.onDelete.bind(this,group.id)}/>
                                        </td>
                                        {this.props.role === 'admin' && <td className='center aligned'>
                                            <Button
                                                tooltip="Rekap Presensi"
                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                floating
                                                small
                                                waves="light"
                                                icon={<Icon>format_list_numbered</Icon>}
                                                onClick={()=>window.location=this.props.prefix + '/data_kelompok/'+this.props.murabbi+'/rekap/'+group.id}
                                            />
                                            <Button
                                                tooltip="Rekap Materi"
                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                floating
                                                small
                                                waves="light"
                                                icon={<Icon>subject</Icon>}
                                                onClick={()=>window.location=this.props.prefix + '/data_kelompok/'+this.props.murabbi+'/madah/'+group.id}
                                            />
                                        </td>}
                                    </tr>
                                )
                            })
                        }
                        {
                            this.state.groups.filter(group=>group.status === 'Nonaktif').map((group,index) => {
                                return(
                                    <tr key={index} style={{color: 'grey'}}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{group.group_name}</td>
                                        <td className='center aligned'>{group.angkatan}</td>
                                        <td className='center aligned'>{this.getJumlahAnggota(group.id)}</td>
                                        <td className='center aligned'>
                                            <ButtonShowMentee onClick={()=>window.location=this.props.prefix + '/data_kelompok/'+this.props.murabbi+'/data_mutarabbi/'+group.id}/>
                                            {/*<a*/}
                                            {/*title="Edit"*/}
                                            {/*className="waves-effect waves-light btn-small"*/}
                                            {/*href={this.props.prefix + '/data_kelompok/'+this.props.murabbi+'/edit/'+group.id}>*/}
                                            {/*<i className="material-icons">*/}
                                            {/*edit*/}
                                            {/*</i>*/}
                                            {/*</a>*/}
                                            <ButtonRevive onClick={this.onRevive.bind(this,group.id)}/>
                                            <ButtonDeletePermanent onClick={this.onDeletePermanent.bind(this,group.id)}/>
                                        </td>
                                        {this.props.role === 'admin' && <td className='center aligned'>
                                            <Button
                                                tooltip="Rekap Presensi"
                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                floating
                                                small
                                                waves="light"
                                                icon={<Icon>format_list_numbered</Icon>}
                                                onClick={()=>window.location=this.props.prefix + '/data_kelompok/'+this.props.murabbi+'/rekap/'+group.id}
                                            />
                                            <Button
                                                tooltip="Rekap Materi"
                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                floating
                                                small
                                                waves="light"
                                                icon={<Icon>subject</Icon>}
                                                onClick={()=>window.location=this.props.prefix + '/data_kelompok/'+this.props.murabbi+'/madah/'+group.id}
                                            />
                                        </td>}
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            <br/>
            <a href={this.props.prefix + '/data_kelompok/'+this.props.murabbi+'/add/'+this.props.id} className="btn-floating btn-large waves-effect waves-light red"><i className="material-icons">add</i></a><label> Tambah Kelompok</label>
                {this.state.editGroup && <div style={{padding: 20, border: '2px solid', borderColor: 'black', borderRadius: 20}}>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <h4>Edit Kelompok</h4>
                    </Col>
                    <Col>
                        <TextInput placeholder='Nama Kelompok' label="Nama Kelompok" type="text" s={3} name="group_name_edit" value={this.state.group_name_edit} onChange={this.handleChange}/>
                        <Select s={12} type='select' label="Kelas" name="levels_id_edit" value={this.state.levels_id_edit} onChange={this.handleChange}>
                            {this.state.levels.filter(level=>level.id < this.state.user_data.levels_id || this.state.user_data.levels_id === 'notset').map((level,index)=>{
                                return(
                                    <option key={index} value={level.id}>{level.level}</option>
                                )
                            })}
                        </Select>
                        <Select s={12} type='select' label="Angkatan Kelompok" name="angkatan_edit" value={this.state.angkatan_edit} onChange={this.handleChange} required>
                            <option value='0'>Pilih Angkatan</option>
                            {
                                this.state.dataAngkatan.map((data,key)=>{
                                    return(
                                        <option key={key} value={data.angkatan}>{data.angkatan}</option>
                                    )
                                })
                            }
                        </Select>
                    </Col>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <Button style={{marginRight: 5}} onClick={this.onSubmitEditGroup}>Edit</Button>
                        <Button style={{marginLeft: 5}} className='red' onClick={this.onClose}>Batal</Button>
                    </Col>
                </div>}
                <div ref='scrollToThis'></div>
    </div>
        )
    }
}

if(document.getElementById('data_kelompok')) {
    const element = document.getElementById('data_kelompok');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<DataKelompok {...props} />, document.getElementById('data_kelompok'));
}
