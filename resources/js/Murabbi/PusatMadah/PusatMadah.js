import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Icon} from 'react-materialize';
import { If, Then, ElseIf, Else } from 'react-if-elseif-else-render';
import axios from 'axios'

export default class PusatMadah extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subjects: [],
            subject_type: [],
            level: '1',
            levels: [],
            user_data: [],
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;
        axios.get('/api/subject/')
            .then(response => {
                this.setState({subjects: response.data});
            });
        axios.get('/api/personality/')
            .then(response => {
                this.setState({subject_type: response.data});
            });
        axios.get('/api/level')
            .then(response => {
                this.setState({levels: response.data});
            });

        axios.get('/api/user/'+this.props.id)
            .then(response => {
                this.setState({user_data: response.data});
            });
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
        console.log(event.target.name + ': ' + event.target.value)
    };

    getJumlah(level){
        let subject = this.state.subjects.filter(subject => subject.levels_id == level && subject.type != 0).map(subject => subject.id);
        return subject.length;
    }

    getMuwashafat(type){
        let muwashafat = this.state.subject_type.filter(personality => personality.id == type).map(personality => personality.personality);
        return muwashafat;
    }

    render() {
        return (
            <div className="container">
                <h3>Pusat Madah</h3>
                <Row>
                    <Select s={12} m={6} type='select' name='level' value={this.state.level} label='Kelas' onChange={this.handleChange}>
                        {this.state.levels.filter(level=>level.id < this.state.user_data.levels_id || this.state.user_data.levels_id === 'notset').map((level,index)=>{
                            return(
                                <option key={index} value={level.id}>{level.level}</option>
                            )
                        })}
                    </Select>
                </Row>
                Jumlah: {this.getJumlah(this.state.level)} madah
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                            <tr>
                                <th className='center aligned'>No.</th>
                                <th>Judul</th>
                                <th className='center aligned'>Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.subjects.filter(subject => subject.levels_id == this.state.level && subject.type != 0).map((subject,index)=>{
                                return(
                                    <tr key={index}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{subject.subject}</td>
                                        <td className='center aligned'>
                                            {
                                                subject.file !== null && (
                                                    <Button
                                                        tooltip="Download"
                                                        floating
                                                        waves="light"
                                                        small
                                                        className="blue"
                                                        style={{marginLeft: 2.5, marginRight: 2.5}}
                                                        icon={<Icon>file_download</Icon>}
                                                        onClick={()=>window.location='/api/storage/'+subject.file}
                                                    />
                                                )
                                            }
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

if(document.getElementById('pusat_madah')) {
    const element = document.getElementById('pusat_madah');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<PusatMadah {...props} />, document.getElementById('pusat_madah'));
}
