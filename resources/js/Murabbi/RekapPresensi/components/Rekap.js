import React from 'react';
import ReactDOM from 'react-dom';
import {Icon, Row, Col} from 'react-materialize';
import OneChartComponent from '../../../../js/components/OneChart';
import axios from 'axios';

export default class Rekap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            group_name: '',
            year: '',
            users: [],
            binaan: [],
            presences: [],
            week: '',
            month: '',
            pekan_sekarang: '',
        };
        console.log(super());
    };

    componentDidMount() {
        let groups_id = window.location.pathname.split('/')[4];
        if(this.props.role === 'admin') {
            groups_id = window.location.pathname.split('/')[5];
        }
        let today = new Date();
        let currentWeekNumber = require('current-week-number');

        let month = today.getMonth() + 1;
        if(month < 10){
            month = '0' + month;
        }
        else{
            month = month.toString();
        }

        let tanggal = today.getDate();
        if(tanggal <= 10){
            tanggal = '0' + tanggal;
        }

        let cdate = today.getFullYear() + '-' + month + '-' + tanggal;
        let year2 = cdate.substr(0, 4);
        let week = currentWeekNumber(cdate);

        axios.get('/api/group/'+groups_id)
            .then(response => {
                this.setState({
                    group_name: response.data.group_name,
                });
            });

        axios.get('/api/presence/'+groups_id)
            .then(response => {
                this.setState({
                    presences: response.data,
                });
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({
                    users: response.data,
                });
            });

        axios.get('/api/user/group/'+groups_id)
            .then(response => {
                this.setState({binaan: response.data});
            });

        let pekan = '';
        let bulan = '';

        // console.log(month);

        if (month === '01'){
            pekan = week;
            bulan = month;
            if(pekan === 0){
                pekan = 1;
                bulan = '1'
            }
            else{
                pekan = week;
                bulan = month;
            }
        }
        else if (month === '02'){
            pekan = week-5;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '1'
            }
            else{
                pekan = week-5;
                bulan = month;
            }
        }
        else if (month === '03'){
            pekan = week-9;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '2'
            }
            else{
                pekan = week-9;
                bulan = month;
            }
        }
        else if (month === '04'){
            pekan = week-13;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '3'
            }
            else{
                pekan = week-13;
                bulan = month;
            }
        }
        else if (month === '05'){
            pekan = week-18;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '4'
            }
            else{
                pekan = week-18;
                bulan = month;
            }
        }
        else if (month === '06'){
            pekan = week-22;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '5'
            }
            else{
                pekan = week-22;
                bulan = month;
            }
        }
        else if (month === '07'){
            pekan = week-26;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '6'
            }
            else{
                pekan = week-26;
                bulan = month;
            }
        }
        else if (month === '08'){
            pekan = week-31;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '7'
            }
            else{
                pekan = week-31;
                bulan = month;
            }
        }
        else if (month === '09'){
            pekan = week-35;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '8'
            }
            else{
                pekan = week-35;
                bulan = month;
            }
        }
        else if (month === '10'){
            pekan = week-39;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '9'
            }
            else{
                pekan = week-39;
                bulan = month;
            }
        }
        else if(month === '11'){
            pekan = week-44;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '10';
            }
            else{
                pekan = week-44;
                bulan = month;
            }
        }
        else if(month === '12'){
            pekan = week-48;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '11';
            }
            else{
                pekan = week-48;
                bulan = month;
            }
        }

        this.setState({
            year: year2,
            pekan_sekarang: pekan,
            month: parseInt(bulan),
        })
    }

    prevMonth = () => {
        let bulan = this.state.month;
        if (bulan <= 1){
            this.setState({
                month : 12,
                year : parseInt(this.state.year)-1
            });
        }
        else{
            this.setState({
                month : parseInt(this.state.month)-1
            });
        }
    };

    nextMonth = () => {
        let bulan = this.state.month;
        if (bulan >= 12){
            this.setState({
                month : 1,
                year : parseInt(this.state.year)+1
            });
        }
        else{
            this.setState({
                month : parseInt(this.state.month)+1
            });
        }
    };

    getPresences(id, week, month){
        const presences = this.state.presences.filter(presences=> presences.users_id === id && presences.week === week.toString() && presences.month === month && presences.year === this.state.year.toString())
            .map(presences=>presences.mentee_presence);
        return presences[0];
    }

    getTotalKehadiran(id, bulan){
        let pekan1 = this.getPresences(id, 1, bulan) !== 1 ? null : 1;
        let pekan2 = this.getPresences(id, 2, bulan) !== 1 ? null : 1;
        let pekan3 = this.getPresences(id, 3, bulan) !== 1 ? null : 1;
        let pekan4 = this.getPresences(id, 4, bulan) !== 1 ? null : 1;
        let pekan5 = this.getPresences(id, 5, bulan) !== 1 ? null : 1;
        let kehadiran = pekan1 + pekan2 + pekan3 + pekan4 + pekan5;
        return parseInt(kehadiran);
    }

    getTotalPencapaian(presences, week, month){
        let total = 0;
        console.log(presences.filter(presences=>presences.week === week.toString() && presences.mentee_presence === 1 && presences.month === month));
        presences.filter(presences=>presences.week === week.toString() && presences.mentee_presence === 1  && presences.month === month).map(presences=>{
            total = total + parseInt(this.getPresences(presences.users_id, week, month))
        });
        if(isNaN(total)){
            return total = 0;
        }
        else{
            return total
        }
    }

    getKehadiran(presences, week, month){
        let kehadiran = this.getTotalPencapaian(presences, week, month);
        if(isNaN(kehadiran)){
            return 0;
        }
        else{
            return kehadiran
        }
    }

    getJumlahNonaktif(){
        let nonaktif = this.state.binaan.filter(binaan => binaan.status === 'Nonaktif').map(binaan => binaan.name);
        return nonaktif.length;
    }

    getName(id){
        id = parseInt(id);
        let name = this.state.users.filter(user => user.id === id).map(user => user.name);
        return name[0];
    }

    render() {
        let murabbi_id = window.location.pathname.split('/')[3];

        if(murabbi_id === 'rekap'){
            murabbi_id = this.props.id;
        }

        let months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }

        return (
            <div className="container">
                <h2>Rekapitulasi Presensi</h2>
                <OneChartComponent
                    label='Kehadiran Binaan'
                    data={[((this.getTotalPencapaian(this.state.presences, 1, monthNumToName(this.state.month))/this.state.binaan.length)*100).toFixed(0), ((this.getTotalPencapaian(this.state.presences, 2, monthNumToName(this.state.month))/this.state.binaan.length)*100).toFixed(0), ((this.getTotalPencapaian(this.state.presences, 3, monthNumToName(this.state.month))/this.state.binaan.length)*100).toFixed(0), ((this.getTotalPencapaian(this.state.presences, 4, monthNumToName(this.state.month))/this.state.binaan.length)*100).toFixed(0), ((this.getTotalPencapaian(this.state.presences, 5, monthNumToName(this.state.month))/this.state.binaan.length)*100).toFixed(0)]}
                    labels={['Pekan 1', 'Pekan 2', 'Pekan 3', 'Pekan 4', 'Pekan 5']}
                    max={100}
                    height={window.innerWidth < 500 ? 250 : null}
                />
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getName(murabbi_id)}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama Kelompok</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.group_name}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Jumlah Anggota Aktif</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.binaan.length - this.getJumlahNonaktif()}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Jumlah Anggota Non-Aktif</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getJumlahNonaktif()}</Row>
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                            <tr>
                                <th rowSpan="2" className='center aligned'>No.</th>
                                <th rowSpan="2" className='center aligned'>Nama Binaan</th>
                                <th colSpan="5" className='center aligned'><a onClick={this.prevMonth} style={{cursor: 'pointer'}}><Icon tiny>chevron_left</Icon></a>{monthNumToName(this.state.month)} {this.state.year}<a onClick={this.nextMonth} style={{cursor: 'pointer'}}><Icon tiny>chevron_right</Icon></a></th>
                                <th rowSpan="2" className='center aligned'>Hadir</th>
                            </tr>
                            <tr>
                                <th className='center aligned'>1</th>
                                <th className='center aligned'>2</th>
                                <th className='center aligned'>3</th>
                                <th className='center aligned'>4</th>
                                <th className='center aligned'>5</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.binaan.filter(binaan => binaan.status === 'Aktif').map((binaan,index) => {
                                return (
                                    <tr key={index}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{binaan.name}</td>
                                        <td className='center aligned'>{this.getPresences(binaan.id, 1, monthNumToName(this.state.month)) === 1 ? <i className='material-icons' style={{color: 'green'}}>check</i> : this.getPresences(binaan.id, 1, monthNumToName(this.state.month)) === 0 ? <i className='material-icons' style={{color: 'red'}}>close</i> : null}</td>
                                        <td className='center aligned'>{this.getPresences(binaan.id, 2, monthNumToName(this.state.month)) === 1 ? <i className='material-icons' style={{color: 'green'}}>check</i> : this.getPresences(binaan.id, 2, monthNumToName(this.state.month)) === 0 ? <i className='material-icons' style={{color: 'red'}}>close</i> : null}</td>
                                        <td className='center aligned'>{this.getPresences(binaan.id, 3, monthNumToName(this.state.month)) === 1 ? <i className='material-icons' style={{color: 'green'}}>check</i> : this.getPresences(binaan.id, 3, monthNumToName(this.state.month)) === 0 ? <i className='material-icons' style={{color: 'red'}}>close</i> : null}</td>
                                        <td className='center aligned'>{this.getPresences(binaan.id, 4, monthNumToName(this.state.month)) === 1 ? <i className='material-icons' style={{color: 'green'}}>check</i> : this.getPresences(binaan.id, 4, monthNumToName(this.state.month)) === 0 ? <i className='material-icons' style={{color: 'red'}}>close</i> : null}</td>
                                        <td className='center aligned'>{this.getPresences(binaan.id, 5, monthNumToName(this.state.month)) === 1 ? <i className='material-icons' style={{color: 'green'}}>check</i> : this.getPresences(binaan.id, 5, monthNumToName(this.state.month)) === 0 ? <i className='material-icons' style={{color: 'red'}}>close</i> : null}</td>
                                        <td className='center aligned'>{this.getTotalKehadiran(binaan.id, monthNumToName(this.state.month))}</td>
                                    </tr>
                                )
                            })
                        }
                        <tr>
                            <td colSpan="2">Jumlah Kehadiran</td>
                            <td className='center aligned'>{this.getKehadiran(this.state.presences, 1, monthNumToName(this.state.month))}</td>
                            <td className='center aligned'>{this.getKehadiran(this.state.presences, 2, monthNumToName(this.state.month))}</td>
                            <td className='center aligned'>{this.getKehadiran(this.state.presences, 3, monthNumToName(this.state.month))}</td>
                            <td className='center aligned'>{this.getKehadiran(this.state.presences, 4, monthNumToName(this.state.month))}</td>
                            <td className='center aligned'>{this.getKehadiran(this.state.presences, 5, monthNumToName(this.state.month))}</td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td colSpan="2">Persentase Kehadiran</td>
                            <td className='center aligned'>{((this.getTotalPencapaian(this.state.presences, 1, monthNumToName(this.state.month))/this.state.binaan.length)*100).toFixed(0) + '%'}</td>
                            <td className='center aligned'>{((this.getTotalPencapaian(this.state.presences, 2, monthNumToName(this.state.month))/this.state.binaan.length)*100).toFixed(0) + '%'}</td>
                            <td className='center aligned'>{((this.getTotalPencapaian(this.state.presences, 3, monthNumToName(this.state.month))/this.state.binaan.length)*100).toFixed(0) + '%'}</td>
                            <td className='center aligned'>{((this.getTotalPencapaian(this.state.presences, 4, monthNumToName(this.state.month))/this.state.binaan.length)*100).toFixed(0) + '%'}</td>
                            <td className='center aligned'>{((this.getTotalPencapaian(this.state.presences, 5, monthNumToName(this.state.month))/this.state.binaan.length)*100).toFixed(0) + '%'}</td>
                            <td> </td>
                        </tr>
                        </tbody>
                    </table>
            <br/>

            </div>
    </div>
        )
    }
}

if(document.getElementById('rekap')) {
    const element = document.getElementById('rekap');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Rekap {...props} />, document.getElementById('rekap'));
}
