import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Icon} from 'react-materialize'
import axios from 'axios'

export default class RekapPresensi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            groups: [],
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;
        axios.get('/api/group/group_name/'+id)
            .then(response => {
                this.setState({groups: response.data});
            });
    }

    render() {
        return (
            <div className="container">
                <h3>Rekapitulasi Presensi</h3>
                Jumlah: {this.state.groups.length} kelompok
                <table className="bordered striped">
                    <thead>
                        <tr>
                            <th className='center aligned'>No.</th>
                            <th>Nama Kelompok</th>
                            <th className='center aligned'>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.groups.filter(group=>group.status === 'Aktif').map((group,index) => {
                            return (
                                <tr key={index}>
                                    <td className='center aligned'>{index+1}</td>
                                    <td>{group.group_name}</td>
                                    <td className='center aligned'>
                                        <Button
                                            waves="light"
                                            small
                                            floating
                                            className="blue"
                                            onClick={()=>window.location='/user/rekapitulasi_presensi/rekap/'+group.id}
                                            icon={<Icon>
                                                remove_red_eye
                                            </Icon>}
                                            />
                                    </td>
                                </tr>
                            )
                        })
                    }
                    {
                        this.state.groups.filter(group=>group.status === 'Nonaktif').map((group,index) => {
                            return (
                                <tr key={index} style={{color: 'grey'}}>
                                    <td className='center aligned'>{index+1}</td>
                                    <td>{group.group_name}</td>
                                    <td className='center aligned'>
                                        <Button
                                            waves="light"
                                            small
                                            floating
                                            className="blue"
                                            onClick={()=>window.location='/user/rekapitulasi_presensi/rekap/'+group.id}
                                            icon={<Icon>
                                                remove_red_eye
                                            </Icon>}
                                        />
                                    </td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>
            </div>
        )
    }
}

if(document.getElementById('rekap_presensi')) {
    const element = document.getElementById('rekap_presensi');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<RekapPresensi {...props} />, document.getElementById('rekap_presensi'));
}
