import React from 'react';
import ReactDOM from 'react-dom';
import {Row, Col, Icon } from 'react-materialize';
import axios from 'axios';

export default class Lihat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            subjects: [],
            groups: [],
            presences: [],
            groups_id: '',
            date: '',
            replaced: '',
            end_time: '',
            start_time: '',
            location: '',
            kultum: '',
            tilawah: '',
            infaq: '',
            subjects_id: '',
            done: '',
            group_notes: '',
            mentee_presence: [],
            mentor_presence: '',
            month: '',
            personal_details: '',
            users_id: [],
            week: '',
            year: '',
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;
        let presence_id = window.location.pathname.split('/')[4];
        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });

        axios.get('/api/subject')
            .then(response => {
                this.setState({subjects: response.data});
            });

        axios.get('/api/group/group_name/'+id)
            .then(response => {
                this.setState({groups: response.data});
            });

        axios.get('/api/presence/lihat/'+presence_id)
            .then(response => {
                this.setState({
                    presences: response.data,
                    groups_id: response.data[0].groups_id,
                    replaced: response.data[0].replaced,
                    date: response.data[0].date,
                    end_time: response.data[0].end_time,
                    start_time: response.data[0].start_time,
                    location: response.data[0].location,
                    subjects_id: response.data[0].subjects_id,
                    done: response.data[0].done,
                    group_notes: response.data[0].group_notes,
                    infaq: response.data[0].infaq,
                    kultum: response.data[0].kultum,
                    mentor_presence: response.data[0].mentor_presence,
                    month: response.data[0].month,
                    tilawah: response.data[0].tilawah,
                    week: response.data[0].week,
                    year: response.data[0].year,
                });
            });
    }

    getName(id){
        id = parseInt(id);
        let name = this.state.users.filter(user => user.id === id).map(user => user.name);
        return name[0];
    }

    getPersentase(){
        let total = 0;
        this.state.presences.filter(presences=>presences.week === this.state.week && presences.mentee_presence === 1).map(presences=>{
            total = total + presences.mentee_presence
        });
        if(isNaN(total)){
            return total = 0;
        }
        else{
            return (total * 100 / this.state.presences.length).toFixed() + '%';
        }
    }

    getGroupName(groups_id){
        let group = this.state.groups.filter(group => group.id === groups_id).map(group => group.group_name);
        return group[0] ;
    }

    getSubject(subject_id){
        let subject = this.state.subjects.filter(subject => subject.id === subject_id).map(subject => subject.subject);
        return subject[0];
    }

    render() {
        let strtotime = require('locutus/php/datetime/strtotime');
        let date = require('locutus/php/datetime/date');

        let a = strtotime(this.state.date);
        let hari = date('l', a);

        let months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        return (
            <div className="container">
                <h2>Laporan Pembinaan</h2>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama Kelompok</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getName(this.props.id)}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama Kelompok</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getGroupName(this.state.groups_id)}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Badal</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.replaced}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Hari</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{hari}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Tanggal</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.date.substr(8,2) + ' ' + monthNumToName(this.state.date.substr(5,2)) + ' ' + this.state.year}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Pekan ke</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.week + ' ' + this.state.month}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Waktu</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.start_time.substr(0,5)} - {this.state.end_time.substr(0,5)}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Tempat</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.location} </Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Madah</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getSubject(this.state.subjects_id)} </Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Kultum</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.kultum}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Tilawah</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.tilawah}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Infaq</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.infaq}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Jumlah Binaan</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.presences.length}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Qadhaya</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.group_notes}</Row>
                <div className="table-responsive">
                    <table className="bordered striped">
                        <thead>
                            <tr>
                                <th className='center aligned'>No.</th>
                                <th className='center aligned'>Nama Binaan</th>
                                <th className='center aligned'>Kehadiran</th>
                                <th className='center aligned'>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.presences.map((presence,index) => {
                                    return(
                                        <tr key={index}>
                                            <td className='center aligned'>{index+1}</td>
                                            <td>{this.getName(presence.users_id)}</td>
                                            <td className='center aligned'>{presence.mentee_presence === 1 ? (<Icon className='green-text' small>check</Icon>) : (<Icon className='red-text' small>clear</Icon>)}</td>
                                            <td className='center aligned'>{presence.personal_details}</td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                    <br/>
                        <h4>Persentase: {this.getPersentase()}</h4>
                </div>
            </div>
        )
    }
}

if(document.getElementById('lihat')) {
    const element = document.getElementById('lihat');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Lihat {...props} />, document.getElementById('lihat'));
}
