import React from 'react';
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Textarea} from 'react-materialize';
import axios from 'axios';
import SearchableSelect from "../../../components/SearchableSelect";

export default class Lapor extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            group_name: '',
            mentors_id: '',
            tanggal: '',
            date_asli: '',
            waktu_mulai: '',
            waktu_berakhir: '',
            tempat: '',
            qadhaya: '',
            madah: '',
            errors: [],
            users: [],
            level: '1',
            year: '',
            gender: '',
            binaan: [],
            presences: [],
            keberjalanan: 'Ya',
            badal: 'Tidak',
            week: '',
            month: '',
            subjects: [],
            madah_lainnya: '',
            mentor_presence: 'Ya',
        };
        console.log(super());
    };

    componentDidMount() {
        let date = require('locutus/php/datetime/date');
        let groups_id = window.location.pathname.split('/')[4];
        let today = new Date();
        let tanggal = today.getDate();
        if(tanggal < 10){
            tanggal = '0' + tanggal;
        }
        let getMonth = today.getMonth() + 1;
        if(getMonth < 10){
            getMonth = '0' + getMonth;
        }
        let cdate = today.getFullYear() + '-' + getMonth + '-' + tanggal;
        let jam = date('H');
        let jam_2 = date('H') - 2;

        jam = parseInt(jam);

        if(jam < 10){
            jam = '0' + jam
        }
        if(jam_2 < 10){
            jam_2 = '0' + jam_2
        }
        let jam_berakhir = jam + ':00';
        let jam_mulai = jam_2 + ':00';

        axios.get('/api/group/'+groups_id)
            .then(response => {
                this.setState({
                    group_name: response.data.group_name,
                    mentors_id: response.data.mentors_id,
                    level: response.data.levels_id
                });
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });

        axios.get('/api/subject')
            .then(response => {
                this.setState({subjects: response.data});
            });

        axios.get('/api/presence/'+groups_id)
            .then(response => {
                this.setState({presences: response.data});
            });

        axios.get('/api/user/group/'+groups_id)
            .then(response => {
                this.setState({binaan: response.data});
            });

        let currentWeekNumber = require('current-week-number');

        let months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }

        let month = cdate.substr(5, 2);
        let year = cdate.substr(0, 4);
        let week = currentWeekNumber(cdate);

        let pekan = '';
        let bulan = '';

        if (month === '01'){
            pekan = week;
            bulan = month;
            if(pekan === 0){
                pekan = 1;
                bulan = '01'
            }
            else{
                pekan = week;
                bulan = month;
            }
        }
        else if (month === '02'){
            pekan = week-5;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '01'
            }
            else{
                pekan = week-5;
                bulan = month;
            }
        }
        else if (month === '03'){
            pekan = week-9;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '02'
            }
            else{
                pekan = week-9;
                bulan = month;
            }
        }
        else if (month === '04'){
            pekan = week-13;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '03'
            }
            else{
                pekan = week-13;
                bulan = month;
            }
        }
        else if (month === '05'){
            pekan = week-18;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '04'
            }
            else{
                pekan = week-18;
                bulan = month;
            }
        }
        else if (month === '06'){
            pekan = week-22;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '05'
            }
            else{
                pekan = week-22;
                bulan = month;
            }
        }
        else if (month === '07'){
            pekan = week-26;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '06'
            }
            else{
                pekan = week-26;
                bulan = month;
            }
        }
        else if (month === '08'){
            pekan = week-31;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '07'
            }
            else{
                pekan = week-31;
                bulan = month;
            }
        }
        else if (month === '09'){
            pekan = week-35;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '08'
            }
            else{
                pekan = week-35;
                bulan = month;
            }
        }
        else if (month === '10'){
            pekan = week-39;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '09'
            }
            else{
                pekan = week-39;
                bulan = month;
            }
        }
        else if(month === '11'){
            pekan = week-44;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '10';
            }
            else{
                pekan = week-44;
                bulan = month;
            }
        }
        else if(month === '12'){
            pekan = week-48;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '11';
            }
            else{
                pekan = week-48;
                bulan = month;
            }
        }

        this.setState({
            year: year,
            week: pekan,
            month: monthNumToName(bulan),
            tanggal: cdate,
            date_asli: cdate,
            waktu_berakhir: jam_berakhir,
            waktu_mulai: jam_mulai,
        })
    }

    validate = (group_name, level, angkatan, binaan) => {
        const errors = [];

        if(group_name.length === 0){
            errors.push('Nama Kelompok tidak boleh kosong');
        }

        if(level.length === 0){
            errors.push('Level tidak boleh kosong');
        }

        if(angkatan.length === 0){
            errors.push('Angkatan tidak boleh kosong');
        }

        if(binaan.length === 0){
            errors.push('Binaan tidak boleh kosong');
        }

        return errors;
    };

    getName(id){
        id = parseInt(id);
        let name = this.state.users.filter(user => user.id === id).map(user => user.name);
        return name[0];
    }

    getGender(id){
        id = parseInt(id);
        let gender = this.state.users.filter(user => user.id === id).map(user => user.gender);
        if(gender[0] === 1){
            return "Ikhwan";
        }
        else if(gender[0] === 2){
            return "Akhwat";
        }
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
        console.log(event.target.name + ': ' + event.target.value)
    };

    handleDate = (event) => {
        let currentWeekNumber = require('current-week-number');

        let tanggal = event.target.value;
        let dates = tanggal.substr(8, 2);
        let month = tanggal.substr(5, 2);
        let year = tanggal.substr(0, 4);
        let tanggal_asli = this.state.date_asli;
        let dates_asli = tanggal_asli.substr(8, 2);
        let month_asli = tanggal_asli.substr(5, 2);
        let year_asli = tanggal_asli.substr(0, 4);
        let newdate = year+'-'+month+'-'+dates;
        let newdate_asli = year_asli+'-'+month_asli+'-'+dates_asli;
        let week = currentWeekNumber(newdate);
        let week_asli = currentWeekNumber(newdate_asli);

        let months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }

        let pekan = '';
        let bulan = '';

        if (month === '01'){
            pekan = week;
            bulan = month;
            if(pekan === 0){
                pekan = 1;
                bulan = '01'
            }
            else{
                pekan = week;
                bulan = month;
            }
        }
        else if (month === '02'){
            pekan = week-5;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '01'
            }
            else{
                pekan = week-5;
                bulan = month;
            }
        }
        else if (month === '03'){
            pekan = week-9;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '02'
            }
            else{
                pekan = week-9;
                bulan = month;
            }
        }
        else if (month === '04'){
            pekan = week-13;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '03'
            }
            else{
                pekan = week-13;
                bulan = month;
            }
        }
        else if (month === '05'){
            pekan = week-18;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '04'
            }
            else{
                pekan = week-18;
                bulan = month;
            }
        }
        else if (month === '06'){
            pekan = week-22;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '05'
            }
            else{
                pekan = week-22;
                bulan = month;
            }
        }
        else if (month === '07'){
            pekan = week-26;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '06'
            }
            else{
                pekan = week-26;
                bulan = month;
            }
        }
        else if (month === '08'){
            pekan = week-31;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '07'
            }
            else{
                pekan = week-31;
                bulan = month;
            }
        }
        else if (month === '09'){
            pekan = week-35;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '08'
            }
            else{
                pekan = week-35;
                bulan = month;
            }
        }
        else if (month === '10'){
            pekan = week-39;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '09'
            }
            else{
                pekan = week-39;
                bulan = month;
            }
        }
        else if(month === '11'){
            pekan = week-44;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '10';
            }
            else{
                pekan = week-44;
                bulan = month;
            }
        }
        else if(month === '12'){
            pekan = week-48;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '11';
            }
            else{
                pekan = week-48;
                bulan = month;
            }
        }

        if(week > week_asli){
            window.alert('Pekan lebih dari');
            tanggal = '';
            pekan = '';
            bulan = '';
        }
        if(year > this.state.year){
            window.alert('Tahun lebih dari');
            tanggal = '';
            pekan = '';
            bulan = '';
        }

        let ceklaporan = this.state.presences.filter(presences=>presences.year === year && presences.month === monthNumToName(bulan) && presences.week === pekan).map(presences=>presences.id);
        if(ceklaporan.length > 0){
            window.alert('Pekan sudah dilaporkan!');
            tanggal = '';
            pekan = '';
        }

        console.log(tanggal);
        console.log(pekan);
        console.log(monthNumToName(bulan));

        this.setState({
            tanggal: tanggal,
            week: pekan,
            month: monthNumToName(bulan)
        });
    };

    handleInputChange = (e) => {
        if (["mentee_presence", "personal_details"].includes(e.target.className) ) {
            let binaan = [...this.state.binaan.filter(data=>data.status === 'Aktif')];
            binaan[e.target.dataset.id][e.target.className] = e.target.type === 'checkbox' ? e.target.checked === true ? 1 : 0 : e.target.value;
            this.setState({ binaan }, () => console.log(this.state.binaan))
        } else {
            this.setState({ [e.target.name]: e.target.value })
        }
    };

    onSubmit = (e) => {
        e.preventDefault();
        let groups_id = window.location.pathname.split('/')[4];

        const presences = this.state.binaan.filter(binaan => binaan.status === 'Aktif').map(binaan=>{

            return this.state.keberjalanan === 'Ya' ? {
                    users_id : binaan.id,
                    mentee_presence : binaan.mentee_presence,
                    personal_details : binaan.personal_details,
                    groups_id : groups_id,
                    date : this.state.tanggal,
                    week : this.state.week,
                    done : this.state.keberjalanan,
                    start_time : this.state.waktu_mulai,
                    end_time : this.state.waktu_berakhir,
                    group_notes : this.state.qadhaya,
                    location : this.state.tempat,
                    mentor_presence : this.state.mentor_presence,
                    replaced : this.state.badal,
                    subjects_id : this.state.madah,
                    month : this.state.month,
                    year : this.state.year,
                    infaq: this.state.infaq,
                    kultum: this.state.kultum,
                    tilawah: this.state.tilawah,
                } :
                {
                    mentee_presence : this.state.mentee_presence,
                    personal_details : this.state.personal_details,
                    date : '',
                    users_id : binaan.id,
                    groups_id : groups_id,
                    week : this.state.week,
                    done : 'Tidak',
                    start_time : '',
                    end_time : '',
                    group_notes : this.state.qadhaya,
                    location : '',
                    mentor_presence : this.state.mentor_presence,
                    replaced : this.state.badal,
                    subjects_id : '',
                    month : this.state.month,
                    year : this.state.year,
                    infaq: '',
                    kultum: '',
                    tilawah: '',
                }
        });

        console.log(presences);

        const subjects =
            {
                number: 0,
                type: 0,
                levels_id: this.state.level,
                subject: this.state.madah_lainnya,
                tool: ''
            };

        console.log(subjects);

        axios.post('/api/subject/store', subjects)
            .then(response=>{
                console.log(response);
                console.log('Materi bertambah')
            })
            .catch(error=>console.log(error));

        axios.post('/api/presence/store', {presences})
            .then(response=>{
                console.log(response);
                console.log({presences});
                window.location = '/user/rekapitulasi_presensi/rekap/'+groups_id
            })
            .catch(error=>console.log(error));

    };

    onBerjalan = () =>{
        console.log('Berjalan');
        document.getElementById('berjalan').style.display = 'block';
        document.getElementById('tidak_berjalan').style.display = 'none';
    };

    onTidakBerjalan = () =>{
        console.log('Tidak Berjalan');
        document.getElementById('berjalan').style.display = 'none';
        document.getElementById('tidak_berjalan').style.display = 'block';
    };

    onHadir = () =>{
        console.log('Hadir');
        document.getElementById('mentor_tidak_hadir').style.display = 'none';
    };

    onLainnya = () =>{
        console.log('Lainnya');
        document.getElementById('madah_lainnya').style.display = 'block';
    };

    onPilihMadah = () =>{
        console.log('Pilih Madah');
        document.getElementById('madah_lainnya').style.display = 'none';
    };

    onTidakHadir = () =>{
        console.log('Tidak Hadir');
        document.getElementById('mentor_tidak_hadir').style.display = 'block';
    };

    handleKeberjalanan = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
        if(event.target.value === 'Ya'){
            this.onBerjalan();
        }
        if(event.target.value === 'Tidak'){
            this.onTidakBerjalan();
        }
    };

    handleKehadiranPembina = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
        if(event.target.value === 'Ya'){
            this.onHadir();
        }
        if(event.target.value === 'Tidak'){
            this.onTidakHadir();
        }
    };

    handleMadah = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
        if(event.target.value === 'Lainnya'){
            this.onLainnya();
        }
        else if(event.target.value !== 'Lainnya'){
            this.onPilihMadah();
        }
    };

    handleChangeMadah = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.name]: event.value});
        }
        if(event.value === 'Lainnya'){
            this.onLainnya();
        }
        else if(event.value !== 'Lainnya'){
            this.onPilihMadah();
        }
    };

    render() {
        let gender = this.state.users.filter(user=>user.id === parseInt(this.props.id)).map(user=>user.gender)[0];

        this.state.gender = this.getGender(this.props.id);
        let ceklaporan = this.state.presences.filter(presences=>presences.year === this.state.date_asli.substr(0,4) && presences.month === this.state.month && presences.week === this.state.week.toString()).map(presences=>presences.id);
        if(ceklaporan.length > 0){
            window.alert('Pekan sudah dilaporkan!');
            this.state.tanggal = '';
            this.state.week = '';
        }

        console.log(this.state.binaan);

        let subjects = this.state.subjects.filter(subject => subject.levels_id === parseInt(this.state.level) && subject.type !== 0).map(subjects=>({
            label: subjects.subject,
            value: subjects.id,
            name: 'madah'
        }));

        subjects.push({
            label: 'Lainnya',
            value: 'Lainnya',
            name: 'madah'
        });

        return (
            <div className="container">
                <h2>Laporan Pembinaan</h2>
                <Row>
                    <Select s={12} type='select' label="Keberjalanan Pembinaan*" name="keberjalanan" onChange={this.handleKeberjalanan} required>
                        <option value="Ya">Berjalan</option>
                        <option value="Tidak">Tidak Berjalan</option>
                    </Select>
                    <TextInput type="text" s={12} label="Nama Kelompok" placeholder={this.state.group_name} disabled/>
                    <TextInput type="text" s={12} label="Nama Pembina" placeholder={this.getName(this.state.mentors_id)} disabled/>
                    <Select s={12} type='select' label="Kehadiran Pembina*" name="mentor_presence" onChange={this.handleKehadiranPembina} required>
                        <option value="Ya">Ya</option>
                        <option value="Tidak">Tidak</option>
                    </Select>
                </Row>
                <div id='mentor_tidak_hadir' style={{display: 'none'}}>
                    <Row>
                    <Select s={12} type='select' label="Dibadal Oleh*" name="badal" onChange={this.handleChange} required>
                        <option value="Mandiri">Mandiri</option>
                        {
                            this.state.users.filter(murabbi => murabbi.role === 1 && murabbi.is_mentor === 1 && murabbi.id !== parseInt(this.props.id) && murabbi.gender === gender).map((murabbi, index)=>{
                                return(
                                    <option key={index} value={murabbi.id}>{murabbi.name}</option>
                                )
                            })
                        }
                    </Select>
                    </Row>
                </div>
                <div id='berjalan' style={{display: 'block'}}>
                    <Row>
                        <div className="input-field col s12">
                            <input type="date" name="tanggal" value={this.state.tanggal} onChange={this.handleDate} required/>
                            <label className="active">Tanggal*</label>
                        </div>
                        <div className="input-field col s12">
                            <input type="time" name="waktu_mulai" style={{width: 100 + 'px'}} value={this.state.waktu_mulai} onChange={this.handleChange} required/> -
                            <input type="time" name="waktu_berakhir" style={{width: 100 + 'px'}} value={this.state.waktu_berakhir} onChange={this.handleChange} required/>
                            <label className="active">Waktu*</label>
                        </div>
                        <TextInput s={12} type='text' label="Tempat*" name="tempat" onChange={this.handleChange} required/>
                        <TextInput s={12} type='text' label="Infaq" name="infaq" onChange={this.handleChange}/>
                        <TextInput s={12} type='text' label="Tilawah" name="tilawah" onChange={this.handleChange}/>
                        <TextInput s={12} type='text' label="Kultum" name="kultum" onChange={this.handleChange}/>
                        <label>Madah*</label>
                        <SearchableSelect s={12} placeholder="Pilih Materi" options={subjects} value={this.state.madah.value} onChange={this.handleChangeMadah} name='madah'/>
                        {/*<Select s={12} type='select' label="Madah*" name="madah" defaultValue='0' onChange={this.handleMadah} required>*/}
                        {/*    <option value='0' disabled>Pilih Madah</option>*/}
                        {/*    {*/}
                        {/*        this.state.subjects.filter(subject => subject.levels_id == this.state.level && subject.type != 0).map((subject,index) => {*/}
                        {/*            return (*/}
                        {/*                <option key={index} value={subject.id}>{subject.subject}</option>*/}
                        {/*            )*/}
                        {/*        })*/}
                        {/*    }*/}
                        {/*    <option value="Lainnya">Lainnya</option>*/}
                        {/*</Select>*/}
                        <div id='madah_lainnya' style={{display: 'none'}}>
                            <TextInput s={12} type='text' label="Madah*" name="madah_lainnya" onChange={this.handleChange} required/>
                        </div>
                        <h5>Binaan Yang Hadir:*</h5>
                        <table className="bordered striped">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Kehadiran</th>
                                <th>Keterangan</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.binaan.filter(binaan => binaan.status === 'Aktif').map((binaan, index) =>{
                                    let valueId = `mentee_presence-${index}`;
                                    return(
                                        <tr key={index}>
                                            <td>{index+1}</td>
                                            <td>{binaan.name}</td>
                                            <td>

                                                <div className="switch">
                                                    <label>
                                                        Tidak hadir
                                                        <input key={binaan.id} type="checkbox" name={valueId} data-id={index} id={valueId} className="mentee_presence" onChange={this.handleInputChange}/>
                                                        <span className="lever"/>
                                                        Hadir
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div className="row">
                                                    <div className="input-field col s12">
                                                        <input key={binaan.id} type="text" name={valueId} data-id={index} id={valueId} className="personal_details" onChange={this.handleInputChange}/>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </Row>
                </div>
                <div id='tidak_berjalan' style={{display: 'none'}}>
                    <Row>
                    <Select s={4} type='select' label="Pekan ke-*" name="week" defaultValue={this.state.week} onChange={this.handleChange} required>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </Select>
                    <Select s={4} type='select' label="Bulan*" name="month" defaultValue={this.state.month} onChange={this.handleChange} required>
                        <option value="Januari">Januari</option>
                        <option value="Februari">Februari</option>
                        <option value="Maret">Maret</option>
                        <option value="April">April</option>
                        <option value="Mei">Mei</option>
                        <option value="Juni">Juni</option>
                        <option value="Juli">Juli</option>
                        <option value="Agustus">Agustus</option>
                        <option value="September">September</option>
                        <option value="Oktober">Oktober</option>
                        <option value="November">November</option>
                        <option value="Desember">Desember</option>
                    </Select>
                    <TextInput s={4} type='text' label="Tahun*" name="year" value={this.state.year} onChange={this.handleChange} required/>
                </Row>
                </div>
                <Row>
                    <Row>
                        <Textarea
                            s={12}
                            m={12}
                            l={12}
                            xl={12}
                            name="qadhaya" label='Qadhaya Rawai' data-length="500" onChange={this.handleChange}
                        />
                    </Row>
                </Row>
                <Button waves='light' onClick={this.onSubmit}>Laporkan</Button>
            </div>
        )
    }
}

if(document.getElementById('lapor')) {
    const element = document.getElementById('lapor');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Lapor {...props} />, document.getElementById('lapor'));
}
