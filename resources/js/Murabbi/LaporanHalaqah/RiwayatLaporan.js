import React from 'react';
import ReactDOM from 'react-dom';
import {Icon, Row, Col, Button} from 'react-materialize';
import axios from 'axios';

export default class RiwayatLaporan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            year: '',
            month: '',
            pekan: [1,2,3,4,5],
            pekan_sekarang: '',
            presences: [],
            group_name: '',
            status: '',
        };
        console.log(super());
    };

    componentDidMount() {
        let currentWeekNumber = require('current-week-number');
        let groups_id = parseInt(window.location.pathname.split('/')[4]);
        let today = new Date();
        let month = today.getMonth() + 1;
        if(month < 10){
            month = '0' + month;
        }
        else{
            month = month.toString();
        }

        console.log(month);

        let tanggal = today.getDate();
        if(tanggal <= 10){
            tanggal = '0' + tanggal;
        }

        let cdate = today.getFullYear() + '-' + month + '-' + tanggal;

        let year2 = cdate.substr(0, 4);
        let week = currentWeekNumber(cdate);

        axios.get('/api/presence')
            .then(response => {
                this.setState({presences: response.data});
            });

        axios.get('/api/group/'+groups_id)
            .then(response => {
                this.setState({
                    group_name: response.data.group_name,
                    status: response.data.status
                });
            });

        let pekan = '';
        let bulan = '';

        if (month === '01'){
            pekan = week;
            bulan = month;
            if(pekan === 0){
                pekan = 1;
                bulan = '1'
            }
            else{
                pekan = week;
                bulan = month;
            }
        }
        else if (month === '02'){
            pekan = week-5;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '1'
            }
            else{
                pekan = week-5;
                bulan = month;
            }
        }
        else if (month === '03'){
            pekan = week-9;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '2'
            }
            else{
                pekan = week-9;
                bulan = month;
            }
        }
        else if (month === '04'){
            pekan = week-13;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '3'
            }
            else{
                pekan = week-13;
                bulan = month;
            }
        }
        else if (month === '05'){
            pekan = week-18;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '4'
            }
            else{
                pekan = week-18;
                bulan = month;
            }
        }
        else if (month === '06'){
            pekan = week-22;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '5'
            }
            else{
                pekan = week-22;
                bulan = month;
            }
        }
        else if (month === '07'){
            pekan = week-26;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '6'
            }
            else{
                pekan = week-26;
                bulan = month;
            }
        }
        else if (month === '08'){
            pekan = week-31;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '7'
            }
            else{
                pekan = week-31;
                bulan = month;
            }
        }
        else if (month === '09'){
            pekan = week-35;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '8'
            }
            else{
                pekan = week-35;
                bulan = month;
            }
        }
        else if (month === '10'){
            pekan = week-39;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '9'
            }
            else{
                pekan = week-39;
                bulan = month;
            }
        }
        else if(month === '11'){
            pekan = week-44;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '10';
            }
            else{
                pekan = week-44;
                bulan = month;
            }
        }
        else if(month === '12'){
            pekan = week-48;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '11';
            }
            else{
                pekan = week-48;
                bulan = month;
            }
        }

        console.log(bulan);

        this.setState({
            year: year2,
            month: parseInt(bulan),
            pekan_sekarang: pekan,
        })
    }

    prevMonth = () => {
        let bulan = this.state.month;
        if (bulan <= 1){
            this.setState({
                month : 12,
                year : this.state.year-1
            });
        }
        else{
            this.setState({
                month : this.state.month-1
            });
        }
    };

    nextMonth = () => {
        let bulan = this.state.month;
        if (bulan >= 12){
            this.setState({
                month : 1,
                year : this.state.year+1
            });
        }
        else{
            this.setState({
                month : this.state.month+1
            });
        }
    };

    cekKeberjalanan(pekan, bulan){
        pekan = pekan.toString();
        let groups_id = parseInt(window.location.pathname.split('/')[4]);
        let keberjalanan = this.state.presences.filter(presence => presence.week === pekan && presence.month === bulan && presence.groups_id === groups_id).map(presence => presence.done);

        return keberjalanan[0];
    }

    cekLaporan(pekan, bulan){
        pekan = pekan.toString();
        let groups_id = parseInt(window.location.pathname.split('/')[4]);
        let laporan = this.state.presences.filter(presence => presence.week === pekan && presence.month === bulan && presence.groups_id === groups_id).map(presence => presence.done);

        return laporan.length;
    }

    getIdLaporan(pekan, bulan){
        pekan = pekan.toString();
        let groups_id = parseInt(window.location.pathname.split('/')[4]);
        let laporan = this.state.presences.filter(presence => presence.week === pekan && presence.month === bulan && presence.groups_id === groups_id).map(presence => presence.presences_id);

        return laporan[0];
    }

    onDelete(presence_id){
        let result = confirm('Hapus laporan ini?');
        if(result === true) {
            axios.delete('/api/presence/hapus/' + presence_id)
                .then(response => location.reload())
                .catch();
        }
    }

    render() {
        let groups_id = parseInt(window.location.pathname.split('/')[4]);

        let months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }

        return (
            <div className="container">
                <h3>Riwayat Laporan</h3>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama Kelompok</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.group_name}</Row>
                <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Status</Row>
                <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.status}</Row>
                <table className="bordered striped">
                    <thead>
                        <tr>
                            <th colSpan='4' className='center aligned'><a onClick={this.prevMonth} style={{cursor: 'pointer'}}><Icon small>chevron_left</Icon></a>{monthNumToName(this.state.month)} {this.state.year}<a onClick={this.nextMonth} style={{cursor: 'pointer'}}><Icon small>chevron_right</Icon></a></th>
                        </tr>
                        <tr>
                            <th className='center aligned'>Pekan</th>
                            <th className='center aligned'>Laporan</th>
                            <th className='center aligned'>Keterlaksanaan</th>
                            <th className='center aligned'>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.pekan.map((pekan, index) => {
                            return (
                                <tr key={index}>
                                    <td className='center aligned'>{pekan}</td>
                                    <td className='center aligned'>
                                        {
                                            this.cekLaporan(pekan, monthNumToName(this.state.month)) !== 0 ?
                                                (<Icon small className='green-text'>check</Icon>)
                                                :
                                                null
                                        }
                                        </td>
                                    <td className='center aligned'>
                                        {
                                            this.cekKeberjalanan(pekan, monthNumToName(this.state.month)) === 'Ya' ?
                                                (<Icon small className='green-text'>check</Icon>)
                                                :
                                                this.cekKeberjalanan(pekan, monthNumToName(this.state.month)) === 'Tidak' ?
                                                (<Icon small className='red-text'>clear</Icon>)
                                                    :
                                                    null
                                        }
                                        </td>
                                    {
                                        this.state.status === 'Aktif' ? (
                                            <td className='center aligned'>
                                                {
                                                    this.cekLaporan(pekan, monthNumToName(this.state.month)) === 0 ?
                                                        (
                                                            <Button
                                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                                tooltip="Lapor"
                                                                floating
                                                                waves="light"
                                                                small
                                                                className="green"
                                                                icon={<Icon>assignment</Icon>}
                                                                onClick={()=>window.location='/user/laporan_halaqah/lapor/'+groups_id}
                                                            />
                                                        )
                                                        :
                                                        (
                                                            <div>
                                                                <Button
                                                                    style={{marginLeft: 2.5, marginRight: 2.5}}
                                                                    tooltip='Lihat'
                                                                    waves="light"
                                                                    small
                                                                    floating
                                                                    className="blue"
                                                                    onClick={()=>window.location='/user/laporan_halaqah/lihat/' + this.getIdLaporan(pekan, monthNumToName(this.state.month))}
                                                                    icon={<Icon>remove_red_eye</Icon>}
                                                                />
                                                                <Button
                                                                    style={{marginLeft: 2.5, marginRight: 2.5}}
                                                                    tooltip='Hapus'
                                                                    waves="light"
                                                                    small
                                                                    floating
                                                                    className="red"
                                                                    onClick={this.onDelete.bind(this, this.getIdLaporan(pekan, monthNumToName(this.state.month)))}
                                                                    icon={<Icon>delete</Icon>}
                                                                />
                                                            </div>
                                                        )
                                                }
                                            </td>
                                        )
                                            :
                                            (
                                                <td className='center aligned'>
                                                    {
                                                        this.cekLaporan(pekan, monthNumToName(this.state.month)) === 0 ?
                                                            (
                                                                <Button
                                                                    style={{marginLeft: 2.5, marginRight: 2.5}}
                                                                    tooltip="Lapor"
                                                                    floating
                                                                    waves="light"
                                                                    small
                                                                    className="grey"
                                                                    icon={<Icon>assignment</Icon>}
                                                                />
                                                            )
                                                            :
                                                            (
                                                                <div>
                                                                    <Button
                                                                        style={{marginLeft: 2.5, marginRight: 2.5}}
                                                                        tooltip='Lihat'
                                                                        waves="light"
                                                                        small
                                                                        floating
                                                                        className="blue"
                                                                        onClick={()=>window.location='/user/laporan_halaqah/lihat/' + this.getIdLaporan(pekan, monthNumToName(this.state.month))}
                                                                        icon={<Icon>remove_red_eye</Icon>}
                                                                    />
                                                                    <Button
                                                                        style={{marginLeft: 2.5, marginRight: 2.5}}
                                                                        tooltip='Hapus'
                                                                        waves="light"
                                                                        small
                                                                        floating
                                                                        className="red"
                                                                        onClick={this.onDelete.bind(this, this.getIdLaporan(pekan, monthNumToName(this.state.month)))}
                                                                        icon={<Icon>delete</Icon>}
                                                                    />
                                                                </div>
                                                            )
                                                    }
                                                </td>
                                            )
                                    }
                                </tr>
                            )
                        })
                    }

                    </tbody>
                </table>
    </div>
        )
    }
}

if(document.getElementById('riwayat_laporan')) {
    const element = document.getElementById('riwayat_laporan');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<RiwayatLaporan {...props} />, document.getElementById('riwayat_laporan'));
}
