import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Icon} from 'react-materialize'
import axios from 'axios'

export default class LaporanHalaqah extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            groups: [],
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;
        axios.get('/api/group/group_name/'+id)
            .then(response => {
                this.setState({groups: response.data});
            });
    }

    render() {
        return (
            <div className="container">
                <h3>Laporan Pembinaan</h3>
                Jumlah: {this.state.groups.length} kelompok
                <table className="bordered striped">
                    <thead>
                        <tr>
                            <th className='center aligned'>No.</th>
                            <th>Nama Kelompok</th>
                            <th className='center aligned'>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.groups.filter(group=>group.status === 'Aktif').map((group, index) => {
                            return (
                                <tr key={index}>
                                    <td className='center aligned'>{index+1}</td>
                                    <td>{group.group_name}</td>
                                    <td className='center aligned'>
                                        <Button
                                            style={{marginLeft: 2.5, marginRight: 2.5}}
                                            tooltip="Lapor"
                                            floating
                                            waves="light"
                                            small
                                            className="green"
                                            icon={<Icon>assignment</Icon>}
                                            onClick={()=>window.location='/user/laporan_halaqah/lapor/'+group.id}
                                        />
                                        <Button
                                            style={{marginLeft: 2.5, marginRight: 2.5}}
                                            tooltip="Riwayat"
                                            floating
                                            waves="light"
                                            small
                                            className="blue"
                                            icon={<Icon>history</Icon>}
                                            onClick={()=>window.location='/user/laporan_halaqah/riwayat/'+group.id}
                                        />
                                    </td>
                                </tr>
                            )
                        })
                    }
                    {
                        this.state.groups.filter(group=>group.status === 'Nonaktif').map((group, index) => {
                            return (
                                <tr key={index} style={{color: 'grey'}}>
                                    <td className='center aligned'>{index+1}</td>
                                    <td>{group.group_name}</td>
                                    <td className='center aligned'>
                                        <Button
                                            style={{marginLeft: 2.5, marginRight: 2.5}}
                                            tooltip="Lapor"
                                            floating
                                            waves="light"
                                            small
                                            className="grey"
                                            icon={<Icon>assignment</Icon>}
                                        />
                                        <Button
                                            style={{marginLeft: 2.5, marginRight: 2.5}}
                                            tooltip="Riwayat"
                                            floating
                                            waves="light"
                                            small
                                            className="blue"
                                            icon={<Icon>history</Icon>}
                                            onClick={()=>window.location='/user/laporan_halaqah/riwayat/'+group.id}
                                        />
                                    </td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>
            </div>
        )
    }
}

if(document.getElementById('laporan_halaqah')) {
    const element = document.getElementById('laporan_halaqah');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<LaporanHalaqah {...props} />, document.getElementById('laporan_halaqah'));
}
