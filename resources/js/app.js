require('./components/Register');
require('./Admin/DataMurabbi/DataMurabbi');
require('./Admin/DataMurabbi/DataMutarabbi');
require('./Admin/DataMurabbi/CapaianMadah');
require('./Admin/DataMurabbi/Qadhaya');
require('./Admin/DataMurabbi/QadhayaPersonal');
require('./Admin/DataMurabbi/MadahPersonal');
require('./Admin/DataMurabbi/Madah');
require('./Admin/DataMurabbi/components/Add');
require('./Admin/DataMurabbi/components/Edit');
require('./Admin/DataMurabbi/components/Transfer');

require('./Admin/RekapLaporan/RekapLaporan');
require('./Admin/Pemantauan/Pemantauan');
require('./Admin/PusatMadah/PusatMadahAdmin');

require('./Admin/Pengaturan/ListPengaturan');
require('./Admin/Pengaturan/ListFakultas');
require('./Admin/Pengaturan/ListAngkatan');
require('./Admin/Pengaturan/ListJenjang');
require('./Admin/Pengaturan/ListSarana');
require('./Admin/Pengaturan/ListProdi');

require('./Admin/FamilyTree');

require('./Murabbi/DataKelompok/DataKelompok');
require('./Murabbi/DataKelompok/components/Add');
require('./Murabbi/DataKelompok/components/Edit');

require('./Murabbi/LaporanHalaqah/LaporanHalaqah');
require('./Murabbi/LaporanHalaqah/RiwayatLaporan');
require('./Murabbi/LaporanHalaqah/components/Lapor');
require('./Murabbi/LaporanHalaqah/components/Lihat');

require('./Murabbi/RekapPresensi/RekapPresensi');
require('./Murabbi/RekapPresensi/components/Rekap');
require('./Murabbi/PusatMadah/PusatMadah');

require('./Mutarabbi/mutabaah/Mutabaah');
require('./Mutarabbi/profile/Profile');
require('./Mutarabbi/group/Group');
require('./Mutarabbi/mutabaah/components/Create');
require('./Mutarabbi/mutabaah/components/Manage');
require('./Mutarabbi/mutabaah/components/Edit');

