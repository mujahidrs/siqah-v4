import React from 'react'
import ReactDOM from 'react-dom'
// import {
//     Container,
//     Dropdown,
//     Grid,
//     Header,
//     Image,
//     List,
//     Menu,
//     Segment,
//     Icon,
//     Button,
//     Form,
//     Table
// } from 'semantic-ui-react'
import axios from 'axios'
import BarChartComponent from "../../components/Chart";

export default class Group extends React.Component {
    constructor(props){
        super(props);

        this.state={
            activities: [],
            evaluations: [],
            categories: [],
            employees : [],
            employee_data: [],
            groups: [],
            group_name: '',
            mentors_id: '',
            groups_id: '',
            month: '',
            year: '',
        };

        console.log(super());
    }

    componentDidMount(){
        let id = this.props.id;
        axios.get('/api/evaluation')
            .then(response=>{
                this.setState({evaluations:response.data});
            });

        axios.get('/api/categories')
            .then(response=>{
                this.setState({categories:response.data});
            });

        axios.get('/api/employee')
            .then(response=>{
               this.setState({employees: response.data});
            });

        axios.get('/api/groups/employee/'+id)
            .then(response=>{
                this.setState({employee_data: response.data});
            });

        axios.get('/api/activity')
            .then(response=>{
                this.setState({activities:response.data});
            });

        let mentors_id = this.state.mentors_id

        axios.get('/api/groups/'+mentors_id)
            .then(response=>{
                this.setState({ groups : response.data });
            });

        var date = require('locutus/php/datetime/date');

        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        this.setState({
            month: parseInt(date('m')),
            year: parseInt(date('Y')),
        });
    }


    getName(id){
        const mentee = this.state.employees.filter(employees => employees.id == id).map((employees) => {
            if(employees.id === id){
                return employees.name;
            }
        })
        return mentee;
    }

    getArrayTotal(activities, employees_id){
        let array = [];
        activities.map(activity=>{
            array = [this.getTotalPencapaian(activities, 1, employees_id), this.getTotalPencapaian(activities, 2, employees_id), this.getTotalPencapaian(activities, 3, employees_id), this.getTotalPencapaian(activities, 4, employees_id), this.getTotalPencapaian(activities, 5, employees_id)]
        })
        this.state.total = array
        return array
    }

    getArrayPoin(groups_id){
        let array = this.state.groups.filter(groups=>groups.groups_id == groups_id).map((groups, index) => this.getPoin(this.state.activities, groups.employees_id))
        return array;
    }

    getArrayLabel(groups_id){
        let array = this.state.groups.filter(groups=>groups.groups_id == groups_id).map((groups, index) => this.getName(groups.employees_id))
        return array;
    }

    onDelete(employees_id){
        axios.delete('/api/activity/delete/'+employees_id)
            .then(response=>{
                var activities = this.state.activities;

                for(var i=0; i< activities.length; i++)
                {
                    if(activities[i].id==employees_id)
                    {
                        activities.splice(i,1);
                        this.setState({activities:activities})
                    }
                }
            });
    }

    prevMonth = () => {
        var bulan = this.state.month;
        if (bulan <= 1){
            this.setState({
                month : 12,
                year : this.state.year-1
            });
        }
        else{
            this.setState({
                month : this.state.month-1
            });
        }
    }

    nextMonth = () => {
        var bulan = this.state.month;
        if (bulan >= 12){
            this.setState({
                month : 1,
                year : this.state.year+1
            });
        }
        else{
            this.setState({
                month : this.state.month+1
            });
        }
    }

    getPencapaian(id, week, employees_id){
        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }
        let value = this.state.evaluations.filter(evaluations=>evaluations.activities_id == id && evaluations.employees_id == employees_id && parseInt(evaluations.week) == week && evaluations.date.substr(5, 2) == this.state.month && evaluations.date.substr(0, 4) == this.state.year)
            .map(evaluations=>evaluations.value);
        value = value[0];
        const categories_id =  this.state.activities.filter(activities=>activities.id == id).map(activities=>activities.categories_id);
        // console.log(categories_id[0]);
        const weight = this.state.categories.filter(categories=>categories.id == categories_id[0]).map(categories=>categories.weight);
        const bobot_kategori = weight[0];
        const jumlah_aktivitas = this.state.activities.length;
        const bobot_aktivitas = bobot_kategori / jumlah_aktivitas;
        let target = this.state.activities.filter(activities=>activities.id == id).map(activities=>activities.target);
        target = target[0];
        const nilai = value / target * bobot_aktivitas;
        const pencapaian = this.state.evaluations.filter(evaluations=>evaluations.activities_id == id && evaluations.employees_id == employees_id && parseInt(evaluations.week) == week && evaluations.date.substr(5, 2) == this.state.month && evaluations.date.substr(0, 4) == this.state.year)
            .map(evaluations=>evaluations.value).reduce((a, b)=>{return (a + b);}, 0);
        console.log(value);
        return pencapaian;
    }

    getTotalPencapaian(activities, week, employees_id){
        let total = 0;
        activities.map(activity=>{
            total = total + parseInt(this.getPencapaian(activity.id, week, employees_id))
        })
        return total
    }

    getPoin(activities, employees_id){
        let poin = 0;
        poin = this.getTotalPencapaian(activities, 1, employees_id) + this.getTotalPencapaian(activities, 2, employees_id) + this.getTotalPencapaian(activities, 3, employees_id) + this.getTotalPencapaian(activities, 4, employees_id) + this.getTotalPencapaian(activities, 5, employees_id)
        return poin;
    }

    render(){
        {
            this.state.employee_data.map(employee => {
                return(this.state.mentors_id= employee.mentors_id)
            })
            this.state.employee_data.map(employee => {
                return(this.state.groups_id= employee.groups_id)
            })
            this.state.employee_data.map(employee => {
                return(this.state.group_name= employee.group_name)
            })
        }

        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        return (
            <div>
                <style>{`
        #user-profile{
            height: 100%;
            }
        #evaluasi{
            text-align: center;
        }

            @media only screen and (max-width: 1279px) {

            #small-menu {
            display: table;
            }

    }

    @media only screen and (max-device-width: 500px) {
            #small-menu {
            display: block;
            overflow-x: auto;
            font-size: 0.75em;
            }

        }
        `}
                </style>
                <Container text style={{ marginTop: '7em' }} >
                    <Header as='h1' id='evaluasi'>Kelompok</Header>
                    <Segment raised>
                        <Form size='large' id='evaluasi'>
                            <BarChartComponent
                                judul='Pencapaian'
                                label='Pencapaian'
                                data={this.getArrayPoin(this.state.groups_id)}
                                labels={this.getArrayLabel(this.state.groups_id)}
                                height={window.innerWidth < 500 ? 250 : null}
                            />
                            <label>Nama Pembina: {this.getName(this.state.mentors_id)}</label><br/>
                            <label>Nama Kelompok: {this.state.group_name}</label>
                            <Table celled definition unstackable id='small-menu'>
                                <Table.Header fullWidth as='thead' className='center aligned'>
                                    <Table.Row>
                                        <Table.HeaderCell rowSpan='2'>No.</Table.HeaderCell>
                                        <Table.HeaderCell rowSpan='2'>Anggota Kelompok</Table.HeaderCell>
                                        <Table.HeaderCell colSpan='5'><Icon name='angle left' size='large' onClick={this.prevMonth}/>{monthNumToName(this.state.month)} {this.state.year}<Icon name='angle right' size='large' onClick={this.nextMonth}/></Table.HeaderCell>
                                        <Table.HeaderCell rowSpan='2'>Poin</Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.HeaderCell>1</Table.HeaderCell>
                                        <Table.HeaderCell>2</Table.HeaderCell>
                                        <Table.HeaderCell>3</Table.HeaderCell>
                                        <Table.HeaderCell>4</Table.HeaderCell>
                                        <Table.HeaderCell>5</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>
                                    {
                                        this.state.groups.filter(groups=>groups.groups_id == this.state.groups_id).map((groups, index) => {
                                            return (
                                                <Table.Row key={groups.id}>
                                                    <Table.Cell className='center aligned'>{index+1}</Table.Cell>
                                                    <Table.Cell>{this.getName(groups.employees_id)}</Table.Cell>
                                                    <Table.Cell>{this.getTotalPencapaian(this.state.activities, 1, groups.employees_id)}</Table.Cell>
                                                    <Table.Cell>{this.getTotalPencapaian(this.state.activities, 2, groups.employees_id)}</Table.Cell>
                                                    <Table.Cell>{this.getTotalPencapaian(this.state.activities, 3, groups.employees_id)}</Table.Cell>
                                                    <Table.Cell>{this.getTotalPencapaian(this.state.activities, 4, groups.employees_id)}</Table.Cell>
                                                    <Table.Cell>{this.getTotalPencapaian(this.state.activities, 5, groups.employees_id)}</Table.Cell>
                                                    <Table.Cell>{this.getPoin(this.state.activities, groups.employees_id)}</Table.Cell>
                                                </Table.Row>
                                            )
                                        })
                                    }
                                </Table.Body>

                            </Table>
                        </Form>
                    </Segment>
                </Container>
            </div>
        )
    }
}

if(document.getElementById('group')) {
    const element = document.getElementById('group');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Group {...props} />, document.getElementById('group'));
}
