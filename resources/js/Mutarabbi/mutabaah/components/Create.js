import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Col, Button, Icon} from 'react-materialize'
import axios from 'axios'

export default class Create extends React.Component {
    constructor(props) {
        super(props);
        this.onChangeActivity = this.onChangeActivity.bind(this);
        this.onChangeRating = this.onChangeRating.bind(this);
        this.onChangeTarget = this.onChangeTarget.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            categories: [],
            activity: '',
            categories_id: '',
            measure: '',
            target: '',
            unit: '',
            period: '',
            users_id: this.props.id,
            errors: [],
        };
        console.log(super());
    };

    componentDidMount(){
        axios.get('/api/categories')
            .then(response=>{
                this.setState({ categories : response.data });
            });
    }

    onChangeActivity(e){
        this.setState({
            activity: e.target.value
        })
    }

    onChangeRating = e => this.setState({ weight: e.target.value })

    onChangeTarget(e){
        this.setState({
            target: e.target.value
        })
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
        console.log(event.target.name + ': ' + event.target.value)
    };

    validate(activity, category, measure, target, unit, period){
        const errors = [];

        if(activity.length === 0){
            errors.push('Aktivitas tidak boleh kosong');
        }

        if(category.length === 0){
            errors.push('Kategori tidak boleh kosong');
        }

        if(measure.length === 0){
            errors.push('Ukuran tidak boleh kosong');
        }

        if(target.length === 0){
            errors.push('Target tidak boleh kosong');
        }

        if(unit.length === 0){
            errors.push('Unit tidak boleh kosong');
        }

        if(period.length === 0){
            errors.push('Periode tidak boleh kosong');
        }

        return errors;
    }

    onSubmit(e){
        e.preventDefault();

        const errors = this.validate(this.state.activity, this.state.categories_id, this.state.measure, this.state.target, this.state.unit, this.state.period);
        if (errors.length > 0) {
            this.setState({ errors });
            return;
        }
        else {
            const activity = {
                activity: this.state.activity,
                measure: this.state.measure,
                categories_id: this.state.categories_id,
                target: this.state.target,
                unit: this.state.unit,
                period: this.state.period,
                users_id: this.state.users_id
            }

            axios.post('/api/activity/store', activity)
                .then(response => window.location = 'manage')
                .catch();
        }
    }

    render(){
        return (
            <div className='container'>
                <h3>Create Activity</h3>
                <form method="post">
                    <Row>
                        <input type='hidden' name='users_id' value={this.state.users_id}/>
                        <TextInput type="text" s={12} label="Aktivitas" name='activity' placeholder="Aktivitas" onChange={this.handleChange} value={this.state.activity}/>
                        <Select s={12} type='select' label="Kategori" name="categories_id" onChange={this.handleChange} value={this.state.categories_id}>
                            {this.state.categories.map((categories, index)=>{
                                return(
                                    <option key={index} value={categories.id}>{categories.category}</option>
                                )
                            })
                            }
                        </Select>
                        <Select s={12} type='select' label="Ukuran" name="measure" onChange={this.handleChange} value={this.state.measure}>
                            <option value="Jumlah">Jumlah</option>
                            <option value="Keterlaksanaan">Keterlaksanaan</option>
                        </Select>
                        <TextInput type="text" s={6} label="Target" name='target' placeholder="Angka" onChange={this.handleChange} value={this.state.target}/>
                        <Select s={6} label="Unit" name="unit" onChange={this.handleChange} value={this.state.unit} required>
                            <option value="kali">kali</option>
                            <option value="rakaat">rakaat</option>
                            <option value="halaman">halaman</option>
                            <option value="lembar">lembar</option>
                            <option value="ayat">ayat</option>
                            <option value="surat">surat</option>
                            <option value="menit">menit</option>
                            <option value="rupiah">rupiah</option>
                            <option value="senyuman">senyuman</option>
                            <option value="ain">ain</option>
                        </Select>
                        <Select s={12} type='select' label="Periode" name="period" onChange={this.handleChange} value={this.state.period}>
                            <option value="Hari">Hari</option>
                            <option value="Pekan">Pekan</option>
                        </Select>
                    </Row>
                </form>
                <Button waves='light' onClick={this.onSubmit}>Submit</Button>
            </div>
        )
    }
}

if(document.getElementById('create')) {
    const element = document.getElementById('create');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Create {...props} />, document.getElementById('create'));
}
