import React from 'react'
import ReactDOM from 'react-dom';
// import {
//     Container,
//     Header,
//     Segment,
//     Button,
//     Form,
//     Select,
//     Input,
//     Message
// } from 'semantic-ui-react'
import axios from 'axios'

export default class Edit extends React.Component {
    constructor(props) {
        super(props);
        this.onChangeActivity = this.onChangeActivity.bind(this);
        this.onChangeRating = this.onChangeRating.bind(this);
        this.onChangeTarget = this.onChangeTarget.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            activity: '',
            categories_id: '',
            measure: '',
            target: '',
            unit: '',
            period: '',
            weight: 1,
            employees_id: this.props.id,
            activities_id: '',
            categories: [],
            errors: [],
        };
        console.log(super());
    };

    onChangeActivity(e){
        this.setState({
            activity: e.target.value
        })
    }

    onChangeRating = e => this.setState({ weight: e.target.value })

    onChangeTarget(e){
        this.setState({
            target: e.target.value
        })
    }

    handleChange = (event, {name, value}) => {
        if (this.state.hasOwnProperty(name)) {
            this.setState({ [name]: value });
        }
    };

    componentDidMount(){
        let id = window.location.pathname.split('/')[2];
        axios.get('/api/categories')
            .then(response=>{
                this.setState({ categories : response.data });
            });

        axios.get('/api/activity/'+id)
            .then(response=>{
                this.setState({
                    activity : response.data.activity,
                    weight : response.data.weight,
                    categories_id : response.data.categories_id,
                    measure : response.data.measure,
                    target : response.data.target,
                    unit : response.data.unit,
                    period : response.data.period,
                    employees_id: response.data.employees_id
                });
            });
        this.setState({
            activities_id : id
        })
    }

    validate(activity, category, measure, target, unit, period){
        const errors = [];

        if(activity.length === 0){
            errors.push('Aktivitas tidak boleh kosong');
        }

        if(category.length === 0){
            errors.push('Kategori tidak boleh kosong');
        }

        if(measure.length === 0){
            errors.push('Ukuran tidak boleh kosong');
        }

        if(target.length === 0){
            errors.push('Target tidak boleh kosong');
        }

        if(unit.length === 0){
            errors.push('Unit tidak boleh kosong');
        }

        if(period.length === 0){
            errors.push('Periode tidak boleh kosong');
        }

        return errors;
    }

    onSubmit(e){
        e.preventDefault();

        const errors = this.validate(this.state.activity, this.state.categories_id, this.state.measure, this.state.target, this.state.unit, this.state.period);
        if (errors.length > 0) {
            this.setState({ errors });
            return;
        }
        else {
            const activity = {
                activity: this.state.activity,
                weight: this.state.weight,
                categories_id: this.state.categories_id,
                measure: this.state.measure,
                target: this.state.target,
                unit: this.state.unit,
                period: this.state.period,
                employees_id: this.state.employees_id
            }

            console.log(activity);

            axios.post('/api/activity/edit/' + this.state.activities_id, activity)
                .then(response => window.location = '/manage')
                .catch();
        }
    }

    render(){
        const pilihan = [
            {
                text: 'Jumlah',
                value: 'Jumlah',
            },
            {
                text: 'Keterlaksanaan',
                value: 'Keterlaksanaan',
            }
        ]

        const pilihan2 = [
            {
                text: 'Hari',
                value: 'Hari',
            },
            {
                text: 'Pekan',
                value: 'Pekan',
            }
        ]

        const pilihan3 = [
            {
                text: 'kali',
                value: 'kali',
            },
            {
                text: 'rakaat',
                value: 'rakaat',
            },
            {
                text: 'halaman',
                value: 'halaman',
            },
            {
                text: 'lembar',
                value: 'lembar',
            },
            {
                text: 'ayat',
                value: 'ayat',
            },
            {
                text: 'surat',
                value: 'surat',
            },
            {
                text: 'menit',
                value: 'menit',
            },
            {
                text: 'rupiah',
                value: 'rupiah',
            },
            {
                text: 'senyuman',
                value: 'senyuman',
            },
            {
                text: 'ain',
                value: 'ain',
            }
        ]

        const pilihan4 =
            this.state.categories.map(categories=>({text: categories.category, value: categories.id}))

        return (
            <div>
                <style>{`
        #user-profile{
            height: 100%;
            }
        #evaluasi{
            text-align: center;
        }
        `}
                </style>
                <Container text style={{ marginTop: '7em' }} >
                    <Header as='h1' id='evaluasi'>Edit Activity</Header>
                    <Segment raised>
                        <Form size='large' onSubmit={this.onSubmit} method='POST'>
                            <Form.Field>
                                <label>Aktivitas</label>
                                <input type='hidden' name='employees_id' value={this.state.employees_id}/>
                                <TextInput
                                    placeholder='Aktivitas'
                                    name='activity'
                                    value={this.state.activity}
                                    onChange={this.handleChange}/>
                            </Form.Field>
                            <Form.Field>
                                <label>Kategori</label>
                                <Select placeholder='Kategori' options={pilihan4} value={this.state.categories_id} onChange={this.handleChange} name='categories_id'/>
                            </Form.Field>
                            <Form.Field>
                                <label>Ukuran</label>
                                <Select placeholder='Ukuran' options={pilihan} value={this.state.measure} onChange={this.handleChange} name='measure'/>
                            </Form.Field>
                            <Form.Field>
                                <label>Target</label>
                                <Form.Group widths='equal'>
                                    <Form.Field>
                                        <TextInput placeholder='Angka' name='target' value={this.state.target} onChange={this.handleChange}/>
                                    </Form.Field>
                                    <Form.Field>
                                        <Select placeholder='Unit' options={pilihan3} value={this.state.unit} onChange={this.handleChange} name='unit'/>
                                    </Form.Field>
                                </Form.Group>
                            </Form.Field>
                            <Form.Field>
                                <label>Periode</label>
                                <Select placeholder='Periode' options={pilihan2} value={this.state.period} onChange={this.handleChange} name='period'/>
                            </Form.Field>
                            <Segment textAlign='center' basic>
                                <Button type='submit' positive>Submit</Button>
                            </Segment>
                            {this.state.errors != '' ?
                                <Message negative>
                                    <Message.Header>Error:</Message.Header>
                                    {this.state.errors.map(error => (
                                        <p key={error}>{error}</p>
                                    ))}
                                </Message>
                                :
                                null
                            }
                        </Form>
                    </Segment>
                </Container>
            </div>
        )
    }
}

if(document.getElementById('edit')) {
    const element = document.getElementById('edit');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Edit {...props} />, document.getElementById('edit'));
}
