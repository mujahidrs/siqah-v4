import React from 'react'
import ReactDOM from 'react-dom'
import { If, Then, ElseIf, Else } from 'react-if-elseif-else-render';
import {Row, TextInput, Select,  Col, Button, Icon} from 'react-materialize'
import axios from 'axios';

export default class Manage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activities: [],
            open: false
        };

        console.log(super());
    };

    handleChange = (event, {name, value}) => {
        if (this.state.hasOwnProperty(name)) {
            this.setState({ [name]: value });
        }
    };

    componentDidMount() {
        let id = this.props.id;
        axios.get('/api/activity/employees/'+id)
            .then(response=>{
                this.setState({activities:response.data});
            });
    }

    onDelete(activity_id){
        axios.delete('/api/activity/delete/'+activity_id)
            .then(response=>{
                var activities = this.state.activities;

                for(var i=0; i< activities.length; i++)
                {
                    if(activities[i].id==activity_id)
                    {
                        activities.splice(i,1);
                        this.setState({activities:activities})
                    }
                }
            });
        this.setState({
            open: false,
        })
    }

    close = () => this.setState({ open: false })
    open = () => this.setState({ open: true })

    render(){

        return (
            <div className='container'>
                <h3>Kelola Aktivitas</h3>
                            <table className="bordered striped">
                                <thead>
                                    <tr>
                                        <th className='center aligned'>No.</th>
                                        <th className='center aligned'>Kegiatan</th>
                                        <th className='center aligned'>Target</th>
                                        <th className='center aligned'>Tindakan</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    {
                                        this.state.activities.map((activities, index) => {
                                            let actId = `activities_id-${index}`, valueId = `value-${index}`;
                                            return (

                                                <tr key={activities.id}>
                                                    <td className='center aligned'>{index+1}</td>
                                                    <td>{activities.activity}</td>
                                                    <td>{activities.target} {activities.unit} / {activities.period}</td>
                                                    <td className='center aligned'>
                                                        <a
                                                            title="Edit"
                                                            className="waves-effect waves-light btn-small"
                                                            href={`/mutarabbi/edit/${activities.id}`}>
                                                            <i className="material-icons">
                                                                edit
                                                            </i>
                                                        </a>
                                                        <a
                                                            title="Hapus"
                                                            className="waves-effect waves-light btn-small"
                                                            onClick={this.onDelete.bind(this,activities.id)}>
                                                            <i className="material-icons">
                                                                delete
                                                            </i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        }
                                        )
                                    }

                                </tbody>

                            </table>
<br/>
                <a href='create' className="btn-floating btn-large waves-effect waves-light red"><i className="material-icons">add</i></a><label> Tambah Aktivitas</label>
            </div>
        )
    }
}

if(document.getElementById('manage')) {
    const element = document.getElementById('manage');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Manage {...props} />, document.getElementById('manage'));
}
