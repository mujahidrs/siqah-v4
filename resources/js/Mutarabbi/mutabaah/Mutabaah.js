import React from 'react'
import ReactDOM from 'react-dom'
import { If, Then, ElseIf, Else } from 'react-if-elseif-else-render';
import {Row, TextInput, Select,  Col, Button, Icon} from 'react-materialize'
import axios from 'axios';

export default class Mutabaah extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);



        this.state = {
            tanggal: '',
            date_asli: '',
            activities: [],
            evaluation: [],
            week: '',
        };

        console.log(super());
    };

    componentDidMount() {
        var date = require('locutus/php/datetime/date');
        let groups_id = window.location.pathname.split('/')[4];
        let today = new Date();
        let tanggal = today.getDate();
        if(tanggal < 10){
            tanggal = '0' + tanggal;
        }
        let getMonth = today.getMonth() + 1;
        if(getMonth < 10){
            getMonth = '0' + getMonth;
        }
        let cdate = today.getFullYear() + '-' + getMonth + '-' + tanggal;
        let jam_berakhir = date('H') + ':00';
        let jam_mulai = date('H') - 2 + ':00';


        let id = this.props.id;
            axios.get('/api/activity')
                .then(response=>{
                    this.setState({activities:response.data});
                });

            axios.get('/api/evaluation/employees/'+id)
                .then(response=>{
                    this.setState({evaluation:response.data});
                });
        var strtotime = require('locutus/php/datetime/strtotime');

        var currentWeekNumber = require('current-week-number');

        let months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }

        let dates = cdate.substr(8, 2);
        let month = cdate.substr(5, 2);
        let year = cdate.substr(0, 4);
        // let newdate = '2018-10-1';
        let week = currentWeekNumber(cdate);
        let a = strtotime(cdate);
        let hari = date('l', a);
        let bln = date('F', a);

        let pekan = '';
        let bulan = '';

        if (month === '01'){
            pekan = week;
            bulan = month;
            if(pekan === 0){
                pekan = 1;
                bulan = '01'
            }
            else{
                pekan = week;
                bulan = month;
            }
        }
        else if (month === '02'){
            pekan = week-5;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '01'
            }
            else{
                pekan = week-5;
                bulan = month;
            }
        }
        else if (month === '03'){
            pekan = week-9;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '02'
            }
            else{
                pekan = week-9;
                bulan = month;
            }
        }
        else if (month === '04'){
            pekan = week-13;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '03'
            }
            else{
                pekan = week-13;
                bulan = month;
            }
        }
        else if (month === '05'){
            pekan = week-18;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '04'
            }
            else{
                pekan = week-18;
                bulan = month;
            }
        }
        else if (month === '06'){
            pekan = week-22;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '05'
            }
            else{
                pekan = week-22;
                bulan = month;
            }
        }
        else if (month === '07'){
            pekan = week-26;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '06'
            }
            else{
                pekan = week-26;
                bulan = month;
            }
        }
        else if (month === '08'){
            pekan = week-31;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '07'
            }
            else{
                pekan = week-31;
                bulan = month;
            }
        }
        else if (month === '09'){
            pekan = week-35;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '08'
            }
            else{
                pekan = week-35;
                bulan = month;
            }
        }
        else if (month === '10'){
            pekan = week-39;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '09'
            }
            else{
                pekan = week-39;
                bulan = month;
            }
        }
        else if(month === '11'){
            pekan = week-44;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '10';
            }
            else{
                pekan = week-44;
                bulan = month;
            }
        }
        else if(month === '12'){
            pekan = week-48;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '11';
            }
            else{
                pekan = week-48;
                bulan = month;
            }
        }

        this.setState({
            year: year,
            week: pekan,
            month: monthNumToName(bulan),
            tanggal: cdate,
            date_asli: cdate,
            waktu_berakhir: jam_berakhir,
            waktu_mulai: jam_mulai,
        })
    }

    handleInput = (e) => {
        if (["value"].includes(e.target.className) ) {
            let activities = [...this.state.activities]
            activities[e.target.dataset.id][e.target.className] = e.target.type === 'checkbox' ? e.target.checked === true ? 1 : 0 : e.target.value;
            this.setState({ activities }, () => console.log(this.state.activities))
        } else {
            this.setState({ [e.target.name]: e.target.value })
        }
    }

    handleChange = (event, {name, value}) => {
        if (this.state.hasOwnProperty(name)) {
            this.setState({ [name]: value });
        }
    };

    handleDate = (event) => {
        var strtotime = require('locutus/php/datetime/strtotime');
        var date = require('locutus/php/datetime/date');

        var currentWeekNumber = require('current-week-number');


        let tanggal = event.target.value;
        let dates = tanggal.substr(8, 2);
        let month = tanggal.substr(5, 2);
        let year = tanggal.substr(0, 4);
        let tanggal_asli = this.state.date_asli;
        let dates_asli = tanggal_asli.substr(8, 2);
        let month_asli = tanggal_asli.substr(5, 2);
        let year_asli = tanggal_asli.substr(0, 4);
        let newdate = year+'-'+month+'-'+dates;
        let newdate_asli = year_asli+'-'+month_asli+'-'+dates_asli;
        let week = currentWeekNumber(newdate);
        let week_asli = currentWeekNumber(newdate_asli);
        let a = strtotime(newdate);
        let b = strtotime(newdate_asli);
        let hari = date('l', a);
        let bln = date('F', a);

        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        //
        // console.log(hari);
        // console.log(bln);

        if (month === '01'){
            // console.log(month);
            // console.log(week);
            var pekan = week;
            var bulan = month;
            if(pekan === 0){
                pekan = 1;
                bulan = '01'
            }
            else{
                pekan = week;
                bulan = month;
            }
        }
        else if (month === '02'){
            var pekan = week-5;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '01'
            }
            else{
                pekan = week-5;
                bulan = month;
            }
        }
        else if (month === '03'){
            var pekan = week-9;
            var bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '02'
            }
            else{
                pekan = week-9;
                bulan = month;
            }
        }
        else if (month === '04'){
            var pekan = week-13;
            var bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '03'
            }
            else{
                pekan = week-13;
                bulan = month;
            }
        }
        else if (month === '05'){
            var pekan = week-18;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '04'
            }
            else{
                pekan = week-18;
                bulan = month;
            }
        }
        else if (month === '06'){
            var pekan = week-22;
            var bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '05'
            }
            else{
                pekan = week-22;
                bulan = month;
            }
        }
        else if (month === '07'){
            var pekan = week-26;
            var bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '06'
            }
            else{
                pekan = week-26;
                bulan = month;
            }
        }
        else if (month === '08'){
            var pekan = week-31;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '07'
            }
            else{
                pekan = week-31;
                bulan = month;
            }
        }
        else if (month === '09'){
            var pekan = week-35;
            var bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '08'
            }
            else{
                pekan = week-35;
                bulan = month;
            }
        }
        else if (month === '10'){
            var pekan = week-39;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '09'
            }
            else{
                pekan = week-39;
                bulan = month;
            }
        }
        else if(month === '11'){
            var pekan = week-44;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '10';
            }
            else{
                pekan = week-44;
                bulan = month;
            }
        }
        else if(month === '12'){
            var pekan = week-48;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '11';
            }
            else{
                pekan = week-48;
                bulan = month;
            }
        }

        // console.log(pekan);
        // console.log(bulan);

        hari === 'Thu' ? hari = 'Kamis' : 0;

        if(week > week_asli){
            window.alert('Pekan lebih dari');
            tanggal = '';
            pekan = '';
            bulan = '';
        }
        if(year > this.state.year){
            window.alert('Tahun lebih dari');
            tanggal = '';
            pekan = '';
            bulan = '';
        }

        // console.log(year);
        // console.log(monthNumToName(month));
        // console.log(week);

        this.setState({
            tanggal: tanggal,
            week: pekan,
            month: monthNumToName(bulan)
        });
    };

    onDelete(evaluation_id){
        axios.delete('/api/evaluation/delete/'+evaluation_id)
            .then(response=>{
                var evaluation = this.state.evaluation;

                for(var i=0; i< evaluation.length; i++)
                {
                    if(evaluation[i].id==evaluation_id)
                    {
                        evaluation.splice(i,1);
                        this.setState({evaluation:evaluation})
                    }
                }
            });
    }

    onSubmit = (e) => {
        e.preventDefault();
        const evaluation = [...this.state.activities].map(activity=>{
            return{
                users_id : this.props.id,
                activities_id:activity.id,
                value:activity.value,
                date: this.state.tanggal,
                week: this.state.week
            }
        });

        console.log(evaluation);

        axios.post('/api/evaluation/store', {evaluation})
            .then(response=>window.location = 'profile')
                .catch();
    }

    cekLaporan(activities_id, date){
        let cek = this.state.evaluation.filter(evaluation=>evaluation.activities_id == activities_id && evaluation.date == date)
            .map(evaluation=>evaluation.value)
        console.log(date);
        return cek.length;
    }

    cekValue(activities_id, date){
        let cek = this.state.evaluation.filter(evaluation=>evaluation.activities_id == activities_id && evaluation.date == date)
            .map(evaluation=>evaluation.value)
        console.log(cek)
        return cek;
    }

    getEvaluationId(activities_id, date){
        let cek = this.state.evaluation.filter(evaluation=>evaluation.activities_id == activities_id && evaluation.date == date)
            .map(evaluation=>evaluation.id)
        return cek[0];
    }

    render(){
        return (
            <div className='container'>
                    <h3>Mutabaah Yaumiyah</h3>
                            <div className="input-field col s12">
                                <input type="date" name="tanggal" value={this.state.tanggal} onChange={this.handleDate} required/>
                                <label className="active">Tanggal</label>
                            </div>
                            Pekan ke-{this.state.week} {this.state.month}
                            <table className="bordered striped">
                                <thead>
                                    <tr>
                                        <th className='center aligned'>No.</th>
                                        <th className='center aligned'>Kegiatan</th>
                                        <th className='center aligned'>Target</th>
                                        <th className='center aligned'>Terlaksana</th>
                                        <th className='center aligned'>Tindakan</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    {
                                        this.state.activities.filter(activities=>activities.users_id == this.props.id || activities.users_id == '0').map((activities, index) => {
                                            let actId = `activities_id-${index}`, valueId = `value-${index}`;
                                            return (

                                                <tr key={activities.id}>
                                                    <td className='center aligned'>{index+1}</td>
                                                    <td>{activities.users_id == 0 ? activities.activity + '*' : activities.activity}</td>
                                                    <td>{activities.target} {activities.unit} / {activities.period}</td>
                                                    <td className='center aligned'>
                                                        {this.cekLaporan(activities.id, this.state.tanggal) == 0 ?
                                                            <If condition={activities.measure === 'Jumlah'}>
                                                                <Then>
                                                                    <TextInput type='text' label="Jumlah" key={activities.activity} placeholder={activities.unit} name={valueId} data-id={index} id={valueId} className='value' onChange={this.handleInput}/>
                                                                    {/*<input type='text' key={activities.activity} placeholder={activities.unit} name={valueId} data-id={index} id={valueId} className='value'/>*/}
                                                                </Then>
                                                                <ElseIf condition={activities.measure === 'Keterlaksanaan'}>
                                                                    {/*<input type='checkbox' key={activities.activity} name={valueId} data-id={index} id={valueId} className='value'/>*/}
                                                                    <div className="switch">
                                                                        <label>
                                                                            <input type="checkbox" key={activities.activity} name={valueId} data-id={index} id={valueId} className='value' onChange={this.handleInput}/>
                                                                            <span className="lever"/>
                                                                            Terlaksana
                                                                        </label>
                                                                    </div>
                                                                </ElseIf>
                                                            </If>
                                                            : null
                                                        }
                                                    </td>
                                                    <td className='center aligned'>
                                                        {this.cekLaporan(activities.id, this.state.tanggal) > 0 ?
                                                            <a
                                                                title="Hapus"
                                                                className="waves-effect waves-light btn-small"
                                                                onClick={this.onDelete.bind(this, this.getEvaluationId(activities.id, this.state.tanggal))}>
                                                                <i className="material-icons">
                                                                    delete
                                                                </i>
                                                            </a>
                                                            :
                                                            <a
                                                                className="waves-effect waves-light btn-small"
                                                                // href={'/murabbi/laporan_halaqah/lapor/'+group.id}
                                                            >
                                                                <i className="material-icons">
                                                                    assignment
                                                                </i>
                                                            </a>
                                                        }
                                                        </td>
                                                </tr>
                                            )
                                        }
                                        )
                                    }

                                </tbody>

                            </table>
                <br/>
                            <Row style={{textAlign: 'center'}}>
                                    <Button waves='light' onClick={this.onSubmit}><Icon>save</Icon>Simpan</Button>
                            </Row>
                            <Row>
                                <a href='manage' className="btn-floating btn-large waves-effect waves-light red"><i className="material-icons">add</i></a><label> Kelola Aktivitas</label>
                            </Row>
            </div>
        )
    }
}

if(document.getElementById('mutabaah')) {
    const element = document.getElementById('mutabaah');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Mutabaah {...props} />, document.getElementById('mutabaah'));
}
