import React from 'react'
import ReactDOM from 'react-dom'
import {Row, TextInput, Select,  Col, Button, Icon} from 'react-materialize'
import OneChartComponent from '../../components/OneChart'
import { If, Then, ElseIf, Else } from 'react-if-elseif-else-render';
import axios from "axios/index";

export default class Profile extends React.Component {
    constructor(){
        super();

        this.state={
            activities: [],
            users: [],
            categories: [],
            total: [],
            evaluations: [],
            pencapaian: [],
            month: '',
            year: '',
            pekan: [],
            raw: true
        }
        console.log(super());
    }

    componentDidMount(){
        let id = this.props.id;
        axios.get('/api/activity')
            .then(response=>{
                this.setState({activities:response.data});
            });
        axios.get('/api/evaluation')
            .then(response=>{
                this.setState({evaluations:response.data});
            });

        axios.get('/api/categories')
            .then(response=>{
                this.setState({categories:response.data});
            });

        axios.get('/api/user')
            .then(response=>{
                this.setState({users:response.data});
            });

        var date = require('locutus/php/datetime/date');

        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        this.setState({
            month: parseInt(date('m')),
            year: parseInt(date('Y')),
        });
    }

    getName(id){
        const mentee = this.state.users.filter(users => users.id === id).map(users => users.name);
        return mentee[0];
    }

    getPencapaian(id, week){
        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }
        let value = this.state.evaluations.filter(evaluations=>evaluations.activities_id == id && evaluations.users_id == this.props.id && parseInt(evaluations.week) == week && evaluations.date.substr(5, 2) == this.state.month && evaluations.date.substr(0, 4) == this.state.year)
            .map(evaluations=>evaluations.value);
        value = value[0];
        const categories_id =  this.state.activities.filter(activities=>activities.id == id).map(activities=>activities.categories_id);
        // console.log(categories_id[0]);
        const weight = this.state.categories.filter(categories=>categories.id == categories_id[0]).map(categories=>categories.weight);
        const bobot_kategori = weight[0];
        const jumlah_aktivitas = this.state.activities.length;
        const bobot_aktivitas = bobot_kategori / jumlah_aktivitas;
        let target = this.state.activities.filter(activities=>activities.id == id).map(activities=>activities.target);
        target = target[0];
        const nilai = value / target * bobot_aktivitas;
        const pencapaian = this.state.evaluations.filter(evaluations=>evaluations.activities_id == id && evaluations.users_id == this.props.id && parseInt(evaluations.week) == week && evaluations.date.substr(5, 2) == this.state.month && evaluations.date.substr(0, 4) == this.state.year)
                .map(evaluations=>evaluations.value).reduce((a, b)=>{return (a + b);}, 0);
        console.log(value);
        return pencapaian;
    }

    getActivityInCategory(categories_id){
        let jumlah = this.state.activities.filter(activities=>activities.users_id == this.props.id || activities.users_id == '0' && activities.categories_id == categories_id).map(activities=>activities.activity);
        return jumlah.length;
    }

    getPencapaianOlah(id, week){
        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }
        // let value = this.state.evaluations.filter(evaluations=>evaluations.activities_id == id && evaluations.week == week && evaluations.date.substr(5, 2) == this.state.month && evaluations.date.substr(0, 4) == this.state.year).map(evaluations=>evaluations.value);
        // value = value[0];
        let value = this.getPencapaian(id, week)
        const categories_id =  this.state.activities.filter(activities=>activities.id == id).map(activities=>activities.categories_id);
        let period =  this.state.activities.filter(activities=>activities.id == id).map(activities=>activities.period);
        period = period[0];
        // console.log(period);
        const weight = this.state.categories.filter(categories=>categories.id == categories_id[0]).map(categories=>categories.weight);
        const bobot_kategori = weight[0];
        const aktivitas = this.state.activities.filter(activities=>activities.categories_id == categories_id && activities.users_id == this.props.id || activities.users_id == '0')
            .map(activities=>activities.activity);
        const jumlah_aktivitas = this.getActivityInCategory(categories_id);
        const bobot_aktivitas = bobot_kategori / jumlah_aktivitas;
        let data_masuk= this.state.evaluations.filter(evaluations=>evaluations.activities_id == id && parseInt(evaluations.week) == week && evaluations.date.substr(5, 2) == this.state.month && evaluations.date.substr(0, 4) == this.state.year)
            .map(evaluations=>evaluations.id);
        data_masuk = data_masuk.length;
        // console.log(id + '-' + data_masuk.length);
        let target = this.state.activities.filter(activities=>activities.id == id).map(activities=>activities.target);
        target = target[0];

        if(period == 'Hari'){
            target = target * 7;
        }
        let nilai = value / target * bobot_aktivitas;
        // console.log(value + '/' + target + '*' + bobot_aktivitas + '=' + nilai)
        const pencapaian = this.state.evaluations.filter(evaluations=>evaluations.activities_id == id && evaluations.week == week && evaluations.date.substr(5, 2) == this.state.month && evaluations.date.substr(0, 4) == this.state.year)
            .map(evaluations=>evaluations.value).reduce((a, b)=>{return (a + b);}, 0);
        if(isNaN(nilai)){
            return nilai = 0;
        }
        else{
        return nilai; }

    }



    getTotalPencapaian(activities, week){
        let total = 0;
         activities.map(activity=>{
             total = total + parseInt(this.getPencapaian(activity.id, week))
        })
        return total
    }

    getTotalPencapaianOlah(activities, week){
        let total = 0;
        activities.map(activity=>{
            total = total + parseInt(this.getPencapaianOlah(activity.id, week))
        })
        return total
    }

    getArrayTotal(activities){
        let array = [];
        activities.map(activity=>{
            array = [this.getTotalPencapaian(activities, 1), this.getTotalPencapaian(activities, 2), this.getTotalPencapaian(activities, 3), this.getTotalPencapaian(activities, 4), this.getTotalPencapaian(activities, 5)]
        })
        this.state.total = array
        return array
    }

    getArrayTotalOlah(activities){
        let array = [];
        activities.map(activity=>{
            array = [this.getTotalPencapaianOlah(activities, 1), this.getTotalPencapaianOlah(activities, 2), this.getTotalPencapaianOlah(activities, 3), this.getTotalPencapaianOlah(activities, 4), this.getTotalPencapaianOlah(activities, 5)]
        })
        this.state.total = array
        return array
    }

    prevMonth = () => {
        var bulan = this.state.month;
        if (bulan <= 1){
            this.setState({
                month : 12,
                year : this.state.year-1
            });
        }
        else{
            this.setState({
                month : this.state.month-1
            });
        }
    }

    nextMonth = () => {
        var bulan = this.state.month;
        if (bulan >= 12){
            this.setState({
                month : 1,
                year : this.state.year+1
            });
        }
        else{
            this.setState({
                month : this.state.month+1
            });
        }
    }

    getActivity(id){
        axios.get('/api/activity/'+id)
            .then(response=>{
                activity:response.data
            });
    }

    toRaw(){
        this.setState({
            raw: true
        })
    }
    toResult(){
        this.setState({
            raw: false
        })
    }

    render(){
        console.log(this.getName(5));
        // console.log(this.getArrayTotal(this.state.activities));
        var strtotime = require('locutus/php/datetime/strtotime');

        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        return (
            <div className='container'>
                <h3>Profile</h3>
                        {this.state.raw == true ?
                            <OneChartComponent
                                judul='Pencapaian'
                                label='Pencapaian'
                                data={this.state.raw == true ? this.getArrayTotal(this.state.activities) : this.getArrayTotalOlah(this.state.activities)}
                                labels={['Pekan 1', 'Pekan 2', 'Pekan 3', 'Pekan 4', 'Pekan 5']}
                                height={window.innerWidth < 500 ? 250 : null}
                            />
                            :
                            <OneChartComponent
                                judul='Pencapaian (%)'
                                label='Pencapaian (%)'
                                data={this.state.raw == true ? this.getArrayTotal(this.state.activities) : this.getArrayTotalOlah(this.state.activities)}
                                labels={['Pekan 1', 'Pekan 2', 'Pekan 3', 'Pekan 4', 'Pekan 5']}
                                max={100}
                                height={window.innerWidth < 500 ? 250 : null}
                            />
                        }
                            <h4>Nama: {this.getName(parseInt(this.props.id))}</h4>
                            <table className="bordered striped">
                                <thead>
                                    <tr>
                                        <th className='center aligned' rowSpan='2'>No.</th>
                                        <th className='center aligned' rowSpan='2'>Kegiatan</th>
                                        <th className='center aligned' rowSpan='2'>Target</th>
                                        <th className='center aligned' colSpan='5'><a onClick={this.prevMonth}><Icon small>chevron_left</Icon></a>{monthNumToName(this.state.month)} {this.state.year}<a onClick={this.nextMonth}><Icon small>chevron_right</Icon></a></th>
                                    </tr>
                                    <tr>
                                        <th className='center aligned'>1</th>
                                        <th className='center aligned'>2</th>
                                        <th className='center aligned'>3</th>
                                        <th className='center aligned'>4</th>
                                        <th className='center aligned'>5</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {
                                        this.state.activities.filter(activities=>activities.users_id == this.props.id || activities.users_id == '0').map((activities, index) => {
                                            return (
                                                <tr key={activities.id}>
                                                    <td className='center aligned'>{index+1}</td>
                                                    <td>{activities.users_id == 0 ? activities.activity + '*' : activities.activity}</td>
                                                    <td>{activities.target} {activities.unit} / {activities.period}</td>
                                                    <td className='center aligned'>
                                                        {
                                                            this.state.raw == true ? this.getPencapaian(activities.id, 1) : this.getPencapaianOlah(activities.id, 1).toFixed(0)
                                                        }
                                                    </td>
                                                    <td className='center aligned'>
                                                        {
                                                            this.state.raw == true ? this.getPencapaian(activities.id, 2) : this.getPencapaianOlah(activities.id, 2).toFixed(0)
                                                        }
                                                    </td>
                                                    <td className='center aligned'>
                                                        {
                                                            this.state.raw == true ? this.getPencapaian(activities.id, 3) : this.getPencapaianOlah(activities.id, 3).toFixed(0)
                                                        }
                                                    </td>
                                                    <td className='center aligned'>
                                                        {
                                                            this.state.raw == true ? this.getPencapaian(activities.id, 4) : this.getPencapaianOlah(activities.id, 4).toFixed(0)
                                                        }
                                                    </td>
                                                    <td className='center aligned'>
                                                        {
                                                            this.state.raw == true ? this.getPencapaian(activities.id, 5) : this.getPencapaianOlah(activities.id, 5).toFixed(0)
                                                        }
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }

                                    <tr>
                                        <td colSpan='3' className='center aligned'>Total</td>
                                        <th className='center aligned'>
                                            {
                                                this.state.raw == true ? this.getTotalPencapaian(this.state.activities, 1) : this.getTotalPencapaianOlah(this.state.activities, 1).toFixed(0)
                                            }
                                        </th>
                                        <th className='center aligned'>
                                            {
                                                this.state.raw == true ? this.getTotalPencapaian(this.state.activities, 2) : this.getTotalPencapaianOlah(this.state.activities, 2).toFixed(0)
                                            }
                                        </th>
                                        <th className='center aligned'>
                                            {
                                                this.state.raw == true ? this.getTotalPencapaian(this.state.activities, 3) : this.getTotalPencapaianOlah(this.state.activities, 3).toFixed(0)
                                            }
                                        </th>
                                        <th className='center aligned'>
                                            {
                                                this.state.raw == true ? this.getTotalPencapaian(this.state.activities, 4) : this.getTotalPencapaianOlah(this.state.activities, 4).toFixed(0)
                                            }
                                        </th>
                                        <th className='center aligned'>
                                            {
                                                this.state.raw == true ? this.getTotalPencapaian(this.state.activities, 5) : this.getTotalPencapaianOlah(this.state.activities, 5).toFixed(0)
                                            }
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                            <br/>
                            <Row style={{textAlign: 'center'}}>
                                <Button waves='light' onClick={()=>this.toRaw()}>Raw</Button>
                                <Button waves='light' onClick={()=>this.toResult()}>Result</Button>
                            </Row>
            </div>
        )
    }
}

if(document.getElementById('profile')) {
    const element = document.getElementById('profile');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Profile {...props} />, document.getElementById('profile'));
}
