import React from 'react'
import {Bar, Line, Pie} from 'react-chartjs-2';
import ReactDOM from "react-dom";



export default class BarChartComponent extends React.Component{
    constructor(props){
        super(props);
        }

        static defaultProps = {
            displayTitle : true,
            displayLegend : true,
            legendPosition : 'top',
            labels: ['Boston', 'Blabla'],
            label: 'Populasi',
            data: [100, 100],
            backgroundColor: 'rgba(255,105,145,0.6)'
        }

    render(){
        return (
            <div>
                <Bar
                    data={{
                        labels: this.props.labels,
                        datasets: [
                            {
                                label: this.props.label,
                                data: this.props.data,
                                backgroundColor: this.props.backgroundColor
                            }]
                    }}
                    height={this.props.height}

                    options={{
                        responsive: true,
                        legend:{
                            display: this.props.displayLegend,
                            position: this.props.legendPosition
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true,
                                    min: 0,
                                    max: this.props.max
                                }
                            }]
                        },
                        maintainAspectRatio: true
                    }}
                />
            </div>
        )
    }
}