import React from 'react';
import {Bar} from 'react-chartjs-2';

export default class MixChartComponent extends React.Component {

    render() {
        return (
            <div>
                <h2 align='center'>{this.props.judul}</h2>
                <Bar
                    data={{
                        datasets: [{
                            label: this.props.label1,
                            type:'line',
                            data: this.props.data1,
                            fill: false,
                            borderColor: 'green',
                            backgroundColor: 'green',
                            pointBorderColor: 'green',
                            pointBackgroundColor: 'green',
                            pointHoverBackgroundColor: 'green',
                            pointHoverBorderColor: 'green',
                            yAxisID: 'y-axis-2'
                        },{
                            type: 'line',
                            label: this.props.label2,
                            data: this.props.data2,
                            fill: false,
                            backgroundColor: 'blue',
                            borderColor: 'blue',
                            hoverBackgroundColor: 'blue',
                            hoverBorderColor: 'blue',
                            yAxisID: 'y-axis-1'
                        }]
                    }}
                    height={this.props.height}
                    options={{
                        responsive: true,
                        tooltips: {
                            mode: 'label'
                        },
                        elements: {
                            line: {
                                fill: false
                            }
                        },
                        scales: {
                            xAxes: [
                                {
                                    display: true,
                                    gridLines: {
                                        display: true
                                    },
                                    labels: this.props.labels,
                                }
                            ],
                            yAxes: [
                                {
                                    type: 'linear',
                                    display: true,
                                    position: 'left',
                                    id: 'y-axis-1',
                                    gridLines: {
                                        display: true
                                    },
                                    labels: {
                                        show: true
                                    },
                                    ticks: {
                                        beginAtZero:true,
                                        min: 0,
                                        max: this.props.max
                                    }

                                },
                                {
                                    type: 'linear',
                                    display: true,
                                    position: 'right',
                                    id: 'y-axis-2',
                                    gridLines: {
                                        display: false
                                    },
                                    labels: {
                                        show: true
                                    },
                                    ticks: {
                                        beginAtZero:true,
                                        min: 0,
                                        max: this.props.max
                                    }
                                }
                            ]
                        }
                    }}
                    plugins={[{
                        afterDraw: (chartInstance, easing) => {
                            const ctx = chartInstance.chart.ctx;
                            // ctx.fillText("This text drawn by a plugin", 100, 100);
                        }
                    }]}
                />
            </div>
        );
    }
}