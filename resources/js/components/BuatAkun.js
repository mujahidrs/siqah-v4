import React from 'react';
import {TextInput, Button, Col} from 'react-materialize';
import axios from "axios/index";

export default class BuatAkun extends React.Component {
    state = {
        users: [],
        username: '',
        email: '',
        confirm_password: '',
        password: '',
    };

    componentDidMount(){
        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });
    }

    onSubmitCreate = (e) => {
        e.preventDefault();

        let cekUsername = this.state.users.filter(data=>data.username === this.state.username).map(data=>data.username);

        if(cekUsername.length === 0){
            if(this.state.email.includes('@') === true){
                if(this.state.confirm_password === this.state.password){
                    const account =
                        {
                            mentee_id: this.props.mentee_id,
                            username: this.state.username,
                            email: this.state.email,
                            password: this.state.password
                        };

                    console.log(account);

                    axios.put('/api/user/createAccount/'+this.props.mentee_id, account)
                        .then(response => window.location.reload())
                        .catch();
                }
                else{
                    alert('Maaf, password konfirmasi tidak sama!');
                }
            }
            else{
                alert('Maaf, email harus menyertakan @!');
            }
        }
        else{
            alert('Maaf, username ini sudah terpakai!');
        }
    };

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    getName(id){
        id = parseInt(id);
        let name = this.state.users.filter(user => user.id === id).map(user => user.name);
        return name[0];
    }

    render() {
        if(this.props.active){
            return (
                <div style={{padding: 20, border: '2px solid', borderColor: 'black', borderRadius: 20}}>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <h4>Buat Akun</h4>
                    </Col>
                    <Col>
                        <TextInput placeholder='Nama Binaan' label="Nama Binaan" type="text" s={3} name="mentee_id" disabled value={this.getName(this.props.mentee_id)} />
                        <TextInput placeholder='Username' label="Username" type="text" s={3} name="username" onChange={this.handleChange}/>
                        <TextInput placeholder='Email' label="Email" type="email" s={3} name="email" onChange={this.handleChange}/>
                        <TextInput placeholder='Password' label="Password" type="password" s={3} name="password" onChange={this.handleChange}/>
                        <TextInput placeholder='Confirm Password' label="Confirm Password" type="password" s={3} name="confirm_password" onChange={this.handleChange}/>
                    </Col>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <Button style={{marginRight: 5}} onClick={this.onSubmitCreate}>Buat</Button>
                        <Button style={{marginLeft: 5}} className='red' onClick={this.props.onClose}>Batal</Button>
                    </Col>
                </div>
            );
        }
        else{
            return(
                <div>

                </div>
            )
        }
    }
}
