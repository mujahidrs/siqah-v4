import React from 'react';
import {TextInput, Button, Col} from 'react-materialize';
import axios from "axios/index";

export default class EditAkun extends React.Component {
    state = {
        users: [],
        username_edit: this.props.username_edit,
        email_edit: this.props.email_edit,
        confirm_password_edit: '',
        password_edit: '',
        changePassword: false
    };

    componentDidMount(){
        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });
    }

    onSubmitEditAccount = (e) => {
        e.preventDefault();

        let cekUsername = this.state.users.filter(data=>data.username === this.state.username_edit && data.id !== this.props.mentee_id).map(data=>data.username);

        if(cekUsername.length === 0){
            if(this.state.email_edit.includes('@') === true) {
                if (this.state.confirm_password_edit === this.state.password_edit) {
                    const account =
                        {
                            mentee_id: this.props.mentee_id,
                            username: this.state.username_edit,
                            email: this.state.email_edit,
                            password: this.state.password_edit
                        };

                    console.log(account);

                    axios.put('/api/user/editAccount/'+this.props.mentee_id, account)
                        .then(response => window.location.reload())
                        .catch();
                }
                else {
                    alert('Maaf, password konfirmasi tidak sama!');
                }
            }
            else{
                alert('Maaf, email harus menyertakan @!');
            }
        }
        else{
            alert('Maaf, username ini sudah terpakai!');
        }
    };

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    onChangePassword = () => {
        this.setState({
            changePassword: !this.state.changePassword,
            password_edit: '',
            confirm_password_edit: ''
        })
    };

    getName(id){
        id = parseInt(id);
        let name = this.state.users.filter(user => user.id === id).map(user => user.name);
        return name[0];
    }

    render() {
        if(this.props.active){
            return (
                <div style={{padding: 20, border: '2px solid', borderColor: 'black', borderRadius: 20}}>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <h4>Edit Akun</h4>
                    </Col>
                    <Col>
                        <TextInput placeholder='Nama Binaan' label="Nama Binaan" type="text" s={3} name="mentee_id" disabled value={this.getName(this.props.mentee_id)} />
                        <TextInput placeholder='Username' label="Username" type="text" s={3} name="username_edit" value={this.state.username_edit} onChange={this.handleChange}/>
                        <TextInput placeholder='Email' label="Email" type="email" s={3} name="email_edit" validate value={this.state.email_edit} onChange={this.handleChange}/>
                        {
                            !this.state.changePassword && <Button onClick={this.onChangePassword}>Ganti Password</Button>
                        }
                        {
                            this.state.changePassword && <div>
                                <TextInput placeholder='Password' label="Password" type="password" s={3} name="password_edit" value={this.state.password_edit} onChange={this.handleChange}/>
                                <TextInput placeholder='Confirm Password' label="Confirm Password" type="password" s={3} name="confirm_password_edit" value={this.state.confirm_password_edit} onChange={this.handleChange}/>
                                <Button className='red' onClick={this.onChangePassword}>Batalkan</Button>
                            </div>
                        }
                    </Col>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <Button style={{marginRight: 5}} onClick={this.onSubmitEditAccount}>Edit</Button>
                        <Button style={{marginLeft: 5}} className='red' onClick={this.props.onClose}>Batal</Button>
                    </Col>
                </div>
            );
        }
        else{
            return(
                <div>

                </div>
            )
        }
    }
}
