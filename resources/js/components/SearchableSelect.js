import React from 'react';
import Select from 'react-select';
import {Col, Row} from "react-materialize";

export default class SearchableSelect extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        if(this.props.defaultValue !== undefined) {
            return (
                <Row>
                    <Select placeholder={this.props.placeholder} options={this.props.options}
                            defaultValue={this.props.defaultLabel !== undefined ? {
                                label: this.props.defaultLabel,
                                value: this.props.defaultValue
                            } : {label: this.props.defaultValue, value: this.props.defaultValue}}
                            value={this.props.value} onChange={this.props.onChange} name={this.props.name}/>
                </Row>
            );
        }

        return (
            <Row>
                <Select placeholder={this.props.placeholder} options={this.props.options} value={this.props.value} onChange={this.props.onChange} name={this.props.name}/>
            </Row>
        );
    }
}
