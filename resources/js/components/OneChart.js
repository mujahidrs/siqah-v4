import React from 'react';
import {Bar} from 'react-chartjs-2';

export default class OneChartComponent extends React.Component {

    render() {
        return (
            <div>
                <Bar
                    data={{
                        datasets: [{
                            label: this.props.label,
                            type:'line',
                            data: this.props.data,
                            fill: false,
                            borderColor: 'blue',
                            backgroundColor: 'blue',
                            pointBorderColor: 'blue',
                            pointBackgroundColor: 'blue',
                            pointHoverBackgroundColor: 'blue',
                            pointHoverBorderColor: 'blue',
                        }]
                    }}
                    options={{
                        responsive: true,
                        tooltips: {
                            mode: 'label'
                        },
                        elements: {
                            line: {
                                fill: false
                            }
                        },
                        scales: {
                            xAxes: [
                                {
                                    display: true,
                                    gridLines: {
                                        display: true
                                    },
                                    labels: this.props.labels,
                                }
                            ],
                            yAxes: [
                                {
                                    type: 'linear',
                                    display: true,
                                    position: 'left',
                                    id: 'y-axis-1',
                                    gridLines: {
                                        display: true
                                    },
                                    labels: {
                                        show: true
                                    },
                                    ticks: {
                                        beginAtZero:true,
                                        min: 0,
                                        max: this.props.max
                                    }
                                }
                            ]
                        }
                    }}
                />
            </div>
        );
    }
}