import React from 'react';
import ReactDOM from 'react-dom';
import {Row, Col, TextInput, Select,  Button, Icon, Collapsible, CollapsibleItem} from 'react-materialize';
import axios from 'axios';

export default class FamilyTree extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;

        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data,});
            });
    }

    render() {
        return (
            <div className="container">
                <h2>Pohon Keluarga</h2>
                <Collapsible accordion={false}>
                    {
                        this.state.users.filter(user=>user.levels_id === 'notset' && user.gender === parseInt(this.props.gender) && user.role !== 0).map((user, key)=>{
                            return(
                                <CollapsibleItem key={key} header={user.name} icon={<Icon>person</Icon>}>
                                    <Collapsible accordion={false}>
                                        {
                                            this.state.users.filter(binaan=>binaan.mentors_id === user.id).map((binaan,index)=>{
                                                return(
                                                    <CollapsibleItem key={index} header={binaan.group_name + ' | ' + binaan.name} icon={<Icon>person_pin</Icon>}>
                                                        <Collapsible accordion={false}>
                                                            {
                                                                this.state.users.filter(cucu=>cucu.mentors_id === binaan.id).map((cucu,index)=>{
                                                                    return(
                                                                        <CollapsibleItem key={index} header={cucu.group_name + ' | ' + cucu.name} icon={<Icon>person_pin_circle</Icon>}>
                                                                            <Collapsible accordion={false}>
                                                                                {
                                                                                    this.state.users.filter(cicit=>cicit.mentors_id === cucu.id).map((cicit,index)=>{
                                                                                        return(
                                                                                            <CollapsibleItem key={index} header={cicit.group_name + ' | ' + cicit.name} icon={<Icon>pin_drop</Icon>}>
                                                                                            </CollapsibleItem>
                                                                                        )
                                                                                    })
                                                                                }
                                                                            </Collapsible>
                                                                        </CollapsibleItem>
                                                                    )
                                                                })
                                                            }
                                                        </Collapsible>
                                                    </CollapsibleItem>
                                                )
                                            })
                                        }
                                    </Collapsible>
                                </CollapsibleItem>
                            )
                        })
                    }
                </Collapsible>
            </div>
        )
    }
}

if(document.getElementById('family_tree')) {
    const element = document.getElementById('family_tree');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<FamilyTree {...props} />, document.getElementById('family_tree'));
}
