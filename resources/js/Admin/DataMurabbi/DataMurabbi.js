import React from 'react';
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Col, Button, Tabs, Tab, Modal, Icon, Table} from 'react-materialize';
import axios from 'axios';
import BuatAkun from "../../components/BuatAkun";
import EditAkun from "../../components/EditAkun";

function ButtonTransfer(props) {
    return(
        <Button
            tooltip="Transfer"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="indigo"
            waves="light"
            icon={<Icon>transfer_within_a_station</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonDelete(props) {
    return(
        <Button
            tooltip="Hapus"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="red"
            waves="light"
            icon={<Icon>delete</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonCreateUser(props) {
    return(
        <Button
            tooltip="Buat Akun"
            floating
            small
            className="blue"
            waves="light"
            icon={<Icon>person_add</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonEditUser(props) {
    return(
        <Button
            tooltip="Edit Akun"
            style={{marginLeft: 5}}
            floating
            small
            className="blue"
            waves="light"
            icon={<Icon>edit</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonSetMentor(props) {
    return(
        <Button
            tooltip="Atur Status Pembina"
            style={{marginLeft: 5}}
            floating
            small
            className="green"
            waves="light"
            icon={<Icon>settings</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonShowGroups(props) {
    return(
        <Button
            tooltip="Lihat Kelompok"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="purple"
            waves="light"
            icon={<Icon>group</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonShowProfile(props) {
    return(
        <Button
            tooltip="Lihat Profil"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            waves="light"
            className="modal-trigger brown"
            href="#modal1"
            icon={<Icon>remove_red_eye</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonEditProfile(props) {
    return(
        <Button
            tooltip="Edit Profil"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="light-blue"
            waves="light"
            icon={<Icon>edit</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonActivate(props) {
    return(
        <Button
            tooltip="Aktifkan"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="pink"
            waves="light"
            icon={<Icon>highlight</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonUnactivate(props) {
    return(
        <Button
            tooltip="Nonaktifkan"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="pink"
            waves="light"
            icon={<Icon>highlight_off</Icon>}
            onClick={props.onClick}
        />
    )
}

export default class DataUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            murabbi: [],
            groups: [],
            users: [],
            gender_user: 0,
            category: 'nama',
            prodi: [],
            gender: 0,
            jumlah: 0,
            search: '',
            filtered: [],
            createAccount: false,
            editAccount: false,
            mentee_id: '',
            username_edit: '',
            email_edit: '',
            changePassword: false,
            password_edit: '',
            confirm_password_edit: '',
            showProfile: [],
            selectTab: 'semua'
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;
        axios.get('/api/user')
            .then(response => {
                this.setState({murabbi: response.data, filtered: response.data});
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });

        axios.get('/api/group')
            .then(response => {
                this.setState({groups: response.data});
            });

        axios.get('/api/user/'+id)
            .then(response => {
                this.setState({gender_user: response.data.gender});
                this.setState({
                    gender: this.state.gender_user === 0 ? 1 : this.state.gender_user
                })
            });

        axios.get('/api/prodi')
            .then(response => {
                this.setState({prodi: response.data});
            });
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({
            filtered: nextProps.murabbi
        });
    }

    getJumlahUserAktif(gender){
        let jumlah = this.state.filtered.filter(murabbi => murabbi.gender === gender && murabbi.status === 'Aktif').map(murabbi=>murabbi.id);
        return jumlah.length;
    }

    getJumlahUserNonaktif(gender){
        let jumlah = this.state.filtered.filter(murabbi => murabbi.gender === gender && murabbi.status === 'Nonaktif').map(murabbi=>murabbi.id);
        return jumlah.length;
    }

    getNamaProdi(prodi_id){
        let nama_prodi = this.state.prodi.filter(prodi => prodi.id === prodi_id).map(prodi=>prodi.prodi);
        return nama_prodi[0];
    }

    handleChangeCategory = (event) => {
        this.setState({category: event.target.value});
    };

    handleChangeGender = (event) => {
        this.setState({gender: parseInt(event.target.value)});
    };

    handleSearch = (e) => {
        // Variable to hold the original version of the list
        let currentList = [];
        // Variable to hold the filtered list before putting into state
        let newList = [];
        let category = '';

        // If the search bar isn't empty
        if (e.target.value !== "") {
            // Assign the original list to currentList
            currentList = this.state.murabbi;

            // Use .filter() to determine which items should be displayed
            // based on the search terms
            newList = currentList.filter(item => {
                console.log(item);
                let lc = '';
                // change current item to lowercase
                if(this.state.category === 'nama'){
                    lc = item.name.toLowerCase();
                }
                if(this.state.category === 'angkatan'){
                    lc = item.angkatan.toLowerCase();
                }
                if(this.state.category === 'kelas'){
                    lc = item.levels_id.toString();
                }
                // change search term to lowercase
                const filter = e.target.value.toLowerCase();
                // check to see if the current list item includes the search term
                // If it does, it will be added to newList. Using lowercase eliminates
                // issues with capitalization in search terms and search content
                return lc.includes(filter);
            });
        } else {
            // If the search bar is empty, set newList to original task list
            newList = this.state.murabbi;
        }
        // Set the filtered state based on what our rules added to newList
        this.setState({
            filtered: newList
        });
    };

    onDelete(user_id){
        let result = confirm('Hapus user ini?');
        if(result === true){
            axios.delete('/api/user/delete/'+user_id)
                .then(response=>{
                    let murabbi = this.state.filtered;

                    for(let i=0; i< murabbi.length; i++)
                    {
                        if(murabbi[i].id === user_id)
                        {
                            murabbi.splice(i,1);
                            this.setState({
                                filtered:murabbi
                            })
                        }
                    }
                });
        }
    }

    nonAktifkan(users_id){
        let result = confirm('Non-Aktifkan user murabbi ini?');
        if(result === true){
            axios.put('/api/user/nonaktif/'+users_id)
                .then(response=>{
                    window.alert("Berhasil Non-Aktifkan!");
                    axios.get('/api/user')
                    .then(response => {
                        this.setState({murabbi: response.data, filtered: response.data});
                    });
                })
                .catch();
        }
    }

    Aktifkan(users_id){
        let result = confirm('Aktifkan user murabbi ini?');
        if(result === true){
            axios.put('/api/user/aktif/'+users_id)
                .then(response=>{
                    window.alert("Berhasil Aktifkan!");
                    axios.get('/api/user')
                    .then(response => {
                        this.setState({murabbi: response.data, filtered: response.data});
                    });
                })
                .catch();
        }
    }

    getJumlahKelompok(mentors_id){
        let jumlah = this.state.groups.filter(group => group.mentors_id === mentors_id).map(group => group.group_name);
        return jumlah.length;
    }

    getGroupBinaan(mentors_id){
        let group = this.state.groups.filter(group => group.mentors_id === mentors_id).map(group => group.id);
        return group;
    }

    getJumlahBinaan(mentors_id){
        let jumlah = this.getGroupBinaan(mentors_id);
        let hasil = [];

        jumlah.map(jumlah => {
            this.state.users.filter(user => user.groups_id === jumlah && user.status === 'Aktif').map(user => {
                hasil = hasil.concat(user.id)
            });

        });

        return hasil.length;
    }

    onCreate = (id) => {
        this.setState({
            createAccount: true,
            editAccount: false,
            mentee_id: id
        })
    };

    onEditAccount = (id) => {
        const users = this.state.users.filter(user=>user.id === id).map((user) => {
            return (
                {
                    mentee_id : user.id,
                    username_edit : user.username,
                    email_edit : user.email,
                    password_edit: '',
                    confirm_password_edit: '',
                }
            )
        });

        this.setState({
            createAccount: false,
            editAccount: true,
            mentee_id: id,
            username_edit: users[0].username_edit,
            email_edit: users[0].email_edit,
        })
    };

    onEditIsMentor = (users_id, is_mentor) => {
        if(is_mentor === 0){
            let result = confirm('Jadikan sebagai Pembina?');
            if(result === true){
                axios.post('/api/setMentor/'+users_id)
                    .then(response=>{
                        console.log(response);
                        if(response.data.success === true){
                            axios.get('/api/user')
                            .then(response => {
                                this.setState({murabbi: response.data, filtered: response.data});
                            });
                        }
                    });
            }
        }
        else{
            let result = confirm('Lepas dari status Pembina?');
            if(result === true){
                axios.post('/api/removeMentor/'+users_id)
                    .then(response=>{
                        console.log(response);
                        if(response.data.success === true){
                            axios.get('/api/user')
                            .then(response => {
                                this.setState({murabbi: response.data, filtered: response.data});
                            });
                        }
                    });
            }
        }
    };

    showProfile = (id) => {
        const users = this.state.users.filter(user=>user.id === id).map(user=>user);
        this.setState({
            showProfile: users[0]
        })
    };

    onClose = () => {
        this.setState({
            createAccount: false
        })
    };

    onClose2 = () => {
        this.setState({
            editAccount: false
        })
    };

    getName(id){
        id = parseInt(id);
        let name = this.state.users.filter(user => user.id === id).map(user => user.name);
        return name[0];
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    onChangePassword = () => {
        this.setState({
            changePassword: !this.state.changePassword,
            password_edit: '',
            confirm_password_edit: ''
        })
    };

    onSubmitEditAccount = (e) => {
        e.preventDefault();

        let cekUsername = this.state.users.filter(data=>data.username === this.state.username_edit && data.id !== this.state.mentee_id).map(data=>data.username);

        if(cekUsername.length === 0){
            if(this.state.email_edit.includes('@') === true) {
                if (this.state.confirm_password_edit === this.state.password_edit) {
                    const account =
                        {
                            mentee_id: this.state.mentee_id,
                            username: this.state.username_edit,
                            email: this.state.email_edit,
                            password: this.state.password_edit
                        };

                    console.log(account);

                    axios.put('/api/user/editAccount/'+this.state.mentee_id, account)
                        .then(response => window.location.reload())
                        .catch();
                }
                else {
                    alert('Maaf, password konfirmasi tidak sama!');
                }
            }
            else{
                alert('Maaf, email harus menyertakan @!');
            }
        }
        else{
            alert('Maaf, username ini sudah terpakai!');
        }
    };

    getFakultas(prodi_id){
        let faculty = this.state.prodi.filter(prodi=> prodi.id === prodi_id).map(prodi=>prodi.faculty);
        return faculty[0];
    }

    render() {
        let sortBy = require('sort-by');
        let arr = this.state.filtered.filter(murabbi => murabbi.gender === this.state.gender && murabbi.status === 'Aktif').map((murabbi, index) => murabbi).sort(sortBy('name'));
        let arr2 = this.state.filtered.filter(murabbi => murabbi.gender === this.state.gender && murabbi.status === 'Nonaktif').map((murabbi, index) => murabbi).sort(sortBy('name'));
        return (
            <div className="container">
                <h2>Data User</h2>
                <Row>
                    <Select s={12} m={6} type='select' label="Jenis Kelamin" name="gender" value={this.state.gender} onChange={this.handleChangeGender} required>
                        <option value='0' disabled>Pilih Jenis Kelamin</option>
                        <option value='1' disabled={this.state.gender_user === 2 ? 'true' : null}>Ikhwan</option>
                        <option value='2' disabled={this.state.gender_user === 1 ? 'true' : null}>Akhwat</option>
                    </Select>
                </Row>
                <Row>
                    <Select s={6} m={6} type='select' label="Kategori" name="category" value={this.state.category} onChange={this.handleChangeCategory} required>
                        <option value='0' disabled>Pilih Kategori Pencarian</option>
                        <option value='nama'>Nama</option>
                        <option value='angkatan'>Angkatan</option>
                        <option value='kelas'>Kelas</option>
                    </Select>
                    <TextInput s={6} m={6} type='text' label='Cari' name='search' onChange={this.handleSearch}/>
                </Row>
                <Row>
                    <Col>
                        Jumlah User Aktif<br/>
                        Jumlah User Nonaktif
                    </Col>
                    <Col>
                        : {this.getJumlahUserAktif(this.state.gender)} orang<br/>
                        : {this.getJumlahUserNonaktif(this.state.gender)} orang
                    </Col>
                </Row>
                <Col style={{width: '100%'}}>
                    <Button waves='light' style={{width: '20%'}} className="blue white-text" onClick={()=>this.setState({selectTab: 'semua'})}>
                        Semua
                    </Button>
                    <Button waves='light' style={{width: '20%'}} className="blue white-text" onClick={()=>this.setState({selectTab: 'admin'})}>
                        Admin
                    </Button>
                    <Button waves='light' style={{width: '20%'}} className="blue white-text" onClick={()=>this.setState({selectTab: 'user'})}>
                        User
                    </Button>
                    <Button waves='light' style={{width: '20%'}} className="blue white-text" onClick={()=>this.setState({selectTab: 'pembina'})}>
                        Pembina
                    </Button>
                    <Button waves='light' style={{width: '20%'}} className="blue white-text" onClick={()=>this.setState({selectTab: 'binaan'})}>
                        Binaan
                    </Button>
                </Col>
                {
                    this.state.selectTab === 'semua' &&
                    (
                        <div>
                            <a className='btn' href={'export_excel/all/'+this.state.gender} style={{width: '100%'}}><i className="material-icons">file_download</i> Excel</a>
                            <a className='btn' href={'family_tree/'+this.state.gender} style={{width: '100%', backgroundColor: 'green'}}><i className="material-icons">nature</i> Pohon Keluarga</a>
                            <Table striped>
                                <thead>
                                <tr>
                                    <th className='center aligned'>No.</th>
                                    <th className='center aligned'>Nama</th>
                                    {/*<th className='center aligned'>Prodi</th>*/}
                                    <th className='center aligned'>Angkatan</th>
                                    <th className='center aligned'>Kelas</th>
                                    <th className='center aligned'>Role</th>
                                    <th className='center aligned'>Username</th>
                                    <th className='center aligned'>Pembina?</th>
                                    {/*<th className='center aligned'>Jumlah Kelompok</th>*/}
                                    {/*<th className='center aligned'>Jumlah Binaan Aktif</th>*/}
                                    <th className='center aligned'>Tindakan</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    arr.map((murabbi, index) => {
                                        return(
                                            <tr key={index}>
                                                <td className='center aligned'>{index+1}</td>
                                                <td>{murabbi.name}</td>
                                                {/*<td>{this.getNamaProdi(murabbi.prodi_id)}</td>*/}
                                                <td className='center aligned'>{murabbi.angkatan}</td>
                                                <td className='center aligned'>{murabbi.levels_id}</td>
                                                <td className='center aligned'>{murabbi.role === 0 ? 'Admin' : murabbi.username !== null ? 'User' : 'Non-User'}</td>
                                                <td className='center aligned'>
                                                    {
                                                        murabbi.username === null ?
                                                            (
                                                                <ButtonCreateUser onClick={()=>this.onCreate(murabbi.id)}/>
                                                                )
                                                            : (
                                                                <div>
                                                                    <span>
                                                                        {murabbi.username}
                                                                    </span>
                                                                    <ButtonEditUser onClick={()=>this.onEditAccount(murabbi.id)}/>
                                                                </div>
                                                            )
                                                    }
                                                </td>
                                                <td className='center aligned'>{murabbi.is_mentor === 1 ? 'Ya' : 'Bukan'}
                                                    <ButtonSetMentor onClick={()=>this.onEditIsMentor(murabbi.id, murabbi.is_mentor)}/>
                                                </td>
                                                {/*<td className='center aligned'>{this.getJumlahKelompok(murabbi.id)}</td>*/}
                                                {/*<td className='center aligned'>{this.getJumlahBinaan(murabbi.id)}</td>*/}
                                                <td className='right aligned'>
                                                    {
                                                        (murabbi.mentors_id !== 'notset' || murabbi.groups_id === null) &&
                                                        (
                                                            <ButtonTransfer onClick={()=>window.location='/admin/data_murabbi/transfer/'+murabbi.id}/>
                                                        )

                                                    }
                                                    {
                                                        murabbi.is_mentor === 1 &&
                                                        (
                                                            <ButtonShowGroups onClick={()=>window.location='/admin/data_kelompok/'+murabbi.id}/>
                                                        )
                                                    }
                                                    <ButtonShowProfile onClick={()=>this.showProfile(murabbi.id)}/>
                                                    <ButtonEditProfile onClick={()=>window.location='/admin/data_murabbi/edit/'+murabbi.id}/>
                                                    <ButtonUnactivate onClick={this.nonAktifkan.bind(this,murabbi.id)}/>
                                                    {/*<a*/}
                                                    {/*className="waves-effect waves-light btn-small"*/}
                                                    {/*onClick={this.onDelete.bind(this,murabbi.id)}>*/}
                                                    {/*<i className="material-icons">*/}
                                                    {/*delete*/}
                                                    {/*</i>*/}
                                                    {/*</a>*/}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                {
                                    arr2.map((murabbi, index) => {
                                        return(
                                            <tr key={index} style={{color: 'grey'}}>
                                                <td className='center aligned'>{index+1}</td>
                                                <td>{murabbi.name}</td>
                                                {/*<td>{this.getNamaProdi(murabbi.prodi_id)}</td>*/}
                                                <td className='center aligned'>{murabbi.angkatan}</td>
                                                <td className='center aligned'>{murabbi.levels_id}</td>
                                                <td className='center aligned'>{murabbi.role === 0 ? 'Admin' : murabbi.username !== null ? 'User' : 'Non-User'}</td>
                                                <td className='center aligned'>
                                                    {
                                                        murabbi.username === null ?
                                                            (
                                                                <ButtonCreateUser onClick={()=>this.onCreate(murabbi.id)}/>
                                                            )
                                                            : (
                                                                <div>
                                                                    <span>
                                                                        {murabbi.username}
                                                                    </span>
                                                                    <ButtonEditUser onClick={()=>this.onEditAccount(murabbi.id)}/>
                                                                </div>
                                                            )
                                                    }
                                                </td>
                                                <td className='center aligned'>{murabbi.is_mentor === 1 ? 'Ya' : 'Bukan'}
                                                    <ButtonSetMentor onClick={()=>this.onEditIsMentor(murabbi.id, murabbi.is_mentor)}/>
                                                </td>
                                                {/*<td className='center aligned'>{this.getJumlahKelompok(murabbi.id)}</td>*/}
                                                {/*<td className='center aligned'>{this.getJumlahBinaan(murabbi.id)}</td>*/}
                                                <td className='right aligned'>
                                                    {
                                                        (murabbi.mentors_id !== 'notset' || murabbi.groups_id === null) &&
                                                        (
                                                            <ButtonTransfer onClick={()=>window.location='/admin/data_murabbi/transfer/'+murabbi.id}/>
                                                        )

                                                    }
                                                    {
                                                        murabbi.is_mentor === 1 &&
                                                        (
                                                            <ButtonShowGroups onClick={()=>window.location='/admin/data_kelompok/'+murabbi.id}/>
                                                        )
                                                    }
                                                    <ButtonShowProfile onClick={()=>this.showProfile(murabbi.id)}/>
                                                    <ButtonEditProfile onClick={()=>window.location='/admin/data_murabbi/edit/'+murabbi.id}/>
                                                    <ButtonActivate onClick={this.Aktifkan.bind(this,murabbi.id)}/>
                                                    <ButtonDelete onClick={this.onDelete.bind(this,murabbi.id)}/>
                                                    {/*<Button*/}
                                                    {/*    floating*/}
                                                    {/*    small*/}
                                                    {/*    waves="light"*/}
                                                    {/*    icon={<Icon>delete</Icon>}*/}
                                                    {/*    onClick={this.onDelete.bind(this,murabbi.id)}*/}
                                                    {/*/>*/}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </Table>
                        </div>
                    )
                }
                {
                    this.state.selectTab === 'admin' &&
                    (
                        <div>
                            <a className='btn' href={'export_excel/admin/'+this.state.gender} style={{width: '100%'}}><i className="material-icons">file_download</i> Excel</a>
                            <a className='btn' href={'family_tree/'+this.state.gender} style={{width: '100%', backgroundColor: 'green'}}><i className="material-icons">nature</i> Pohon Keluarga</a>
                            <Table striped>
                                <thead>
                                <tr>
                                    <th className='center aligned'>No.</th>
                                    <th className='center aligned'>Nama</th>
                                    {/*<th className='center aligned'>Prodi</th>*/}
                                    <th className='center aligned'>Angkatan</th>
                                    <th className='center aligned'>Kelas</th>
                                    <th className='center aligned'>Username</th>
                                    <th className='center aligned'>Pembina?</th>
                                    {/*<th className='center aligned'>Jumlah Kelompok</th>*/}
                                    {/*<th className='center aligned'>Jumlah Binaan Aktif</th>*/}
                                    <th className='center aligned'>Tindakan</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    arr.filter(murabbi=>murabbi.role === 0).map((murabbi, index) => {
                                        return(
                                            <tr key={index}>
                                                <td className='center aligned'>{index+1}</td>
                                                <td>{murabbi.name}</td>
                                                {/*<td>{this.getNamaProdi(murabbi.prodi_id)}</td>*/}
                                                <td className='center aligned'>{murabbi.angkatan}</td>
                                                <td className='center aligned'>{murabbi.levels_id}</td>
                                                <td className='center aligned'>
                                                    {
                                                        murabbi.username === null ?
                                                            (
                                                                <ButtonCreateUser onClick={()=>this.onCreate(murabbi.id)}/>
                                                            )
                                                            : (
                                                                <div>
                                                                    <span>
                                                                        {murabbi.username}
                                                                    </span>
                                                                    <ButtonEditUser onClick={()=>this.onEditAccount(murabbi.id)}/>
                                                                </div>
                                                            )
                                                    }
                                                </td>
                                                <td className='center aligned'>{murabbi.is_mentor === 1 ? 'Ya' : 'Bukan'}
                                                    <ButtonSetMentor onClick={()=>this.onEditIsMentor(murabbi.id, murabbi.is_mentor)}/>
                                                </td>
                                                {/*<td className='center aligned'>{this.getJumlahKelompok(murabbi.id)}</td>*/}
                                                {/*<td className='center aligned'>{this.getJumlahBinaan(murabbi.id)}</td>*/}
                                                <td className='right aligned'>
                                                    {
                                                        (murabbi.mentors_id !== 'notset' || murabbi.groups_id === null) &&
                                                        (
                                                            <ButtonTransfer onClick={()=>window.location='/admin/data_murabbi/transfer/'+murabbi.id}/>
                                                        )

                                                    }
                                                    {
                                                        murabbi.is_mentor === 1 &&
                                                        (
                                                            <ButtonShowGroups onClick={()=>window.location='/admin/data_kelompok/'+murabbi.id}/>
                                                        )
                                                    }
                                                    <ButtonShowProfile onClick={()=>this.showProfile(murabbi.id)}/>
                                                    <ButtonEditProfile onClick={()=>window.location='/admin/data_murabbi/edit/'+murabbi.id}/>
                                                    <ButtonUnactivate onClick={this.nonAktifkan.bind(this,murabbi.id)}/>
                                                    {/*<a*/}
                                                    {/*className="waves-effect waves-light btn-small"*/}
                                                    {/*onClick={this.onDelete.bind(this,murabbi.id)}>*/}
                                                    {/*<i className="material-icons">*/}
                                                    {/*delete*/}
                                                    {/*</i>*/}
                                                    {/*</a>*/}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                {
                                    arr2.filter(murabbi=>murabbi.role === 0).map((murabbi, index) => {
                                        return(
                                            <tr key={index} style={{color: 'grey'}}>
                                                <td className='center aligned'>{index+1}</td>
                                                <td>{murabbi.name}</td>
                                                {/*<td>{this.getNamaProdi(murabbi.prodi_id)}</td>*/}
                                                <td className='center aligned'>{murabbi.angkatan}</td>
                                                <td className='center aligned'>{murabbi.levels_id}</td>
                                                <td className='center aligned'>
                                                    {
                                                        murabbi.username === null ?
                                                            (
                                                                <ButtonCreateUser onClick={()=>this.onCreate(murabbi.id)}/>
                                                            )
                                                            : (
                                                                <div>
                                                                    <span>
                                                                        {murabbi.username}
                                                                    </span>
                                                                    <ButtonEditUser onClick={()=>this.onEditAccount(murabbi.id)}/>
                                                                </div>
                                                            )
                                                    }
                                                </td>
                                                <td className='center aligned'>{murabbi.is_mentor === 1 ? 'Ya' : 'Bukan'}
                                                    <ButtonSetMentor onClick={()=>this.onEditIsMentor(murabbi.id, murabbi.is_mentor)}/>
                                                </td>
                                                {/*<td className='center aligned'>{this.getJumlahKelompok(murabbi.id)}</td>*/}
                                                {/*<td className='center aligned'>{this.getJumlahBinaan(murabbi.id)}</td>*/}
                                                <td className='right aligned'>
                                                    {
                                                        (murabbi.mentors_id !== 'notset' || murabbi.groups_id === null) &&
                                                        (
                                                            <ButtonTransfer onClick={()=>window.location='/admin/data_murabbi/transfer/'+murabbi.id}/>
                                                        )

                                                    }
                                                    {
                                                        murabbi.is_mentor === 1 &&
                                                        (
                                                            <ButtonShowGroups onClick={()=>window.location='/admin/data_kelompok/'+murabbi.id}/>
                                                        )
                                                    }
                                                    <ButtonShowProfile onClick={()=>this.showProfile(murabbi.id)}/>
                                                    <ButtonEditProfile onClick={()=>window.location='/admin/data_murabbi/edit/'+murabbi.id}/>
                                                    <ButtonActivate onClick={this.Aktifkan.bind(this,murabbi.id)}/>
                                                    {/*<ButtonDelete onClick={this.onDelete.bind(this,murabbi.id)}/>*/}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </Table>
                        </div>
                    )
                }
                {
                    this.state.selectTab === 'user' &&
                    (
                        <div>
                            <a className='btn' href={'export_excel/user/'+this.state.gender} style={{width: '100%'}}><i className="material-icons">file_download</i> Excel</a>
                            <a className='btn' href={'family_tree/'+this.state.gender} style={{width: '100%', backgroundColor: 'green'}}><i className="material-icons">nature</i> Pohon Keluarga</a>
                            <Table striped>
                                <thead>
                                <tr>
                                    <th className='center aligned'>No.</th>
                                    <th className='center aligned'>Nama</th>
                                    {/*<th className='center aligned'>Prodi</th>*/}
                                    <th className='center aligned'>Angkatan</th>
                                    <th className='center aligned'>Kelas</th>
                                    <th className='center aligned'>Username</th>
                                    <th className='center aligned'>Pembina?</th>
                                    {/*<th className='center aligned'>Jumlah Kelompok</th>*/}
                                    {/*<th className='center aligned'>Jumlah Binaan Aktif</th>*/}
                                    <th className='center aligned'>Tindakan</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    arr.filter(murabbi=>murabbi.role === 1).map((murabbi, index) => {
                                        return(
                                            <tr key={index}>
                                                <td className='center aligned'>{index+1}</td>
                                                <td>{murabbi.name}</td>
                                                {/*<td>{this.getNamaProdi(murabbi.prodi_id)}</td>*/}
                                                <td className='center aligned'>{murabbi.angkatan}</td>
                                                <td className='center aligned'>{murabbi.levels_id}</td>
                                                <td className='center aligned'>
                                                    {
                                                        murabbi.username === null ?
                                                            (
                                                                <ButtonCreateUser onClick={()=>this.onCreate(murabbi.id)}/>
                                                            )
                                                            : (
                                                                <div>
                                                                    <span>
                                                                        {murabbi.username}
                                                                    </span>
                                                                    <ButtonEditUser onClick={()=>this.onEditAccount(murabbi.id)}/>
                                                                </div>
                                                            )
                                                    }
                                                </td>
                                                <td className='center aligned'>{murabbi.is_mentor === 1 ? 'Ya' : 'Bukan'}
                                                    <ButtonSetMentor onClick={()=>this.onEditIsMentor(murabbi.id, murabbi.is_mentor)}/>
                                                </td>
                                                {/*<td className='center aligned'>{this.getJumlahKelompok(murabbi.id)}</td>*/}
                                                {/*<td className='center aligned'>{this.getJumlahBinaan(murabbi.id)}</td>*/}
                                                <td className='right aligned'>
                                                    {
                                                        (murabbi.mentors_id !== 'notset' || murabbi.groups_id === null) &&
                                                        (
                                                            <ButtonTransfer onClick={()=>window.location='/admin/data_murabbi/transfer/'+murabbi.id}/>
                                                        )

                                                    }
                                                    {
                                                        murabbi.is_mentor === 1 &&
                                                        (
                                                            <ButtonShowGroups onClick={()=>window.location='/admin/data_kelompok/'+murabbi.id}/>
                                                        )
                                                    }
                                                    <ButtonShowProfile onClick={()=>this.showProfile(murabbi.id)}/>
                                                    <ButtonEditProfile onClick={()=>window.location='/admin/data_murabbi/edit/'+murabbi.id}/>
                                                    <ButtonUnactivate onClick={this.nonAktifkan.bind(this,murabbi.id)}/>
                                                    {/*<a*/}
                                                    {/*className="waves-effect waves-light btn-small"*/}
                                                    {/*onClick={this.onDelete.bind(this,murabbi.id)}>*/}
                                                    {/*<i className="material-icons">*/}
                                                    {/*delete*/}
                                                    {/*</i>*/}
                                                    {/*</a>*/}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                {
                                    arr2.filter(murabbi=>murabbi.role === 1).map((murabbi, index) => {
                                        return(
                                            <tr key={index} style={{color: 'grey'}}>
                                                <td className='center aligned'>{index+1}</td>
                                                <td>{murabbi.name}</td>
                                                {/*<td>{this.getNamaProdi(murabbi.prodi_id)}</td>*/}
                                                <td className='center aligned'>{murabbi.angkatan}</td>
                                                <td className='center aligned'>{murabbi.levels_id}</td>
                                                <td className='center aligned'>
                                                    {
                                                        murabbi.username === null ?
                                                            (
                                                                <ButtonCreateUser onClick={()=>this.onCreate(murabbi.id)}/>
                                                            )
                                                            : (
                                                                <div>
                                                                    <span>
                                                                        {murabbi.username}
                                                                    </span>
                                                                    <ButtonEditUser onClick={()=>this.onEditAccount(murabbi.id)}/>
                                                                </div>
                                                            )
                                                    }
                                                </td>
                                                <td className='center aligned'>{murabbi.is_mentor === 1 ? 'Ya' : 'Bukan'}
                                                    <ButtonSetMentor onClick={()=>this.onEditIsMentor(murabbi.id, murabbi.is_mentor)}/>
                                                </td>
                                                {/*<td className='center aligned'>{this.getJumlahKelompok(murabbi.id)}</td>*/}
                                                {/*<td className='center aligned'>{this.getJumlahBinaan(murabbi.id)}</td>*/}
                                                <td className='right aligned'>
                                                    {
                                                        (murabbi.mentors_id !== 'notset' || murabbi.groups_id === null) &&
                                                        (
                                                            <ButtonTransfer onClick={()=>window.location='/admin/data_murabbi/transfer/'+murabbi.id}/>
                                                        )

                                                    }
                                                    {
                                                        murabbi.is_mentor === 1 &&
                                                        (
                                                            <ButtonShowGroups onClick={()=>window.location='/admin/data_kelompok/'+murabbi.id}/>
                                                        )
                                                    }
                                                    <ButtonShowProfile onClick={()=>this.showProfile(murabbi.id)}/>
                                                    <ButtonEditProfile onClick={()=>window.location='/admin/data_murabbi/edit/'+murabbi.id}/>
                                                    <ButtonActivate onClick={this.Aktifkan.bind(this,murabbi.id)}/>
                                                    {/*<ButtonDelete onClick={this.onDelete.bind(this,murabbi.id)}/>*/}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </Table>
                        </div>
                    )
                }
                {
                    this.state.selectTab === 'pembina' &&
                    (
                        <div>
                            <a className='btn' href={'export_excel/pembina/'+this.state.gender} style={{width: '100%'}}><i className="material-icons">file_download</i> Excel</a>
                            <a className='btn' href={'family_tree/'+this.state.gender} style={{width: '100%', backgroundColor: 'green'}}><i className="material-icons">nature</i> Pohon Keluarga</a>
                            <Table striped>
                                <thead>
                                <tr>
                                    <th className='center aligned'>No.</th>
                                    <th className='center aligned'>Nama</th>
                                    {/*<th className='center aligned'>Prodi</th>*/}
                                    <th className='center aligned'>Angkatan</th>
                                    <th className='center aligned'>Kelas</th>
                                    <th className='center aligned'>Role</th>
                                    <th className='center aligned'>Username</th>
                                    <th className='center aligned'>Jumlah Kelompok</th>
                                    <th className='center aligned'>Jumlah Binaan Aktif</th>
                                    <th className='center aligned'>Tindakan</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    arr.filter(murabbi=>murabbi.is_mentor === 1).map((murabbi, index) => {
                                        return(
                                            <tr key={index}>
                                                <td className='center aligned'>{index+1}</td>
                                                <td>{murabbi.name}</td>
                                                {/*<td>{this.getNamaProdi(murabbi.prodi_id)}</td>*/}
                                                <td className='center aligned'>{murabbi.angkatan}</td>
                                                <td className='center aligned'>{murabbi.levels_id}</td>
                                                <td className='center aligned'>{murabbi.role === 0 ? 'Admin' : murabbi.username !== null ? 'User' : 'Non-User'}</td>
                                                <td className='center aligned'>
                                                    {
                                                        murabbi.username === null ?
                                                            (
                                                                <ButtonCreateUser onClick={()=>this.onCreate(murabbi.id)}/>
                                                            )
                                                            : (
                                                                <div>
                                                                    <span>
                                                                        {murabbi.username}
                                                                    </span>
                                                                    <ButtonEditUser onClick={()=>this.onEditAccount(murabbi.id)}/>
                                                                </div>
                                                            )
                                                    }
                                                </td>
                                                <td className='center aligned'>{this.getJumlahKelompok(murabbi.id)}</td>
                                                <td className='center aligned'>{this.getJumlahBinaan(murabbi.id)}</td>
                                                <td className='right aligned'>
                                                    {
                                                        (murabbi.mentors_id !== 'notset' || murabbi.groups_id === null) &&
                                                        (
                                                            <ButtonTransfer onClick={()=>window.location='/admin/data_murabbi/transfer/'+murabbi.id}/>
                                                        )

                                                    }
                                                    {
                                                        murabbi.is_mentor === 1 &&
                                                        (
                                                            <ButtonShowGroups onClick={()=>window.location='/admin/data_kelompok/'+murabbi.id}/>
                                                        )
                                                    }
                                                    <ButtonShowProfile onClick={()=>this.showProfile(murabbi.id)}/>
                                                    <ButtonEditProfile onClick={()=>window.location='/admin/data_murabbi/edit/'+murabbi.id}/>
                                                    <ButtonUnactivate onClick={this.nonAktifkan.bind(this,murabbi.id)}/>
                                                    {/*<a*/}
                                                    {/*className="waves-effect waves-light btn-small"*/}
                                                    {/*onClick={this.onDelete.bind(this,murabbi.id)}>*/}
                                                    {/*<i className="material-icons">*/}
                                                    {/*delete*/}
                                                    {/*</i>*/}
                                                    {/*</a>*/}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                {
                                    arr2.filter(murabbi=>murabbi.is_mentor === 1).map((murabbi, index) => {
                                        return(
                                            <tr key={index} style={{color: 'grey'}}>
                                                <td className='center aligned'>{index+1}</td>
                                                <td>{murabbi.name}</td>
                                                {/*<td>{this.getNamaProdi(murabbi.prodi_id)}</td>*/}
                                                <td className='center aligned'>{murabbi.angkatan}</td>
                                                <td className='center aligned'>{murabbi.levels_id}</td>
                                                <td className='center aligned'>{murabbi.role === 0 ? 'Admin' : murabbi.username !== null ? 'User' : 'Non-User'}</td>
                                                <td className='center aligned'>
                                                    {
                                                        murabbi.username === null ?
                                                            (
                                                                <ButtonCreateUser onClick={()=>this.onCreate(murabbi.id)}/>
                                                            )
                                                            : (
                                                                <div>
                                                                    <span>
                                                                        {murabbi.username}
                                                                    </span>
                                                                    <ButtonEditUser onClick={()=>this.onEditAccount(murabbi.id)}/>
                                                                </div>
                                                            )
                                                    }
                                                </td>
                                                <td className='center aligned'>{this.getJumlahKelompok(murabbi.id)}</td>
                                                <td className='center aligned'>{this.getJumlahBinaan(murabbi.id)}</td>
                                                <td className='right aligned'>
                                                    {
                                                        (murabbi.mentors_id !== 'notset' || murabbi.groups_id === null) &&
                                                        (
                                                            <ButtonTransfer onClick={()=>window.location='/admin/data_murabbi/transfer/'+murabbi.id}/>
                                                        )

                                                    }
                                                    {
                                                        murabbi.is_mentor === 1 &&
                                                        (
                                                            <ButtonShowGroups onClick={()=>window.location='/admin/data_kelompok/'+murabbi.id}/>
                                                        )
                                                    }
                                                    <ButtonShowProfile onClick={()=>this.showProfile(murabbi.id)}/>
                                                    <ButtonEditProfile onClick={()=>window.location='/admin/data_murabbi/edit/'+murabbi.id}/>
                                                    <ButtonActivate onClick={this.Aktifkan.bind(this,murabbi.id)}/>
                                                    {/*<ButtonDelete onClick={this.onDelete.bind(this,murabbi.id)}/>*/}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </Table>
                        </div>
                    )
                }
                {
                    this.state.selectTab === 'binaan' &&
                    (
                        <div>
                            <a className='btn' href={'export_excel/binaan/'+this.state.gender} style={{width: '100%'}}><i className="material-icons">file_download</i> Excel</a>
                            <a className='btn' href={'family_tree/'+this.state.gender} style={{width: '100%', backgroundColor: 'green'}}><i className="material-icons">nature</i> Pohon Keluarga</a>
                            <Table striped>
                                <thead>
                                <tr>
                                    <th className='center aligned'>No.</th>
                                    <th className='center aligned'>Nama</th>
                                    {/*<th className='center aligned'>Prodi</th>*/}
                                    <th className='center aligned'>Angkatan</th>
                                    <th className='center aligned'>Kelas</th>
                                    <th className='center aligned'>Role</th>
                                    <th className='center aligned'>Username</th>
                                    <th className='center aligned'>Nama Pembina</th>
                                    <th className='center aligned'>Nama Kelompok</th>
                                    {/*<th className='center aligned'>Jumlah Kelompok</th>*/}
                                    {/*<th className='center aligned'>Jumlah Binaan Aktif</th>*/}
                                    <th className='center aligned'>Tindakan</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    arr.filter(murabbi=>murabbi.is_mentor === 0 && murabbi.role === 1).map((murabbi, index) => {
                                        return(
                                            <tr key={index}>
                                                <td className='center aligned'>{index+1}</td>
                                                <td>{murabbi.name}</td>
                                                {/*<td>{this.getNamaProdi(murabbi.prodi_id)}</td>*/}
                                                <td className='center aligned'>{murabbi.angkatan}</td>
                                                <td className='center aligned'>{murabbi.levels_id}</td>
                                                <td className='center aligned'>{murabbi.role === 0 ? 'Admin' : murabbi.username !== null ? 'User' : 'Non-User'}</td>
                                                <td className='center aligned'>{murabbi.username === null ? <ButtonCreateUser onClick={()=>this.onCreate(murabbi.id)}/> : (
                                                    <div>
                                                <span>
                                                    {murabbi.username}
                                                </span>
                                                        <ButtonEditUser onClick={()=>this.onEditAccount(murabbi.id)}/>
                                                    </div>
                                                )}</td>
                                                <td className='center aligned'>{this.getName(murabbi.mentors_id)}</td>
                                                <td className='center aligned'>{murabbi.group_name}</td>
                                                {/*<td className='center aligned'>{this.getJumlahKelompok(murabbi.id)}</td>*/}
                                                {/*<td className='center aligned'>{this.getJumlahBinaan(murabbi.id)}</td>*/}
                                                <td className='right aligned'>
                                                    {
                                                        (murabbi.mentors_id !== 'notset' || murabbi.groups_id === null) &&
                                                        (
                                                            <ButtonTransfer onClick={()=>window.location='/admin/data_murabbi/transfer/'+murabbi.id}/>
                                                        )

                                                    }
                                                    {
                                                        murabbi.is_mentor === 1 &&
                                                        (
                                                            <ButtonShowGroups onClick={()=>window.location='/admin/data_kelompok/'+murabbi.id}/>
                                                        )
                                                    }
                                                    <ButtonShowProfile onClick={()=>this.showProfile(murabbi.id)}/>
                                                    <ButtonEditProfile onClick={()=>window.location='/admin/data_murabbi/edit/'+murabbi.id}/>
                                                    <ButtonUnactivate onClick={this.nonAktifkan.bind(this,murabbi.id)}/>
                                                    {/*<a*/}
                                                    {/*className="waves-effect waves-light btn-small"*/}
                                                    {/*onClick={this.onDelete.bind(this,murabbi.id)}>*/}
                                                    {/*<i className="material-icons">*/}
                                                    {/*delete*/}
                                                    {/*</i>*/}
                                                    {/*</a>*/}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                {
                                    arr2.filter(murabbi=>murabbi.is_mentor === 0 && murabbi.role === 1).map((murabbi, index) => {
                                        return(
                                            <tr key={index} style={{color: 'grey'}}>
                                                <td className='center aligned'>{index+1}</td>
                                                <td>{murabbi.name}</td>
                                                {/*<td>{this.getNamaProdi(murabbi.prodi_id)}</td>*/}
                                                <td className='center aligned'>{murabbi.angkatan}</td>
                                                <td className='center aligned'>{murabbi.levels_id}</td>
                                                <td className='center aligned'>{murabbi.role === 0 ? 'Admin' : murabbi.username !== null ? 'User' : 'Non-User'}</td>
                                                <td className='center aligned'>
                                                    {
                                                        murabbi.username === null ?
                                                            (
                                                                <ButtonCreateUser onClick={()=>this.onCreate(murabbi.id)}/>
                                                            )
                                                            : (
                                                                <div>
                                                                    <span>
                                                                        {murabbi.username}
                                                                    </span>
                                                                    <ButtonEditUser onClick={()=>this.onEditAccount(murabbi.id)}/>
                                                                </div>
                                                            )
                                                    }
                                                </td>
                                                <td className='center aligned'>{this.getName(murabbi.mentors_id)}</td>
                                                <td className='center aligned'>{murabbi.group_name}</td>
                                                {/*<td className='center aligned'>{this.getJumlahKelompok(murabbi.id)}</td>*/}
                                                {/*<td className='center aligned'>{this.getJumlahBinaan(murabbi.id)}</td>*/}
                                                <td className='right aligned'>
                                                    {
                                                        (murabbi.mentors_id !== 'notset' || murabbi.groups_id === null) &&
                                                        (
                                                            <ButtonTransfer onClick={()=>window.location='/admin/data_murabbi/transfer/'+murabbi.id}/>
                                                        )
                                                    }
                                                    {
                                                        murabbi.is_mentor === 1 &&
                                                        (
                                                            <ButtonShowGroups onClick={()=>window.location='/admin/data_kelompok/'+murabbi.id}/>
                                                        )
                                                    }
                                                    <ButtonShowProfile onClick={()=>this.showProfile(murabbi.id)}/>
                                                    <ButtonEditProfile onClick={()=>window.location='/admin/data_murabbi/edit/'+murabbi.id}/>
                                                    <ButtonActivate onClick={this.Aktifkan.bind(this,murabbi.id)}/>
                                                    {/*<ButtonDelete onClick={this.onDelete.bind(this,murabbi.id)}/>*/}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </Table>
                        </div>
                    )
                }
                <a onClick={()=>window.location = '/admin/data_murabbi/add'} className="btn-floating btn-large waves-effect waves-light red"><i className="material-icons">add</i></a><label> Tambah User</label>
                <BuatAkun mentee_id={this.state.mentee_id} active={this.state.createAccount} onClose={this.onClose}/>
                {this.state.editAccount && <div style={{padding: 20, border: '2px solid', borderColor: 'black', borderRadius: 20}}>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <h4>Edit Akun</h4>
                    </Col>
                    <Col>
                        <TextInput placeholder='Nama Binaan' label="Nama Binaan" type="text" s={3} name="mentee_id" disabled value={this.getName(this.state.mentee_id)} />
                        <TextInput placeholder='Username' label="Username" type="text" s={3} name="username_edit" value={this.state.username_edit} onChange={this.handleChange}/>
                        <TextInput placeholder='Email' label="Email" type="email" s={3} name="email_edit" validate value={this.state.email_edit} onChange={this.handleChange}/>
                        {
                            !this.state.changePassword && <Button onClick={this.onChangePassword}>Ganti Password</Button>
                        }
                        {
                            this.state.changePassword && <div>
                                <TextInput placeholder='Password' label="Password" type="password" s={3} name="password_edit" value={this.state.password_edit} onChange={this.handleChange}/>
                                <TextInput placeholder='Confirm Password' label="Confirm Password" type="password" s={3} name="confirm_password_edit" value={this.state.confirm_password_edit} onChange={this.handleChange}/>
                                <Button className='red' onClick={this.onChangePassword}>Batalkan</Button>
                            </div>
                        }
                    </Col>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <Button style={{marginRight: 5}} onClick={this.onSubmitEditAccount}>Edit</Button>
                        <Button style={{marginLeft: 5}} className='red' onClick={this.onClose2}>Batal</Button>
                    </Col>
                </div>}
                <Modal id="modal1" header="Profil User">
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama Lengkap</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.name}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Username</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.username}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Alamat Email</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.email}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Jenis Kelamin</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.gender === 1 ? 'Ikhwan' : 'Akhwat'}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Role</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.role === 0 ? 'Admin' : this.state.showProfile.username !== null ? 'User' : 'Non-User'}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Pembina?</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.is_mentor === 0 ? 'Tidak' : 'Ya'}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Fakultas</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getFakultas(this.state.showProfile.prodi_id)}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Prodi</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getNamaProdi(this.state.showProfile.prodi_id)}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Angkatan</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.angkatan}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama Pembina</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getName(this.state.showProfile.mentors_id)}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Kelas</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.levels_id}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama Kelompok</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.group_name}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Status</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.status}</Row>
                </Modal>
            </div>
        )
    }
}

if(document.getElementById('data_murabbi')) {
    const element = document.getElementById('data_murabbi');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<DataUser {...props} />, document.getElementById('data_murabbi'));
}
