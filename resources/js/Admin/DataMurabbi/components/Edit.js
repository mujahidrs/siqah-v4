import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Collection, CollectionItem} from 'react-materialize'
import axios from 'axios'

export default class Edit extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            faculties: [],
            prodies: [],
            name: '',
            username: '',
            email: '',
            gender: '',
            faculty: '',
            prodi: '',
            angkatan: '',
            role: '',
            is_mentor: '',
            password: '',
            confirm_password: '',
            errors: [],
            year: '',
            changePassword: false,
        };
        console.log(super());
    }

    componentDidMount() {
        let id = window.location.pathname.split('/')[4];
        let today = new Date();
        let tanggal = today.getDate();
        if(tanggal <= 10){
            tanggal = '0' + tanggal;
        }
        let sdate = tanggal + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
        axios.get('/api/faculty')
            .then(response => {
                this.setState({faculties: response.data});
            });

        axios.get('/api/prodi')
            .then(response => {
                this.setState({prodies: response.data});
            });

        axios.get('/api/user/'+id)
            .then(response => {
                this.setState({
                    name: response.data.name,
                    username: response.data.username,
                    email: response.data.email,
                    gender: response.data.gender,
                    role: response.data.role,
                    is_mentor: response.data.is_mentor,
                    prodi: response.data.prodi_id,
                    angkatan: response.data.angkatan,
                    faculty: this.state.prodies.filter(prodi => prodi.id == response.data.prodi_id).map(prodi => prodi.faculty_id)
                });

            });

        let year = today.getFullYear();

        this.setState({
            year: year
        })
    }

    getFacultyId(prodi_id){

    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty('name')) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    validate(name, username, email, gender, faculty, role, prodi, password, confirm_password, angkatan, is_mentor){
        const errors = [];

        if(name.length === 0){
            errors.push('Nama tidak boleh kosong');
        }

        if(username.length === 0){
            errors.push('Username tidak boleh kosong');
        }

        if(email.length === null){
            errors.push('Email tidak boleh kosong');
        }

        if(gender.length === 0){
            errors.push('Jenis Kelamin tidak boleh kosong');
        }

        if(faculty.length === 0){
            errors.push('Fakultas tidak boleh kosong');
        }

        if(prodi.length === null){
            errors.push('Prodi tidak boleh kosong');
        }

        if(role.length === 0){
            errors.push('Role tidak boleh kosong');
        }

        if(is_mentor.length === 0){
            errors.push('Pembina tidak boleh kosong');
        }

        if(angkatan.length === 0){
            errors.push('Angkatan tidak boleh kosong');
        }

        if(this.state.changePassword){
            if(password.length === 0){
                errors.push('Password tidak boleh kosong');
            }

            if(confirm_password !== password) {
                errors.push('Confirm Password tidak sama');
            }
        }

        return errors;
    }


    createOption () {
        let option = [];
        let year = this.state.year;

        for (let i = 0; i <= 10; i++) {
            option.push(<option key={i} value={year}>{year}</option>);
            year = year - 1;
        }
        return option
    }

    onSubmit(e){
        e.preventDefault();

        const {name, username, email, gender, faculty, role, prodi, password, confirm_password, angkatan, is_mentor} = this.state;

        const errors = this.validate(name, username, email, gender, faculty, role, prodi, password, confirm_password, angkatan, is_mentor);
        if (errors.length > 0) {
            this.setState({ errors });
            return true;
        }
        else {
            const edit =
                {
                    name: name,
                    username: username,
                    email: email,
                    gender: gender,
                    prodi: prodi,
                    angkatan: angkatan,
                    role: role,
                    is_mentor: is_mentor,
                    password: password,
                };

            let id = window.location.pathname.split('/')[4];

            axios.put('/api/user/edit/'+id, edit)
                .then(response => window.location = '/admin/data_murabbi')
                .catch();
        }
    }

    onChangePassword = () => {
        this.setState({
            changePassword: !this.state.changePassword,
            password: '',
            confirm_password: ''
        })
    };

    render() {
        return (
            <div className="container">
                <h2>Edit User</h2>
                <div className="row">
                    <form className="col s12" method="post" action="#" role="form">
                        <Row>
                            <TextInput type="text" s={6} label="Nama" name="name" value={this.state.name} onChange={this.handleChange} required autoFocus/>
                            <TextInput type="text" s={6} label="Username" name="username" value={this.state.username} onChange={this.handleChange} required/>
                            <TextInput type="text" s={12} label="Alamat Email" name="email" value={this.state.email} onChange={this.handleChange} required/>
                            <Select s={6} label="Jenis Kelamin" name="gender" value={this.state.gender} onChange={this.handleChange} required>
                                <option value='0'>Pilih Jenis Kelamin</option>
                                <option value='1'>Ikhwan</option>
                                <option value='2'>Akhwat</option>
                            </Select>
                            <Select s={6} label="Role" name="role" value={this.state.role} onChange={this.handleChange} required>
                                <option>Pilih Role</option>
                                <option value='1'>User</option>
                                <option value='0'>Admin</option>
                            </Select>
                            <Select s={6} label="Pembina?" name="is_mentor" value={this.state.is_mentor} onChange={this.handleChange} required>
                                <option>Pilih Opsi</option>
                                <option value='0'>Tidak</option>
                                <option value='1'>Ya</option>
                            </Select>
                            <Select s={6} label="Fakultas" name="faculty" value={this.state.faculty} onClick={(e)=>console.log(e)} onChange={this.handleChange} required>
                                <option value='0'>Pilih Fakultas</option>
                                {this.state.faculties.map((faculty, index) =>{
                                    return(
                                        <option key={index} value={index+1}>{faculty.faculty}</option>
                                    )
                                })
                                }
                            </Select>
                            <Select s={6} label="Prodi" name="prodi" value={this.state.prodi} onChange={this.handleChange} required>
                                <option value='0'>Pilih Prodi</option>
                                {this.state.prodies.filter(prodi => prodi.faculty_id == this.state.faculty).map((prodi, index) =>{
                                    return(
                                        <option key={index} value={prodi.id}>{prodi.prodi}</option>
                                    )
                                })
                                }
                            </Select>
                            <Select s={6} label="Angkatan" name="angkatan" value={this.state.angkatan} onChange={this.handleChange} required>
                                <option value='0'>Pilih Angkatan</option>
                                {this.createOption()}
                            </Select>
                            {
                                !this.state.changePassword && <Button onClick={this.onChangePassword}>Ganti Password</Button>
                            }
                            {
                                this.state.changePassword && <div>
                                    <TextInput type="password" s={12} label="Password" name="password" value={this.state.password} onChange={this.handleChange} required/>
                                    <TextInput type="password" s={12} label="Konfirmasi Password" name="confirm_password" value={this.state.confirm_password} onChange={this.handleChange} required/>
                                    <Button className='red' onClick={this.onChangePassword}>Batalkan</Button>
                                </div>
                            }
                        </Row>
                        <Button waves='light' type='submit' onClick={this.onSubmit}>Register</Button>
                        {this.state.errors != '' ?
                            <div>
                                <p>Error:</p>
                                {this.state.errors.map(error => (
                                    <p key={error}>{error}</p>
                                ))}
                            </div>
                            :
                            null
                        }
                    </form>
                </div>
            </div>
        )
    }
}

if(document.getElementById('edit_murabbi')) {
    const element = document.getElementById('edit_murabbi');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Edit {...props} />, document.getElementById('edit_murabbi'));
}
