import React from 'react'
import ReactDOM from 'react-dom';
import {Row, Col, TextInput,  Button, Collection, CollectionItem} from 'react-materialize'
import axios from 'axios'
import Select from 'react-select';

export default class Transfer extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            faculties: [],
            prodies: [],
            name: '',
            username: '',
            email: '',
            gender: '',
            faculty: '',
            prodi: '',
            angkatan: '',
            role: '1',
            password: '',
            confirm_password: '',
            errors: [],
            year: '',
            users: [],
            mentors_id: '',
            group_name: '',
            groups_id: '',
            new_mentors_id: '',
            new_groups: [],
            new_groups_id: '',
            id: '',
        };
        console.log(super());
    }

    componentDidMount() {
        let id = window.location.pathname.split('/')[4];
        let today = new Date();
        let tanggal = today.getDate();
        if(tanggal <= 10){
            tanggal = '0' + tanggal;
        }
        let sdate = tanggal + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
        axios.get('/api/faculty')
            .then(response => {
                this.setState({faculties: response.data});
            });

        axios.get('/api/prodi')
            .then(response => {
                this.setState({prodies: response.data});
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });

        axios.get('/api/user/'+id)
            .then(response => {
                this.setState({
                    id: parseInt(id),
                    name: response.data.name,
                    mentors_id: response.data.mentors_id,
                    group_name: response.data.group_name,
                    groups_id: response.data.groups_id,
                    username: response.data.username,
                    email: response.data.email,
                    gender: response.data.gender,
                    prodi: response.data.prodi_id,
                    angkatan: response.data.angkatan,
                    faculty: this.state.prodies.filter(prodi => prodi.id == response.data.prodi_id).map(prodi => prodi.faculty_id)
                });

            });

        let year = today.getFullYear();

        this.setState({
            year: year
        })
    }

    getFacultyId(prodi_id){

    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty('name')) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    handleChangeSelect = (event, {name, value}) => {
        if (this.state.hasOwnProperty(name)) {
            this.setState({ [name]: event.value });
        }

    };

    handleChangeNewMentor = (event, {name, value}) => {
        console.log(event, event.name, event.value);
        if (this.state.hasOwnProperty('name')) {
            this.setState({[event.name]: event.value});

            axios.get('/api/group/murabbi/'+event.value)
                .then(response => {
                    this.setState({new_groups: response.data});
                });
        }
    };

    handleChangeNewGroup = (event, {name, value}) => {
        console.log(event, event.name, event.value);
        if (this.state.hasOwnProperty('name')) {
            this.setState({[event.name]: event.value});
        }
    };

    validate(name, username, email, gender, faculty, role, prodi, password, confirm_password, angkatan){
        const errors = [];

        if(name.length === 0){
            errors.push('Nama tidak boleh kosong');
        }

        if(username.length === 0){
            errors.push('Username tidak boleh kosong');
        }

        if(email.length === null){
            errors.push('Email tidak boleh kosong');
        }

        if(gender.length === 0){
            errors.push('Jenis Kelamin tidak boleh kosong');
        }

        if(faculty.length === 0){
            errors.push('Fakultas tidak boleh kosong');
        }

        if(prodi.length === null){
            errors.push('Prodi tidak boleh kosong');
        }

        if(role.length === 0){
            errors.push('Role tidak boleh kosong');
        }

        if(angkatan.length === 0){
            errors.push('Angkatan tidak boleh kosong');
        }

        if(password.length === 0){
            errors.push('Password tidak boleh kosong');
        }

        if(confirm_password !== password) {
            errors.push('Confirm Password tidak sama');
        }

        return errors;
    }


    createOption () {
        let option = [];
        let year = this.state.year;

        for (let i = 0; i <= 10; i++) {
            option.push(<option key={i} value={year}>{year}</option>);
            year = year - 1;
        }
        return option
    }

    onSubmit(e){
        e.preventDefault();

        const transfer =
            {
                new_groups_id: this.state.new_groups_id,
            };

        let id = window.location.pathname.split('/')[4];

        axios.post('/api/user/transfer/'+id, transfer)
            .then(response => window.location = '/admin/data_murabbi')
            .catch();
    }

    getName(id){
        id = parseInt(id);
        let name = this.state.users.filter(user => user.id === id).map(user => user.name);
        return name[0];
    }

    render() {
        console.log(this.state.new_mentors_id);

        let new_mentor = this.state.users.filter(user=>user.is_mentor === 1 && user.id !== this.state.mentors_id && user.id !== this.state.id && user.gender === this.state.gender).map(user=>({
            label: user.name,
            value: user.id,
            name: 'new_mentors_id'
        }));

        let new_group = this.state.new_groups.map(group=>({
            label: group.group_name,
            value: group.id,
            name: 'new_groups_id'
        }));

        return (
            <div className="container">
                <h2>Transfer Binaan</h2>
                <div className="row">
                    <form className="col s12" method="post" action="#" role="form">
                        <Row>
                            <Col s={2}>Nama</Col>
                            <Col s={10}>: {this.state.name}</Col>
                        </Row>
                        <Row>
                            <Col s={2}>Nama Pembina</Col>
                            <Col s={10}>: {this.getName(this.state.mentors_id)}</Col>
                        </Row>
                        <Row>
                            <Col s={2}>Nama Kelompok</Col>
                            <Col s={10}>: {this.state.group_name}</Col>
                        </Row>
                        <Row>
                            <Col>Transfer Ke:</Col>
                        </Row>
                        <Row>
                            <Col s={6}>
                            <Select placeholder="Nama Pembina Baru" options={new_mentor} value={this.state.new_mentors_id.value} onChange={this.handleChangeNewMentor} name='new_mentors_id'/>
                            </Col>
                            <Col s={6}>
                                <Select placeholder="Nama Kelompok Baru" options={new_group} value={this.state.new_groups_id.value} onChange={this.handleChangeNewGroup} name='new_groups_id'/>
                            </Col>
                            {/*<Select placeholder='Keterlaksanaan Mentoring' options={pilihan} value={this.state.done.value} onChange={this.handleChangeSelect} name='done'/>*/}
                            {/*<Select s={6} label="Nama Pembina Baru" name="new_mentors_id" value={this.state.new_mentors_id} onClick={(e)=>console.log(e)} onChange={this.handleChangeNewMentor} required>*/}
                            {/*    <option value='0'>Pilih Pembina Baru</option>*/}
                            {/*    {this.state.users.filter(user=>user.is_mentor === 1 && user.id !== this.state.mentors_id && user.id !== this.state.id && user.gender === this.state.gender).map((user, index) =>{*/}
                            {/*        return(*/}
                            {/*            <option key={index} value={user.id}>{user.name}</option>*/}
                            {/*        )*/}
                            {/*    })*/}
                            {/*    }*/}
                            {/*</Select>*/}
                            {/*<Select s={6} label="Nama Kelompok Baru" name="new_groups_id" value={this.state.new_groups_id} onClick={(e)=>console.log(e)} onChange={this.handleChange} required>*/}
                            {/*    <option value='0'>Pilih Kelompok Baru</option>*/}
                            {/*    {this.state.new_groups.map((group, index) =>{*/}
                            {/*        return(*/}
                            {/*            <option key={index} value={group.id}>{group.group_name}</option>*/}
                            {/*        )*/}
                            {/*    })*/}
                            {/*    }*/}
                            {/*</Select>*/}
                        </Row>
                        {
                            this.state.new_mentors_id.length === 0 || this.state.new_groups_id.length === 0 ?
                                (
                                    <Button waves='light' type='submit' disabled>Submit</Button>
                                )
                                :
                                (
                                    <Button waves='light' type='submit' onClick={this.onSubmit}>Submit</Button>
                                )
                        }
                    </form>
                </div>
            </div>
        )
    }
}

if(document.getElementById('transfer_murabbi')) {
    const element = document.getElementById('transfer_murabbi');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Transfer {...props} />, document.getElementById('transfer_murabbi'));
}
