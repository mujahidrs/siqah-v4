import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Icon} from 'react-materialize'
import axios from 'axios'

export default class Qadhaya extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            year: '',
            month: '',
            pekan: [1,2,3,4,5],
            presences: [],
            users: [],
            groups: [],
            group_name: '',
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;
        var currentWeekNumber = require('current-week-number');
        let groups_id = window.location.pathname.split('/')[5];
        let today = new Date();
        let year = today.getFullYear();
        let month = today.getMonth() + 1;
        if(month <= 10){
            month = '0' + month;
        }

        let tanggal = today.getDate();
        if(tanggal <= 10){
            tanggal = '0' + tanggal;
        }

        let cdate = today.getFullYear() + '-' + month + '-' + tanggal;

        let dates = cdate.substr(8, 2);
        let month2 = cdate.substr(5, 2);
        let year2 = cdate.substr(0, 4);
        // let newdate = '2018-10-1';
        let week = currentWeekNumber(cdate);

        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        axios.get('/api/group/'+groups_id)
            .then(response => {
                this.setState({groups: response.data});
            });

        axios.get('/api/presence')
            .then(response => {
                this.setState({presences: response.data});
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });

        axios.get('/api/group/'+groups_id)
            .then(response => {
                this.setState({
                    group_name: response.data.group_name
                });
            });

        if (month === '01'){
            var pekan = week;
            var bulan = month;
            if(pekan === 0){
                pekan = 1;
                bulan = '1'
            }
            else{
                pekan = week;
                bulan = month;
            }
        }
        else if (month === '02'){
            var pekan = week-5;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '1'
            }
            else{
                pekan = week-5;
                bulan = month;
            }
        }
        else if (month === '03'){
            var pekan = week-9;
            var bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '2'
            }
            else{
                pekan = week-9;
                bulan = month;
            }
        }
        else if (month === '04'){
            var pekan = week-13;
            var bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '3'
            }
            else{
                pekan = week-13;
                bulan = month;
            }
        }
        else if (month === '05'){
            var pekan = week-18;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '4'
            }
            else{
                pekan = week-18;
                bulan = month;
            }
        }
        else if (month === '06'){
            var pekan = week-22;
            var bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '5'
            }
            else{
                pekan = week-22;
                bulan = month;
            }
        }
        else if (month === '07'){
            var pekan = week-26;
            var bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '6'
            }
            else{
                pekan = week-26;
                bulan = month;
            }
        }
        else if (month === '08'){
            var pekan = week-31;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '7'
            }
            else{
                pekan = week-31;
                bulan = month;
            }
        }
        else if (month === '09'){
            var pekan = week-35;
            var bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '8'
            }
            else{
                pekan = week-35;
                bulan = month;
            }
        }
        else if (month === '10'){
            var pekan = week-39;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '9'
            }
            else{
                pekan = week-39;
                bulan = month;
            }
        }
        else if(month === '11'){
            var pekan = week-44;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '10';
            }
            else{
                pekan = week-44;
                bulan = month;
            }
        }
        else if(month === '12'){
            var pekan = week-48;
            var bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '11';
            }
            else{
                pekan = week-48;
                bulan = month;
            }
        }

        this.setState({
            year: year2,
            month: parseInt(bulan)
        })
    }

    prevMonth = () => {
        var bulan = this.state.month;
        if (bulan <= 1){
            this.setState({
                month : 12,
                year : this.state.year-1
            });
        }
        else{
            this.setState({
                month : this.state.month-1
            });
        }
    }

    nextMonth = () => {
        var bulan = this.state.month;
        if (bulan >= 12){
            this.setState({
                month : 1,
                year : this.state.year+1
            });
        }
        else{
            this.setState({
                month : this.state.month+1
            });
        }
    }

    getPresencesId(groups_id, week){
        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }
        let presences_id = this.state.presences.filter(presence=>presence.groups_id == groups_id && presence.week == week && presence.month == monthNumToName(this.state.month) && presence.year == this.state.year).map(presence=>presence.presences_id);
        return presences_id[0];
    }

    getQadhaya(groups_id, week){
        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }
        let qadhaya = this.state.presences.filter(presence=>presence.groups_id == groups_id && presence.week == week && presence.month == monthNumToName(this.state.month) && presence.year == this.state.year).map(presence=>presence.group_notes);
        return qadhaya[0];
    }

    getHariTanggal(groups_id, week){
        let date = require('locutus/php/datetime/date');
        let strtotime = require('locutus/php/datetime/strtotime');
        let months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];
        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }
        let tanggal = this.state.presences.filter(presence=>presence.groups_id == groups_id && presence.week == week && presence.month == monthNumToName(this.state.month) && presence.year == this.state.year).map(presence=>presence.date);
        tanggal = tanggal.toString();
        tanggal = tanggal.substr(0,10);

        let tgl = tanggal.substr(8,2);
        let bln = tanggal.substr(5,2);
        let thn = tanggal.substr(0,4);
        let a = strtotime(tanggal);
        let hari = date('l', a);

        if(a != false){
            return hari + ', ' + tgl + ' ' + monthNumToName(bln) + ' ' + thn;
        }
    }

    getName(id){
        let name = this.state.users.filter(user => user.id == id).map(user => user.name);
        return name[0];
    }

    render() {
        let groups_id = window.location.pathname.split('/')[4];

        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        return (
            <div className="container">
                <h3>Qadhaya Kelompok</h3>
                Nama : {this.getName(this.props.murabbi)}<br/>
                Nama Kelompok: {this.state.groups.group_name}<br/>
                Kelas: {this.state.groups.levels_id}<br/>
                <table className="bordered striped">
                    <thead>
                    <tr>
                        <th colSpan='4' className='center aligned'><a onClick={this.prevMonth}><Icon small>chevron_left</Icon></a>{monthNumToName(this.state.month)} {this.state.year}<a onClick={this.nextMonth}><Icon small>chevron_right</Icon></a></th>
                    </tr>
                    <tr>
                        <th className='center aligned'>Pekan</th>
                        <th className='center aligned'>Hari, Tanggal</th>
                        <th className='center aligned'>Qadhaya</th>
                        <th className='center aligned'>Tindakan</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.pekan.map((pekan, index) => {
                            return (
                                <tr key={index}>
                                    <td className='center aligned'>{pekan}</td>
                                    <td className='center aligned'>{this.getHariTanggal(this.state.groups.id,pekan)}</td>
                                    <td className='center aligned'>{this.getQadhaya(this.state.groups.id,pekan)}</td>
                                    <td className='center aligned'>
                                        {this.getQadhaya(this.state.groups.id,pekan) != null ?
                                            <a
                                                title='Personal'
                                                className="waves-effect waves-light btn-small"
                                                href={this.props.prefix + '/data_kelompok/'+this.props.murabbi+'/qadhaya/'+this.state.groups.id+'/personal/'+this.getPresencesId(this.state.groups.id,pekan)}>
                                                <i className="material-icons">
                                                    account_circle
                                                </i>
                                            </a>
                                            : null
                                        }

                                    </td>
                                </tr>
                            )
                        })
                    }

                    </tbody>
                </table>
            </div>
        )
    }
}

if(document.getElementById('qadhaya')) {
    const element = document.getElementById('qadhaya');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Qadhaya {...props} />, document.getElementById('qadhaya'));
}
