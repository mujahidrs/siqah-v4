import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Col} from 'react-materialize'
import axios from 'axios'

export default class CapaianMadah extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            groups: [],
            grup: [],
            subjects: [],
            users: [],
            presences: [],
            gender_user: '',
            gender: 0,
        };
        console.log(super());
    };

    componentDidMount() {
        let groups_id = window.location.pathname.split('/')[5];
        axios.get('/api/user/'+window.location.pathname.split('/')[3])
            .then(response => {
                this.setState({gender_user: response.data.gender});
                this.setState({
                    gender: this.state.gender_user
                })
            });

        axios.get('/api/group/'+groups_id)
            .then(response => {
                this.setState({groups: response.data});
            });

        axios.get('/api/group/')
            .then(response => {
                this.setState({grup: response.data});
            });

        axios.get('/api/presence')
            .then(response => {
                this.setState({presences: response.data});
            });

        axios.get('/api/subject')
            .then(response => {
                this.setState({subjects: response.data});
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });
    }

    getName(id){
        let name = this.state.users.filter(user => user.id == id).map(user => user.name);
        return name[0];
    }

    getMurabbi(groups_id){
        let murabbi = this.state.grup.filter(group => group.id == groups_id).map(group => group.mentors_id);
        return murabbi[0];
    }

    getKelompok(groups_id){
        let group_name = this.state.grup.filter(group => group.id == groups_id).map(group => group.group_name);
        return group_name[0];
    }

    getLevel(groups_id){
        let level = this.state.grup.filter(group => group.id == groups_id).map(group => group.levels_id);
        return level[0];
    }

    getSubject(id){
        let subject = this.state.subjects.filter(subject => subject.id == id).map(subject => subject.subject);
        return subject[0];
    }

    getHariTanggal(tanggal){
        let date = require('locutus/php/datetime/date');
        let strtotime = require('locutus/php/datetime/strtotime');
        let months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];
        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }
        let tgl = tanggal.substr(8,2);
        let bln = tanggal.substr(5,2);
        let thn = tanggal.substr(0,4);
        let a = strtotime(tanggal);
        let hari = date('l', a);
        return hari + ', ' + tgl + ' ' + monthNumToName(bln) + ' ' + thn;
    }

    render() {
        let mutarabbi_id = window.location.pathname.split('/')[7];

        return (
            <div className="container">
                <h3>Capaian Madah</h3>
                Nama Murabbi: {this.getName(this.state.groups.mentors_id)}<br/>
        Nama Binaan: {this.getName(mutarabbi_id)}<br/>
        Nama Kelompok: {this.state.groups.group_name}<br/>
        Angkatan Kelompok: {this.state.groups.angkatan}<br/>
        Kelas: {this.state.groups.levels_id}<br/>
        <div className="table-responsive">
            <table className="bordered striped">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Madah</th>
                        <th>Keterangan</th>
                        <th>Hari, tanggal</th>
                        <th>Nama Murabbi</th>
                        <th>Nama Kelompok</th>
                        <th>Kelas</th>
                    </tr>
                </thead>
                <tbody>
                {
                    this.state.presences.filter(presence=>presence.users_id == mutarabbi_id && presence.mentee_presence == '1').map((presence, index) =>{
                        return(
                            <tr key={index}>
                                <td>{index+1}</td>
                                <td>{this.getSubject(presence.subjects_id)}</td>
                                <td>{presence.personal_details}</td>
                                <td>{this.getHariTanggal(presence.date)}</td>
                                <td>{this.getName(this.getMurabbi(presence.groups_id))}</td>
                                <td>{this.getKelompok(presence.groups_id)}</td>
                                <td>{this.getLevel(presence.groups_id)}</td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        </div>
            </div>
        )
    }
}

if(document.getElementById('capaian_madah')) {
    const element = document.getElementById('capaian_madah');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<CapaianMadah {...props} />, document.getElementById('capaian_madah'));
}
