import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Icon, Col} from 'react-materialize'
import axios from 'axios'

export default class MadahPersonal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            year: '',
            month: '',
            week: '',
            date: '',
            presences: [],
            users: [],
            subjects: [],
            groups: [],
            group_name: '',
        };
        console.log(super());
    };

    componentDidMount() {
        let groups_id = window.location.pathname.split('/')[5];
        let presences_id = window.location.pathname.split('/')[7];

        axios.get('/api/group/'+groups_id)
            .then(response => {
                this.setState({groups: response.data});
            });

        axios.get('/api/presence/lihat/'+presences_id)
            .then(response => {
                this.setState({
                    presences: response.data,
                    week: response.data[0].week,
                    date: response.data[0].date,
                    year: response.data[0].year
                });
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });

        axios.get('/api/subject')
            .then(response => {
                this.setState({subjects: response.data});
            });

        axios.get('/api/group/'+groups_id)
            .then(response => {
                this.setState({
                    group_name: response.data.group_name
                });
            });
    }

    getSubject(subjects_id){
        let subject = this.state.subjects.filter(subject=>subject.id == subjects_id).map(subject=>subject.subject);
        return subject[0];
    }

    getHariTanggal(){
        let date = require('locutus/php/datetime/date');
        let strtotime = require('locutus/php/datetime/strtotime');
        let months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];
        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        let tanggal = this.state.presences.map(presence=>presence.date);
        tanggal = tanggal.toString();
        tanggal = tanggal.substr(0,10);

        let tgl = tanggal.substr(8,2);
        let bln = tanggal.substr(5,2);
        let thn = tanggal.substr(0,4);
        let a = strtotime(tanggal);
        let hari = date('l', a);

        if(a != false){
            return hari + ', ' + tgl + ' ' + monthNumToName(bln) + ' ' + thn;
        }
    }

    getName(id){
        let name = this.state.users.filter(user => user.id == id).map(user => user.name);
        return name[0];
    }

    getDatePresences(){
        let date = this.state.presences.map(presences=>presences.date);
        return date;
    }

    render() {
        let groups_id = window.location.pathname.split('/')[5];
        let presences_id = window.location.pathname.split('/')[7];

        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        let pekan = this.state.presences.map(presence=>presence.week);
        let madah = this.state.presences.map(presence=>presence.subjects_id);
        let qadhaya = this.state.presences.map(presence=>presence.group_notes);
        let date = this.state.presences.map(presence=>presence.date);
        return (
            <div className="container">
                <h2>Madah Personal</h2>
                <Row>
                    <Col>
                        Nama <br/>
                        Nama Kelompok<br/>
                        Kelas<br/>
                        Hari, tanggal<br/>
                        Pekan<br/>
                        Bulan<br/>
                        Tahun<br/>
                        Madah<br/>
                        Qadhaya Kelompok
                    </Col>
                    <Col>
                        : {this.getName(this.props.murabbi)}<br/>
                        : {this.state.groups.group_name}<br/>
                        : {this.state.groups.levels_id}<br/>
                        : {this.getHariTanggal()}<br/>
                        : {this.state.week}<br/>
                        : {monthNumToName(this.state.date.substr(5,2))}<br/>
                        : {this.state.year}<br/>
                        : {this.getSubject(madah[0])}<br/>
                        : {qadhaya[0]}
                    </Col>
                </Row>
                <h4>Yang Hadir:</h4>
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Binaan</th>
                            <th>Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.presences.filter(presence=>presence.mentee_presence == 1).map((presence,index)=>{
                            return (
                                <tr key={index}>
                                    <td>{index+1}</td>
                                    <td>{this.getName(presence.users_id)}</td>
                                    <td>{presence.personal_details}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                    <br/>
                </div>
                <h4>Yang Tidak Hadir:</h4>
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Binaan</th>
                            <th>Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.presences.filter(presence=>presence.mentee_presence == 0).map((presence,index)=>{
                            return (
                                <tr key={index}>
                                    <td>{index+1}</td>
                                    <td>{this.getName(presence.users_id)}</td>
                                    <td>{presence.personal_details}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                    <br/>
                </div>
            </div>
        )
    }
}

if(document.getElementById('madah_personal')) {
    const element = document.getElementById('madah_personal');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<MadahPersonal {...props} />, document.getElementById('madah_personal'));
}
