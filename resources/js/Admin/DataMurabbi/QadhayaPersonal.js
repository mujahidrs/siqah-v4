import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Icon} from 'react-materialize'
import axios from 'axios'

export default class QadhayaPersonal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            year: '',
            month: '',
            week: '',
            date: '',
            presences: [],
            users: [],
            groups: [],
            group_name: '',
        };
        console.log(super());
    };

    componentDidMount() {
        let groups_id = window.location.pathname.split('/')[5];
        let presences_id = window.location.pathname.split('/')[7];

        axios.get('/api/group/'+groups_id)
            .then(response => {
                this.setState({groups: response.data});
            });

        axios.get('/api/presence/lihat/'+presences_id)
            .then(response => {
                this.setState({
                    presences: response.data,
                    week: response.data[0].week,
                    date: response.data[0].date,
                    year: response.data[0].year
                });
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });

        axios.get('/api/group/'+groups_id)
            .then(response => {
                this.setState({
                    group_name: response.data.group_name
                });
            });
    }

    getHariTanggal(){
        let date = require('locutus/php/datetime/date');
        let strtotime = require('locutus/php/datetime/strtotime');
        let months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];
        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }
        let tanggal = this.state.presences.map(presence=>presence.date);
        tanggal = tanggal.toString();
        tanggal = tanggal.substr(0,10);

        let tgl = tanggal.substr(8,2);
        let bln = tanggal.substr(5,2);
        let thn = tanggal.substr(0,4);
        let a = strtotime(tanggal);
        let hari = date('l', a);

        if(a != false){
            return hari + ', ' + tgl + ' ' + monthNumToName(bln) + ' ' + thn;
        }
    }

    getName(id){
        let name = this.state.users.filter(user => user.id == id).map(user => user.name);
        return name[0];
    }

    getDatePresences(){
        let date = this.state.presences.map(presences=>presences.date);
        return date;
    }

    render() {
        let groups_id = window.location.pathname.split('/')[5];
        let presences_id = window.location.pathname.split('/')[7];

        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        let pekan = this.state.presences.map(presence=>presence.week);
        let qadhaya = this.state.presences.map(presence=>presence.group_notes);
        let date = this.state.presences.map(presence=>presence.date);
        return (
            <div className="container">
                <h2>Qadhaya Personal</h2>
                Nama : {this.getName(this.props.murabbi)}<br/>
                Nama Kelompok: {this.state.groups.group_name}<br/>
                Kelas: {this.state.groups.levels_id}<br/>
                Hari, tanggal: {this.getHariTanggal()}<br/>
                Pekan: {this.state.week}<br/>
                Bulan: {monthNumToName(this.state.date.substr(5,2))}<br/>
                Tahun: {this.state.year}<br/>
                Qadhaya Kelompok: {qadhaya[0]}<br/>
                <h4>Yang Hadir:</h4>
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Binaan</th>
                                <th>Hadir</th>
                                <th>Qadhaya Personal</th>
                            </tr>
                        </thead>
                        <tbody>
                        {this.state.presences.filter(presence=>presence.mentee_presence == 1).map((presence,index)=>{
                            return (
                                <tr key={index}>
                                    <td>{index+1}</td>
                                    <td>{this.getName(presence.users_id)}</td>
                                    <td>{presence.mentee_presence}</td>
                                    <td>{presence.personal_details}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                    <br/>
                </div>
                <h4>Yang Tidak Hadir:</h4>
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Binaan</th>
                            <th>Hadir</th>
                            <th>Qadhaya Personal</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.presences.filter(presence=>presence.mentee_presence == 0).map((presence,index)=>{
                            return (
                                <tr key={index}>
                                    <td>{index+1}</td>
                                    <td>{this.getName(presence.users_id)}</td>
                                    <td>{presence.mentee_presence}</td>
                                    <td>{presence.personal_details}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                    <br/>
                </div>
            </div>
        )
    }
}

if(document.getElementById('qadhaya_personal')) {
    const element = document.getElementById('qadhaya_personal');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<QadhayaPersonal {...props} />, document.getElementById('qadhaya_personal'));
}
