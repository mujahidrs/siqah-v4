import React from 'react';
import ReactDOM from 'react-dom';
import {TextInput, Button, Col, Icon, Row, Modal, Select} from 'react-materialize';
import axios from 'axios';
import BuatAkun from "../../components/BuatAkun";
import SearchableSelect from "../../components/SearchableSelect";
import scrollToComponent from "react-scroll-to-component";

function ButtonTransfer(props) {
    return(
        <Button
            tooltip="Transfer"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="indigo"
            waves="light"
            icon={<Icon>transfer_within_a_station</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonDelete(props) {
    return(
        <Button
            tooltip="Hapus"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="red"
            waves="light"
            icon={<Icon>delete</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonCreateUser(props) {
    return(
        <Button
            tooltip="Buat Akun"
            floating
            small
            className="blue"
            waves="light"
            icon={<Icon>person_add</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonEditUser(props) {
    return(
        <Button
            tooltip="Edit Akun"
            style={{marginLeft: 5}}
            floating
            small
            className="blue"
            waves="light"
            icon={<Icon>edit</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonSetMentor(props) {
    return(
        <Button
            tooltip="Atur Status Pembina"
            style={{marginLeft: 5}}
            floating
            small
            className="green"
            waves="light"
            icon={<Icon>settings</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonShowGroups(props) {
    return(
        <Button
            tooltip="Lihat Kelompok"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="purple"
            waves="light"
            icon={<Icon>group</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonShowProfile(props) {
    return(
        <Button
            tooltip="Lihat Profil"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            waves="light"
            className="modal-trigger brown"
            href="#modal1"
            icon={<Icon>remove_red_eye</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonEditProfile(props) {
    return(
        <Button
            tooltip="Edit Profil"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="light-blue"
            waves="light"
            icon={<Icon>edit</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonActivate(props) {
    return(
        <Button
            tooltip="Aktifkan"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="pink"
            waves="light"
            icon={<Icon>highlight</Icon>}
            onClick={props.onClick}
        />
    )
}

function ButtonUnactivate(props) {
    return(
        <Button
            tooltip="Nonaktifkan"
            style={{marginLeft: 2.5, marginRight: 2.5}}
            floating
            small
            className="pink"
            waves="light"
            icon={<Icon>highlight_off</Icon>}
            onClick={props.onClick}
        />
    )
}

export default class DataMutarabbi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            murabbi: [],
            mutarabbi: [],
            users: [],
            groups: [],
            gender_user: '',
            prodi: '',
            faculty: '',
            mentee_name: '',
            angkatan_mentee: '',
            prodies: [],
            gender: 0,
            jumlah: 0,
            isHidden: true,
            isHidden2: true,
            isHidden3: true,
            isHidden4: true,
            faculties: [],
            year: '',
            username: '',
            username_edit: '',
            email: '',
            email_edit: '',
            password: '',
            password_edit: '',
            confirm_password: '',
            confirm_password_edit: '',
            mentee_id: '',
            changePassword: false,
            showProfile: false,
            clicked: false,
            iterasi: [0,1,2,3,4,5,6,7,8,9,10]
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;
        let groups_id = window.location.pathname.split('/')[5];
        axios.get('/api/user/murabbi')
            .then(response => {
                this.setState({murabbi: response.data});
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });

        axios.get('/api/user/'+window.location.pathname.split('/')[3])
            .then(response => {
                this.setState({gender_user: response.data.gender});
                this.setState({
                    gender: this.state.gender_user
                })
            });

        axios.get('/api/prodi')
            .then(response => {
                this.setState({prodies: response.data});
            });

        axios.get('/api/group/'+groups_id)
            .then(response => {
                this.setState({groups: response.data});
            });

        axios.get('/api/faculty')
            .then(response => {
                this.setState({faculties: response.data});
            });

        axios.get('/api/group/mutarabbi/'+groups_id)
            .then(response => {
                this.setState({mutarabbi: response.data});
            });

        let today = new Date();
        let tanggal = today.getDate();
        if(tanggal <= 10){
            tanggal = '0' + tanggal;
        }
        let sdate = tanggal + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
        let year = today.getFullYear();

        this.setState({
            year: year
        })
    }

    getName(id){
        id = parseInt(id);
        let name = this.state.users.filter(user => user.id === id).map(user => user.name);
        return name[0];
    }

    getProdi(id){
        let prodi = this.state.users.filter(user => user.id === id).map(user => user.prodi_id);
        return prodi[0];
    }

    getAngkatan(id){
        let angkatan = this.state.users.filter(user => user.id === id).map(user => user.angkatan);
        return angkatan[0];
    }

    getJumlahMurabbi(gender){
        let jumlah = 0;
        jumlah = this.state.murabbi.filter(murabbi => murabbi.gender === gender).map(murabbi=>murabbi.id);
        return jumlah.length;
    }

    getNamaProdi(prodi_id){
        let nama_prodi = '';
        nama_prodi = this.state.prodies.filter(prodi => prodi.id === prodi_id).map(prodi=>prodi.prodi);
        return nama_prodi[0];
    }

    getNamaFakultas(faculty_id){
        let nama_fakultas = '';
        nama_fakultas = this.state.faculties.filter(faculty => faculty.id === faculty_id).map(faculty=>faculty.faculty);
        return nama_fakultas[0];
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    handleChangeFaculty = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.name]: parseInt(event.value)});
        }
        this.setState({
            prodi: {value: ''},
            prodi_edit: {value: ''},
        })
    };

    handleChangeProdi = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.name]: parseInt(event.value)});
        }
    };

    handleChangeAngkatan = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.name]: parseInt(event.value)});
        }
    };

    getFakultas(prodi_id){
        let faculty = this.state.prodies.filter(prodi=> prodi.id === prodi_id).map(prodi=>prodi.faculty_id);
        return faculty[0];
    }

    onDelete = (users_id) => {
        let result = confirm('Hapus user mutarabbi ini?');
        if(result === true){
            axios.delete('/api/user/delete/'+users_id)
                .then(response=>{
                    let mutarabbi = this.state.mutarabbi;

                    for(let i=0; i< mutarabbi.length; i++)
                    {
                        if(mutarabbi[i].id === users_id)
                        {
                            mutarabbi.splice(i,1);
                            this.setState({
                                mutarabbi:mutarabbi
                            })
                        }
                    }
                });
        }
    };

    nonAktifkan = (users_id) => {
        let result = confirm('Non-Aktifkan user mutarabbi ini?');
        let groups_id = window.location.pathname.split('/')[5];
        if(result === true){
            axios.put('/api/user/nonaktif/'+users_id)
                .then(response=>{
                    window.alert("Berhasil Non-Aktifkan!");
                    axios.get('/api/group/mutarabbi/'+groups_id)
                            .then(response => {
                                this.setState({mutarabbi: response.data});
                            });
                })
                .catch();
        }
    };

    Aktifkan = (users_id) => {
        let result = confirm('Aktifkan user mutarabbi ini?');
        let groups_id = window.location.pathname.split('/')[5];
        if(result === true){
            axios.put('/api/user/aktif/'+users_id)
                .then(response=>{
                    window.alert("Berhasil Aktifkan!");
                    axios.get('/api/group/mutarabbi/'+groups_id)
                            .then(response => {
                                this.setState({mutarabbi: response.data});
                            });
                })
                .catch();
        }
    };

    onAdd = () => {
        this.setState({
            isHidden: false,
            isHidden2: true,
            isHidden3: true,
            isHidden4: true,
        })
    };

    onCreate = (id) => {
        this.setState({
            isHidden: true,
            isHidden2: true,
            isHidden3: false,
            isHidden4: true,
            mentee_id: id
        })

        scrollToComponent(this.refs.scrollToThis);
    };

    onEditAccount = (users_id) => {
        const users = this.state.users.filter(user=>user.id === users_id).map((user) => {
            return (
                {
                    mentee_id : user.id,
                    username_edit : user.username,
                    email_edit : user.email,
                    password_edit: '',
                    confirm_password_edit: '',
                }
            )
        });

        this.setState({
            isHidden4: false,
            isHidden3: true,
            isHidden2: true,
            isHidden: true,
            mentee_id : users[0].mentee_id,
            username_edit : users[0].username_edit,
            email_edit : users[0].email_edit,
            password_edit : users[0].password_edit,
            confirm_password_edit : users[0].confirm_password_edit,
        });

        scrollToComponent(this.refs.scrollToThis);
    };

    onEditIsMentor = (users_id, is_mentor) => {
        let groups_id = window.location.pathname.split('/')[5];
        if(is_mentor === 0){
            let result = confirm('Jadikan sebagai Pembina?');
            if(result === true){
                axios.post('/api/setMentor/'+users_id)
                    .then(response=>{
                        console.log(response);
                        if(response.data.success === true){
                            axios.get('/api/group/mutarabbi/'+groups_id)
                            .then(response => {
                                this.setState({mutarabbi: response.data});
                            });
                        }
                    });
            }
        }
        else{
            let result = confirm('Lepas dari status Pembina?');
            if(result === true){
                axios.post('/api/removeMentor/'+users_id)
                    .then(response=>{
                        console.log(response);
                        if(response.data.success === true){
                            axios.get('/api/user')
                            .then(response => {
                                this.setState({users: response.data});
                            });
                        }
                    });
            }
        }
    };

    onClose = () => {
        this.setState({
            isHidden: true
        })
    };

    onClose2 = () => {
        this.setState({
            isHidden2: true
        })
    };

    onClose3 = () => {
        this.setState({
            isHidden3: true
        })
    };

    onClose4 = () => {
        this.setState({
            isHidden4: true
        })
    };

    onEdit = (users_id) => {
        const users = this.state.users.filter(user=>user.id === users_id).map((user) => {
            return (
                {
                    users_id_edit : user.id,
                    mentee_name_edit : user.name,
                    prodi_edit: user.prodi_id,
                    angkatan_mentee_edit: user.angkatan,
                }
            )
        });

        // console.log(users[0].prodi_edit);

        this.setState({
            isHidden4: true,
            isHidden3: true,
            isHidden2: false,
            isHidden: true,
            users_id_edit : users[0].users_id_edit,
            mentee_name_edit : users[0].mentee_name_edit,
            prodi_edit : users[0].prodi_edit,
            angkatan_mentee_edit : users[0].angkatan_mentee_edit,
            faculty_edit : this.getFakultas(users[0].prodi_edit),
        })
    };

    createOption () {
        let option = [];
        let year = this.state.year;

        for (let i = 0; i <= 10; i++) {
            option.push(<option key={i} value={year}>{year}</option>);
            year = year - 1;
        }
        return option
    }

    onSubmit = (e) => {
        e.preventDefault();

        if(this.state.mentee_name.length !== 0 && this.state.prodi.length !== 0 && this.state.angkatan_mentee.length !== 0){
            this.setState({
                clicked: true,
            })

            const group =
                {
                    mentee_name: this.state.mentee_name,
                    mentors_id : this.props.id,
                    group_name : this.state.groups.group_name,
                    levels_id : this.state.groups.levels_id,
                    angkatan: this.state.angkatan_mentee,
                    groups_id: this.state.groups.id,
                    role : 1,
                    password : this.state.mentee_name.replace(/[^A-Z0-9]/ig, "").toLowerCase(),
                    username : null,
                    email : null,
                    status : 'Aktif',
                    is_mentor : 0,
                    gender : this.state.gender,
                    prodi_id : this.state.prodi,
                };


            console.log(group);

            axios.post('/api/group/storeMenteePersonal', group)
                .then(response => window.location.reload())
                .catch(error=>console.log(error));
        }
        else{
            window.alert('Isian belum lengkap');
        }
        };

    onSubmitEdit = (e) => {
        e.preventDefault();

        if(this.state.mentee_name_edit.length !== 0 && this.state.prodi_edit.length !== 0 && this.state.angkatan_mentee_edit.length !== 0){
            const group =
                {
                    mentee_name: this.state.mentee_name_edit,
                    mentors_id : this.props.id,
                    group_name : this.state.groups.group_name,
                    levels_id : this.state.groups.levels_id,
                    angkatan: this.state.angkatan_mentee_edit,
                    role : 1,
                    password : this.state.mentee_name_edit.replace(/[^A-Z0-9]/ig, "").toLowerCase(),
                    username : null,
                    email : null,
                    status : 'Aktif',
                    is_mentor : 0,
                    gender : this.state.gender,
                    prodi_id : this.state.prodi_edit,
                    groups_id : this.state.groups.id,
                };


            // console.log(group);

            axios.put('/api/group/updateMentee/'+this.state.users_id_edit, group)
                .then(response => window.location.reload())
                .catch();
        }
        else{
            window.alert('Isian belum lengkap');
        }
    };

    onSubmitCreate = (e) => {
        e.preventDefault();

        let cekUsername = this.state.users.filter(data=>data.username === this.state.username).map(data=>data.username);

        if(cekUsername.length === 0){
            if(this.state.email.includes('@') === true){
                if(this.state.confirm_password === this.state.password){
                    const account =
                        {
                            mentee_id: this.state.mentee_id,
                            username: this.state.username,
                            email: this.state.email,
                            password: this.state.password
                        };

                    // console.log(account);

                    axios.put('/api/user/createAccount/'+this.state.mentee_id, account)
                        .then(response => window.location.reload())
                        .catch();
                }
                else{
                    alert('Maaf, password konfirmasi tidak sama!');
                }
            }
            else{
                alert('Maaf, email harus menyertakan @!');
            }
        }
        else{
            alert('Maaf, username ini sudah terpakai!');
        }
    };

    onSubmitEditAccount = (e) => {
        e.preventDefault();

        let cekUsername = this.state.users.filter(data=>data.username === this.state.username_edit && data.id !== this.state.mentee_id).map(data=>data.username);

        if(cekUsername.length === 0){
            if(this.state.email_edit.includes('@') === true) {
                if (this.state.confirm_password_edit === this.state.password_edit) {
                    const account =
                        {
                            mentee_id: this.state.mentee_id,
                            username: this.state.username_edit,
                            email: this.state.email_edit,
                            password: this.state.password_edit
                        };

                    // console.log(account);

                    axios.put('/api/user/editAccount/'+this.state.mentee_id, account)
                        .then(response => window.location.reload())
                        .catch();
                }
                else {
                    alert('Maaf, password konfirmasi tidak sama!');
                }
            }
            else{
                alert('Maaf, email harus menyertakan @!');
            }
        }
        else{
            alert('Maaf, username ini sudah terpakai!');
        }
    };

    getJumlahNonaktif(){
        let nonaktif = this.state.mutarabbi.filter(binaan => binaan.status === 'Nonaktif').map(binaan => binaan.name);
        return nonaktif.length;
    }

    onChangePassword = () => {
        this.setState({
            changePassword: !this.state.changePassword,
            password_edit: '',
            confirm_password_edit: ''
        })
    };

    showProfile = (id) => {
        const users = this.state.users.filter(user=>user.id === id).map(user=>user);
        this.setState({
            showProfile: users[0]
        })
    };

    sabar = () => {
        window.alert('Sabar akhi');
    };

    render() {
        // console.log(this.state);
        let murabbi_id = window.location.pathname.split('/')[3];
        let groups_id = window.location.pathname.split('/')[5];

        let faculty = this.state.faculties.map((faculty,index)=>(
            {
                label: faculty.faculty,
                value: index+1,
                name: 'faculty'
            }
        ));

        let prodi = this.state.prodies.filter(prodi => prodi.faculty_id === this.state.faculty).map((prodi, index) =>(
            {
                label: prodi.prodi,
                value: prodi.id,
                name: 'prodi'
            }
        ));

        let angkatan = this.state.iterasi.map(i=>{
            let year = this.state.year;
            return (
                {
                    label: year-i,
                    value: year-i,
                    name: 'angkatan_mentee'
                }
            )
        });

        let faculty_edit = this.state.faculties.map((faculty,index)=>(
            {
                label: faculty.faculty,
                value: index+1,
                name: 'faculty_edit'
            }
        ));

        let prodi_edit = this.state.prodies.filter(prodi => prodi.faculty_id === this.state.faculty_edit).map((prodi, index) =>(
            {
                label: prodi.prodi,
                value: prodi.id,
                name: 'prodi_edit'
            }
        ));

        let angkatan_edit = this.state.iterasi.map(i=>{
            let year = this.state.year;
            return (
                {
                    label: year-i,
                    value: year-i,
                    name: 'angkatan_mentee_edit'
                }
            )
        });

        return (
            <div className="container">
                <h2>Data Binaan</h2>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getName(murabbi_id)}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama Kelompok</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.groups.group_name}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Angkatan Kelompok</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.groups.angkatan}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Jenis Kelamin</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.gender === 1 ? 'Ikhwan' : 'Akhwat'}</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Jumlah Aktif</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.mutarabbi.length - this.getJumlahNonaktif()} orang</Row>
                    <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Jumlah Non-Aktif</Row>
                    <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getJumlahNonaktif()} orang</Row>
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                            <tr>
                                <th className='center aligned'>No.</th>
                                <th className='center aligned'>Nama</th>
                                {/*<th className='center aligned'>Prodi</th>*/}
                                {/*<th className='center aligned'>Angkatan</th>*/}
                                <th className='center aligned'>Username</th>
                                <th className='center aligned'>Pembina?</th>
                                <th className='center aligned'>Tindakan</th>
                                {this.props.prefix === '/admin' && <th className='center aligned'>Menu</th>}
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.mutarabbi.filter(mutarabbi => mutarabbi.status !== 'Nonaktif').map((mutarabbi, index) => {
                                return(
                                    <tr key={index}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{this.getName(mutarabbi.id)}</td>
                                        {/*<td>{this.getNamaProdi(mutarabbi.prodi_id)}</td>*/}
                                        {/*<td className='center aligned'>{mutarabbi.angkatan}</td>*/}
                                        <td className='center aligned'>{
                                            mutarabbi.username === null ?
                                                <ButtonCreateUser onClick={()=>this.onCreate(mutarabbi.id)}/> :
                                                (
                                                    <div>
                                                    <span>
                                                        {mutarabbi.username}
                                                    </span>
                                                        <ButtonEditUser onClick={()=>this.onEditAccount(mutarabbi.id)}/>
                                                    </div>
                                                )
                                        }</td>
                                        <td className='center aligned'>{mutarabbi.is_mentor === 1 ? 'Ya' : 'Bukan'}
                                            <ButtonSetMentor onClick={()=>this.onEditIsMentor(mutarabbi.id, mutarabbi.is_mentor)}/>
                                        </td>
                                        <td className='right aligned'>
                                            {
                                                mutarabbi.is_mentor === 1 &&
                                                (
                                                    <ButtonShowGroups
                                                        onClick={() => window.location = this.props.prefix + '/data_kelompok/' + mutarabbi.id}/>
                                                )
                                            }
                                            <ButtonShowProfile onClick={() => this.showProfile(mutarabbi.id)}/>
                                            <ButtonEditProfile onClick={() => this.onEdit(mutarabbi.id)}/>
                                            <ButtonUnactivate onClick={() => this.nonAktifkan(mutarabbi.id)}/>
                                        </td>
                                        {this.props.prefix === '/admin' &&
                                            <td className='center aligned'>
                                            <Button
                                                tooltip="Capaian Madah"
                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                floating
                                                small
                                                waves="light"
                                                icon={<Icon>subject</Icon>}
                                                onClick={() => window.location = this.props.prefix + '/data_kelompok/' + murabbi_id + '/data_mutarabbi/' + groups_id + '/capaian_madah/' + mutarabbi.id}
                                            />
                                            {
                                                (mutarabbi.mentors_id !== 'notset' || mutarabbi.groups_id === null) &&
                                                (
                                                    <ButtonTransfer
                                                        onClick={() => window.location = this.props.prefix + '/data_murabbi/transfer/' + mutarabbi.id}/>
                                                )

                                            }
                                        </td>
                                        }
                                    </tr>
                                )
                            })
                        }
                        {
                            this.state.mutarabbi.filter(mutarabbi => mutarabbi.status === 'Nonaktif').map((mutarabbi, index) => {
                                return(
                                    <tr key={index} style={{color: 'grey'}}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{this.getName(mutarabbi.id)}</td>
                                        <td>{this.getNamaProdi(mutarabbi.prodi_id)}</td>
                                        <td className='center aligned'>{mutarabbi.angkatan}</td>
                                        <td className='center aligned'>{
                                            mutarabbi.username === null ?
                                                <ButtonCreateUser onClick={()=>this.onCreate(mutarabbi.id)}/> :
                                                (
                                                    <div>
                                                    <span>
                                                        {mutarabbi.username}
                                                    </span>
                                                        <ButtonEditUser onClick={()=>this.onEditAccount(mutarabbi.id)}/>
                                                    </div>
                                                )
                                        }</td>
                                        <td className='center aligned'>{mutarabbi.is_mentor === 1 ? 'Ya' : 'Bukan'}
                                            <ButtonSetMentor onClick={()=>this.onEditIsMentor(mutarabbi.id, mutarabbi.is_mentor)}/>
                                        </td>
                                        <td className='right aligned'>
                                            {
                                                mutarabbi.is_mentor === 1 &&
                                                (
                                                    <ButtonShowGroups onClick={()=>window.location=this.props.prefix + '/data_kelompok/'+mutarabbi.id}/>
                                                )
                                            }
                                            <ButtonShowProfile onClick={()=>this.showProfile(mutarabbi.id)}/>
                                            <ButtonEditProfile onClick={()=>this.onEdit(mutarabbi.id)}/>
                                            <ButtonActivate onClick={()=>this.Aktifkan(mutarabbi.id)}/>
                                            {/*{this.props.role === 'admin' && <a*/}
                                            {/*    className="waves-effect waves-light btn-small"*/}
                                            {/*    onClick={()=>this.onDelete(mutarabbi.id)}>*/}
                                            {/*    <i className="material-icons">*/}
                                            {/*        delete*/}
                                            {/*    </i>*/}
                                            {/*</a>}*/}
                                        </td>
                                        {this.props.prefix === '/admin' &&
                                        <td className='center aligned'>
                                            <Button
                                                tooltip="Capaian Madah"
                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                floating
                                                small
                                                waves="light"
                                                icon={<Icon>subject</Icon>}
                                                onClick={() => window.location = this.props.prefix + '/data_kelompok/' + murabbi_id + '/data_mutarabbi/' + groups_id + '/capaian_madah/' + mutarabbi.id}
                                            />
                                            {
                                                (mutarabbi.mentors_id !== 'notset' || mutarabbi.groups_id === null) &&
                                                (
                                                    <ButtonTransfer
                                                        onClick={() => window.location = this.props.prefix + '/data_murabbi/transfer/' + mutarabbi.id}/>
                                                )

                                            }
                                        </td>
                                        }
                                    </tr>
                                )
                            })
                        }
                        </tbody>
            </table>
            <br/>
        </div>
                {this.state.isHidden === this.state.isHidden2 === this.state.isHidden3 === this.state.isHidden4 && (
                    <div>
                        <a onClick={this.onAdd} className="btn-floating btn-large waves-effect waves-light red"><i className="material-icons">add</i></a><label> Tambah Binaan</label>
                    </div>
                )}
                {!this.state.isHidden && <div style={{padding: 20, border: '2px solid', borderColor: 'black', borderRadius: 20}}>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <h4>Tambah Binaan</h4>
                    </Col>
                    <Col>
                        <TextInput placeholder='Nama Binaan' label="Nama Binaan" type="text" s={3} name="mentee_name" onChange={this.handleChange}/>
                        <SearchableSelect s={3} placeholder="Pilih Fakultas" options={faculty} value={this.state.faculty.value} onChange={this.handleChangeFaculty} name='faculty'/>
                        <SearchableSelect s={3} placeholder="Pilih Prodi" options={prodi} value={this.state.prodi.value} onChange={this.handleChangeProdi} name='prodi'/>
                        <SearchableSelect s={2} placeholder="Pilih Angkatan" options={angkatan} value={this.state.angkatan_mentee.value} onChange={this.handleChangeAngkatan} name='angkatan_mentee'/>
                    </Col>
                    <Col s={12} style={{textAlign: 'center'}}>
                        {
                            this.state.clicked === false ? (
                                <Button style={{marginRight: 5}} onClick={this.onSubmit}>Tambah</Button>
                            )
                                :
                                (
                                    <Button style={{marginRight: 5}} onClick={this.sabar}>Tambah <Icon right>
                                        cloud_upload
                                    </Icon></Button>
                                )
                        }
                        <Button style={{marginLeft: 5}} className='red' onClick={this.onClose}>Batal</Button>
                    </Col>
                </div>}
                {!this.state.isHidden2 && <div style={{padding: 20, border: '2px solid', borderColor: 'black', borderRadius: 20}}>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <h4>Edit Binaan</h4>
                    </Col>
                    <Col>
                        <TextInput placeholder='Nama Binaan' value={this.state.mentee_name_edit} type="text" s={3} name="mentee_name_edit" onChange={this.handleChange}/>
                        <SearchableSelect s={3} placeholder="Pilih Fakultas" options={faculty_edit} defaultLabel={this.getNamaFakultas(this.state.faculty_edit)} defaultValue={this.state.faculty_edit} value={this.state.faculty_edit.value} onChange={this.handleChangeFaculty} name='faculty_edit'/>
                        <SearchableSelect s={3} placeholder="Pilih Prodi" options={prodi_edit} defaultLabel={this.getNamaProdi(this.state.prodi_edit)} defaultValue={this.state.prodi_edit} value={this.state.prodi_edit.value} onChange={this.handleChangeProdi} name='prodi_edit'/>
                        <SearchableSelect s={2} placeholder="Pilih Angkatan" options={angkatan_edit} defaultValue={this.state.angkatan_mentee_edit} value={this.state.angkatan_mentee_edit.value} onChange={this.handleChangeAngkatan} name='angkatan_mentee_edit'/>
                    </Col>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <Button style={{marginRight: 5}} onClick={this.onSubmitEdit}>Edit</Button>
                        <Button style={{marginLeft: 5}} className='red' onClick={this.onClose2}>Batal</Button>
                    </Col>
                </div>}
                <BuatAkun mentee_id={this.state.mentee_id} active={!this.state.isHidden3} onClose={this.onClose3}/>
                {!this.state.isHidden4 && <div style={{padding: 20, border: '2px solid', borderColor: 'black', borderRadius: 20}}>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <h4>Edit Akun</h4>
                    </Col>
                    <Col>
                        <TextInput placeholder='Nama Binaan' label="Nama Binaan" type="text" s={3} name="mentee_id" disabled value={this.getName(this.state.mentee_id)} />
                        <TextInput placeholder='Username' label="Username" type="text" s={3} name="username_edit" value={this.state.username_edit} onChange={this.handleChange}/>
                        <TextInput placeholder='Email' label="Email" type="email" s={3} name="email_edit" validate value={this.state.email_edit} onChange={this.handleChange}/>
                        {
                            !this.state.changePassword && <Button onClick={this.onChangePassword}>Ganti Password</Button>
                        }
                        {
                            this.state.changePassword && <div>
                                <TextInput placeholder='Password' label="Password" type="password" s={3} name="password_edit" value={this.state.password_edit} onChange={this.handleChange}/>
                                <TextInput placeholder='Confirm Password' label="Confirm Password" type="password" s={3} name="confirm_password_edit" value={this.state.confirm_password_edit} onChange={this.handleChange}/>
                                <Button className='red' onClick={this.onChangePassword}>Batalkan</Button>
                            </div>
                        }
                    </Col>
                    <Col s={12} style={{textAlign: 'center'}}>
                        <Button style={{marginRight: 5}} onClick={this.onSubmitEditAccount}>Edit</Button>
                        <Button style={{marginLeft: 5}} className='red' onClick={this.onClose4}>Batal</Button>
                    </Col>
                </div>}
                <Modal id="modal1" header="Profil User">
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama Lengkap</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.name}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Username</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.username}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Alamat Email</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.email}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Jenis Kelamin</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.gender === 1 ? 'Ikhwan' : 'Akhwat'}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Role</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.role === 0 ? 'Admin' : this.state.showProfile.username !== null ? 'User' : 'Non-User'}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Pembina?</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.is_mentor === 0 ? 'Tidak' : 'Ya'}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Fakultas</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getFakultas(this.state.showProfile.prodi_id)}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Prodi</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getNamaProdi(this.state.showProfile.prodi_id)}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Angkatan</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.angkatan}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama Pembina</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.getName(this.state.showProfile.mentors_id)}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Kelas</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.levels_id}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Nama Kelompok</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.group_name}</Row>
                        <Row style={{marginBottom: 0, fontWeight: 'bold'}}>Status</Row>
                        <Row style={{boxShadow: '5px 5px #888888', minHeight: 30, marginBottom: 5, backgroundColor: 'lightgrey', padding: 5}}>{this.state.showProfile.status}</Row>
                </Modal>
                <div ref='scrollToThis'></div>
    </div>
        )
    }
}

if(document.getElementById('data_mutarabbi')) {
    const element = document.getElementById('data_mutarabbi');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<DataMutarabbi {...props} />, document.getElementById('data_mutarabbi'));
}
