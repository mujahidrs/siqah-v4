import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Icon, Table} from 'react-materialize'
import axios from 'axios'

export default class Pemantauan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            murabbi: [],
            presences: [],
            groups: [],
            users: [],
            gender_user: '',
            prodi: [],
            gender: 0,
            jumlah: 0,
            bulan: '',
            tahun: '',
            week: '',
            angkatan_kelompok: '',
            level: 1,
            dataAngkatan: [],
            levels: [],
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;

        let today = new Date();
        let year = today.getFullYear();
        // let month = today.getMonth() + 1;
        let tanggal = today.getDate();
        if(tanggal < 10){
            tanggal = '0' + tanggal;
        }
        let getMonth = today.getMonth() + 1;
        if(getMonth < 10){
            getMonth = '0' + getMonth;
        }
        let cdate = today.getFullYear() + '-' + getMonth + '-' + tanggal;
        let month = cdate.substr(5, 2);
        var currentWeekNumber = require('current-week-number');
        let week = currentWeekNumber(cdate);

        month = month.toString();

        let pekan = '';
        let bulan = '';

        if (month === '01'){
            pekan = week;
            bulan = month;
            if(pekan === 0){
                pekan = 1;
                bulan = '01'
            }
            else{
                pekan = week;
                bulan = month;
            }
        }
        else if (month === '02'){
            pekan = week-5;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '01'
            }
            else{
                pekan = week-5;
                bulan = month;
            }
        }
        else if (month === '03'){
            pekan = week-9;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '02'
            }
            else{
                pekan = week-9;
                bulan = month;
            }
        }
        else if (month === '04'){
            pekan = week-13;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '03'
            }
            else{
                pekan = week-13;
                bulan = month;
            }
        }
        else if (month === '05'){
            pekan = week-18;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '04'
            }
            else{
                pekan = week-18;
                bulan = month;
            }
        }
        else if (month === '06'){
            pekan = week-22;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '05'
            }
            else{
                pekan = week-22;
                bulan = month;
            }
        }
        else if (month === '07'){
            pekan = week-26;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '06'
            }
            else{
                pekan = week-26;
                bulan = month;
            }
        }
        else if (month === '08'){
            pekan = week-31;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '07'
            }
            else{
                pekan = week-31;
                bulan = month;
            }
        }
        else if (month === '09'){
            pekan = week-35;
            bulan = month;
            if(pekan === 0){
                pekan = 4;
                bulan = '08'
            }
            else{
                pekan = week-35;
                bulan = month;
            }
        }
        else if (month === '10'){
            pekan = week-39;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '09'
            }
            else{
                pekan = week-39;
                bulan = month;
            }
        }
        else if(month === '11'){
            pekan = week-44;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '10';
            }
            else{
                pekan = week-44;
                bulan = month;
            }
        }
        else if(month === '12'){
            pekan = week-48;
            bulan = month;
            if(pekan === 0){
                pekan = 5;
                bulan = '11';
            }
            else{
                pekan = week-48;
                bulan = month;
            }
        }

        axios.get('/api/presence')
            .then(response => {
                this.setState({presences: response.data});
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({users: response.data});
            });

        axios.get('/api/group')
            .then(response => {
                this.setState({groups: response.data});
            });

        axios.get('/api/user/'+id)
            .then(response => {
                this.setState({gender_user: response.data.gender});
                this.setState({
                    gender: this.state.gender_user
                })
            });

        axios.get('/api/angkatan/')
            .then(response => {
                this.setState({
                    dataAngkatan: response.data,
                    angkatan_kelompok: response.data[0].angkatan
                })
            });

        axios.get('/api/level')
            .then(response => {
                this.setState({levels: response.data});
            });

        this.setState({
            week: pekan,
            bulan: parseInt(month),
            tahun: year,
        })
    }

    getJumlahPembina(gender){
        let jumlah = 0;
        jumlah = this.state.murabbi.filter(murabbi => murabbi.gender == gender).map(murabbi=>murabbi.id);
        return jumlah.length;
    }

    getNamaProdi(prodi_id){
        let nama_prodi = '';
        nama_prodi = this.state.prodi.filter(prodi => prodi.id == prodi_id).map(prodi=>prodi.prodi);
        return nama_prodi[0];
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    onDelete(user_id){
        let result = confirm('Hapus user murabbi ini?');
        if(result == true){
            axios.delete('/api/user/delete/'+user_id)
                .then(response=>{
                    var murabbi = this.state.murabbi;

                    for(var i=0; i< murabbi.length; i++)
                    {
                        if(murabbi[i].id==user_id)
                        {
                            murabbi.splice(i,1);
                            this.setState({
                                murabbi:murabbi
                            })
                        }
                    }
                });
        }
    }

    getJumlahKelompok(mentors_id){
        let jumlah = this.state.groups.filter(group => group.mentors_id == mentors_id).map(group => group.group_name);
        return jumlah.length;
    }

    getGroupBinaan(mentors_id){
        let group = this.state.groups.filter(group => group.mentors_id == mentors_id).map(group => group.id);
        return group;
    }

    getJumlahBinaan(groups_id){
        let jumlah = this.state.users.filter(user => user.groups_id == groups_id).map(user => user.id);

        return jumlah.length;
    }

    getGender(users_id){
        let gender = this.state.users.filter(user => user.id == users_id).map(user => user.gender);
        return gender[0];
    }

    getMentorId(groups_id){
        let mentor = this.state.groups.filter(group => group.id == groups_id).map(group => group.mentors_id);
        return mentor[0];
    }

    getGroupName(groups_id){
        let group_name = this.state.groups.filter(group => group.id == groups_id).map(group => group.group_name);
        return group_name;
    }

    getName(users_id){
        let name = this.state.users.filter(user => user.id == users_id).map(user => user.name);
        return name;
    }

    getPresence(groups_id) {
        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        let laporan = this.state.presences.filter(presence => presence.groups_id == groups_id && this.getGender(this.getMentorId(presence.groups_id)) == this.state.gender && this.getAngkatanKelompok(presence.groups_id) == this.state.angkatan_kelompok && this.getLevel(presence.groups_id) == this.state.level && presence.week == this.state.week && presence.month == monthNumToName(this.state.bulan) && presence.year == this.state.tahun).map(presence => presence.done);
        return laporan[0];
    }

    getJKB(week){
        let jumlah = 0;
        this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map(group => {
            this.getPresences(group.id, week) != 0 ?
                jumlah = jumlah + 1
                :
                jumlah = jumlah
        });
        return jumlah;
    }

    getJBH(week){
        let jumlah = 0;
        this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map(group => {
            jumlah = jumlah + this.getPresences(group.id, week);
        });
        return jumlah;
    }

    getJK(){
        let jumlah = this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level)
            .map(group => group.id);
        return jumlah.length;
    }

    getJM(){
        var intersection = require('array-intersection');
        let jumlah = this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level)
            .map(group => group.mentors_id);

        let irisan = intersection(jumlah);
        return irisan.length;
    }

    getTJB(){
        let jumlah = 0;
        this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map(group => {
            jumlah = jumlah + this.getJumlahBinaan(group.id);
        });
        return jumlah;
    }

    getPK(week){
        let persentase = this.getJBH(week) / this.getTJB() * 100;
        if(isNaN(persentase)){
            return 0 + '%';
        }
        else {
            return persentase.toFixed(0) + '%';
        }
    }

    getPKB(week){
        let persentase = this.getJKB(week) / this.getJK() * 100;
        if(isNaN(persentase)){
            return 0 + '%';
        }
        else {
            return persentase.toFixed(0) + '%';
        }
    }

    getJKTB(week){
        let jumlah = 0;
        jumlah = this.getJK(week) - this.getJKB(week);
        return jumlah;
    }

    getAngkatanKelompok(groups_id){
        let angkatan_kelompok = this.state.groups.filter(group => group.id == groups_id).map(group => group.angkatan);
        return angkatan_kelompok[0];
    }

    getLevel(groups_id){
        let level = this.state.groups.filter(group => group.id == groups_id).map(group => group.levels_id);
        return level[0];
    }

    createOption () {
        let option = [];
        let date = new Date();
        let year = date.getFullYear();

        for (let i = 2019; i <= year; i++) {
            option.push(<option key={i} value={year}>{year}</option>);
            year = year - 1;
        }
        return option
    }

    render() {
        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        if(this.state.gender == 0){
            this.state.gender = 1;
        }

        let jumlah_laporan = this.state.groups.filter(group => this.getPresence(group.id) == 'Ya' && this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map(group=>group.id);
        let jumlah_belum_laporan = this.state.groups.filter(group => this.getPresence(group.id) == null && this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map(group=>group.id);
        return (
            <div className="container">
                <h2>Pemantauan</h2>
                <Row>
                    <Select s={6} m={4} type='select' label='Jenis Kelamin' name='gender' value={this.state.gender} onChange={this.handleChange}>
                        <option value="1">Ikhwan</option>
                        <option value="2">Akhwat</option>
                    </Select>
                    <Select s={6} m={4} type='select' label='Angkatan Kelompok' name='angkatan_kelompok' value={this.state.angkatan_kelompok} onChange={this.handleChange}>
                        {
                            this.state.dataAngkatan.map((data,key)=>{
                                return (
                                    <option key={key} value={data.angkatan}>{data.angkatan}</option>
                                )
                            })
                        }
                    </Select>
                    <Select s={12} m={4} type='select' label='Kelas' name='level' value={this.state.level} onChange={this.handleChange}>
                        {this.state.levels.map((level,index)=>{
                            return(
                                <option key={index} value={level.id}>{level.level}</option>
                            )
                        })}
                    </Select>
                </Row>
                <Row>
                    <Select s={6} m={4} type='select' label='Pekan ke-' name='week' value={this.state.week} onChange={this.handleChange}>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </Select>
                    <Select s={6} m={4} type='select' label='Bulan' name='bulan' value={this.state.bulan} onChange={this.handleChange}>
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </Select>
                    <Select s={12} m={4} type='select' label='Tahun' name='tahun' value={this.state.tahun} onChange={this.handleChange}>
                        {this.createOption()}
                    </Select>
                </Row>
                Jumlah Laporan: {jumlah_laporan.length} kelompok
                    <Table striped>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Pembina</th>
                                <th>Nama Kelompok</th>
                                <th>Laporan</th>
                                <th>Berjalan</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.groups.filter(group => this.getPresence(group.id) == 'Ya' && this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map((group, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index+1}</td>
                                        <td>{this.getName(group.mentors_id)}</td>
                                        <td>{group.group_name}</td>
                                        <td>{this.getPresence(group.id) == null ? <i style={{color: 'blue', fontSize: 30 + 'pt'}}>?</i> : <i className="material-icons" style={{color: 'green', fontSize: 30 + 'pt'}}>check</i>}</td>
                                        <td>{this.getPresence(group.id) == 'Ya' ? <i className="material-icons" style={{color: 'green', fontSize: 30 + 'pt'}}>check</i> : this.getPresence(group.id) == 'Tidak' ? <i className="material-icons" style={{color: 'red', fontSize: 30 + 'pt'}}>clear</i> : <i style={{color: 'blue', fontSize: 30 + 'pt'}}>?</i>}</td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </Table>
                <h4>Yang Belum Laporan: {jumlah_belum_laporan.length} kelompok</h4>
                    <Table striped>
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Pembina</th>
                            <th>Nama Kelompok</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.groups.filter(group => this.getPresence(group.id) == null && this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map((group, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index+1}</td>
                                        <td>{this.getName(group.mentors_id)}</td>
                                        <td>{group.group_name}</td>
                                    </tr>
                                )
                            })
                        }

                        </tbody>
                    </Table>
            </div>
        )
    }
}

if(document.getElementById('pemantauan')) {
    const element = document.getElementById('pemantauan');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Pemantauan {...props} />, document.getElementById('pemantauan'));
}
