import React from 'react';
import ReactDOM from 'react-dom';
import {Col, Row, TextInput, Select,  Button, Icon} from 'react-materialize';
import axios from 'axios';
import scrollToComponent from 'react-scroll-to-component';

export default class ListAngkatan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            angkatan: [],
            isHidden: true,
            isHidden2: true,
            angkatan_tambah: '',
            angkatan_edit: '',
        };
        console.log(super());
    };

    componentDidMount() {
        axios.get('/api/angkatan')
            .then(response => {
                this.setState({angkatan: response.data});
            });
    }

    onEditAngkatan(id){
        const angkatan = this.state.angkatan.filter(angkatan=>angkatan.id === id).map((angkatan) => {
            return (
                {
                    id_edit : angkatan.id,
                    angkatan_edit : angkatan.angkatan,
                }
            )
        })

        this.setState({
            isHidden: false,
            isHidden2: true,
            id_edit : angkatan[0].id_edit,
            angkatan_edit : angkatan[0].angkatan_edit,
        })
        scrollToComponent(this.refs.editAngkatan);
    }

    onTambahAngkatan(){
        this.setState({
            isHidden2: false,
            isHidden: true,
        })
    }

    onSubmitEdit(e){
        e.preventDefault();

        const angkatan =
            {
                angkatan: this.state.angkatan_edit,
            }


        console.log(angkatan);

        axios.put('/api/angkatan/update/'+this.state.id_edit, angkatan)
            .then(response => {
                axios.get('/api/angkatan')
                    .then(response => {
                        this.setState({angkatan: response.data});
                    });
                this.onClose();
            })
            .catch();
    }

    onSubmitTambah(e){
        e.preventDefault();

        const angkatan =
            {
                angkatan: this.state.angkatan_tambah,
            };


        console.log(angkatan);

        axios.post('/api/angkatan/store/', angkatan)
            .then(response => {
                axios.get('/api/angkatan')
                    .then(response => {
                        this.setState({angkatan: response.data});
                    });
                this.onClose2();
            })
            .catch();
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    onClose(){
        this.setState({
            isHidden: true
        })
    }

    onClose2(){
        this.setState({
            isHidden2: true
        })
    }

    onDelete(angkatan_id){
        let result = confirm('Hapus angkatan ini?');
        if(result == true){
            axios.delete('/api/angkatan/delete/'+angkatan_id)
                .then(response=>{
                    var angkatan = this.state.angkatan;

                    for(var i=0; i< angkatan.length; i++)
                    {
                        if(angkatan[i].id==angkatan_id)
                        {
                            angkatan.splice(i,1);
                            this.setState({
                                angkatan:angkatan
                            })
                        }
                    }
                });
        }
    }

    render() {
        return (
            <div className="container">
                <h2>List Angkatan Terpantau</h2>
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                            <tr>
                                <th className='center aligned'>No.</th>
                                <th className='center aligned'>Angkatan</th>
                                <th className='center aligned'>Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.angkatan.map((angkatan, index) => {
                                return(
                                    <tr key={index}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{angkatan.angkatan}</td>
                                        <td className='center aligned'>
                                            <Button
                                                tooltip="Edit"
                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                floating
                                                small
                                                className="light-blue"
                                                waves="light"
                                                icon={<Icon>edit</Icon>}
                                                onClick={this.onEditAngkatan.bind(this,angkatan.id)}
                                            />
                                            <Button
                                                tooltip="Hapus"
                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                floating
                                                small
                                                className="red"
                                                waves="light"
                                                icon={<Icon>delete</Icon>}
                                                onClick={this.onDelete.bind(this,angkatan.id)}
                                            />
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
            </table>
            <br/>
        </div>
                <div ref='editAngkatan'></div>
                <a onClick={this.onTambahAngkatan.bind(this)} className="btn-floating btn-large waves-effect waves-light red"><Icon small>add</Icon></a><label> Tambah Angkatan</label>

                {!this.state.isHidden && <div>
                    <Row>
                        <TextInput placeholder='Nama Angkatan' value={this.state.angkatan_edit} type="text" s={10} name="angkatan_edit" onChange={this.handleChange}/>
                        <Col s={2}><a className="btn-floating btn-large waves-effect waves-light red" onClick={this.onClose.bind(this)}><Icon small>close</Icon></a></Col>
                    </Row>
                    <Col s={12} style={{textAlign: 'center'}}><Button onClick={this.onSubmitEdit.bind(this)}>Edit</Button></Col>
                </div>}
                {!this.state.isHidden2 && <div>
                    <Row>
                        <TextInput placeholder='Nama Angkatan' value={this.state.angkatan_tambah} type="text" s={10} name="angkatan_tambah" onChange={this.handleChange}/>
                        <Col s={2}><a className="btn-floating btn-large waves-effect waves-light red" onClick={this.onClose2.bind(this)}><Icon small>close</Icon></a></Col>
                    </Row>
                    <Col s={12} style={{textAlign: 'center'}}><Button onClick={this.onSubmitTambah.bind(this)}>Tambah</Button></Col>
                </div>}
    </div>
        )
    }
}

if(document.getElementById('angkatan')) {
    const element = document.getElementById('angkatan');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<ListAngkatan {...props} />, document.getElementById('angkatan'));
}
