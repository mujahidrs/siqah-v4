import React from 'react';
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Icon} from 'react-materialize';
import axios from 'axios';

export default class ListPengaturan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;
    }

    render() {
        return (
            <div className="container">
                <h2>List Pengaturan</h2>
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                            <tr>
                                <th className='center aligned'>No.</th>
                                <th className='center aligned'>Pengaturan</th>
                                <th className='center aligned'>Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className='center aligned'>1</td>
                                <td>Fakultas & Prodi</td>
                                <td className='center aligned'>
                                    <Button
                                        tooltip="Lihat"
                                        style={{marginLeft: 2.5, marginRight: 2.5}}
                                        floating
                                        small
                                        waves="light"
                                        icon={<Icon>remove_red_eye</Icon>}
                                        onClick={()=>window.location="pengaturan/fakultas"}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className='center aligned'>2</td>
                                <td>Angkatan Terpantau</td>
                                <td className='center aligned'>
                                    <Button
                                        tooltip="Lihat"
                                        style={{marginLeft: 2.5, marginRight: 2.5}}
                                        floating
                                        small
                                        waves="light"
                                        icon={<Icon>remove_red_eye</Icon>}
                                        onClick={()=>window.location="pengaturan/angkatan"}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td className='center aligned'>3</td>
                                <td>Jenjang Kelas</td>
                                <td className='center aligned'>
                                    <Button
                                        tooltip="Lihat"
                                        style={{marginLeft: 2.5, marginRight: 2.5}}
                                        floating
                                        small
                                        waves="light"
                                        icon={<Icon>remove_red_eye</Icon>}
                                        onClick={()=>window.location="pengaturan/jenjang"}
                                    />
                                </td>
                            </tr>
                        </tbody>
            </table>
        </div>
    </div>
        )
    }
}

if(document.getElementById('pengaturan')) {
    const element = document.getElementById('pengaturan');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<ListPengaturan {...props} />, document.getElementById('pengaturan'));
}
