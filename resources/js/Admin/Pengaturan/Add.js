import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Collection, CollectionItem} from 'react-materialize'
import axios from 'axios'

export default class Add extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            faculties: [],
            prodies: [],
            name: '',
            username: '',
            email: '',
            gender: '',
            faculty: '',
            prodi: '',
            angkatan: '',
            role: '1',
            password: '',
            confirm_password: '',
            errors: [],
            year: '',
        };
        console.log(super());
    };

    componentDidMount() {
        let today = new Date();
        let tanggal = today.getDate();
        if(tanggal <= 10){
            tanggal = '0' + tanggal;
        }
        let sdate = tanggal + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
        axios.get('/api/faculty')
            .then(response => {
                this.setState({faculties: response.data});
            });

        axios.get('/api/prodi')
            .then(response => {
                this.setState({prodies: response.data});
            });

        let year = today.getFullYear();

        this.setState({
            year: year
        })
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty('name')) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    validate(name, username, email, gender, faculty, role, prodi, password, confirm_password, angkatan){
        const errors = [];

        if(name.length === 0){
            errors.push('Nama tidak boleh kosong');
        }

        if(username.length === 0){
            errors.push('Username tidak boleh kosong');
        }

        if(email.length === 0){
            errors.push('Email tidak boleh kosong');
        }

        if(gender.length === 0){
            errors.push('Jenis Kelamin tidak boleh kosong');
        }

        if(faculty.length === 0){
            errors.push('Fakultas tidak boleh kosong');
        }

        if(prodi.length === 0){
            errors.push('Prodi tidak boleh kosong');
        }

        if(role.length === 0){
            errors.push('Role tidak boleh kosong');
        }

        if(angkatan.length === 0){
            errors.push('Angkatan tidak boleh kosong');
        }

        if(password.length === 0){
            errors.push('Password tidak boleh kosong');
        }

        if(confirm_password !== password) {
            errors.push('Confirm Password tidak sama');
        }

        return errors;
    }


    createOption () {
        let option = [];
        let year = this.state.year;

        for (let i = 0; i <= 10; i++) {
            option.push(<option key={i} value={year}>{year}</option>);
            year = year - 1;
        }
        return option
    }

    onSubmit(e){
        e.preventDefault();

        const errors = this.validate(this.state.name, this.state.username, this.state.email, this.state.gender, this.state.faculty, this.state.role, this.state.prodi, this.state.password, this.state.confirm_password, this.state.angkatan);
        if (errors.length > 0) {
            this.setState({ errors });
            return;
        }
        else {
            const register =
                {
                    name: this.state.name,
                    username: this.state.username,
                    email: this.state.email,
                    gender: this.state.gender,
                    prodi: this.state.prodi,
                    angkatan: this.state.angkatan,
                    role: this.state.role,
                    password: this.state.password,
                };

            console.log(register);

            axios.post('/api/user/store', register)
                .then(response => window.location = '/admin/data_murabbi')
                .catch();
        }
    }

    render() {
        return (
            <div className="container">
                <h2>Tambah Pembina</h2>
                    <div className="row">
                        <form className="col s12" method="post" action="#" role="form">
                            <Row>
                                <TextInput type="text" s={6} label="Name" name="name" value={this.state.name} onChange={this.handleChange} required autoFocus/>
                                <TextInput type="text" s={6} label="Username" name="username" value={this.state.username} onChange={this.handleChange} required/>
                                <TextInput type="text" s={12} label="E-Mail Address" name="email" value={this.state.email} onChange={this.handleChange} required/>
                                <Select s={6} label="Jenis Kelamin" name="gender" defaultValue='0' onChange={this.handleChange} required>
                                    <option value='0'>Pilih Jenis Kelamin</option>
                                    <option value='1'>Male</option>
                                    <option value='2'>Female</option>
                                </Select>
                                <Select s={6} label="Faculty" name="faculty" defaultValue='0' onChange={this.handleChange} required>
                                    <option value='0'>Pilih Fakultas</option>
                                    {this.state.faculties.map((faculty, index) =>{
                                        return(
                                            <option key={index} value={index+1}>{faculty.faculty}</option>
                                        )
                                    })
                                    }
                                </Select>
                                <Select s={6} label="Prodi" name="prodi" defaultValue='0' onChange={this.handleChange} required>
                                    <option value='0'>Pilih Prodi</option>
                                    {this.state.prodies.filter(prodi => prodi.faculty_id == this.state.faculty).map((prodi, index) =>{
                                        return(
                                            <option key={index} value={prodi.id}>{prodi.prodi}</option>
                                        )
                                    })
                                    }
                                </Select>
                                <Select s={6} label="Angkatan" name="angkatan" defaultValue='0' onChange={this.handleChange} required>
                                    <option value='0'>Pilih Angkatan</option>
                                    {this.createOption()}
                                </Select>
                                <TextInput type="password" s={12} label="Password" name="password" value={this.state.password} onChange={this.handleChange} required/>
                                <TextInput type="password" s={12} label="Confirm Password" name="confirm_password" value={this.state.confirm_password} onChange={this.handleChange} required/>
                            </Row>
                            <Button waves='light' type='submit' onClick={this.onSubmit}>Register</Button>
                            {this.state.errors != '' ?
                                <div>
                                    <p>Error:</p>
                                    {this.state.errors.map(error => (
                                        <p key={error}>{error}</p>
                                    ))}
                                </div>
                                :
                                null
                            }
                        </form>
                    </div>
            </div>
        )
    }
}

if(document.getElementById('add_murabbi')) {
    const element = document.getElementById('add_murabbi');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<Add {...props} />, document.getElementById('add_murabbi'));
}
