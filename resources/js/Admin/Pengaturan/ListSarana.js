import React from 'react';
import ReactDOM from 'react-dom';
import {Col, Row, TextInput, Select,  Button, Icon} from 'react-materialize';
import axios from 'axios';
import scrollToComponent from 'react-scroll-to-component';

export default class ListSarana extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            faculties: [],
            isHidden: true,
            isHidden2: true,
            faculty: '',
            faculty_edit: '',
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;
        axios.get('/api/faculty')
            .then(response => {
                this.setState({faculties: response.data});
            });
    }

    onEditFakultas(id){
        const fakultas = this.state.faculties.filter(faculty=>faculty.id === id).map((faculty) => {
            return (
                {
                    id_edit : faculty.id,
                    faculty_edit : faculty.faculty,
                }
            )
        })

        this.setState({
            isHidden: false,
            isHidden2: true,
            id_edit : fakultas[0].id_edit,
            faculty_edit : fakultas[0].faculty_edit,
        })
        scrollToComponent(this.refs.editFakultas);
    }

    onTambahFakultas(){
        this.setState({
            isHidden2: false,
            isHidden: true,
        })
    }

    onSubmitEdit(e){
        e.preventDefault();

        const fakultas =
            {
                faculty: this.state.faculty_edit,
            }


        console.log(fakultas);

        axios.put('/api/faculty/update/'+this.state.id_edit, fakultas)
            .then(response => window.location.reload())
            .catch();
    }

    onSubmitTambah(e){
        e.preventDefault();

        const fakultas =
            {
                faculty: this.state.faculty,
            }


        console.log(fakultas);

        axios.post('/api/faculty/store/', fakultas)
            .then(response => window.location.reload())
            .catch();
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    onClose(){
        this.setState({
            isHidden: true
        })
    }

    onClose2(){
        this.setState({
            isHidden2: true
        })
    }

    onDelete(faculties_id){
        let result = confirm('Hapus fakultas ini?');
        if(result == true){
            axios.delete('/api/faculty/delete/'+faculties_id)
                .then(response=>{
                    var faculties = this.state.faculties;

                    for(var i=0; i< faculties.length; i++)
                    {
                        if(faculties[i].id==faculties_id)
                        {
                            faculties.splice(i,1);
                            this.setState({
                                faculties:faculties
                            })
                        }
                    }
                });
        }
    }

    render() {
        return (
            <div className="container">
                <h2>List Fakultas</h2>
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                            <tr>
                                <th className='center aligned'>No.</th>
                                <th className='center aligned'>Fakultas</th>
                                <th className='center aligned'>Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.faculties.map((faculty, index) => {
                                return(
                                    <tr key={index}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{faculty.faculty}</td>
                                        <td className='center aligned'>
                                            <a href={'fakultas/' + faculty.id + '/prodi'}
                                               className="waves-effect waves-light btn-small">
                                                <Icon small>
                                                    remove_red_eye
                                                </Icon>
                                            </a>
                                            <a
                                               onClick={this.onEditFakultas.bind(this,faculty.id)}
                                               className="waves-effect waves-light btn-small">
                                                <Icon small>
                                                    edit
                                                </Icon>
                                            </a>
                                            <a
                                                onClick={this.onDelete.bind(this,faculty.id)}
                                                className="waves-effect waves-light btn-small">
                                                <Icon small>
                                                    delete
                                                </Icon>
                                            </a>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
            </table>
            <br/>
        </div>
                <div ref='editFakultas'></div>
                <a onClick={this.onTambahFakultas.bind(this)} className="btn-floating btn-large waves-effect waves-light red"><Icon small>add</Icon></a><label> Tambah Fakultas</label>

                {!this.state.isHidden && <div>
                    <Row>
                        <TextInput placeholder='Nama Fakultas' value={this.state.faculty_edit} type="text" s={11} name="faculty_edit" onChange={this.handleChange}/>
                        <Col s={1}><a className="btn-floating btn-large waves-effect waves-light red" onClick={this.onClose.bind(this)}><Icon small>close</Icon></a></Col>
                    </Row>
                    <Col s={12} style={{textAlign: 'center'}}><Button onClick={this.onSubmitEdit.bind(this)}>Edit</Button></Col>
                </div>}
                {!this.state.isHidden2 && <div>
                    <Row>
                        <TextInput placeholder='Nama Fakultas' value={this.state.faculty} type="text" s={11} name="faculty" onChange={this.handleChange}/>
                        <Col s={1}><a className="btn-floating btn-large waves-effect waves-light red" onClick={this.onClose2.bind(this)}><Icon small>close</Icon></a></Col>
                    </Row>
                    <Col s={12} style={{textAlign: 'center'}}><Button onClick={this.onSubmitTambah.bind(this)}>Tambah</Button></Col>
                </div>}
    </div>
        )
    }
}

if(document.getElementById('sarana')) {
    const element = document.getElementById('sarana');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<ListSarana {...props} />, document.getElementById('sarana'));
}
