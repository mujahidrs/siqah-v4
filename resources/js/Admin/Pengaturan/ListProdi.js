import React from 'react';
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Icon, Col} from 'react-materialize';
import axios from 'axios';
import scrollToComponent from "react-scroll-to-component";

export default class ListProdi extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            faculties: [],
            faculty_id: '',
            prodies: [],
            prodies_edit: '',
            prodi: '',
            isHidden: true,
            isHidden2: true,
        };
        console.log(super());
    };

    componentDidMount() {
        let id = window.location.pathname.split('/')[4];
        console.log(id);
        axios.get('/api/faculty/')
            .then(response => {
                this.setState({
                    faculties: response.data
                });
            });
        axios.get('/api/faculty/'+id+'/prodi')
            .then(response => {
                this.setState({
                    prodies: response.data,
                    faculty_id: response.data[0].faculty_id
                });
            });
    }

    getFaculty(id){
        let faculty = this.state.faculties.filter(faculty=>faculty.id == id).map(faculty=>faculty.faculty);
        return faculty[0];
    }

    onEditProdi(id){
        const prodi = this.state.prodies.filter(prodi=>prodi.id === id).map((prodi) => {
            return (
                {
                    id_edit : prodi.id,
                    prodies_edit : prodi.prodi,
                }
            )
        })

        this.setState({
            isHidden: false,
            isHidden2: true,
            id_edit : prodi[0].id_edit,
            prodies_edit : prodi[0].prodies_edit,
        })
        scrollToComponent(this.refs.editProdi);
    }

    onTambahProdi(){
        this.setState({
            isHidden2: false,
            isHidden: true,
        })
    }

    onSubmitEdit(e){
        e.preventDefault();

        const prodi =
            {
                prodi: this.state.prodies_edit,
            }


        console.log(prodi);

        axios.put('/api/prodi/update/'+this.state.id_edit, prodi)
            .then(response => window.location.reload())
            .catch();
    }

    onSubmitTambah(e){
        e.preventDefault();

        const prodi =
            {
                prodi: this.state.prodi,
                faculty_id: window.location.pathname.split('/')[4],
            }


        console.log(prodi);

        axios.post('/api/prodi/store/', prodi)
            .then(response => window.location.reload())
            .catch();
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    onClose(){
        this.setState({
            isHidden: true
        })
    }

    onClose2(){
        this.setState({
            isHidden2: true
        })
    }

    onDelete(prodi_id){
        let result = confirm('Hapus prodi ini?');
        if(result == true){
            axios.delete('/api/prodi/delete/'+prodi_id)
                .then(response=>{
                    var prodi = this.state.prodies;

                    for(var i=0; i< prodi.length; i++)
                    {
                        if(prodi[i].id==prodi_id)
                        {
                            prodi.splice(i,1);
                            this.setState({
                                prodi:prodi
                            })
                        }
                    }
                });
        }
    }

    render() {
        return (
            <div className="container">
                <h2>List Prodi</h2>
                Fakultas: {this.getFaculty(this.state.faculty_id)}
                <br/>
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                            <tr>
                                <th className='center aligned'>No.</th>
                                <th className='center aligned'>Prodi</th>
                                <th className='center aligned'>Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.prodies.map((prodi, index) => {
                                return(
                                    <tr key={index}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{prodi.prodi}</td>
                                        <td className='center aligned'>
                                            <a
                                                onClick={this.onEditProdi.bind(this,prodi.id)}
                                                className="waves-effect waves-light btn-small">
                                                <i className="material-icons">
                                                    edit
                                                </i>
                                            </a>
                                            <a
                                                onClick={this.onDelete.bind(this,prodi.id)}
                                                className="waves-effect waves-light btn-small">
                                                <i className="material-icons">
                                                    delete
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
            </table>
            <br/>
        </div>
                <div ref='editProdi'></div>
                <a onClick={this.onTambahProdi.bind(this)} className="btn-floating btn-large waves-effect waves-light red"><i className="material-icons">add</i></a><label> Tambah Prodi</label>

                {!this.state.isHidden && <div>
                    <Row>
                        <TextInput placeholder='Nama Prodi' value={this.state.prodies_edit} type="text" s={11} name="prodies_edit" onChange={this.handleChange}/>
                        <Col s={1}><a className="btn-floating btn-large waves-effect waves-light red" onClick={this.onClose.bind(this)}><i className="material-icons">close</i></a></Col>
                    </Row>
                    <Col s={12} style={{textAlign: 'center'}}><Button onClick={this.onSubmitEdit.bind(this)}>Edit</Button></Col>
                </div>}
                {!this.state.isHidden2 && <div>
                    <Row>
                        <TextInput placeholder='Nama Prodi' value={this.state.prodi} type="text" s={11} name="prodi" onChange={this.handleChange}/>
                        <Col s={1}><a className="btn-floating btn-large waves-effect waves-light red" onClick={this.onClose2.bind(this)}><i className="material-icons">close</i></a></Col>
                    </Row>
                    <Col s={12} style={{textAlign: 'center'}}><Button onClick={this.onSubmitTambah.bind(this)}>Tambah</Button></Col>
                </div>}
    </div>
        )
    }
}

if(document.getElementById('prodi')) {
    const element = document.getElementById('prodi');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<ListProdi {...props} />, document.getElementById('prodi'));
}
