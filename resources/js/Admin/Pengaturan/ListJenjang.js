import React from 'react';
import ReactDOM from 'react-dom';
import {Col, Row, TextInput, Select,  Button, Icon} from 'react-materialize';
import axios from 'axios';
import scrollToComponent from 'react-scroll-to-component';

export default class ListJenjang extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            levels: [],
            isHidden: true,
            isHidden2: true,
            level: '',
            level_edit: '',
        };
        console.log(super());
    };

    componentDidMount() {
        axios.get('/api/level')
            .then(response => {
                this.setState({levels: response.data});
            });
    }

    onEditJenjang(id){
        const level = this.state.levels.filter(level=>level.id === id).map((level) => {
            return (
                {
                    id_edit : level.id,
                    level_edit : level.level,
                }
            )
        })

        this.setState({
            isHidden: false,
            isHidden2: true,
            id_edit : level[0].id_edit,
            level_edit : level[0].level_edit,
        })
        scrollToComponent(this.refs.editJenjang);
    }

    onTambahJenjang(){
        this.setState({
            isHidden2: false,
            isHidden: true,
        })
    }

    onSubmitEdit(e){
        e.preventDefault();

        const level =
            {
                level: this.state.level_edit,
            }


        console.log(level);

        axios.put('/api/level/update/'+this.state.id_edit, level)
            .then(response => {
                axios.get('/api/level')
                    .then(response => {
                        this.setState({levels: response.data});
                    });
                this.onClose();
            })
            .catch();
    }

    onSubmitTambah(e){
        e.preventDefault();

        const level =
            {
                level: this.state.level,
            }


        console.log(level);

        axios.post('/api/level/store/', level)
            .then(response => {
                axios.get('/api/level')
                    .then(response => {
                        this.setState({levels: response.data});
                    });
                this.onClose2();
            })
            .catch();
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    onClose(){
        this.setState({
            isHidden: true
        })
    }

    onClose2(){
        this.setState({
            isHidden2: true
        })
    }

    onDelete(levels_id){
        let result = confirm('Hapus level ini?');
        if(result == true){
            axios.delete('/api/level/delete/'+levels_id)
                .then(response=>{
                    var levels = this.state.levels;

                    for(var i=0; i< levels.length; i++)
                    {
                        if(levels[i].id==levels_id)
                        {
                            levels.splice(i,1);
                            this.setState({
                                levels:levels
                            })
                        }
                    }
                });
        }
    }

    render() {
        return (
            <div className="container">
                <h2>List Jenjang</h2>
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                            <tr>
                                <th className='center aligned'>No.</th>
                                <th className='center aligned'>Jenjang</th>
                                <th className='center aligned'>Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.levels.map((level, index) => {
                                return(
                                    <tr key={index}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{level.level}</td>
                                        <td className='center aligned'>
                                            <Button
                                                tooltip="Edit"
                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                floating
                                                small
                                                className="light-blue"
                                                waves="light"
                                                icon={<Icon>edit</Icon>}
                                                onClick={this.onEditJenjang.bind(this,level.id)}
                                            />
                                            <Button
                                                tooltip="Hapus"
                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                floating
                                                small
                                                className="red"
                                                waves="light"
                                                icon={<Icon>delete</Icon>}
                                                onClick={this.onDelete.bind(this,level.id)}
                                            />
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
            </table>
            <br/>
        </div>
                <div ref='editJenjang'></div>
                <a onClick={this.onTambahJenjang.bind(this)} className="btn-floating btn-large waves-effect waves-light red"><Icon small>add</Icon></a><label> Tambah Jenjang</label>

                {!this.state.isHidden && <div>
                    <Row>
                        <TextInput placeholder='Nama Jenjang' value={this.state.level_edit} type="text" s={10} name="level_edit" onChange={this.handleChange}/>
                        <Col s={2}><a className="btn-floating btn-large waves-effect waves-light red" onClick={this.onClose.bind(this)}><Icon small>close</Icon></a></Col>
                    </Row>
                    <Col s={12} style={{textAlign: 'center'}}><Button onClick={this.onSubmitEdit.bind(this)}>Edit</Button></Col>
                </div>}
                {!this.state.isHidden2 && <div>
                    <Row>
                        <TextInput placeholder='Nama Jenjang' value={this.state.level} type="text" s={10} name="level" onChange={this.handleChange}/>
                        <Col s={2}><a className="btn-floating btn-large waves-effect waves-light red" onClick={this.onClose2.bind(this)}><Icon small>close</Icon></a></Col>
                    </Row>
                    <Col s={12} style={{textAlign: 'center'}}><Button onClick={this.onSubmitTambah.bind(this)}>Tambah</Button></Col>
                </div>}
    </div>
        )
    }
}

if(document.getElementById('jenjang')) {
    const element = document.getElementById('jenjang');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<ListJenjang {...props} />, document.getElementById('jenjang'));
}
