import React from 'react'
import ReactDOM from 'react-dom';
import {Row, TextInput, Select,  Button, Icon} from 'react-materialize';
import { If, Then, ElseIf, Else } from 'react-if-elseif-else-render';
import axios from 'axios'

export default class PusatMadahAdmin extends React.Component {
    constructor(props) {
        super(props);
        this.onAdd = this.onAdd.bind(this);
        this.onClose = this.onClose.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onCloseEdit = this.onCloseEdit.bind(this);
        this.onSubmitEdit = this.onSubmitEdit.bind(this);
        this.state = {
            subjects: [],
            subject: '',
            tool: 'Mentoring',
            levels_id: '1',
            type: '1',
            subjects_edit: '',
            subjects_id_edit: '',
            tool_edit: '',
            levels_id_edit: '',
            type_edit: '',
            subject_types: [],
            level: '1',
            isHidden: true,
            isHidden2: true,
            file: '',
            file_edit: '',
            result: '',
            levels: [],
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;
        axios.get('/api/subject/')
            .then(response => {
                this.setState({subjects: response.data.sort((a, b) => {
                        if(a.number < b.number) return -1;
                        else if (a.number > b.number) return 1;
                        return 0;
                    })});
            });
        axios.get('/api/level')
            .then(response => {
                this.setState({levels: response.data});
            });
    }

    handleChange = (event) => {
        if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
        console.log(event.target.name + ': ' + event.target.value)
    };

    getJumlah(level){
        let subject = this.state.subjects.filter(subject => subject.levels_id == level && subject.type != 0).map(subject => subject.id);
        return subject.length;
    }

    getJenis(type){
        let jenis = this.state.subject_types.filter(subject_type => subject_type.id == type).map(subject_type => subject_type.subject_type);
        return jenis;
    }

    onAdd(){
        this.setState({
            isHidden: false,
            isHidden2: true
        })
        console.log(this.state.isHidden);
    }

    onEdit(subjects_id){
        const subject = this.state.subjects.filter(subject=>subject.id === subjects_id).map(subject => {
            return (
                {
                    subjects_id_edit : subject.id,
                    subjects_edit : subject.subject,
                    type_edit : subject.type,
                    tool_edit: subject.tool,
                    levels_id_edit: subject.levels_id,
                }
            )
        })

        this.setState({
            isHidden2: false,
            isHidden: true,
            subjects_id_edit : subject[0].subjects_id_edit,
            subjects_edit : subject[0].subjects_edit,
            type_edit : subject[0].type_edit,
            tool_edit : subject[0].tool_edit,
            file_edit : '',
            levels_id_edit : subject[0].levels_id_edit,
        })
    }

    onDelete = (subjects_id) =>{
        let result = confirm('Hapus madah ini?');
        if(result === true){
            axios.delete('/api/subject/delete/'+subjects_id)
                .then(response=>{
                    var subjects = this.state.subjects;

                    for(var i=0; i< subjects.length; i++)
                    {
                        if(subjects[i].id==subjects_id)
                        {
                            subjects.splice(i,1);
                            this.setState({
                                subjects:subjects
                            })
                        }
                    }
                });
        }
    }

    onClose(){
        this.setState({
            isHidden: true
        })
        console.log(this.state.isHidden);
    }

    onCloseEdit(){
        this.setState({
            isHidden2: true
        })
        console.log(this.state.isHidden2);
    }

    onChange(e){
        if (this.state.hasOwnProperty['name']) {
            this.setState({
                [e.target.name]: e.target.files[0]
            });
        }
        console.log(e.target.name + ': ' + e.target.files[0])
    }

    onUpload(subject){
        let formData = new FormData();
        formData.append('file', subject.file);
        formData.append('name', subject.file.name);
        formData.append('subject', subject.subject);

        console.log(formData);

        axios.post('/api/upload/', formData)
            .then(response => {
                console.log('result', response)
            })
            .catch(error => {
                console.log(error);
            })
    }

    onSubmit(e){
        e.preventDefault();

        let subject = {};

        if(this.state.file.length === 0){
            subject =
                {
                    subject: this.state.subject,
                    type : 1,
                    levels_id : this.state.levels_id,
                    file: null,
                    file_name: null,
                };
        }
        else{
            subject =
                {
                    subject: this.state.subject,
                    type : 1,
                    levels_id : this.state.levels_id,
                    file: this.state.file,
                    file_name: this.state.subject + '.' + this.state.file.name.split('.')[this.state.file.name.split('.').length - 1],
                };

            this.onUpload(subject);
        }

        console.log(subject);

        axios.post('/api/subject/store', subject)
            .then(response => {
                if(response.data.success === true){
                    axios.get('/api/subject/')
                        .then(response => {
                            this.setState({subjects: response.data.sort((a, b) => {
                                    if(a.number < b.number) return -1;
                                    else if (a.number > b.number) return 1;
                                    return 0;
                                })});
                        });
                    this.onClose();
                }
                else{
                    window.alert('Error');
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    onSubmitEdit(e){
        e.preventDefault();

        const subject =
            {
                id: this.state.subjects_id_edit,
                subject: this.state.subjects_edit,
                type : 1,
                levels_id : this.state.levels_id_edit,
                file: this.state.file_edit,
                file_name: this.state.file_edit.name,
            }

        console.log(subject);

        if(subject.file != ''){
            this.onUpload(subject.file)
        }

        axios.post('/api/subject/edit/'+subject.id, subject)
            .then(response => window.location.reload())
            .catch();
    }

    onSwitch = (x, y, id) => {
        axios.post('/api/subject/switch/'+id,{x,y})
            .then(response=>{
                console.log(response);
                axios.get('/api/subject/')
                    .then(response => {
                        this.setState({subjects: response.data.sort((a, b) => {
                                if(a.number < b.number) return -1;
                                else if (a.number > b.number) return 1;
                                return 0;
                            })});
                    });
            })
            .catch(error=>console.log(error));
    }

    render() {
        let subjects = this.state.subjects.filter(subject => subject.levels_id == this.state.level && subject.type != 0);

        return (
            <div className="container">
                <h3>Pusat Materi</h3>
                <Row>
                    <Select s={12} m={6} type='select' name='level' value={this.state.level} label='Kelas' onChange={this.handleChange}>
                        {this.state.levels.map((level,index)=>{
                            return(
                                <option key={index} value={level.id}>{level.level}</option>
                            )
                        })}
                    </Select>
                </Row>
                Jumlah: {this.getJumlah(this.state.level)} madah
                <div className="table-responsive">
                    <table className="border striped">
                        <thead>
                            <tr>
                                <th className='center aligned'>No.</th>
                                <th>Judul</th>
                                <th className='center aligned'>Tindakan</th>
                                <th className='center aligned'>Atur Posisi</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            subjects.map((subject,index)=>{
                                return(
                                    <tr key={index}>
                                        <td className='center aligned'>{subject.number}</td>
                                        <td>{subject.subject}</td>
                                        <td className='center aligned'>
                                            {
                                                subject.file !== null && (
                                                    <Button
                                                        tooltip="Download"
                                                        floating
                                                        waves="light"
                                                        small
                                                        className="blue"
                                                        style={{marginLeft: 2.5, marginRight: 2.5}}
                                                        icon={<Icon>file_download</Icon>}
                                                        onClick={()=>window.location='/api/storage/'+subject.file}
                                                    />
                                                )
                                            }
                                            {/*<Button*/}
                                            {/*    tooltip="Edit"*/}
                                            {/*    floating*/}
                                            {/*    waves="light"*/}
                                            {/*    small*/}
                                            {/*    className="aqua"*/}
                                            {/*    style={{marginLeft: 2.5, marginRight: 2.5}}*/}
                                            {/*    icon={<Icon>edit</Icon>}*/}
                                            {/*    onClick={this.onEdit.bind(this,subject.id)}*/}
                                            {/*/>*/}
                                            <Button
                                                tooltip="Hapus"
                                                floating
                                                waves="light"
                                                small
                                                className="red"
                                                style={{marginLeft: 2.5, marginRight: 2.5}}
                                                icon={<Icon>delete</Icon>}
                                                onClick={()=>this.onDelete(subject.id)}
                                            />
                                        </td>
                                        <td className='center aligned'>
                                            {
                                                index === 0 && subjects.length !== 1 &&
                                                    (
                                                        <Button
                                                            tooltip="Turunkan"
                                                            floating
                                                            waves="light"
                                                            small
                                                            className="green"
                                                            style={{marginLeft: 2.5, marginRight: 2.5}}
                                                            icon={<Icon>keyboard_arrow_down</Icon>}
                                                            onClick={()=>this.onSwitch(subject.number, subject.number+1, subject.id)}
                                                        />
                                                    )
                                            }
                                            {
                                                index > 0 && index < subjects.length-1 &&
                                                (
                                                    <div>
                                                        <Button
                                                            tooltip="Naikkan"
                                                            floating
                                                            waves="light"
                                                            small
                                                            className="green"
                                                            style={{marginLeft: 2.5, marginRight: 2.5}}
                                                            icon={<Icon>keyboard_arrow_up</Icon>}
                                                            onClick={()=>this.onSwitch(subject.number, subject.number-1, subject.id)}
                                                        />
                                                        <Button
                                                            tooltip="Turunkan"
                                                            floating
                                                            waves="light"
                                                            small
                                                            className="green"
                                                            style={{marginLeft: 2.5, marginRight: 2.5}}
                                                            icon={<Icon>keyboard_arrow_down</Icon>}
                                                            onClick={()=>this.onSwitch(subject.number, subject.number+1, subject.id)}
                                                        />
                                                    </div>
                                                )
                                            }
                                            {
                                                index === subjects.length-1 && subjects.length !== 1 &&
                                                (
                                                    <div>
                                                        <Button
                                                            tooltip="Naikkan"
                                                            floating
                                                            waves="light"
                                                            small
                                                            className="green"
                                                            style={{marginLeft: 2.5, marginRight: 2.5}}
                                                            icon={<Icon>keyboard_arrow_up</Icon>}
                                                            onClick={()=>this.onSwitch(subject.number, subject.number-1, subject.id)}
                                                        />
                                                    </div>
                                                )
                                            }
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                </div>
                <a onClick={()=>this.onAdd()} className="btn-floating btn-large waves-effect waves-light red"><i className="material-icons">add</i></a><label> Tambah Madah</label>
                {this.state.isHidden == false ? (
                    <div>
                        <h5>Tambah Madah</h5>
                        <Row>
                            <TextInput s={12} type='text' name='subject' label='Judul Madah' onChange={this.handleChange} required/>
                            <Select s={12} type='select' name='levels_id' label='Kelas' onChange={this.handleChange} required>
                                {this.state.levels.map((level,index)=>{
                                    return(
                                        <option key={index} value={level.id}>{level.level}</option>
                                    )
                                })}
                            </Select>
                            <TextInput type="file" label="File" name='file' s={12} placeholder='Format yg diizinkan .doc, .docx, .ppt, .pptx' onChange={(e)=>this.onChange(e)}/>
                        </Row>
                        <Button style={{marginRight: 5}} onClick={this.onSubmit}>Submit</Button>
                        <Button style={{marginLeft: 5}} className='red' onClick={this.onClose}>Tutup</Button>
                    </div>
                )
                :null}

                {this.state.isHidden2 == false ? (
                        <div>
                            <h5>Edit Madah</h5>
                            <Row>
                                <TextInput s={12} type='text' name='subjects_edit' label='Judul Madah' defaultValue={this.state.subjects_edit} onChange={this.handleChange} required/>
                                <Select s={12} type='select' name='type_edit' label='Jenis' defaultValue={this.state.type_edit} onChange={this.handleChange} required>
                                    {
                                        this.state.subject_types.map((subject_type,index) => {
                                            return(
                                                <option key={index} value={subject_type.id}>{subject_type.subject_type}</option>
                                            )
                                        })
                                    }
                                </Select>
                                <Select s={12} type='select' name='levels_id_edit' label='Kelas' defaultValue={this.state.levels_id_edit} onChange={this.handleChange} required>
                                    {this.state.levels.map((level,index)=>{
                                        return(
                                            <option key={index} value={level.id}>{level.level}</option>
                                        )
                                    })}
                                </Select>
                                <TextInput type="file" label="File" name='file_edit' s={12} defaultValue={this.state.file_edit} onChange={(e)=>this.onChange(e)}/>
                            </Row>
                            <Button onClick={this.onSubmitEdit}>Submit</Button>
                            <Button onClick={this.onCloseEdit}>Tutup</Button>
                        </div>
                    )
                    :null}

            </div>
        )
    }
}

if(document.getElementById('pusat_madah_admin')) {
    const element = document.getElementById('pusat_madah_admin');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<PusatMadahAdmin {...props} />, document.getElementById('pusat_madah_admin'));
}
