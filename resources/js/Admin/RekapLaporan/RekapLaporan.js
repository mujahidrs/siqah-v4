import React from 'react'
import ReactDOM from 'react-dom';
import {Row, Col, TextInput, Select,  Button, Icon, Checkbox, Table} from 'react-materialize';
import MixChartComponent from '../../../js/components/MixChart';
import axios from 'axios';

export default class RekapLaporan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            murabbi: [],
            presences: [],
            groups: [],
            users: [],
            gender_user: '',
            prodi: [],
            gender: 0,
            jumlah: 0,
            bulan: '',
            tahun: '',
            angkatan_kelompok: '',
            level: 1,
            bulan1: false,
            bulan2: false,
            bulan3: false,
            bulan4: false,
            bulan5: false,
            bulan6: false,
            bulan7: false,
            bulan8: false,
            bulan9: false,
            bulan10: false,
            bulan11: false,
            bulan12: false,
            dataAngkatan: [],
            levels: [],
        };
        console.log(super());
    };

    componentDidMount() {
        let id = this.props.id;

        let today = new Date();
        let year = today.getFullYear();
        let month = today.getMonth() + 1;
        document.getElementById('pilihan-1').style.display = 'block';
        axios.get('/api/presence')
            .then(response => {
                this.setState({presences: response.data});
            });

        axios.get('/api/user')
            .then(response => {
                this.setState({
                    users: response.data,
                    murabbi: response.data.filter(item=>item.is_mentor === 1)
                });
            });

        axios.get('/api/group')
            .then(response => {
                this.setState({groups: response.data});
            });

        axios.get('/api/user/'+id)
            .then(response => {
                this.setState({gender_user: response.data.gender});
                this.setState({
                    gender: this.state.gender_user
                })
            });

        axios.get('/api/angkatan')
            .then(response => {
                this.setState({
                    dataAngkatan: response.data,
                    angkatan_kelompok: response.data[0].angkatan
                });
            });

        axios.get('/api/level')
            .then(response => {
                this.setState({levels: response.data});
            });

        this.setState({
            bulan: month,
            tahun: year,
        })
    }

    getJumlahMurabbi(gender){
        let jumlah = 0;
        jumlah = this.state.murabbi.filter(murabbi => murabbi.gender == gender).map(murabbi=>murabbi.id);
        return jumlah.length;
    }

    getNamaProdi(prodi_id){
        let nama_prodi = '';
        nama_prodi = this.state.prodi.filter(prodi => prodi.id == prodi_id).map(prodi=>prodi.prodi);
        return nama_prodi[0];
    }

    handleChange = (event) => {
        if(event.target.type == 'checkbox'){
            if(event.target.checked == true){
                this.setState({[event.target.id]: true});
            }
            else if(event.target.checked == false){
                this.setState({[event.target.id]: false});
            }
        }
        else if (this.state.hasOwnProperty['name']) {
            this.setState({[event.target.name]: event.target.value});
        }
    };

    onDelete(user_id){
        let result = confirm('Hapus user murabbi ini?');
        if(result == true){
            axios.delete('/api/user/delete/'+user_id)
                .then(response=>{
                    var murabbi = this.state.murabbi;

                    for(var i=0; i< murabbi.length; i++)
                    {
                        if(murabbi[i].id==user_id)
                        {
                            murabbi.splice(i,1);
                            this.setState({
                                murabbi:murabbi
                            })
                        }
                    }
                });
        }
    }

    getJumlahKelompok(mentors_id){
        let jumlah = this.state.groups.filter(group => group.mentors_id == mentors_id).map(group => group.group_name);
        return jumlah.length;
    }

    getGroupBinaan(mentors_id){
        let group = this.state.groups.filter(group => group.mentors_id == mentors_id).map(group => group.id);
        return group;
    }

    getJumlahBinaan(groups_id){
        let jumlah = this.state.users.filter(user => user.groups_id == groups_id).map(user => user.id);

        return jumlah.length;
    }

    getGender(users_id){
        let gender = this.state.users.filter(user => user.id == users_id).map(user => user.gender);
        return gender;
    }

    getName(users_id){
        let name = this.state.users.filter(user => user.id == users_id).map(user => user.name);
        return name;
    }

    getPresences(groups_id, week, bulan){
        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        let presences = this.state.presences.filter(presence => presence.groups_id == groups_id && presence.week == week && presence.month == monthNumToName(bulan) && presence.year == this.state.tahun && presence.mentee_presence == 1).map(presence => presence.users_id);
        return presences.length;
    }

    totalPresences(groups_id, bulan){
        let total = this.getPresences(groups_id, 1, bulan) + this.getPresences(groups_id, 2, bulan) + this.getPresences(groups_id, 3, bulan) + this.getPresences(groups_id, 4, bulan) + this.getPresences(groups_id, 5, bulan);
        return total/5;
    }

    totalJKB(bulan){
        let total = this.getJKB(1, bulan) + this.getJKB(2, bulan) + this.getJKB(3, bulan) + this.getJKB(4, bulan) + this.getJKB(5, bulan);
        return total/5;
    }

    totalPKB(bulan){
        let total = parseInt(this.getPKB(1, bulan)) + parseInt(this.getPKB(2, bulan)) + parseInt(this.getPKB(3, bulan)) + parseInt(this.getPKB(4, bulan)) + parseInt(this.getPKB(5, bulan));
        return total/5;
    }
    totalJKTB(bulan){
        let total = this.getJKTB(1, bulan) + this.getJKTB(2, bulan) + this.getJKTB(3, bulan) + this.getJKTB(4, bulan) + this.getJKTB(5, bulan);
        return total/5;
    }
    totalJBH(bulan){
        let total = this.getJBH(1, bulan) + this.getJBH(2, bulan) + this.getJBH(3, bulan) + this.getJBH(4, bulan) + this.getJBH(5, bulan);
        return total/5;
    }
    totalPK(bulan){
        let total = parseInt(this.getPK(1, bulan)) + parseInt(this.getPK(2, bulan)) + parseInt(this.getPK(3, bulan)) + parseInt(this.getPK(4, bulan)) + parseInt(this.getPK(5, bulan));
        return total/5;
    }

    getJKB(week, bulan){
        let jumlah = 0;
        this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map(group => {
            this.getPresences(group.id, week, bulan) != 0 ?
                jumlah = jumlah + 1
                :
                jumlah = jumlah
        });
        return jumlah;
    }

    getJBH(week, bulan){
        let jumlah = 0;
        this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map(group => {
            jumlah = jumlah + this.getPresences(group.id, week, bulan);
        });
        return jumlah;
    }

    getJK(){
        let jumlah = this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level)
            .map(group => group.id);
        return jumlah.length;
    }

    getJM(){
        var intersection = require('array-intersection');
        let jumlah = this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level)
            .map(group => group.mentors_id);

        let irisan = intersection(jumlah);
        return irisan.length;
    }

    getTJB(){
        let jumlah = 0;
        this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map(group => {
            jumlah = jumlah + this.getJumlahBinaan(group.id);
        });
        return jumlah;
    }

    getPK(week, bulan){
        let persentase = this.getJBH(week, bulan) / this.getTJB() * 100;
        if(isNaN(persentase)){
            return 0;
        }
        else {
            return persentase.toFixed(0);
        }
    }

    getPKB(week, bulan){
        let persentase = this.getJKB(week, bulan) / this.getJK() * 100;
        if(isNaN(persentase)){
            return 0;
        }
        else {
            return persentase.toFixed(0);
        }
    }

    getJKTB(week, bulan){
        let jumlah = 0;
        jumlah = this.getJK(week) - this.getJKB(week, bulan);
        return jumlah;
    }

    createOption () {
        let option = [];
        let date = new Date();
        let year = date.getFullYear();
        let tahunan = year;

        for (let i = 2019; i <= year; i++) {
            option.push(<option key={i} value={tahunan}>{tahunan}</option>);
            tahunan = tahunan - 1;
        }
        return option
    }

    createBulan () {
        let option = [];

        for (let i = 1; i <= 12; i++) {
            option.push(<th key={i} className='center aligned'>{i}</th>);
        }
        return option
    }

    createSubHeader(bulan){
        let subheader = [];

        for (let i = 1; i <= 5; i++){
            subheader.push(<th className='center aligned' id={'bulan-' + bulan}>{i}</th>);
        }

        return subheader
    }

    createJKB(bulan){
        let jkb = [];

        for (let i = 1; i <= 5; i++){
            jkb.push(<td className='center aligned'>{this.getJKB(i,bulan)}</td>);
        }

        return jkb
    }

    createPKB(bulan){
        let pkb = [];

        for (let i = 1; i <= 5; i++){
            pkb.push(<td className='center aligned'>{this.getPKB(i,bulan) + '%'}</td>);
        }

        return pkb
    }

    createPK(bulan){
        let pk = [];

        for (let i = 1; i <= 5; i++){
            pk.push(<td className='center aligned'>{this.getPK(i,bulan) + '%'}</td>);
        }

        return pk
    }

    createJKTB(bulan){
        let jktb = [];

        for (let i = 1; i <= 5; i++){
            jktb.push(<td className='center aligned'>{this.getJKTB(i,bulan)}</td>);
        }

        return jktb
    }

    createJBH(bulan){
        let jbh = [];

        for (let i = 1; i <= 5; i++){
            jbh.push(<td className='center aligned'>{this.getJBH(i,bulan)}</td>);
        }

        return jbh
    }

    createPresences(group_id, bulan){
        let presences = [];

        for (let i = 1; i <= 5; i++){
            presences.push(<td className='center aligned'>{this.getPresences(group_id, i, bulan)}</td>);
        }

        return presences
    }

    createHeader(bulan){
        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }
        let header = [];

        header.push(<th colSpan="5" className='center aligned'>{monthNumToName(bulan)}</th>);
        return header
    }

    goTo(pilihan){
        for(let i = 1; i <= 3; i++){
            document.getElementById('pilihan-'+i).style.display = 'none';
        }
        document.getElementById(pilihan).style.display = 'block';
    }

    chartPKB(bulan) {
        let data = [
            this.getPKB(1,bulan),
            this.getPKB(2,bulan),
            this.getPKB(3,bulan),
            this.getPKB(4,bulan),
            this.getPKB(5,bulan)
        ];
        return data;
    }

    chartPK(bulan) {
        let data = [
            this.getPK(1,bulan),
            this.getPK(2,bulan),
            this.getPK(3,bulan),
            this.getPK(4,bulan),
            this.getPK(5,bulan)
        ];
        return data;
    }

    chartLabel(bulan){
        let data = [];
        if(bulan == 1){
            data = [
                'Januari-1',
                'Januari-2',
                'Januari-3',
                'Januari-4',
                'Januari-5',
            ];
        }
        if(bulan == 2){
            data = [
                'Februari-1',
                'Februari-2',
                'Februari-3',
                'Februari-4',
                'Februari-5',
            ];
        }
        if(bulan == 3){
            data = [
                'Maret-1',
                'Maret-2',
                'Maret-3',
                'Maret-4',
                'Maret-5',
            ];
        }
        if(bulan == 4){
            data = [
                'April-1',
                'April-2',
                'April-3',
                'April-4',
                'April-5',
            ];
        }
        if(bulan == 5){
            data = [
                'Mei-1',
                'Mei-2',
                'Mei-3',
                'Mei-4',
                'Mei-5',
            ];
        }
        if(bulan == 6){
            data = [
                'Juni-1',
                'Juni-2',
                'Juni-3',
                'Juni-4',
                'Juni-5',
            ];
        }
        if(bulan == 7){
            data = [
                'Juli-1',
                'Juli-2',
                'Juli-3',
                'Juli-4',
                'Juli-5',
            ];
        }
        if(bulan == 8){
            data = [
                'Agustus-1',
                'Agustus-2',
                'Agustus-3',
                'Agustus-4',
                'Agustus-5',
            ];
        }
        if(bulan == 9){
            data = [
                'September-1',
                'September-2',
                'September-3',
                'September-4',
                'September-5',
            ];
        }
        if(bulan == 10){
            data = [
                'Oktober-1',
                'Oktober-2',
                'Oktober-3',
                'Oktober-4',
                'Oktober-5',
            ];
        }
        if(bulan == 11){
            data = [
                'November-1',
                'November-2',
                'November-3',
                'November-4',
                'November-5',
            ];
        }
        if(bulan == 12){
            data = [
                'Desember-1',
                'Desember-2',
                'Desember-3',
                'Desember-4',
                'Desember-5',
            ];
        }

        return data;
    }

    getKeterlaksanaan = (groups_id, bulan) => {
        let pekan1 = this.getPresences(groups_id, 1, bulan) === 0 ? 0 : 1;
        let pekan2 = this.getPresences(groups_id, 2, bulan) === 0 ? 0 : 1;
        let pekan3 = this.getPresences(groups_id, 3, bulan) === 0 ? 0 : 1;
        let pekan4 = this.getPresences(groups_id, 4, bulan) === 0 ? 0 : 1;
        let pekan5 = this.getPresences(groups_id, 5, bulan) === 0 ? 0 : 1;
        let total = pekan1 + pekan2 + pekan3 + pekan4 + pekan5;
        return total;
    };

    getKeterlaksanaanDinamis = (groups_id) => {
        let bulan1 = this.state.bulan1 === true ? this.getKeterlaksanaan(groups_id, 1) : 0;
        let bulan2 = this.state.bulan2 === true ? this.getKeterlaksanaan(groups_id, 2) : 0;
        let bulan3 = this.state.bulan3 === true ? this.getKeterlaksanaan(groups_id, 3) : 0;
        let bulan4 = this.state.bulan4 === true ? this.getKeterlaksanaan(groups_id, 4) : 0;
        let bulan5 = this.state.bulan5 === true ? this.getKeterlaksanaan(groups_id, 5) : 0;
        let bulan6 = this.state.bulan6 === true ? this.getKeterlaksanaan(groups_id, 6) : 0;
        let bulan7 = this.state.bulan7 === true ? this.getKeterlaksanaan(groups_id, 7) : 0;
        let bulan8 = this.state.bulan8 === true ? this.getKeterlaksanaan(groups_id, 8) : 0;
        let bulan9 = this.state.bulan9 === true ? this.getKeterlaksanaan(groups_id, 9) : 0;
        let bulan10 = this.state.bulan10 === true ? this.getKeterlaksanaan(groups_id, 10) : 0;
        let bulan11 = this.state.bulan11 === true ? this.getKeterlaksanaan(groups_id, 11) : 0;
        let bulan12 = this.state.bulan12 === true ? this.getKeterlaksanaan(groups_id, 12) : 0;
        let total = parseInt(bulan1) + parseInt(bulan2) + parseInt(bulan3) + parseInt(bulan4) + parseInt(bulan5) + parseInt(bulan6) + parseInt(bulan7) + parseInt(bulan8) + parseInt(bulan9) + parseInt(bulan10) + parseInt(bulan11)+ parseInt(bulan12);
        return total;
    };

    getRerataKehadiranDinamis = (groups_id) => {
        let bulan = [];
        if(this.state.bulan1 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 1));
        }
        if(this.state.bulan2 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 2));
        }
        if(this.state.bulan3 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 3));
        }
        if(this.state.bulan4 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 4));
        }
        if(this.state.bulan5 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 5));
        }
        if(this.state.bulan6 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 6));
        }
        if(this.state.bulan7 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 7));
        }
        if(this.state.bulan8 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 8));
        }
        if(this.state.bulan9 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 9));
        }
        if(this.state.bulan10 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 10));
        }
        if(this.state.bulan11 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 11));
        }
        if(this.state.bulan12 === true){
            bulan.push(this.getRerataKehadiran(groups_id, 12));
        }
        let hasil = 0;
        let akhir = 0;
        if(bulan.length !== 0){
            hasil = bulan.reduce((x, y) => x + y);
            akhir = hasil/bulan.length;
        }
        return akhir;
    };



    getRerataKehadiran = (groups_id, bulan) => {
        let pekan1 = this.getPresences(groups_id, 1, bulan);
        let pekan2 = this.getPresences(groups_id, 2, bulan);
        let pekan3 = this.getPresences(groups_id, 3, bulan);
        let pekan4 = this.getPresences(groups_id, 4, bulan);
        let pekan5 = this.getPresences(groups_id, 5, bulan);
        let total = pekan1 + pekan2 + pekan3 + pekan4 + pekan5;
        return total/5;
    };

    render() {
        if(this.state.gender == 0){
            this.state.gender = 1;
        }
        var months = [
            'Januari', 'Februari', 'Maret', 'April', 'Mei',
            'Juni', 'Juli', 'Agustus', 'September',
            'Oktober', 'November', 'Desember'
        ];

        function monthNumToName(monthnum) {
            return months[monthnum - 1] || '';
        }
        function monthNameToNum(monthname) {
            var month = months.indexOf(monthname);
            return month ? month + 1 : 0;
        }

        function concatData(...args) {
            return args.reduce((acc, val) => [...acc, ...val]);
        }

        let PKB1  = '';
        let PKB2  = '';
        let PKB3  = '';
        let PKB4  = '';
        let PKB5  = '';
        let PKB6  = '';
        let PKB7  = '';
        let PKB8  = '';
        let PKB9  = '';
        let PKB10 = '';
        let PKB11 = '';
        let PKB12 = '';

        let PK1  = '';
        let PK2  = '';
        let PK3  = '';
        let PK4  = '';
        let PK5  = '';
        let PK6  = '';
        let PK7  = '';
        let PK8  = '';
        let PK9  = '';
        let PK10 = '';
        let PK11 = '';
        let PK12 = '';

        let label1  = '';
        let label2  = '';
        let label3  = '';
        let label4  = '';
        let label5  = '';
        let label6  = '';
        let label7  = '';
        let label8  = '';
        let label9  = '';
        let label10 = '';
        let label11 = '';
        let label12 = '';

        let createHeader1 = '';
        let createHeader2 = '';
        let createHeader3 = '';
        let createHeader4 = '';
        let createHeader5 = '';
        let createHeader6 = '';
        let createHeader7 = '';
        let createHeader8 = '';
        let createHeader9 = '';
        let createHeader10 = '';
        let createHeader11 = '';
        let createHeader12 = '';

        let createSubHeader1 = '';
        let createSubHeader2 = '';
        let createSubHeader3 = '';
        let createSubHeader4 = '';
        let createSubHeader5 = '';
        let createSubHeader6 = '';
        let createSubHeader7 = '';
        let createSubHeader8 = '';
        let createSubHeader9 = '';
        let createSubHeader10 = '';
        let createSubHeader11 = '';
        let createSubHeader12 = '';

        let createJKB1 = '';
        let createJKB2 = '';
        let createJKB3 = '';
        let createJKB4 = '';
        let createJKB5 = '';
        let createJKB6 = '';
        let createJKB7 = '';
        let createJKB8 = '';
        let createJKB9 = '';
        let createJKB10 = '';
        let createJKB11 = '';
        let createJKB12 = '';

        let createPKB1 = '';
        let createPKB2 = '';
        let createPKB3 = '';
        let createPKB4 = '';
        let createPKB5 = '';
        let createPKB6 = '';
        let createPKB7 = '';
        let createPKB8 = '';
        let createPKB9 = '';
        let createPKB10 = '';
        let createPKB11 = '';
        let createPKB12 = '';

        let createJKTB1 = '';
        let createJKTB2 = '';
        let createJKTB3 = '';
        let createJKTB4 = '';
        let createJKTB5 = '';
        let createJKTB6 = '';
        let createJKTB7 = '';
        let createJKTB8 = '';
        let createJKTB9 = '';
        let createJKTB10 = '';
        let createJKTB11 = '';
        let createJKTB12 = '';

        let createJBH1 = '';
        let createJBH2 = '';
        let createJBH3 = '';
        let createJBH4 = '';
        let createJBH5 = '';
        let createJBH6 = '';
        let createJBH7 = '';
        let createJBH8 = '';
        let createJBH9 = '';
        let createJBH10 = '';
        let createJBH11 = '';
        let createJBH12 = '';

        let createPK1 = '';
        let createPK2 = '';
        let createPK3 = '';
        let createPK4 = '';
        let createPK5 = '';
        let createPK6 = '';
        let createPK7 = '';
        let createPK8 = '';
        let createPK9 = '';
        let createPK10 = '';
        let createPK11 = '';
        let createPK12 = '';

        if(this.state.bulan1 == true){
            PKB1  = this.chartPKB(1);
            PK1  = this.chartPK(1);
            label1  = this.chartLabel(1);
            createHeader1 = this.createHeader(1);
            createSubHeader1 = this.createSubHeader(1);
            createJKB1 = this.createJKB(1);
            createPKB1 = this.createPKB(1);
            createJKTB1 = this.createJKTB(1);
            createJBH1 = this.createJBH(1);
            createPK1 = this.createPK(1);
        }
        if(this.state.bulan2 == true){
            PKB2  = this.chartPKB(2);
            PK2  = this.chartPK(2);
            label2  = this.chartLabel(2);
            createHeader2 = this.createHeader(2);
            createSubHeader2 = this.createSubHeader(2);
            createJKB2 = this.createJKB(2);
            createPKB2 = this.createPKB(2);
            createJKTB2 = this.createJKTB(2);
            createJBH2 = this.createJBH(2);
            createPK2 = this.createPK(2);
        }
        if(this.state.bulan3 == true){
            PKB3  = this.chartPKB(3);
            PK3  = this.chartPK(3);
            label3  = this.chartLabel(3);
            createHeader3 = this.createHeader(3);
            createSubHeader3 = this.createSubHeader(3);
            createJKB3 = this.createJKB(3);
            createPKB3 = this.createPKB(3);
            createJKTB3 = this.createJKTB(3);
            createJBH3 = this.createJBH(3);
            createPK3 = this.createPK(3);
        }
        if(this.state.bulan4 == true){
            PKB4  = this.chartPKB(4);
            PK4  = this.chartPK(4);
            label4  = this.chartLabel(4);
            createHeader4 = this.createHeader(4);
            createSubHeader4 = this.createSubHeader(4);
            createJKB4 = this.createJKB(4);
            createPKB4 = this.createPKB(4);
            createJKTB4 = this.createJKTB(4);
            createJBH4 = this.createJBH(4);
            createPK4 = this.createPK(4);
        }
        if(this.state.bulan5 == true){
            PKB5  = this.chartPKB(5);
            PK5  = this.chartPK(5);
            label5  = this.chartLabel(5);
            createHeader5 = this.createHeader(5);
            createSubHeader5 = this.createSubHeader(5);
            createJKB5 = this.createJKB(5);
            createPKB5 = this.createPKB(5);
            createJKTB5 = this.createJKTB(5);
            createJBH5 = this.createJBH(5);
            createPK5 = this.createPK(5);
        }
        if(this.state.bulan6 == true){
            PKB6  = this.chartPKB(6);
            PK6  = this.chartPK(6);
            label6  = this.chartLabel(6);
            createHeader6 = this.createHeader(6);
            createSubHeader6 = this.createSubHeader(6);
            createJKB6 = this.createJKB(6);
            createPKB6 = this.createPKB(6);
            createJKTB6 = this.createJKTB(6);
            createJBH6 = this.createJBH(6);
            createPK6 = this.createPK(6);
        }
        if(this.state.bulan7 == true){
            PKB7  = this.chartPKB(7);
            PK7  = this.chartPK(7);
            label7  = this.chartLabel(7);
            createHeader7 = this.createHeader(7);
            createSubHeader7 = this.createSubHeader(7);
            createJKB7 = this.createJKB(7);
            createPKB7 = this.createPKB(7);
            createJKTB7 = this.createJKTB(7);
            createJBH7 = this.createJBH(7);
            createPK7 = this.createPK(7);
        }
        if(this.state.bulan8 == true){
            PKB8  = this.chartPKB(8);
            PK8  = this.chartPK(8);
            label8  = this.chartLabel(8);
            createHeader8 = this.createHeader(8);
            createSubHeader8 = this.createSubHeader(8);
            createJKB8 = this.createJKB(8);
            createPKB8 = this.createPKB(8);
            createJKTB8 = this.createJKTB(8);
            createJBH8 = this.createJBH(8);
            createPK8 = this.createPK(8);
        }
        if(this.state.bulan9 == true){
            PKB9  = this.chartPKB(9);
            PK9  = this.chartPK(9);
            label9  = this.chartLabel(9);
            createHeader9 = this.createHeader(9);
            createSubHeader9 = this.createSubHeader(9);
            createJKB9 = this.createJKB(9);
            createPKB9 = this.createPKB(9);
            createJKTB9 = this.createJKTB(9);
            createJBH9 = this.createJBH(9);
            createPK9 = this.createPK(9);
        }
        if(this.state.bulan10 == true){
            PKB10  = this.chartPKB(10);
            PK10  = this.chartPK(10);
            label10  = this.chartLabel(10);
            createHeader10 = this.createHeader(10);
            createSubHeader10 = this.createSubHeader(10);
            createJKB10 = this.createJKB(10);
            createPKB10 = this.createPKB(10);
            createJKTB10 = this.createJKTB(10);
            createJBH10 = this.createJBH(10);
            createPK10 = this.createPK(10);
        }
        if(this.state.bulan11 == true){
            PKB11  = this.chartPKB(11);
            PK11  = this.chartPK(11);
            label11  = this.chartLabel(11);
            createHeader11 = this.createHeader(11);
            createSubHeader11 = this.createSubHeader(11);
            createJKB11 = this.createJKB(11);
            createPKB11 = this.createPKB(11);
            createJKTB11 = this.createJKTB(11);
            createJBH11 = this.createJBH(11);
            createPK11 = this.createPK(11);
        }
        if(this.state.bulan12 == true){
            PKB12  = this.chartPKB(12);
            PK12  = this.chartPK(12);
            label12  = this.chartLabel(12);
            createHeader12 = this.createHeader(12);
            createSubHeader12 = this.createSubHeader(12);
            createJKB12 = this.createJKB(12);
            createPKB12 = this.createPKB(12);
            createJKTB12 = this.createJKTB(12);
            createJBH12 = this.createJBH(12);
            createPK12 = this.createPK(12);
        }

        let data1 = concatData(PKB1, PKB2, PKB3, PKB4, PKB5, PKB6, PKB7, PKB8, PKB9, PKB10, PKB11, PKB12);

        let data2 = concatData(PK1, PK2, PK3, PK4, PK5, PK6, PK7, PK8, PK9, PK10, PK11, PK12);

        let labels = concatData(label1, label2, label3, label4, label5, label6, label7, label8, label9, label10, label11, label12);

        // console.log(data1);

        return (
            <div className="container">
                <h2>Rekapitulasi Laporan</h2>
                <Row>
                    <Col s={4} style={{textAlign: 'center'}}>
                        <Button waves='light' onClick={()=>this.goTo('pilihan-1')}>Pekanan</Button>
                    </Col>
                    <Col s={4} style={{textAlign: 'center'}}>
                        <Button waves='light' onClick={()=>this.goTo('pilihan-2')}>Bulanan</Button>
                    </Col>
                    <Col s={4} style={{textAlign: 'center'}}>
                        <Button waves='light' onClick={()=>this.goTo('pilihan-3')}>Dinamis</Button>
                    </Col>
                </Row>
                <div id='pilihan-1' style={{display: 'none'}}>
                <Row>
                    <Select s={6} m={2} type='select' name='gender' label='Jenis Kelamin' value={this.state.gender} onChange={this.handleChange}>
                        <option value="1">Ikhwan</option>
                        <option value="2">Akhwat</option>
                    </Select>
                    <Select s={6} m={3} type='select' name='angkatan_kelompok' label='Angkatan Kelompok' value={this.state.angkatan_kelompok} onChange={this.handleChange}>
                        <option value='0'>Pilih Angkatan</option>
                        {
                            this.state.dataAngkatan.map((data,key)=>{
                                return(
                                    <option key={key} value={data.angkatan}>{data.angkatan}</option>
                                )
                            })
                        }
                    </Select>
                    <Select s={6} m={2} type='select' name='level' label='Kelas' value={this.state.level} onChange={this.handleChange}>
                        {this.state.levels.map((level,index)=>{
                            return(
                                <option key={index} value={level.id}>{level.level}</option>
                            )
                        })}
                    </Select>
                    <Select s={6} m={3} type='select' name='bulan' label='Bulan' value={this.state.bulan} onChange={this.handleChange}>
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </Select>
                    <Select s={12} m={2} type='select' name='tahun' label='Tahun' value={this.state.tahun} onChange={this.handleChange}>
                        {this.createOption()}
                    </Select>
                </Row>

                <MixChartComponent
                    label1='Persentase Keterlaksanaan'
                    label2='Persentase Kehadiran'
                    data1={[this.getPKB(1, this.state.bulan), this.getPKB(2, this.state.bulan), this.getPKB(3, this.state.bulan), this.getPKB(4, this.state.bulan), this.getPKB(5, this.state.bulan)]}
                    data2={[this.getPK(1, this.state.bulan), this.getPK(2, this.state.bulan), this.getPK(3, this.state.bulan), this.getPK(4, this.state.bulan), this.getPK(5, this.state.bulan)]}
                    labels={['Pekan 1', 'Pekan 2', 'Pekan 3', 'Pekan 4', 'Pekan 5']}
                    max={100}
                    height={window.innerWidth < 500 ? 250 : null}
                />

                        Jumlah : {this.getJM()} orang <br/>
                        Jumlah Binaan: {this.getTJB()} orang<br/>
                        Jumlah Kelompok: {this.getJK()} kelompok <br/>

            <Table striped>
                <thead>
                    <tr>
                        <th rowSpan="2" className='center aligned'>No.</th>
                        <th rowSpan="2" className='center aligned'>Nama </th>
                        <th rowSpan="2" className='center aligned'>Nama Kelompok</th>
                        <th rowSpan="2" className='center aligned'>Jumlah Binaan</th>
                        <th colSpan="5" className='center aligned'>{monthNumToName(this.state.bulan)}</th>
                        <th rowSpan="2" className='center aligned'>Keterlaksanaan</th>
                        <th rowSpan="2" className='center aligned'>Rerata Kehadiran</th>
                    </tr>
                    <tr>
                        <th className='center aligned'>1</th>
                        <th className='center aligned'>2</th>
                        <th className='center aligned'>3</th>
                        <th className='center aligned'>4</th>
                        <th className='center aligned'>5</th>
                    </tr>
                </thead>
                <tbody>
                {
                    this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map((group, index) => {
                        return (
                            <tr key={index}>
                                <td className='center aligned'>{index+1}</td>
                                <td>{this.getName(group.mentors_id)}</td>
                                <td>{group.group_name}</td>
                                <td className='center aligned'>{this.getJumlahBinaan(group.id)}</td>
                                <td className='center aligned'>{this.getPresences(group.id, 1, this.state.bulan)}</td>
                                <td className='center aligned'>{this.getPresences(group.id, 2, this.state.bulan)}</td>
                                <td className='center aligned'>{this.getPresences(group.id, 3, this.state.bulan)}</td>
                                <td className='center aligned'>{this.getPresences(group.id, 4, this.state.bulan)}</td>
                                <td className='center aligned'>{this.getPresences(group.id, 5, this.state.bulan)}</td>
                                <td className='center aligned'>{this.getKeterlaksanaan(group.id, this.state.bulan)}</td>
                                <td className='center aligned'>{this.getRerataKehadiran(group.id, this.state.bulan).toFixed(2)}</td>
                            </tr>
                        )
                    })
                }
                <tr>
                    <td colSpan="4">Jumlah Kelompok Berjalan</td>
                    <td className='center aligned'>{this.getJKB(1, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJKB(2, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJKB(3, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJKB(4, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJKB(5, this.state.bulan)}</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colSpan="4">Persentase Kelompok Berjalan</td>
                    <td className='center aligned'>{this.getPKB(1, this.state.bulan) + '%'}</td>
                    <td className='center aligned'>{this.getPKB(2, this.state.bulan) + '%'}</td>
                    <td className='center aligned'>{this.getPKB(3, this.state.bulan) + '%'}</td>
                    <td className='center aligned'>{this.getPKB(4, this.state.bulan) + '%'}</td>
                    <td className='center aligned'>{this.getPKB(5, this.state.bulan) + '%'}</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colSpan="4">Jumlah Kelompok Tidak Berjalan</td>
                    <td className='center aligned'>{this.getJKTB(1, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJKTB(2, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJKTB(3, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJKTB(4, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJKTB(5, this.state.bulan)}</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colSpan="4">Jumlah Binaan Hadir</td>
                    <td className='center aligned'>{this.getJBH(1, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJBH(2, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJBH(3, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJBH(4, this.state.bulan)}</td>
                    <td className='center aligned'>{this.getJBH(5, this.state.bulan)}</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colSpan="4">Persentase Kehadiran</td>
                    <td className='center aligned'>{this.getPK(1, this.state.bulan) + '%'}</td>
                    <td className='center aligned'>{this.getPK(2, this.state.bulan) + '%'}</td>
                    <td className='center aligned'>{this.getPK(3, this.state.bulan) + '%'}</td>
                    <td className='center aligned'>{this.getPK(4, this.state.bulan) + '%'}</td>
                    <td className='center aligned'>{this.getPK(5, this.state.bulan) + '%'}</td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </Table>
                </div>
                <div id='pilihan-2' style={{display: 'none'}}>
                    <Row>
                        <Select s={3} type='select' name='gender' label='Jenis Kelamin' value={this.state.gender} onChange={this.handleChange}>
                            <option value="1">Ikhwan</option>
                            <option value="2">Akhwat</option>
                        </Select>
                        <Select s={3} type='select' name='angkatan_kelompok' label='Angkatan Kelompok' value={this.state.angkatan_kelompok} onChange={this.handleChange}>
                            <option value='0'>Pilih Angkatan</option>
                            {
                                this.state.dataAngkatan.map((data,key)=>{
                                    return(
                                        <option key={key} value={data.angkatan}>{data.angkatan}</option>
                                    )
                                })
                            }
                        </Select>
                        <Select s={3} type='select' name='level' label='Kelas' value={this.state.level} onChange={this.handleChange}>
                            {this.state.levels.map((level,index)=>{
                                return(
                                    <option key={index} value={level.id}>{level.level}</option>
                                )
                            })}
                        </Select>
                        <Select s={3} type='select' name='tahun' label='Tahun' value={this.state.tahun} onChange={this.handleChange}>
                            {this.createOption()}
                        </Select>
                    </Row>

                    <MixChartComponent
                        label1='Persentase Keterlaksanaan'
                        label2='Persentase Kehadiran'
                        data1={[this.totalPKB(1), this.totalPKB(2), this.totalPKB(3), this.totalPKB(4), this.totalPKB(5), this.totalPKB(6), this.totalPKB(7), this.totalPKB(8), this.totalPKB(9), this.totalPKB(10), this.totalPKB(11), this.totalPKB(12)]}
                        data2={[this.totalPK(1), this.totalPK(2), this.totalPK(3), this.totalPK(4), this.totalPK(5), this.totalPK(6), this.totalPK(7), this.totalPK(8), this.totalPK(9), this.totalPK(10), this.totalPK(11), this.totalPK(12)]}
                        labels={['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']}
                        max={100}
                        height={window.innerWidth < 500 ? 250 : null}
                    />

                    Jumlah : {this.getJM()} orang <br/>
                    Jumlah Binaan: {this.getTJB()} orang<br/>
                    Jumlah Kelompok: {this.getJK()} kelompok <br/>

                    <Table striped>
                        <thead>
                        <tr>
                            <th rowSpan="2" className='center aligned'>No.</th>
                            <th rowSpan="2" className='center aligned'>Nama </th>
                            <th rowSpan="2" className='center aligned'>Nama Kelompok</th>
                            <th rowSpan="2" className='center aligned'>Jumlah Binaan</th>
                            <th colSpan="12" className='center aligned'>{this.state.tahun}</th>
                        </tr>
                        <tr>
                            {this.createBulan()}
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map((group, index) => {
                                return (
                                    <tr key={index}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{this.getName(group.mentors_id)}</td>
                                        <td>{group.group_name}</td>
                                        <td className='center aligned'>{this.getJumlahBinaan(group.id)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 1)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 2)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 3)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 4)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 5)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 6)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 7)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 8)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 9)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 10)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 11)}</td>
                                        <td className='center aligned'>{this.totalPresences(group.id, 12)}</td>
                                    </tr>
                                )
                            })
                        }
                        <tr>
                            <td colSpan="4">Jumlah Kelompok Berjalan</td>
                            <td className='center aligned'>{this.totalJKB(1)}</td>
                            <td className='center aligned'>{this.totalJKB(2)}</td>
                            <td className='center aligned'>{this.totalJKB(3)}</td>
                            <td className='center aligned'>{this.totalJKB(4)}</td>
                            <td className='center aligned'>{this.totalJKB(5)}</td>
                            <td className='center aligned'>{this.totalJKB(6)}</td>
                            <td className='center aligned'>{this.totalJKB(7)}</td>
                            <td className='center aligned'>{this.totalJKB(8)}</td>
                            <td className='center aligned'>{this.totalJKB(9)}</td>
                            <td className='center aligned'>{this.totalJKB(10)}</td>
                            <td className='center aligned'>{this.totalJKB(11)}</td>
                            <td className='center aligned'>{this.totalJKB(12)}</td>
                        </tr>
                        <tr>
                            <td colSpan="4">Persentase Kelompok Berjalan</td>
                            <td className='center aligned'>{this.totalPKB(1) + '%'}</td>
                            <td className='center aligned'>{this.totalPKB(2) + '%'}</td>
                            <td className='center aligned'>{this.totalPKB(3) + '%'}</td>
                            <td className='center aligned'>{this.totalPKB(4) + '%'}</td>
                            <td className='center aligned'>{this.totalPKB(5) + '%'}</td>
                            <td className='center aligned'>{this.totalPKB(6) + '%'}</td>
                            <td className='center aligned'>{this.totalPKB(7) + '%'}</td>
                            <td className='center aligned'>{this.totalPKB(8) + '%'}</td>
                            <td className='center aligned'>{this.totalPKB(9) + '%'}</td>
                            <td className='center aligned'>{this.totalPKB(10) + '%'}</td>
                            <td className='center aligned'>{this.totalPKB(11) + '%'}</td>
                            <td className='center aligned'>{this.totalPKB(12) + '%'}</td>
                        </tr>
                        <tr>
                            <td colSpan="4">Jumlah Kelompok Tidak Berjalan</td>
                            <td className='center aligned'>{this.totalJKTB(1)}</td>
                            <td className='center aligned'>{this.totalJKTB(2)}</td>
                            <td className='center aligned'>{this.totalJKTB(3)}</td>
                            <td className='center aligned'>{this.totalJKTB(4)}</td>
                            <td className='center aligned'>{this.totalJKTB(5)}</td>
                            <td className='center aligned'>{this.totalJKTB(6)}</td>
                            <td className='center aligned'>{this.totalJKTB(7)}</td>
                            <td className='center aligned'>{this.totalJKTB(8)}</td>
                            <td className='center aligned'>{this.totalJKTB(9)}</td>
                            <td className='center aligned'>{this.totalJKTB(10)}</td>
                            <td className='center aligned'>{this.totalJKTB(11)}</td>
                            <td className='center aligned'>{this.totalJKTB(12)}</td>
                        </tr>
                        <tr>
                            <td colSpan="4">Jumlah Binaan Hadir</td>
                            <td className='center aligned'>{this.totalJBH(1)}</td>
                            <td className='center aligned'>{this.totalJBH(2)}</td>
                            <td className='center aligned'>{this.totalJBH(3)}</td>
                            <td className='center aligned'>{this.totalJBH(4)}</td>
                            <td className='center aligned'>{this.totalJBH(5)}</td>
                            <td className='center aligned'>{this.totalJBH(6)}</td>
                            <td className='center aligned'>{this.totalJBH(7)}</td>
                            <td className='center aligned'>{this.totalJBH(8)}</td>
                            <td className='center aligned'>{this.totalJBH(9)}</td>
                            <td className='center aligned'>{this.totalJBH(10)}</td>
                            <td className='center aligned'>{this.totalJBH(11)}</td>
                            <td className='center aligned'>{this.totalJBH(12)}</td>
                        </tr>
                        <tr>
                            <td colSpan="4">Persentase Kehadiran</td>
                            <td className='center aligned'>{this.totalPK(1) + '%'}</td>
                            <td className='center aligned'>{this.totalPK(2) + '%'}</td>
                            <td className='center aligned'>{this.totalPK(3) + '%'}</td>
                            <td className='center aligned'>{this.totalPK(4) + '%'}</td>
                            <td className='center aligned'>{this.totalPK(5) + '%'}</td>
                            <td className='center aligned'>{this.totalPK(6) + '%'}</td>
                            <td className='center aligned'>{this.totalPK(7) + '%'}</td>
                            <td className='center aligned'>{this.totalPK(8) + '%'}</td>
                            <td className='center aligned'>{this.totalPK(9) + '%'}</td>
                            <td className='center aligned'>{this.totalPK(10) + '%'}</td>
                            <td className='center aligned'>{this.totalPK(11) + '%'}</td>
                            <td className='center aligned'>{this.totalPK(12) + '%'}</td>
                        </tr>
                        </tbody>
                    </Table>
                </div>
                <div id='pilihan-3' style={{display: 'none'}}>
                    <Row>
                        <Select s={3} type='select' name='gender' label='Jenis Kelamin' value={this.state.gender} onChange={this.handleChange}>
                            <option value="1">Ikhwan</option>
                            <option value="2">Akhwat</option>
                        </Select>
                        <Select s={3} type='select' name='angkatan_kelompok' label='Angkatan Kelompok' value={this.state.angkatan_kelompok} onChange={this.handleChange}>
                            <option value='0'>Pilih Angkatan</option>
                            {
                                this.state.dataAngkatan.map((data,key)=>{
                                    return(
                                        <option key={key} value={data.angkatan}>{data.angkatan}</option>
                                    )
                                })
                            }
                        </Select>
                        <Select s={3} type='select' name='level' label='Kelas' value={this.state.level} onChange={this.handleChange}>
                            {this.state.levels.map((level,index)=>{
                                return(
                                    <option key={index} value={level.id}>{level.level}</option>
                                )
                            })}
                        </Select>
                        <Select s={3} type='select' name='tahun' label='Tahun' value={this.state.tahun} onChange={this.handleChange}>
                            {this.createOption()}
                        </Select>
                    </Row>
                    <Row>
                        <Col>
                            <Checkbox label="Jan" id="bulan1" onChange={this.handleChange}/>
                        </Col>
                        <Col>
                            <Checkbox label="Feb" id="bulan2" onChange={this.handleChange}/>
                        </Col>
                        <Col>
                            <Checkbox label="Mar" id="bulan3" onChange={this.handleChange}/>
                        </Col>
                        <Col>
                            <Checkbox label="Apr" id="bulan4" onChange={this.handleChange}/>
                        </Col>
                        <Col>
                            <Checkbox label="Mei" id="bulan5" onChange={this.handleChange}/>
                        </Col>
                        <Col>
                            <Checkbox label="Juni" id="bulan6" onChange={this.handleChange}/>
                        </Col>
                        <Col>
                            <Checkbox label="Juli" id="bulan7" onChange={this.handleChange}/>
                        </Col>
                        <Col>
                            <Checkbox label="Agust" id="bulan8" onChange={this.handleChange}/>
                        </Col>
                        <Col>
                            <Checkbox label="Sept" id="bulan9" onChange={this.handleChange}/>
                        </Col>
                        <Col>
                            <Checkbox label="Okt" id="bulan10" onChange={this.handleChange}/>
                        </Col>
                        <Col>
                            <Checkbox label="Nov" id="bulan11" onChange={this.handleChange}/>
                        </Col>
                        <Col>
                            <Checkbox label="Des" id="bulan12" onChange={this.handleChange}/>
                        </Col>
                    </Row>

                    <MixChartComponent
                        label1='Persentase Keterlaksanaan'
                        label2='Persentase Kehadiran'
                        data1={data1}
                        data2={data2}
                        labels={labels}
                        max={100}
                        height={window.innerWidth < 500 ? 250 : null}
                    />

                    Jumlah : {this.getJM()} orang <br/>
                    Jumlah Binaan: {this.getTJB()} orang<br/>
                    Jumlah Kelompok: {this.getJK()} kelompok <br/>

                    <Table striped>
                        <thead>
                        <tr>
                            <th rowSpan="2" className='center aligned'>No.</th>
                            <th rowSpan="2" className='center aligned'>Nama </th>
                            <th rowSpan="2" className='center aligned'>Nama Kelompok</th>
                            <th rowSpan="2" className='center aligned'>Jumlah Binaan</th>
                            {createHeader1}
                            {createHeader2}
                            {createHeader3}
                            {createHeader4}
                            {createHeader5}
                            {createHeader6}
                            {createHeader7}
                            {createHeader8}
                            {createHeader9}
                            {createHeader10}
                            {createHeader11}
                            {createHeader12}
                            <th rowSpan="2" className='center aligned'>Keterlaksanaan</th>
                            <th rowSpan="2" className='center aligned'>Rerata Kehadiran</th>
                        </tr>
                        <tr>
                            {createSubHeader1}
                            {createSubHeader2}
                            {createSubHeader3}
                            {createSubHeader4}
                            {createSubHeader5}
                            {createSubHeader6}
                            {createSubHeader7}
                            {createSubHeader8}
                            {createSubHeader9}
                            {createSubHeader10}
                            {createSubHeader11}
                            {createSubHeader12}
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.groups.filter(group => this.getGender(group.mentors_id) == this.state.gender && group.angkatan == this.state.angkatan_kelompok && group.levels_id == this.state.level).map((group, index) => {
                                return (
                                    <tr key={index}>
                                        <td className='center aligned'>{index+1}</td>
                                        <td>{this.getName(group.mentors_id)}</td>
                                        <td>{group.group_name}</td>
                                        <td className='center aligned'>{this.getJumlahBinaan(group.id)}</td>
                                        {this.state.bulan1 == true? this.createPresences(group.id, 1) : null}
                                        {this.state.bulan2 == true? this.createPresences(group.id, 2) : null}
                                        {this.state.bulan3 == true? this.createPresences(group.id, 3) : null}
                                        {this.state.bulan4 == true? this.createPresences(group.id, 4) : null}
                                        {this.state.bulan5 == true? this.createPresences(group.id, 5) : null}
                                        {this.state.bulan6 == true? this.createPresences(group.id, 6) : null}
                                        {this.state.bulan7 == true? this.createPresences(group.id, 7) : null}
                                        {this.state.bulan8 == true? this.createPresences(group.id, 8) : null}
                                        {this.state.bulan9 == true? this.createPresences(group.id, 9) : null}
                                        {this.state.bulan10 == true? this.createPresences(group.id, 10) : null}
                                        {this.state.bulan11 == true? this.createPresences(group.id, 11) : null}
                                        {this.state.bulan12 == true? this.createPresences(group.id, 12) : null}
                                        <td className='center aligned'>{this.getKeterlaksanaanDinamis(group.id)}</td>
                                        <td className='center aligned'>{this.getRerataKehadiranDinamis(group.id).toFixed(2)}</td>
                                    </tr>
                                )
                            })
                        }
                        <tr>
                            <td colSpan="4">Jumlah Kelompok Berjalan</td>
                            {createJKB1}
                            {createJKB2}
                            {createJKB3}
                            {createJKB4}
                            {createJKB5}
                            {createJKB6}
                            {createJKB7}
                            {createJKB8}
                            {createJKB9}
                            {createJKB10}
                            {createJKB11}
                            {createJKB12}
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colSpan="4">Persentase Kelompok Berjalan</td>
                            {createPKB1}
                            {createPKB2}
                            {createPKB3}
                            {createPKB4}
                            {createPKB5}
                            {createPKB6}
                            {createPKB7}
                            {createPKB8}
                            {createPKB9}
                            {createPKB10}
                            {createPKB11}
                            {createPKB12}
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colSpan="4">Jumlah Kelompok Tidak Berjalan</td>
                            {createJKTB1}
                            {createJKTB2}
                            {createJKTB3}
                            {createJKTB4}
                            {createJKTB5}
                            {createJKTB6}
                            {createJKTB7}
                            {createJKTB8}
                            {createJKTB9}
                            {createJKTB10}
                            {createJKTB11}
                            {createJKTB12}
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colSpan="4">Jumlah Binaan Hadir</td>
                            {createJBH1}
                            {createJBH2}
                            {createJBH3}
                            {createJBH4}
                            {createJBH5}
                            {createJBH6}
                            {createJBH7}
                            {createJBH8}
                            {createJBH9}
                            {createJBH10}
                            {createJBH11}
                            {createJBH12}
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colSpan="4">Persentase Kehadiran</td>
                            {createPK1}
                            {createPK2}
                            {createPK3}
                            {createPK4}
                            {createPK5}
                            {createPK6}
                            {createPK7}
                            {createPK8}
                            {createPK9}
                            {createPK10}
                            {createPK11}
                            {createPK12}
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </Table>
                </div>

        </div>
        )
    }
}

if(document.getElementById('rekap_laporan')) {
    const element = document.getElementById('rekap_laporan');

    const props = Object.assign({}, element.dataset);

    ReactDOM.render(<RekapLaporan {...props} />, document.getElementById('rekap_laporan'));
}
