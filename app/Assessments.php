<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Assessments extends Authenticatable
{
    protected $table = 'assessments';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'users_id', 'instruments_id', 'value'
    ];

    public $timestamps = false;
}
