<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Instruments extends Authenticatable
{
    protected $table = 'instruments';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'personalities_id', 'indicator'
    ];

    public $timestamps = false;
}
