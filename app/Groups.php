<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Groups extends Authenticatable
{
    protected $table = 'groups';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mentors_id', 'levels_id', 'group_name', 'angkatan', 'status'
    ];

    public $timestamps = false;
}
