<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Subjects extends Authenticatable
{
    protected $table = 'subjects';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'personalities_id', 'levels_id', 'subject'
    ];

    public $timestamps = false;
}
