<?php

namespace App\Exports;

use App\Groups;
use App\Prodi;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Sheet;

class UserExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $users = User::query()
            ->select('id', 'name', 'email', 'role', 'username', 'gender', 'prodi_id', 'angkatan', 'groups_id', 'status', 'is_mentor')
            ->where('role', '=', 1)
            ->where('gender', '=', 1)
            ->get();

        foreach ($users as $key=>$user) {
            $users[$key]->setAttribute('id', $key+1);
            if($user->groups_id !== null) {
                $users[$key]->setAttribute('groups_id', $this->getNamaKelompok($user->groups_id));
            }
            if($user->prodi_id !== null) {
                $users[$key]->setAttribute('prodi_id', $this->getNamaProdi($user->prodi_id));
            }
            if($user->gender !== null) {
                $users[$key]->setAttribute('gender', $this->getJenisKelamin($user->gender));
            }
            if($user->role !== null) {
                $users[$key]->setAttribute('role', $this->getRole($user->role));
            }
            if($user->is_mentor !== null) {
                $users[$key]->setAttribute('is_mentor', $this->getIsMentor($user->is_mentor));
            }
        }

        return $users;
    }

    private function getNamaKelompok($groups_id){
        $group = Groups::query()->where('id', '=', $groups_id)->first();
        return $group->group_name;
    }

    private function getNamaProdi($prodi_id){
        $prodi = Prodi::query()->where('id', '=', $prodi_id)->first();
        return $prodi->prodi;
    }

    private function getRole($role){
        $new_role = 'User';

        if($role === 0) {
            $new_role = 'Admin';
        }

        return $new_role;
    }

    private function getIsMentor($is_mentor){
        $new_is_mentor = 'Ya';

        if($is_mentor === 0) {
            $new_is_mentor = 'Tidak';
        }

        return $new_is_mentor;
    }

    private function getJenisKelamin($gender){
        $jenis_kelamin = 'Ikhwan';

        if($gender === 2) {
            $jenis_kelamin = 'Akhwat';
        }

        return $jenis_kelamin;
    }

    public function headings(): array
    {
        return [
            'No.',
            'Nama',
            'Email',
            'Role',
            'Username',
            'Gender',
            'Prodi',
            'Angkatan',
            'Nama Kelompok',
            'Status',
            'Pembina?'
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });

        return [
            AfterSheet::class    => function(AfterSheet $event) {
                set_time_limit(20);
                $headerCellRange = 'A1:K1'; // All headers

                $styleBordersArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ];

                $styleAlignmentCenter = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ];

                $styleAlignmentLeft = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    ],
                ];

                $highestRow = $event->sheet->getHighestRow();
                $event->sheet->getDelegate()->getStyle($headerCellRange)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($headerCellRange)->applyFromArray($styleAlignmentCenter, False);
                $event->sheet->getDelegate()->getStyle("A1:K".($highestRow))->applyFromArray($styleAlignmentCenter, False);
                $event->sheet->getDelegate()->getStyle("B2:C".($highestRow))->applyFromArray($styleAlignmentLeft, False);
                $event->sheet->getDelegate()->getStyle("G2:G".($highestRow))->applyFromArray($styleAlignmentLeft, False);
                $event->sheet->getDelegate()->getStyle("I2:I".($highestRow))->applyFromArray($styleAlignmentLeft, False);
                $event->sheet->getDelegate()->getStyle("A1:K".($highestRow))->applyFromArray($styleBordersArray, False);
//                $event->sheet->getDelegate()->getStyle('A1:K25')->applyFromArray($styleBordersArray);
            },
        ];
    }
}
