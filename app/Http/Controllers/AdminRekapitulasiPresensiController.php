<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminRekapitulasiPresensiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function rekap($murabbi)
    {
        $prefix = Request()->route()->getPrefix();
        return view('admin.data_murabbi.data_kelompok.rekap')->with('murabbi', $murabbi)->with('prefix', $prefix)->with('role', 'admin');
    }

    public function qadhaya($murabbi)
    {
        $prefix = Request()->route()->getPrefix();
        return view('admin.data_murabbi.data_kelompok.qadhaya')->with('murabbi', $murabbi)->with('prefix', $prefix)->with('role', 'admin');
    }

    public function qadhaya_personal($murabbi, $id)
    {
        $prefix = Request()->route()->getPrefix();
        return view('admin.data_murabbi.data_kelompok.qadhaya_personal')->with('murabbi', $murabbi)->with('kelompok', $id)->with('prefix', $prefix)->with('role', 'admin');
    }

    public function madah($murabbi)
    {
        $prefix = Request()->route()->getPrefix();
        return view('admin.data_murabbi.data_kelompok.madah')->with('murabbi', $murabbi)->with('prefix', $prefix)->with('role', 'admin');
    }

    public function madah_personal($murabbi, $id)
    {
        $prefix = Request()->route()->getPrefix();
        return view('admin.data_murabbi.data_kelompok.madah_personal')->with('murabbi', $murabbi)->with('kelompok', $id)->with('prefix', $prefix)->with('role', 'admin');
    }
}
