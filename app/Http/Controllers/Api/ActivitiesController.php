<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Activity;

class ActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Activity::all();
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $activity = new Activity();
        $activity->activity = $request->activity;
        $activity->measure = $request->measure;
        $activity->categories_id = $request->categories_id;
        $activity->target = $request->target;
        $activity->period = $request->period;
        $activity->unit = $request->unit;
        $activity->users_id = $request->users_id;
        $activity->save();

        return response()->json([
            'success' => true,
            'message' => 'Create activity success'
        ]);
    }

    public function edit(Request $request, $id)
    {
        $this->validate(request(),[
            'activity' => 'required|string|max:50',
            'categories_id' => 'required|integer|max:2',
            'measure' => 'required|string|max:20',
            'period' => 'required|string|max:6',
            'target' => 'required|integer|max:10',
            'unit' => 'required|string|max:10',
            'users_id' => 'required|integer|max:10',
        ]);

        $activity = Activity::find($id);
        $activity->activity = $request->activity;
        $activity->measure = $request->measure;
        $activity->categories_id = $request->categories_id;
        $activity->target = $request->target;
        $activity->period = $request->period;
        $activity->unit = $request->unit;
        $activity->users_id = $request->users_id;
        $activity->save();

        return response()->json([
            'success' => true,
            'message' => 'Edit activity success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activity = Activity::find($id);
        return $activity;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function users_id($id)
    {
        $employees = Activity::where('users_id', '=', $id)->get();
        return $employees;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function edit($id)
//    {
//        $activity = Activity::find($id);
//        return $activity;
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = Activity::find($id);
        $activity->delete();
    }
}
