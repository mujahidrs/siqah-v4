<?php


namespace App\Http\Controllers\Api;


use App\Faculty;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UpdateFacultyController extends Controller
{
    public function __invoke(Request $request, $id)
    {
        Faculty::query()->find($id)->update(['faculty' => $request->get('faculty')]);

        return response()->json([
            'success' => true,
            'message' => 'Update Faculty Success'
        ]);
    }
}
