<?php


namespace App\Http\Controllers\Api;


use App\Level;
use App\Http\Controllers\Controller;

class DeleteLevelController extends Controller
{
    public function __invoke($id)
    {
        Level::query()->where('id', '=', $id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Delete Level Success'
        ]);
    }
}
