<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Prodi;
use Illuminate\Http\Request;

class UpdateProdiController extends Controller
{
    public function __invoke(Request $request, $id)
    {
        Prodi::query()->find($id)->update(['prodi' => $request->get('prodi')]);

        return response()->json([
            'success' => true,
            'message' => 'Update prodi success'
        ]);
    }
}
