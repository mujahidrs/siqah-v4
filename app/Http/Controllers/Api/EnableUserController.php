<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\App;

class EnableUserController extends Controller
{
    public function __invoke($id)
    {
        User::query()->find($id)->update(['status' => 'Aktif']);

        return response()->json([
            'success' => true,
            'message' => 'Enable User Success'
        ]);
    }
}
