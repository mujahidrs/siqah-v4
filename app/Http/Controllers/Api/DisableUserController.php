<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\App;

class DisableUserController extends Controller
{
    public function __invoke($id)
    {
        User::query()->find($id)->update(['status' => 'Nonaktif']);

        return response()->json([
            'success' => true,
            'message' => 'Disable User Success'
        ]);
    }
}
