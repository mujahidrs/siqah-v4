<?php declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Level;
use Illuminate\Http\Request;

/**
 * @author Mujahid Robbani Sholahudin <mrobbanisholahudin@izi.or.id>
 */
class StoreLevelController extends Controller
{
    public function __invoke(Request $request)
    {
        Level::query()->create(['level' => $request->get('level')]);

        return response()->json([
            'success' => true,
            'message' => 'Store Level Success'
        ]);
    }
}