<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Prodi;

class DeleteProdiController extends Controller
{
    public function __invoke($id)
    {
        Prodi::query()->where('id', '=', $id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Delete Prodi Success'
        ]);
    }
}
