<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;

class AutoAssignUserRoleController extends Controller
{
    public function __invoke()
    {
        $user = User::all();

        for($i = 0; $i < count($user); $i++) {
            $user[$i]->password = Hash::make($user[$i]->username);
            $user[$i]->update();
            if($user[$i]->role == 0){
                $user[$i]->assignRole('admin');
            }
            if($user[$i]->role == 1){
                $user[$i]->assignRole('user');
            }

        }
    }
}
