<?php


namespace App\Http\Controllers\Api;


use App\Activity;
use App\Assessments;
use App\Evaluation;
use App\Http\Controllers\Controller;
use App\Presences;
use App\User;
use Zend\Diactoros\Response\JsonResponse;

class DeleteUserController extends Controller
{
    public function __invoke($id)
    {
        User::query()->where('id', '=', $id)->delete();
//        Activity::query()->where('users_id', '=', $id)->delete();
//        Assessments::query()->where('users_id', '=', $id)->delete();
//        Evaluation::query()->where('users_id', '=', $id)->delete();
        Presences::query()->where('users_id', '=', $id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Delete User Success'
        ]);
    }
}
