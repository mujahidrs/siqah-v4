<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Category::all();
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'activity' => 'required|string|max:50',
            'weight' => 'required|integer|max:10',
            'category' => 'required|string|max:20',
            'period' => 'required|string|max:6',
            'target' => 'required|integer|max:10',
            'unit' => 'required|string|max:10',
            'employees_id' => 'required|integer|max:10',
        ]);

        $activity = new Activity();
        $activity->activity = $request->activity;
        $activity->category = $request->category;
        $activity->weight = $request->weight;
        $activity->target = $request->target;
        $activity->period = $request->period;
        $activity->unit = $request->unit;
        $activity->employees_id = $request->employees_id;
        $activity->save();

        return response()->json([
            'success' => true,
            'message' => 'Create activity success'
        ]);
    }

    public function edit(Request $request, $id)
    {

        $instruments = Category::find($id);
        $instruments->category = $request->category;
        $instruments->weight = $request->weight;
        $instruments->save();


        return response()->json([
            'success' => true,
            'message' => 'Edit Kategori success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activity = Activity::find($id);
        return $activity;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function employees_id($id)
    {
        $employees = Activity::where('employees_id', '=', $id)->get();
        return $employees;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function edit($id)
//    {
//        $activity = Activity::find($id);
//        return $activity;
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = Activity::find($id);
        $activity->delete();
    }
}
