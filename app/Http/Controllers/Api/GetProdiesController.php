<?php


namespace App\Http\Controllers\Api;


use App\Faculty;
use App\Http\Controllers\Controller;
use App\Prodi;

class GetProdiesController extends Controller
{
    public function __invoke()
    {
        $prodies = Prodi::all();

        foreach ($prodies as $key => $prodi){
            $group = Faculty::query()->where('id', '=', $prodi->faculty_id)->first();
            $prodies[$key]->setAttribute('faculty',$group->getAttribute('faculty'));
        }

        return $prodies;
    }
}
