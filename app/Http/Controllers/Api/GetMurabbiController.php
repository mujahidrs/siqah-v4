<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\User;

class GetMurabbiController extends Controller
{
    public function __invoke()
    {
        return User::query()->where('is_mentor', '=', 1)->get();
    }
}
