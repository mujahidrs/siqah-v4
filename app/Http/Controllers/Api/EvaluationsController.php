<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Evaluation;

class EvaluationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Evaluation::all();
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


//        $this->validate(request(),[
//            'users_id' => 'required|integer|max:5',
//            'activities_id' => 'required|integer|max:5',
//            'date' => 'required|date|max:15',
//            'value' => 'required|string|max:5',
//        ]);

//        dd($request->get('evaluation')[0]['users_id']);


        for($i = 0; $i < count($request->get('evaluation')); $i++) {
            $evaluation = new Evaluation();
            $evaluation->users_id = $request->get('evaluation')[$i]['users_id'];
            $evaluation->activities_id = $request->get('evaluation')[$i]['activities_id'];
            $evaluation->date = date("Y-m-d", strtotime($request->get('evaluation')[$i]['date']));
            $evaluation->week = $request->get('evaluation')[$i]['week'];
            if(isset($request->get('evaluation')[$i]['value'])){
                $evaluation->value = $request->get('evaluation')[$i]['value'];
                $evaluation->save();
            }
            else{
                $evaluation->save();
            }
        }


        return response()->json([
            'success' => true,
            'message' => 'Create evaluation success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activity = Evaluation::find($id);
        return $activity;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function users_id($id)
    {
        $employees = Evaluation::where('users_id', '=', $id)->get();
        return $employees;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = Evaluation::find($id);
        $activity->delete();
    }
}
