<?php

namespace App\Http\Controllers\Api;

use App\Presences;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PresencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Presences::all();
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $presences_id_terakhir = DB::table('presences')->select('presences_id')->orderBy('presences_id', 'desc')->first();
        if($presences_id_terakhir == null){
            $presences_id_terakhir = 0;
        }
        else{
            $presences_id_terakhir = $presences_id_terakhir->presences_id;
        }

        $presences_id_input = $presences_id_terakhir + 1;

        $subjects_id_terakhir = DB::table('subjects')->select('id')->orderBy('id', 'desc')->first();
        $subjects_id_terakhir = $subjects_id_terakhir->id;
        $subjects_id_input = $subjects_id_terakhir;

        for($i = 0; $i < count($request->get('presences')); $i++) {
            $presences = new Presences();
//            dd($request->get('presences')[$i]['subjects_id']);
            $presences->users_id = $request->get('presences')[$i]['users_id'];
            $presences->presences_id = $presences_id_input;
            $presences->groups_id = $request->get('presences')[$i]['groups_id'];

            if(isset($request->get('presences')[$i]['mentee_presence'])){
                $presences->mentee_presence = $request->get('presences')[$i]['mentee_presence'];
            }

            $presences->replaced = $request->get('presences')[$i]['replaced'];
            $presences->mentor_presence = $request->get('presences')[$i]['mentor_presence'];

            if(isset($request->get('presences')[$i]['personal_details'])){
                $presences->personal_details = $request->get('presences')[$i]['personal_details'];
            }

            if($request->get('presences')[$i]['date'] == null){
                $presences->date = $request->get('presences')[$i]['date'];
            }
            else{
                $presences->date = date("Y-m-d", strtotime($request->get('presences')[$i]['date']));
            }

            $presences->start_time = $request->get('presences')[$i]['start_time'];
            $presences->end_time = $request->get('presences')[$i]['end_time'];
            $presences->location = $request->get('presences')[$i]['location'];


            if($request->get('presences')[$i]['subjects_id'] == 'Lainnya'){
                $presences->subjects_id = $subjects_id_input;
            }
            else{
                $presences->subjects_id = $request->get('presences')[$i]['subjects_id'];
            }

            $presences->done = $request->get('presences')[$i]['done'];
            $presences->group_notes = $request->get('presences')[$i]['group_notes'];
            $presences->week = $request->get('presences')[$i]['week'];
            $presences->month = $request->get('presences')[$i]['month'];
            $presences->year = $request->get('presences')[$i]['year'];
            if(isset($request->get('presences')[$i]['infaq'])){
                $presences->infaq = $request->get('presences')[$i]['infaq'];
            }
            if(isset($request->get('presences')[$i]['kultum'])){
                $presences->kultum = $request->get('presences')[$i]['kultum'];
            }
            if(isset($request->get('presences')[$i]['tilawah'])){
                $presences->tilawah = $request->get('presences')[$i]['tilawah'];
            }
            $presences->save();
        }


        return response()->json([
            'success' => true,
            'message' => 'Create Presences success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = DB::table('presences')->where('groups_id', $id)->get();
        return $result;
    }

    public function lihat($id)
    {
        $result = DB::table('presences')->where('presences_id', $id)->get();
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('presences')->where('presences_id', '=', $id)->delete();
    }
}
