<?php


namespace App\Http\Controllers\Api;


use App\Level;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UpdateLevelController extends Controller
{
    public function __invoke(Request $request, $id)
    {
        Level::query()->find($id)->update(['level' => $request->get('level')]);

        return response()->json([
            'success' => true,
            'message' => 'Update Level Success'
        ]);
    }
}
