<?php


namespace App\Http\Controllers\Api;


use App\Faculty;
use App\Http\Controllers\Controller;

class ShowFacultyController extends Controller
{
    public function __invoke($id)
    {
        return Faculty::query()->find($id)->first();
    }
}
