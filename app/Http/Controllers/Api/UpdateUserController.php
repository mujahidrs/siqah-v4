<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UpdateUserController extends Controller
{
    public function __invoke(Request $request, $id)
    {
        User::query()->find($id)->update([
               'name' => $request->get('name'),
               'username' => $request->get('username'),
               'email' => $request->get('email'),
               'gender' => $request->get('gender'),
               'role' => $request->get('role'),
               'is_mentor' => $request->get('is_mentor'),
               'prodi_id' => $request->get('prodi'),
               'angkatan' => $request->get('angkatan')
            ]);

        if(isset($request->password)){
            User::query()->find($id)->update(['password' => Hash::make($request->get('password'))]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Update user success'
        ]);
    }
}
