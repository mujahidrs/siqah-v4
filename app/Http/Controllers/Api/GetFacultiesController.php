<?php


namespace App\Http\Controllers\Api;


use App\Faculty;
use App\Http\Controllers\Controller;

class GetFacultiesController extends Controller
{
    public function __invoke()
    {
        return Faculty::all();
    }
}
