<?php

namespace App\Http\Controllers\Api;

use App\Subjects;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function __invoke(Request $request)
    {
        $nama = $request->get('subject');
        $nama_file = $request->get('name');
        $explode = explode(".", $nama_file);
        $ekstensi = end($explode);

        $filename = "$nama.$ekstensi";

        Storage::disk('local')->putFileAs('public/',$request->file('file'), $filename);

        return response()->json([
            'success' => true,
            'message' => 'Upload File successful',
            'filename' => $filename
        ]);
    }
}
