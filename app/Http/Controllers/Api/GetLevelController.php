<?php declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Level;

/**
 * @author Mujahid Robbani Sholahudin <mrobbanisholahudin@izi.or.id>
 */
class GetLevelController extends Controller
{
    public function __invoke()
    {
        return Level::all();
    }
}