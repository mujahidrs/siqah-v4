<?php

namespace App\Http\Controllers\Api;

use App\Groups;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Groups::all();
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $group = new Groups();
            $group->mentors_id = $request->mentors_id;
            $group->levels_id = $request->levels_id;
            $group->group_name = $request->group_name;
            $group->angkatan = $request->angkatan;
            $group->status = $request->status;
            $group->save();

        return response()->json([
            'success' => true,
            'message' => 'Create group success'
        ]);
    }

    public function storeMentee(Request $request)
    {
        $groups_id_terakhir = DB::table('groups')->select('id')->orderBy('id', 'desc')->first();
        if($groups_id_terakhir == null){
            $groups_id_terakhir = 0;
        }
        else{
            $groups_id_terakhir = $groups_id_terakhir->id;
        }

        for($i = 0; $i < count($request->get('group_2')); $i++) {
            $user = new User();
            $user->name = $request->get('group_2')[$i]['mentee_name'];
            $user->username = $request->get('group_2')[$i]['username'];
            $user->email = $request->get('group_2')[$i]['email'];
            $user->gender = $request->get('group_2')[$i]['gender'];
            $user->role = $request->get('group_2')[$i]['role'];
            $user->prodi_id = $request->get('group_2')[$i]['prodi_id'];
            $user->angkatan = $request->get('group_2')[$i]['angkatan'];
            $user->is_mentor = $request->get('group_2')[$i]['is_mentor'];
            $user->status = $request->get('group_2')[$i]['status'];
            $user->groups_id = $groups_id_terakhir;
            $user->password = Hash::make($request->get('group_2')[$i]['password']);
            $user->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Create user success'
        ]);
    }

    public function storeMenteePersonal(Request $request)
    {
        $user = new User;
        $user->name = $request->mentee_name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->role = $request->role;
        $user->prodi_id = $request->prodi_id;
        $user->groups_id = $request->groups_id;
        $user->angkatan = $request->angkatan;
        $user->is_mentor = $request->is_mentor;
        $user->status = $request->status;
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Create mentee success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $groups = Groups::find($id);
        return $groups;
    }

    public function group($id)
    {
        $group_name = Groups::groupBy('group_name')
//            ->select('id', 'group_name', 'angkatan')
            ->where('mentors_id', '=', $id)
            ->get();

        return $group_name;
    }

    public function mentee($id)
    {
        $mutarabbi = DB::table('users')->where('groups_id','=',$id)->get();

        return $mutarabbi;
    }

    public function murabbi($id)
    {
        return Groups::query()->where('mentors_id', '=', $id)->get();
    }

    public function edit(Request $request, $id)
    {
        $group = Groups::find($id);
        $group->group_name = $request->group_name;
        $group->levels_id = $request->levels_id;
        $group->angkatan = $request->angkatan;
        $group->update();

        return response()->json([
            'success' => true,
            'message' => 'Update group success'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->role = $request->role;
        $user->prodi_id = $request->prodi;
        $user->angkatan = $request->angkatan;
        $user->password = Hash::make($request->password);
        $user->update();

        return response()->json([
            'success' => true,
            'message' => 'Update user success'
        ]);
    }

    public function updateMentee(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->mentee_name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->role = $request->role;
        $user->prodi_id = $request->prodi_id;
        $user->groups_id = $request->groups_id;
        $user->angkatan = $request->angkatan;
        $user->password = Hash::make($request->password);
        $user->update();

        return response()->json([
            'success' => true,
            'message' => 'Update mentee success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::query()->where('groups_id', '=', $id)->update(['groups_id' => null, 'groups_id_backup' => $id]);
        Groups::query()->where('id', '=', $id)->update(['status' => 'Nonaktif']);

        return response()->json([
            'success' => true,
            'message' => 'Delete group success'
        ]);
    }

    public function destroyPermanent($id)
    {
        Groups::query()->where('id', '=', $id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Delete group success'
        ]);
    }

    public function revive($id)
    {
        User::query()->where('groups_id_backup', '=', $id)->update(['groups_id' => $id]);
        Groups::query()->where('id', '=', $id)->update(['status' => 'Aktif']);

        return response()->json([
            'success' => true,
            'message' => 'Revive group success'
        ]);
    }
}
