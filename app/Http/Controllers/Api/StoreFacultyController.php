<?php


namespace App\Http\Controllers\Api;


use App\Faculty;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StoreFacultyController extends Controller
{
    public function __invoke(Request $request)
    {
        Faculty::query()->create(['faculty' => $request->get('faculty')]);

        return response()->json([
            'success' => true,
            'message' => 'Store Faculty Success'
        ]);
    }
}
