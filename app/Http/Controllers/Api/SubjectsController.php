<?php

namespace App\Http\Controllers\Api;

use App\Subjects;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Subjects::all();
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $final_record = Subjects::query()
            ->where('levels_id','=', $request->levels_id)
            ->orderBy('number', 'desc')->first();

        if($final_record === null){
            $final_number = 0;
        }
        else{
            $final_number = $final_record->number;
        }

        $subjects = new Subjects();
        $subjects->number = $final_number + 1;
        if($request->number === 0){
            $subjects->number = $request->number;
        }
        $subjects->type = $request->type;
        $subjects->levels_id = $request->levels_id;
        $subjects->subject = $request->subject;
        if(($request->file_name) != '/madah/undefined'){
            $subjects->file = $request->file_name;
        }
        $subjects->save();

        return response()->json([
            'success' => true,
            'message' => 'Create Subject success'
        ]);
    }

    public function switch(Request $request, $id){
        $x = $request->get('x');
        $y = $request->get('y');
        $temp1 = $x;
        $temp2 = $y;

        Subjects::query()->where('number', '=', $y)->update(['number' => $temp1]);
        Subjects::query()->where('id', '=', $id)->update(['number' => $temp2]);

        return response()->json([
            'success' => true,
            'message' => 'Create Subject success',
            'data' => Subjects::query()->where('type', '!=', 0)->get()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subjects = Subjects::find($id);
        $subjects->type = $request->type;
        $subjects->levels_id = $request->levels_id;
        $subjects->subject = $request->subject;
        if(($request->file_name) != '/madah/undefined'){
            $subjects->file = $request->file_name;
        }
        $subjects->update();

        return response()->json([
            'success' => true,
            'message' => 'Edit Subject success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subjects = Subjects::find($id);
        $filepath = $subjects->file;
//        dd($filepath);
//        File::delete(substr($filepath,1));
        Storage::disk()->delete('public/'.$subjects->file);
        $subjects->delete();
    }
}
