<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Prodi;

class GetProdiByIdController extends Controller
{
    public function __invoke($id)
    {
        return Prodi::query()
            ->select('faculty_id')
            ->where('id', '=', $id)->get();
    }
}
