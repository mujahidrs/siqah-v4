<?php


namespace App\Http\Controllers\Api;


use App\Faculty;
use App\Http\Controllers\Controller;

class DeleteFacultyController extends Controller
{
    public function __invoke($id)
    {
        Faculty::query()->where('id', '=', $id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Delete Faculty Success'
        ]);
    }
}
