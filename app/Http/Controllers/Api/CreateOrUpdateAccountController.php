<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CreateOrUpdateAccountController extends Controller
{
    public function __invoke(Request $request, $id)
    {
        User::query()->find($id)->update(
                [
                    'username' => $request->get('username'),
                    'email' => $request->get('email')
                ]
            );

        if(isset($request->password)) {
            User::query()->find($id)->update(['password' => Hash::make($request->get('password'))]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Create or Update Account Success'
        ]);
    }
}
