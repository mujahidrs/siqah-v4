<?php


namespace App\Http\Controllers\Api;


use App\Groups;
use App\Http\Controllers\Controller;
use App\User;

class GetUserController extends Controller
{
    public function __invoke()
    {
        $users = User::all();

        foreach ($users as $key => $user){
            if($user->groups_id !== null){
                $group = Groups::query()->where('id', '=', $user->groups_id)->first();
                $users[$key]->setAttribute('levels_id',$group->getAttribute('levels_id'));
                $users[$key]->setAttribute('mentors_id',$group->getAttribute('mentors_id'));
                $users[$key]->setAttribute('group_name',$group->getAttribute('group_name'));
            }
            else{
                $users[$key]->setAttribute('levels_id','notset');
                $users[$key]->setAttribute('mentors_id','notset');
                $users[$key]->setAttribute('group_name','notset');
            }
        }

        return $users;
    }
}
