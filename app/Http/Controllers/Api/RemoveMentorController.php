<?php declare(strict_types=1);

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\User;

/**
 * @author Mujahid Robbani Sholahudin <mrobbanisholahudin@izi.or.id>
 */
class RemoveMentorController extends Controller
{
    public function __invoke($id)
    {
        User::query()->find($id)->update(['is_mentor' => 0]);

        return response()->json([
            'success' => true,
            'message' => 'Set Mentor Success'
        ]);
    }
}