<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Prodi;
use Illuminate\Http\Request;

class StoreProdiController extends Controller
{
    public function __invoke(Request $request)
    {
        Prodi::query()->create([
            'prodi' => $request->get('prodi'),
            'faculty_id' => $request->get('faculty_id')
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Store prodi success'
        ]);
    }
}
