<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Presences;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TransferUserController extends Controller
{
    public function __invoke(Request $request, $id)
    {
        User::query()->find($id)->update([
               'groups_id' => $request->get('new_groups_id')
            ]);

        return response()->json([
            'success' => true,
            'message' => 'Transfer user success'
        ]);
    }
}
