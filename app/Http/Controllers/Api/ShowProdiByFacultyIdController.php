<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Prodi;

class ShowProdiByFacultyIdController extends Controller
{
    public function __invoke($id)
    {
        return Prodi::query()->where('faculty_id', '=', $id)->get();
    }
}
