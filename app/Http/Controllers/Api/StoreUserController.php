<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StoreUserController extends Controller
{
    public function __invoke(Request $request)
    {
        User::query()->create([
            'name' => $request->get('name'),
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'gender' => $request->get('gender'),
            'role' => $request->get('role'),
            'prodi_id' => $request->get('prodi'),
            'angkatan' => $request->get('angkatan'),
            'is_mentor' => $request->get('is_mentor'),
            'status' => $request->get('status'),
            'password' => Hash::make($request->get('password'))
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Store user success'
        ]);
    }
}
