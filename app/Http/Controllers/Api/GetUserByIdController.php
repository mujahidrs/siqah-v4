<?php


namespace App\Http\Controllers\Api;


use App\Groups;
use App\Http\Controllers\Controller;
use App\User;

class GetUserByIdController extends Controller
{
    public function __invoke($id)
    {
        $user = User::query()->where('id', '=', $id)->first();

        if($user->groups_id !== null){
            $group = Groups::query()->where('id', '=', $user->groups_id)->first();
            $user->setAttribute('levels_id',$group->getAttribute('levels_id'));
            $user->setAttribute('mentors_id',$group->getAttribute('mentors_id'));
            $user->setAttribute('group_name',$group->getAttribute('group_name'));
        }
        else {
            $user->setAttribute('levels_id', 'notset');
            $user->setAttribute('mentors_id', 'notset');
            $user->setAttribute('group_name', 'notset');
        }

        return $user;
    }
}
