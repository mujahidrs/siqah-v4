<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\User;

class GetGroupMemberByIdController extends Controller
{
    public function __invoke($id)
    {
        return User::query()->select('id', 'name', 'status')->where('groups_id', '=', $id)->get();
    }
}
