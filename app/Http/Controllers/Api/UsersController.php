<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = User::all();
        return $result;
    }

    public function murabbi()
    {
        $result = DB::table('users')->where('role', '1')->get();
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->role = $request->role;
        $user->prodi_id = $request->prodi;
        $user->angkatan = $request->angkatan;
        $user->is_mentor = $request->is_mentor;
        $user->status = $request->status;
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Create user success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return $user;
    }

    public function group($id)
    {
        $user = DB::table('users')->select('id', 'name', 'status')->where('groups_id', '=', $id)->get();
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->role = $request->role;
        $user->prodi_id = $request->prodi;
        $user->angkatan = $request->angkatan;
        if(isset($request->password)){
            $user->password = Hash::make($request->password);
        }
        $user->update();

        return response()->json([
            'success' => true,
            'message' => 'Update user success'
        ]);
    }

    public function createAccount(Request $request, $id)
    {
        $user = User::find($id);
        $user->username = $request->username;
        $user->email = $request->email;
        if(isset($request->password)){
            $user->password = Hash::make($request->password);
        }
        $user->update();

        return response()->json([
            'success' => true,
            'message' => 'Create account success'
        ]);
    }

    public function editAccount(Request $request, $id)
    {
        $user = User::find($id);
        $user->username = $request->username;
        $user->email = $request->email;
        if(isset($request->password)){
            $user->password = Hash::make($request->password);
        }
        $user->update();

        return response()->json([
            'success' => true,
            'message' => 'Edit account success'
        ]);
    }

    public function nonaktif($id){
        $user = User::find($id);
        $user->status = 'Nonaktif';
        $user->update();
    }

    public function aktif($id){
        $user = User::find($id);
        $user->status = 'Aktif';
        $user->update();
    }

    public function updateAll()
    {
        $user = User::all();

        for($i = 0; $i < count($user); $i++) {
            $user[$i]->password = Hash::make($user[$i]->username);
            $user[$i]->update();
            if($user[$i]->role == 0){
                $user[$i]->assignRole('admin');
            }
            if($user[$i]->role == 1){
                $user[$i]->assignRole('user');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('users')->where('id', '=', $id)->delete();
        DB::table('activities')->where('users_id', '=', $id)->delete();
        DB::table('assessments')->where('users_id', '=', $id)->delete();
        DB::table('evaluations')->where('users_id', '=', $id)->delete();
        DB::table('presences')->where('users_id', '=', $id)->delete();
    }
}
