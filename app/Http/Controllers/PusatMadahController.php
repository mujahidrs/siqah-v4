<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PusatMadahController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('murabbi.pusat_madah.pusat_madah');
    }
}
