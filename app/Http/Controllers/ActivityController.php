<?php

namespace App\Http\Controllers;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mutarabbi.mutabaah.components.create');
    }

    public function edit()
    {
        return view('mutarabbi.mutabaah.components.edit');
    }

    public function manage()
    {
        return view('mutarabbi.mutabaah.components.manage');
    }
}
