<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PengaturanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pengaturan.pengaturan');
    }

    public function fakultas()
    {
        return view('admin.pengaturan.fakultas');
    }

    public function angkatan()
    {
        return view('admin.pengaturan.angkatan');
    }

    public function jenjang()
    {
        return view('admin.pengaturan.jenjang');
    }

    public function sarana()
    {
        return view('admin.pengaturan.sarana');
    }

    public function prodi($id)
    {
        return view('admin.pengaturan.prodi');
    }
}
