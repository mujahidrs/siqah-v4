<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminDataKelompokController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.data_murabbi.data_kelompok.data_kelompok');
    }

    public function add($murabbi)
    {
        $prefix = Request()->route()->getPrefix();
        return view('admin.data_murabbi.data_kelompok.components.add')->with('murabbi', $murabbi)->with('prefix', $prefix)->with('role', 'admin');
    }

    public function edit($murabbi)
    {
        $prefix = Request()->route()->getPrefix();
        return view('admin.data_murabbi.data_kelompok.components.edit')->with('murabbi', $murabbi)->with('prefix', $prefix)->with('role', 'admin');
    }

    public function data_mutarabbi($murabbi)
    {
        $prefix = Request()->route()->getPrefix();
        return view('admin.data_murabbi.data_kelompok.data_mutarabbi')->with('murabbi', $murabbi)->with('prefix', $prefix)->with('role', 'admin');
    }

    public function kelompok($murabbi)
    {
        $prefix = Request()->route()->getPrefix();
        return view('admin.data_murabbi.data_kelompok.data_kelompok')->with('murabbi', $murabbi)->with('prefix', $prefix)->with('role', 'admin');
    }

    public function capaian_madah($murabbi, $id)
    {
        $prefix = Request()->route()->getPrefix();
        return view('admin.data_murabbi.data_kelompok.capaian_madah')->with('murabbi', $murabbi)->with('id', $id)->with('prefix', $prefix)->with('role', 'admin');
    }
}
