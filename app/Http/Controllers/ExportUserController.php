<?php

namespace App\Http\Controllers;

use App\Exports\AdminExport;
use App\Exports\AkhwatAdminExport;
use App\Exports\AkhwatAllUserExport;
use App\Exports\AkhwatBinaanExport;
use App\Exports\AkhwatPembinaExport;
use App\Exports\AkhwatUserExport;
use App\Exports\AllUserExport;
use App\Exports\BinaanExport;
use App\Exports\PembinaExport;
use Illuminate\Http\Request;

use App\User;

use App\Exports\UserExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ExportUserController extends Controller
{
    public function index()
    {
        $user = User::all();
        return view('exports.data_user',['user'=>$user]);
    }

    public function export_excel($category, $gender)
    {
        if($category === 'all'){
            if($gender === '2'){
                return Excel::download(new AkhwatAllUserExport, 'data_all_akhwat.xlsx');
            }
            else {
                return Excel::download(new AllUserExport, 'data_all.xlsx');
            }
        }
        if($category === 'admin'){
            if($gender === '2'){
                return Excel::download(new AkhwatAdminExport, 'data_admin_akhwat.xlsx');
            }
            else {
                return Excel::download(new AdminExport, 'data_admin.xlsx');
            }
        }
        if($category === 'user'){
            if($gender === '2'){
                return Excel::download(new AkhwatUserExport, 'data_user_akhwat.xlsx');
            }
            else {
                return Excel::download(new UserExport, 'data_user.xlsx');
            }
        }
        if($category === 'pembina'){
            if($gender === '2'){
                return Excel::download(new AkhwatPembinaExport, 'data_pembina_akhwat.xlsx');
            }
            else {
                return Excel::download(new PembinaExport, 'data_pembina.xlsx');
            }
        }
        if($category === 'binaan'){
            if($gender === '2'){
                return Excel::download(new AkhwatBinaanExport, 'data_binaan_akhwat.xlsx');
            }
            else {
                return Excel::download(new BinaanExport, 'data_binaan.xlsx');
            }
        }
    }
}
