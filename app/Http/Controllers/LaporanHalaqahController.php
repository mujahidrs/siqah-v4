<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LaporanHalaqahController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('murabbi.laporan_halaqah.laporan_halaqah');
    }

    public function lapor()
    {
        return view('murabbi.laporan_halaqah.components.lapor');
    }

    public function lihat($id)
    {
        $presences_id = DB::table('presences')->select('groups_id')->where('presences_id', '=', $id)->get();
        return view('murabbi.laporan_halaqah.components.lihat')->with('id', $presences_id[0]->groups_id);
    }

    public function riwayat()
    {
        return view('murabbi.laporan_halaqah.riwayat_laporan');
    }
}
