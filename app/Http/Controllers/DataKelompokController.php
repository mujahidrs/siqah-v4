<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataKelompokController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('murabbi.data_kelompok.data_kelompok');
    }

    public function add($murabbi)
    {
        $prefix = Request()->route()->getPrefix();
        return view('murabbi.data_kelompok.components.add')->with('murabbi', $murabbi)->with('prefix', $prefix);
    }

    public function edit($murabbi)
    {
        $prefix = Request()->route()->getPrefix();
        return view('murabbi.data_kelompok.components.edit')->with('murabbi', $murabbi)->with('prefix', $prefix);
    }

    public function data_mutarabbi($murabbi)
    {
        $prefix = Request()->route()->getPrefix();
        return view('murabbi.data_kelompok.data_mutarabbi')->with('murabbi', $murabbi)->with('prefix', $prefix);
    }

    public function kelompok($murabbi)
    {
        $prefix = Request()->route()->getPrefix();
        return view('murabbi.data_kelompok.data_kelompok')->with('murabbi', $murabbi)->with('prefix', $prefix);
    }

}
