<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FamilyTreeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index($gender)
    {
        return view('admin.family_tree')->with('gender', $gender);
    }
}
