<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataMurabbiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        dd(Request()->route()->getPrefix());
        return view('admin.data_murabbi.data_murabbi');
    }

    public function add()
    {
        return view('admin.data_murabbi.components.add');
    }

    public function edit()
    {
        return view('admin.data_murabbi.components.edit');
    }

    public function transfer()
    {
        return view('admin.data_murabbi.components.transfer');
    }
}
