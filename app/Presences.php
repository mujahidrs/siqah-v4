<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Presences extends Model
{
    protected $table = 'presences';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'week', 'done', 'mentee_presence', 'replaced', 'personal_details', 'presences_id', 'users_id', 'start_time', 'end_time', 'group_notes', 'location', 'mentor_presence', 'subjects_id', 'groups_id', 'month', 'year', 'infaq', 'kultum', 'tilawah'
    ];

    public $timestamps = false;
}
