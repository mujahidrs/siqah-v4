<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Angkatan extends Authenticatable
{
    protected $table = 'angkatan_terpantau';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'angkatan'
    ];

    public $timestamps = false;
}
