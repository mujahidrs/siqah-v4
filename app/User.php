<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    protected static function boot()
    {
        parent::boot();

        static::created(function($user) {
            if($user->role == 0){
                $user->assignRole('admin');
            }
            else if($user->role == 1){
                $user->assignRole('user');
            }
        });
    }

    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'username', 'gender', 'prodi_id', 'angkatan', 'groups_id', 'is_mentor', 'status', 'groups_id_backup'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
