<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191);
			$table->string('email', 191)->nullable();
			$table->dateTime('email_verified_at')->nullable();
			$table->string('password', 191);
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->boolean('role');
			$table->string('username', 191)->unique()->nullable();
			$table->boolean('gender');
			$table->integer('prodi_id')->nullable()->index('id_prodi');
			$table->string('angkatan', 4);
			$table->integer('groups_id')->nullable()->index('groups_id');
			$table->string('status', 10)->nullable();
			$table->boolean('is_mentor')->nullable();
            $table->integer('groups_id_backup')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
