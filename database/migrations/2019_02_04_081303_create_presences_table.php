<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePresencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('presences', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('presences_id');
			$table->integer('users_id')->index('employees_id');
			$table->integer('subjects_id')->nullable()->index('subjects_id');
			$table->integer('groups_id')->index('groups_id');
			$table->string('mentor_presence', 10);
			$table->string('replaced', 10)->nullable();
			$table->integer('mentee_presence')->nullable()->default(0);
			$table->date('date')->nullable();
			$table->string('week', 1);
			$table->string('month', 20)->nullable();
			$table->string('year', 4)->nullable();
			$table->time('start_time')->nullable();
			$table->time('end_time')->nullable();
			$table->string('location', 191)->nullable();
			$table->string('personal_details', 191)->nullable();
			$table->string('group_notes', 191)->nullable();
			$table->string('done', 10);
			$table->string('infaq', 15)->nullable();
			$table->text('kultum', 65535)->nullable();
			$table->string('tilawah', 50)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('presences');
	}

}
