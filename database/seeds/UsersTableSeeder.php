<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['id' => '1', 'guard_name' => 'web', 'name' => 'admin']);
        Role::create(['id' => '2', 'guard_name' => 'web', 'name' => 'murabbi']);
        Role::create(['id' => '3', 'guard_name' => 'web', 'name' => 'mutarabbi']);
        DB::table('users')->insert(['id' => '1', 'name' => 'admin', 'gender' => '1', 'username' => 'admin', 'email' => 'admin@siqah.fsialbiruni.org', 'password' => bcrypt('admin'), 'role' => '1', 'prodi_id' => '3', 'angkatan' => '2013', 'groups_id' => null]);
        DB::table('model_has_roles')->insert(['role_id' => '1', 'model_type' => 'App\User', 'model_id' => '1',]);
        DB::table('categories')->insert(['category' => 'Jasmaniyah', 'weight' => '20',]);
        DB::table('categories')->insert(['category' => 'Ruhiyah', 'weight' => '60',]);
        DB::table('categories')->insert(['category' => 'Fikriyah', 'weight' => '20',]);
        DB::table('levels')->insert(['level' => 'Pemula',]);
        DB::table('levels')->insert(['level' => 'Muda',]);
        DB::table('levels')->insert(['level' => 'Madya',]);
        DB::table('faculties')->insert(['faculty' => 'Fakultas Teknik']);
        DB::table('faculties')->insert(['faculty' => 'Fakultas Ilmu Pendidikan']);
        DB::table('faculties')->insert(['faculty' => 'Fakultas Bahasa dan Seni']);
        DB::table('faculties')->insert(['faculty' => 'Fakultas Matematika dan Ilmu Pengetahuan Alam']);
        DB::table('faculties')->insert(['faculty' => 'Fakultas Ekonomi']);
        DB::table('faculties')->insert(['faculty' => 'Fakultas Ilmu Keolahragaan']);
        DB::table('faculties')->insert(['faculty' => 'Fakultas Psikologi']);
        DB::table('faculties')->insert(['faculty' => 'Fakultas Ilmu Sosial']);
        DB::table('prodi')->insert(['faculty_id' => '1', 'prodi' => 'S1 Pendidikan Informatika']);
        DB::table('personalities')->insert(['personality' => 'Salimul Aqidah']);
        DB::table('personalities')->insert(['personality' => 'Shahihul Ibadah']);
        DB::table('personalities')->insert(['personality' => 'Matinul Khuluq']);
        DB::table('personalities')->insert(['personality' => 'Qadirun \'alal Kasbi']);
        DB::table('personalities')->insert(['personality' => 'Mutsaqqafal Fikri']);
        DB::table('personalities')->insert(['personality' => 'Qawiyyul Jism']);
        DB::table('personalities')->insert(['personality' => 'Mujahadatun Li Nafsihi']);
        DB::table('personalities')->insert(['personality' => 'Munazhamun fii Syu-unihi']);
        DB::table('personalities')->insert(['personality' => 'Haritsun \'ala Waqtihi']);
        DB::table('personalities')->insert(['personality' => 'Nafi\'un Li Ghairihi']);
    }
}
